package com.akritiv.esm.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.akritiv.esm.config.security.SecurityUser;
import com.akritiv.esm.helpers.CustomComponentHelper;
import com.akritiv.esm.helpers.SObjectHelper;
import com.akritiv.esm.util.ESM_SObjectType;
import com.akritiv.esm.util.Encryption;
import com.akritiv.esm.util.SfdcApi;
import com.palominolabs.crm.sf.rest.RestSObject;

public abstract class AbstractAkritivAppController {
	
	@Autowired
	protected SfdcApi restConnection;
//	protected RestConnection restConnection;
	@Autowired
	protected SObjectHelper sObjectHelper;
	@Autowired
	protected CustomComponentHelper customComponentHelper;
	@Autowired
	protected HttpSession session;
	@Autowired
	protected Environment env;
	
	protected String encryptString(String value){
		return Encryption.encrypt(value);
	}
	protected String decryptString(String value){
		return Encryption.decrypt(value);
	}
	protected static final UserDetails getCurrentUserDetails(){
		SecurityUser userDetails =
				(SecurityUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return userDetails;
	}
	protected final RestSObject getLoggedInUserObject(){
		Map<String, String> whereFieldsMap =new HashMap<String, String>();
		whereFieldsMap.put("akritivesm__Email__c", "'" + getCurrentUserDetails().getUsername() + "'");
		List<RestSObject> users = sObjectHelper.getRecordsUsingWhere(ESM_SObjectType.BUYER_USER, whereFieldsMap);
		RestSObject user = (users.size() > 0) ? users.get(0) : null ;
		return user;
	}
}
