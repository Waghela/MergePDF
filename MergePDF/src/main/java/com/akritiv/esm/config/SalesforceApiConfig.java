package com.akritiv.esm.config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.akritiv.esm.util.SfdcApi;
import com.codahale.metrics.MetricRegistry;
import com.palominolabs.crm.sf.rest.RestConnection;
import com.palominolabs.crm.sf.rest.RestConnectionPool;
import com.palominolabs.crm.sf.rest.RestConnectionPoolImpl;
import com.palominolabs.crm.sf.soap.ApiException;
import com.palominolabs.crm.sf.soap.BindingConfig;
import com.palominolabs.crm.sf.soap.ConnectionPool;
import com.palominolabs.crm.sf.soap.ConnectionPoolImpl;

@Configuration
public class SalesforceApiConfig {
	
	private static String SF_Token;
	private static String SF_Url;
	private static String SF_OrgId;
	
	public static String getSF_OrgId() {
		return SF_OrgId;
	}

	public static String getSF_Token() {
		return SF_Token;
	}

	public static String getSF_Url() {
		return SF_Url;
	}
	
	@Autowired
	private Environment env;
	
	public RestConnection createRestConnection(){
		Integer orgId = Integer.valueOf(env.getRequiredProperty("sfdc.orgid"));
		System.out.println("Rest Connection Starts "+(new Date()));
		RestConnection restConnection = restConnectionPool(orgId).getRestConnection(orgId);
		System.out.println("Rest Connection Ends "+(new Date()));
		return restConnection;	
	}
	
	@Bean 
	public SfdcApi sfdcApi(){
		return SfdcApi.getInstance(this);
	}
	
	private RestConnectionPool<Integer> restConnectionPool(Integer orgId){
		RestConnectionPoolImpl<Integer> pool = new RestConnectionPoolImpl<Integer>(new MetricRegistry());
		try {
			BindingConfig config = getBindingConfig();
			
			SF_Url = config.getPartnerServerUrl();
			SF_Token = config.getSessionId();
			SF_OrgId = config.getOrgId().getFullId();
			
			printBindingConfigs(config);
			pool.configureOrg(orgId, new URL(config.getPartnerServerUrl()).getHost(), config.getSessionId());
		}catch (MalformedURLException e) {
			e.printStackTrace();
			System.err.println("ERROR :: com.akritiv.esm.config.SalesforceConfig.java -> restConnectionPool()");
		}
		return pool;
	}
	
	private BindingConfig getBindingConfig(){
	
		ConnectionPool<Integer> repository = new ConnectionPoolImpl<Integer>("testPartnerKey", new MetricRegistry());
	//	repository.configureOrg(1, env.getRequiredProperty("sfdc.username"), env.getRequiredProperty("sfdc.password"), 1);
		try{
			if(Boolean.parseBoolean(env.getRequiredProperty("sfdc.sandbox"))){
				repository.configureSandboxOrg(1, env.getRequiredProperty("sfdc.sandbox.username"), env.getRequiredProperty("sfdc.sandbox.password"), 1);
				return repository.getSandboxConnectionBundle(1).getBindingConfig();
			}else{
				repository.configureOrg(1, env.getRequiredProperty("sfdc.username"), env.getRequiredProperty("sfdc.password"), 1);
				return repository.getConnectionBundle(1).getBindingConfig();
			}
			//return repository.getConnectionBundle(1).getBindingConfig();
		} catch (ApiException e) {
			e.printStackTrace();
			System.err.println("ERROR :: com.akritiv.esm.config.SalesforceConfig.java -> getBindingConfig()");
		}
		return null;
	}
	
	private void printBindingConfigs(BindingConfig config){
		StringBuilder builder = new StringBuilder();
		builder.append("=============================================================================================").append("\n");
		builder.append("ApexServerUrl \t\t: " + config.getApexServerUrl()).append("\n");
		builder.append("MetadataServerUrl \t: " + config.getMetadataServerUrl()).append("\n");
		builder.append("PartnerServerUrl \t: " + config.getPartnerServerUrl()).append("\n");
		builder.append("SessionId \t\t: " + config.getSessionId()).append("\n");
		builder.append("Username \t\t: " + config.getUsername()).append("\n");
		builder.append("OrgId \t\t\t: " + config.getOrgId()).append("\n");
		builder.append("=============================================================================================");
		System.err.println(builder.toString());
	}

//	@Bean
//	public SObjectHelper sObjectHelper(RestConnection restConnection){
//		return new SObjectHelperImpl(restConnection);
//	}
}
