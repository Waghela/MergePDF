package com.akritiv.esm.config.security;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.akritiv.esm.data.entities.User;
import com.akritiv.esm.data.services.UserService;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.util.CommonFunctions;
import com.akritiv.esm.util.ESM_SObjectType;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.RestSObjectImpl;

@Component
public class SecurityUserDetailsService extends AbstractRestController implements UserDetailsService{
	
	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userService.findUserByUsername(userName);
		if(user == null){
			throw new UsernameNotFoundException("Username " + userName + " not found");
		}
		else{
			System.out.println("Login Sucessfully");
			Id id = new Id(user.getSaleforceId());
			RestSObject restSObject = RestSObjectImpl.getNewWithId(ESM_SObjectType.BUYER_USER, id);
	
			restSObject.setField("Last_Login_Date__c",CommonFunctions.getStandardSalesforceDateTime());
	
			try {
				restConnection.update(restSObject);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new SecurityUser(user);
	}
	
}
