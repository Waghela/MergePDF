//package com.akritiv.esm.config;
//
//import java.io.File;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.ServletContext;
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ResourceLoader;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//
//import com.akritiv.esm.helpers.CustomComponentHelper;
//import com.akritiv.esm.helpers.CustomComponentHelperImpl;
//import com.akritiv.esm.helpers.SObjectHelper;
//import com.akritiv.esm.helpers.SObjectHelperImpl;
//import com.akritiv.esm.util.SearchComponent;
//import com.akritiv.esm.util.SearchComponent.Field;
//import com.akritiv.esm.util.SearchComponent.FieldType;
//import com.akritiv.esm.util.SfdcApi;
//
//@Configuration
//public class HelperConfig {
//	
//	@Autowired
//	ResourceLoader resourceLoader;
//	@Autowired
//	ServletContext servletContext;
//	
//	@Bean
//	public SObjectHelper sObjectHelper(SfdcApi restConnection){
//		return new SObjectHelperImpl(restConnection);
//	}
//	
//	@Bean
//	public CustomComponentHelper customComponentHelper() throws Exception{
//		CustomComponentHelper customComponentHelper = new CustomComponentHelperImpl(this.getSearchComponentMap());
//		return customComponentHelper;
//	}
//	
//	private Map<String, SearchComponent> getSearchComponentMap() throws Exception{
//		Map<String, SearchComponent> map = new HashMap<String, SearchComponent>();
//		
//		try {
//			File xmlFile = null;
//			//---------------------
//////			try(ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext()){
//////				Resource resource = appContext.getResource("components/SearchComponents.xml");
//////				xmlFile = resource.getFile();
//////				System.err.println("Path\t\t:" + xmlFile.getPath());
//////				System.err.println("AbsolutePath \t:" + xmlFile.getAbsolutePath());
//////				System.err.println("CanonicalPath\t:" + xmlFile.getCanonicalPath());
//////			}
////			
//////			try(FileSystemXmlApplicationContext appContext = new FileSystemXmlApplicationContext()){
//////				Resource resource = appContext.getResource("classpath:components/SearchComponents.xml");
//////				xmlFile = resource.getFile();
//////				System.err.println("Path\t\t:" + xmlFile.getPath());
//////				System.err.println("AbsolutePath \t:" + xmlFile.getAbsolutePath());
//////				System.err.println("CanonicalPath\t:" + xmlFile.getCanonicalPath());
//////			}
//			
////			ClassLoader classLoader = getClass().getClassLoader();
////			xmlFile = new File(classLoader.getResource("components/SearchComponents.xml").getFile());
//			
////			xmlFile = resourceLoader.getResource("classpath:components/SearchComponents.xml").getFile();
//////			xmlFile = resourceLoader.getResource("/WEB-INF/components/SearchComponents.xml").getFile();
////			xmlFile = new File(servletContext.getResource("/WEB-INF/components/SearchComponents.xml").getFile());
//			xmlFile = new File(servletContext.getRealPath("")+"/WEB-INF/components/SearchComponents.xml");
//			System.err.println("Path\t\t:" + xmlFile.getPath());
//			System.err.println("AbsolutePath \t:" + xmlFile.getAbsolutePath());
//			System.err.println("CanonicalPath\t:" + xmlFile.getCanonicalPath());
//			//---------------------
//			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
//			DocumentBuilder builder = builderFactory.newDocumentBuilder();
////			Document document = builder.parse(xmlFile);
//			Document document = builder.parse(getClass().getClassLoader().getResourceAsStream("components/SearchComponents.xml"));
//			
//			document.getDocumentElement().normalize();
//			
//			NodeList components = document.getElementsByTagName("component");
//			for (int i = 0; i < components.getLength(); i++) {
//				SearchComponent component = new SearchComponent();
//				
//				Node componentNode = components.item(i);
//				if(componentNode.getNodeType() == Node.ELEMENT_NODE){
//					Element componentEle = (Element) componentNode;
//					component.setTitle(componentEle.getAttribute("title"));
//					component.setId(componentEle.getAttribute("id"));
//					
//					NodeList fields = componentEle.getElementsByTagName("field");
//					for (int j = 0; j < fields.getLength(); j++) {
//						Field field = component.new Field();
//						
//						Node fieldNode = fields.item(j);
//						if(fieldNode.getNodeType() == Node.ELEMENT_NODE){
//							Element fieldEle = (Element) fieldNode;
//							
//							field.setLabel(fieldEle.getAttribute("label"));
//							field.setName(fieldEle.getAttribute("name"));
//							field.setType(fieldEle.getAttribute("type"));
//							field.setRangeOpr(fieldEle.getAttribute("rangeOpr"));
//							if(fieldEle.getAttribute("type").equalsIgnoreCase(FieldType.PICKLIST)){
//								NodeList values = fieldEle.getElementsByTagName("value");
//								for (int k = 0; k < values.getLength(); k++) {
//									
//									Node value = values.item(k);
//									if(value.getNodeType() == Node.ELEMENT_NODE){
//										Element valueEle = (Element) value;
//										field.addPicklistValue(valueEle.getFirstChild().getNodeValue());
//									}
//								}
//							}
//							component.addField(field);
//						}
//					}
//				}
//				map.put(component.getId(), component);
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
////			throw new Exception(e.getMessage(), e.getCause());
//		}
//		
//		return map;
//	}
////	private File getSearchComponentsXmlFile(){
////		InputStream in = getClass().getClassLoader().getResourceAsStream("components/SearchComponents.xml");
////		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
////	}
//	
//	
////	private Map<String, SearchComponent> getSearchComponentMap() throws Exception{
////		Map<String, SearchComponent> map = new HashMap<String, SearchComponent>();
////		
////		try {
////			File xmlFile = null;
////			try(ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext()){
////				Resource resource = appContext.getResource("components/SearchComponents.xml");
////				xmlFile = resource.getFile();
////				System.err.println("Path\t\t:" + xmlFile.getPath());
////				System.err.println("AbsolutePath \t:" + xmlFile.getAbsolutePath());
////				System.err.println("CanonicalPath\t:" + xmlFile.getCanonicalPath());
////			}
////			
////			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
////			DocumentBuilder builder = builderFactory.newDocumentBuilder();
////			Document document = builder.parse(xmlFile);
////			
////			document.getDocumentElement().normalize();
////			
////			NodeList components = document.getElementsByTagName("component");
////			for (int i = 0; i < components.getLength(); i++) {
////				SearchComponent component = new SearchComponent();
////				
////				Node componentNode = components.item(i);
////				if(componentNode.getNodeType() == Node.ELEMENT_NODE){
////					Element componentEle = (Element) componentNode;
////					component.setTitle(componentEle.getAttribute("title"));
////					component.setId(componentEle.getAttribute("id"));
////					
////					NodeList fields = componentEle.getElementsByTagName("field");
////					for (int j = 0; j < fields.getLength(); j++) {
////						Field field = component.new Field();
////						
////						Node fieldNode = fields.item(j);
////						if(fieldNode.getNodeType() == Node.ELEMENT_NODE){
////							Element fieldEle = (Element) fieldNode;
////							
////							field.setLabel(fieldEle.getAttribute("label"));
////							field.setName(fieldEle.getAttribute("name"));
////							field.setType(fieldEle.getAttribute("type"));
////							
////							if(fieldEle.getAttribute("type").equalsIgnoreCase(FieldType.PICKLIST)){
////								NodeList values = fieldEle.getElementsByTagName("value");
////								for (int k = 0; k < values.getLength(); k++) {
////									
////									Node value = values.item(k);
////									if(value.getNodeType() == Node.ELEMENT_NODE){
////										Element valueEle = (Element) value;
////										field.addPicklistValue(valueEle.getFirstChild().getNodeValue());
////									}
////								}
////							}
////							component.addField(field);
////						}
////					}
////				}
////				map.put(component.getId(), component);
////			}
////			
////		} catch (Exception e) {
////			e.printStackTrace();
//////			throw new Exception(e.getMessage(), e.getCause());
////		}
////		
////		return map;
////	}
//}
