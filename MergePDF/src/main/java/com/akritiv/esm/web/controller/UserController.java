package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller 
@RequestMapping(value = "/user")
@WebController(name = "UserController",folder = "user")
public class UserController extends AbstractWebController{
	
	@RequestMapping(value = "/userProfile",method = RequestMethod.GET)
	public ModelAndView userProfile(Model model){
		return modelAndView(model,"userProfile");
	}
	
	@RequestMapping(value = "/changePassword",method = RequestMethod.GET)
	public ModelAndView changePassword(Model model){
		return modelAndView(model, "changePassword");
	}
}
