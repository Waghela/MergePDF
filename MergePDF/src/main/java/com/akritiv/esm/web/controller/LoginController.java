package com.akritiv.esm.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.data.entities.User;
import com.akritiv.esm.data.repositories.RoleRepository;
import com.akritiv.esm.data.repositories.UserRepository;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.util.CommonFunctions;
import com.akritiv.esm.util.ESM_SObjectType;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.RestSObjectImpl;

@Controller
@RequestMapping(value = "/auth")
public class LoginController extends AbstractRestController{
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
		
		ModelAndView model = new ModelAndView("login");
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}
		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		return model;
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public void  sucessLogin(HttpServletRequest request, HttpServletResponse response) {
		try{
			System.out.println("Login Sucessfully");
			User user = userRepository.findUserByUsername( getCurrentUserDetails().getUsername());
			Id id = new Id(user.getSaleforceId());
			RestSObject restSObject = RestSObjectImpl.getNewWithId(ESM_SObjectType.BUYER_USER, id);
	
			restSObject.setField("Last_Login_Date__c",CommonFunctions.getStandardSalesforceDateTime());
	
			restConnection.update(restSObject);
			response.sendRedirect("../");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
