package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller
@RequestMapping(value = "/component")
@WebController(folder = "component",name = "ComponentController")
public class ComponentController extends AbstractWebController{

	@RequestMapping(value = "/datetimepicker")
	public ModelAndView dateTimePickerComponent(Model model){
		return modelAndView(model, "datetimepicker");
	}

	@RequestMapping(value = "/search")
	public ModelAndView searchComponent(Model model){
		return modelAndView(model, "search");
	}

	@RequestMapping(value = "/attachment",method = RequestMethod.GET)
	public ModelAndView list(Model model){
		return modelAndView(model,"attachment");
	}
	
	@RequestMapping(value = "/layoutsection")
	public ModelAndView layoutsection(Model model){
		return modelAndView(model, "layoutsection");
	}
	
	@RequestMapping(value = "/layoutsectionfield")
	public ModelAndView layoutsectionfield(Model model){
		return modelAndView(model, "layoutsectionfield");
	}

	@RequestMapping(value = "/criteriaitem")
	public ModelAndView criteriaitem(Model model){
		return modelAndView(model, "criteriaitem");
	}

}
