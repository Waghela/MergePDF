package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller
@WebController(folder = "notesAndAttachments",name = "NotesAndAttachmentsController")
public class NotesAndAttachmentsController extends AbstractWebController{

	@RequestMapping(value = "/notesAndAttachments")
	public ModelAndView notesAndAttachmentsComponent(Model model){
		return modelAndView(model, "view");
	}
}
