package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller 
@RequestMapping(value = "/invoice")
@WebController(name = "InvoiceController",folder = "invoice")
public class InvoiceController extends AbstractWebController{
	
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ModelAndView list(Model model){
		model.addAttribute("SearchComponent", componentHelper.getInvoiceSearchComponent());
		return modelAndView(model,"list");
	}
	
	@RequestMapping(value = "/details",method = RequestMethod.GET)
	public ModelAndView details(Model model){
		return modelAndView(model,"details");
	}

	@RequestMapping(value = "/details2",method = RequestMethod.GET)
	public ModelAndView details2(Model model){
		return modelAndView(model,"details2");
	}
}
