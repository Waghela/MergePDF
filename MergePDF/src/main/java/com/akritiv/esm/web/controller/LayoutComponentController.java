package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller 
@RequestMapping(value = "/layout-component")
@WebController(name = "LayoutComponentController",folder = "layout-component")
public class LayoutComponentController extends AbstractWebController{

	@RequestMapping(value = "/{name}",method = RequestMethod.GET)
	public ModelAndView component(@PathVariable("name") String componentName,Model model){
		return modelAndView(model, componentName);
	}

	@RequestMapping(value = "/settings/{name}",method = RequestMethod.GET)
	public ModelAndView componentSettings(@PathVariable("name") String componentSettingsName,Model model){
		return modelAndView(model, "settings/"+componentSettingsName);
	}
}
