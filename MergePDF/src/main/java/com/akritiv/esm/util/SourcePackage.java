package com.akritiv.esm.util;

public interface SourcePackage {
	public static final String ESM = "com.akritiv.esm";
	
	public static final String ANNOTATIONS = "com.akritiv.esm.annotations";
	
	public static final String CONFIG = "com.akritiv.esm.config";
	public static final String CONFIG_SECURITY = "com.akritiv.esm.config.security";
	
	public static final String CORE = "com.akritiv.esm.core";
	
	public static final String DATA = "com.akritiv.esm.data";
	public static final String DATA_ENTITIES = "com.akritiv.esm.data.entities";
	public static final String DATA_REPOSITORIES = "com.akritiv.esm.data.repositories";
	public static final String DATA_SERVICES = "com.akritiv.esm.data.services";
	
	public static final String HELPERS = "com.akritiv.esm.helpers";
	
	public static final String REST = "com.akritiv.esm.rest";
	public static final String REST_CONTROLLER = "com.akritiv.esm.rest.controller";
	public static final String REST_UTIL = "com.akritiv.esm.rest.util";
	
	public static final String UTIL = "com.akritiv.esm.util";
	
	public static final String WEB = "com.akritiv.esm.web";
	public static final String WEB_CONTROLLER = "com.akritiv.esm.web.controller";
} 
