package com.akritiv.esm.util;

import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

public final class Encryption {
	private static Cipher cipher;
	private static SecretKey secretKey;
	
	public static void main(String[] args) {
		String encrypted = encrypt("Kaushik");
		System.out.println(encrypted);
		String decrypted = decrypt(encrypted);
		System.out.println(decrypted);
	}

	private static final Cipher getCipherInstance()
			throws NoSuchAlgorithmException, NoSuchPaddingException {
		if (cipher == null)
			cipher = Cipher.getInstance("AES");
		return cipher;
	}

	private static final SecretKey getSecretKey()
			throws NoSuchAlgorithmException {
		if (secretKey == null) {
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(128);
			secretKey = keyGenerator.generateKey();
		}
		return secretKey;
	}

	public static final String encrypt(String plainText) {
		try {
			byte[] plainTextByte = plainText.getBytes();
			getCipherInstance().init(Cipher.ENCRYPT_MODE, getSecretKey());
			byte[] encryptedByte = cipher.doFinal(plainTextByte);
			String encryptedText = Base64.encodeBase64String(encryptedByte);
			return encryptedText;
		} catch (Exception e) {
			System.err.println("Encrypt ERROR :: " + e.getLocalizedMessage());
			return null;
		}
	}

	public static final String decrypt(String encryptedText) {
		try {
			byte[] encryptedTextByte = Base64.decodeBase64(encryptedText);
			getCipherInstance().init(Cipher.DECRYPT_MODE, getSecretKey());
			byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
			String decryptedText = new String(decryptedByte);
			return decryptedText;
		} catch (Exception e) {
			System.err.println("Decrypt ERROR :: " + e.getLocalizedMessage());
			return null;
		}
	}
}
