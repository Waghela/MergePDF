package com.akritiv.esm.util;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.akritiv.esm.config.SalesforceApiConfig;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.core.SObject;
import com.palominolabs.crm.sf.rest.ApiException;
import com.palominolabs.crm.sf.rest.BasicSObjectMetadataResult;
import com.palominolabs.crm.sf.rest.DescribeGlobalResult;
import com.palominolabs.crm.sf.rest.RestConnection;
import com.palominolabs.crm.sf.rest.RestQueryLocator;
import com.palominolabs.crm.sf.rest.RestQueryResult;
import com.palominolabs.crm.sf.rest.SObjectDescription;
import com.palominolabs.crm.sf.rest.SaveResult;
import com.palominolabs.crm.sf.rest.UpsertResult;

public class SfdcApi {
	private static final String INVALID_SESSION_ID = "INVALID_SESSION_ID";

	private RestConnection restConnection = null;
	private SalesforceApiConfig salesforceApiConfig = null;

	private SfdcApi(SalesforceApiConfig salesforceApiConfig) {
		this.salesforceApiConfig = salesforceApiConfig;
		newRestConnection();
	}
	
	private RestConnection newRestConnection(){
		this.restConnection = salesforceApiConfig.createRestConnection();
		return restConnection;
	}
	public RestConnection getRestConnection(){
		return restConnection;
	}

	public static SfdcApi getInstance(SalesforceApiConfig salesforceApiConfig) {
		SfdcApi sfdcApi = new SfdcApi(salesforceApiConfig);
		return sfdcApi;
	}

	public SaveResult create(SObject sObject) throws IOException {
		SaveResult saveResult = null;
		try {
			saveResult = restConnection.create(sObject);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				saveResult = newRestConnection().create(sObject);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
		return saveResult;
	}

	public void delete(String sObjectType, Id id) throws IOException {

		try {
			restConnection.delete(sObjectType, id);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				newRestConnection().delete(sObjectType, id);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}

	}

	public DescribeGlobalResult describeGlobal() throws IOException {
		DescribeGlobalResult describeGlobalResult = null;
		try {
			describeGlobalResult = restConnection.describeGlobal();
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				describeGlobalResult = newRestConnection().describeGlobal();
			} else {
				throw e;

			}
		} catch (Exception e) {
			throw e;
		}
		return describeGlobalResult;
	}

	public SObjectDescription describeSObject(String sObjectType) throws IOException {
		SObjectDescription sObjectDescription = null;
		try {
			sObjectDescription = restConnection.describeSObject(sObjectType);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				sObjectDescription = newRestConnection().describeSObject(sObjectType);
			} else {
				throw e;

			}
		} catch (Exception e) {
			throw e;
		}
		return sObjectDescription;
	}

	public BasicSObjectMetadataResult getBasicObjectInfo(String sObjectType) throws IOException {
		BasicSObjectMetadataResult basicSObjectMetadataResult = null;
		try {
			basicSObjectMetadataResult = restConnection.getBasicObjectInfo(sObjectType);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				basicSObjectMetadataResult = newRestConnection().getBasicObjectInfo(sObjectType);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
		return basicSObjectMetadataResult;
	}

	public RestQueryResult query(String soql) throws IOException {
		RestQueryResult restQueryResult = null;
		try {
			restQueryResult = restConnection.query(soql);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				restQueryResult = newRestConnection().query(soql);
			} else {
				throw e;
			}

		} catch (Exception e) {
			throw e;
		}
		return restQueryResult;
	}

	public RestQueryResult queryMore(RestQueryLocator queryLocator) throws IOException {
		RestQueryResult restQueryResult = null;
		try {
			restQueryResult = restConnection.queryMore(queryLocator);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				restQueryResult = newRestConnection().queryMore(queryLocator);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
		return restQueryResult;
	}

	public SObject retrieve(String sObjectType, Id id, List<String> fields) throws IOException {
		SObject sobject = null;
		try {
			sobject = restConnection.retrieve(sObjectType, id, fields);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				sobject = newRestConnection().retrieve(sObjectType, id, fields);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
		return sobject;
	}

	public List<SObject> search(String sosl) throws IOException {
		List<SObject> listSObject = null;
		try {
			listSObject = restConnection.search(sosl);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				listSObject = newRestConnection().search(sosl);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}
		return listSObject;
	}

	public void update(SObject sObject) throws IOException{
		try {
			restConnection.update(sObject);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				newRestConnection().update(sObject);
			} else {
				throw e;
			}
		} catch (Exception e) {
			throw e;
		}

	}

	public UpsertResult upsert(SObject sObject, String externalIdField) throws IOException {
		UpsertResult upsertResult = null;
		try {
			upsertResult = restConnection.upsert(sObject, externalIdField);
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				upsertResult = newRestConnection().upsert(sObject, externalIdField);
			} else {
				throw e;
			}

		} catch (Exception e) {
			throw e;
		}
		return upsertResult;
	}
	public HttpServletResponse getAttachmentFileResponse(HttpServletResponse response, String body, String name)  throws IOException {
		try {
			HttpGet get = new HttpGet(SalesforceApiConfig.getSF_Url().split("services")[0] + body);

			get.addHeader("Authorization", "Bearer " + SalesforceApiConfig.getSF_Token());
			CloseableHttpClient c = HttpClients.createDefault();
			CloseableHttpResponse re = c.execute(get);
			HttpEntity he = re.getEntity();

			InputStream in = he.getContent();

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			byte[] buf = new byte[2048];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			out.close();
			in.close();

			byte[] responseFile = out.toByteArray();

			File path = new File(System.getProperty("java.io.tmpdir") + "/" + "download");
			if (!path.exists()) {
				path.mkdirs();
			}

			File downloadFile = new File(path + "/" + name);
			FileOutputStream fos = new FileOutputStream(downloadFile);
			fos.write(responseFile);
			fos.close();

			FileInputStream fis = null;
			fis = new FileInputStream(downloadFile);

			response.setHeader("Content-length", "");
			response.setHeader("Content-type", "application/octet-stream");

			response.setHeader("Content-Disposition", "inline; filename=\"" + name + "\"");

			response.setContentType("application/octet-stream");

			response.setContentLength((int) downloadFile.length());
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fis.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			c.close();
			re.close();
			fis.close();
			responseOutputStream.flush();
			responseOutputStream.close();
		} catch (ApiException e) {
			if (e.getErrors().get(0).getErrorCode().equalsIgnoreCase(INVALID_SESSION_ID)) {
				newRestConnection();
				response = getAttachmentFileResponse(response, body,name);
			} else {
				throw e;
			}

		} catch (Exception e) {
			throw e;
		}
		return response;
	}

}
