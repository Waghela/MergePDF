package com.akritiv.esm.data.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akritiv.esm.data.entities.SObject;

public interface SObjectRepository extends JpaRepository<SObject, Serializable>{

	List<SObject> findByActive(boolean active);
	SObject findByName(String name);
}
