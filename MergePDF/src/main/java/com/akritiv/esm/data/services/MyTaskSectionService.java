package com.akritiv.esm.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akritiv.esm.data.entities.MyTaskSection;
import com.akritiv.esm.data.repositories.MyTaskSectionRepository;

@Service
@Transactional
public class MyTaskSectionService {

	@Autowired
	MyTaskSectionRepository repository;
	
	public List<MyTaskSection> findAll(){
		return repository.findAll();
	}
	
	public List<MyTaskSection> findAllOrderBySequenceAsc(){
		return repository.findAll(sortBySequenceAsc());
	}
	
	public MyTaskSection find(Long id){
		return repository.findOne(id);
	}
	
	public List<MyTaskSection> allActiveSections(){
		return repository.findByActive(true);
	}
	
	public List<MyTaskSection> allActiveSectionsOrderBySequenceAsc(){
		return repository.findByActiveOrderBySequenceAsc(true);
	}
	
	public List<MyTaskSection> allInactiveSections(){
		return repository.findByActive(false);
	}
	
	private Sort sortBySequenceAsc(){
		return new Sort(Sort.Direction.ASC,"sequence");
	}
	
	public MyTaskSection create(MyTaskSection myTaskSection){
		return repository.save(myTaskSection);
	}
	
	public MyTaskSection update(MyTaskSection myTaskSection){
		return repository.save(myTaskSection);
	}

	public MyTaskSection save(MyTaskSection myTaskSection){
		return repository.save(myTaskSection);
	}
	
	public void deleteMyTaskSection(Long id){
		repository.delete(id);
	}
	
	public void delete(MyTaskSection myTaskSection){
		repository.delete(myTaskSection);
	}
}
