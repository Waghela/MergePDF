package com.akritiv.esm.data.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String saleforceId;
	
	private String firstName;
	private String lastName;
	private String middleInitial;
	
//	private Boolean enableDelegation;
//	private String delegateUserId;
//	
//	private String supervisorId;
	
	private String status;
	
	@Type(type="text")
	private String leCodes;

	private String email;

	@Column(nullable = false, unique = true)
	private String username;
	
	@Column(nullable = false)
	private String password;

	@ManyToOne(cascade = CascadeType.ALL)
	private Role role;
	private String federationid;
	
	public User() {
		// DEFAULT CONSTRUCTOR
	}

	public User(Long id, String firstName, String lastName, String username, String password) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.setPassword(password);
	}

	// GETTERS

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public Role getRole() {
		return role;
	}

	public String getFullname() {
		return this.getFirstName() + " " + this.getLastName();
	}

	// SETTERS

	public void setId(Long id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

//	public Boolean isEnableDelegation() {
//		return enableDelegation;
//	}
//
//	public void setEnableDelegation(Boolean enableDelegation) {
//		this.enableDelegation = enableDelegation;
//	}

	public String getSaleforceId() {
		return saleforceId;
	}

	public void setSaleforceId(String saleforceId) {
		this.saleforceId = saleforceId;
	}

//	public String getDelegateUserId() {
//		return delegateUserId;
//	}
//
//	public void setDelegateUserId(String delegateUserId) {
//		this.delegateUserId = delegateUserId;
//	}
//
//	public String getSupervisorId() {
//		return supervisorId;
//	}
//
//	public void setSupervisorId(String supervisorId) {
//		this.supervisorId = supervisorId;
//	}
//
//
//	public Boolean getEnableDelegation() {
//		return enableDelegation;
//	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLeCodes() {
		return leCodes;
	}
	
	public void setFederationid(String federationid) {
		this.federationid = federationid;
	}

	public String getFederationid() {
		return federationid;
	}

	public String getLeCodesAsStringArray() {
		if(leCodes != null && !leCodes.isEmpty()){
			String[] les = leCodes.split(",");
			StringBuilder sb = new StringBuilder();
			for(String str : les){
				sb.append("'" + str + "',");
			}
			return sb.toString().substring(0,sb.toString().length() - 1);
		}
		else{
			return "''";
		}
	}

	public void setLeCodes(String leCodes) {
		this.leCodes = leCodes;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}

}
