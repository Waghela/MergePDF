package com.akritiv.esm.data.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.akritiv.esm.data.entities.SObjectField;

public interface SObjectFieldRepository extends JpaRepository<SObjectField, Serializable>{
	@Query("select sf from SObjectField sf where sf.sObject.id = ?1")
	List<SObjectField> getSobjectFieldsBySobjectId(Long sObjectId);
	
	@Query("select sf from SObjectField sf where sf.sObject.name = ?1 AND sf.type = 'reference' AND custom = true ")
	List<SObjectField> getSobjectReferenceFieldsBySobjectName(String name);
	
	List<SObjectField> getSobjectFieldByName(String sObjectFieldName);
}
