package com.akritiv.esm.data.entities;

import java.util.Iterator;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "sobject_layout")
public class SObjectLayout {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "sobject_layout_id")
	private Long id;
	
	private String sObjectApiName;
	private String title;
	private String type;	// [ DETAILS/EDIT ]
	private boolean custom;
	private boolean active;
	@Type(type="text")
	private String actionComponentSettings;

	@OneToMany(mappedBy = "sObjectLayout", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<SObjectLayoutSection> sections;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getsObjectApiName() {
		return sObjectApiName;
	}

	public void setsObjectApiName(String sObjectApiName) {
		this.sObjectApiName = sObjectApiName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isCustom() {
		return custom;
	}

	public void setCustom(boolean custom) {
		this.custom = custom;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public JsonNode getActionComponentSettings() {
		if(this.actionComponentSettings == null)
			return null;
		
		JsonNode node = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readTree(this.actionComponentSettings);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}

	public void setActionComponentSettings(String actionComponentSettings) {
		this.actionComponentSettings = actionComponentSettings;
	}

	public List<SObjectLayoutSection> getSections() {
		return sections;
	}
	
	public void setSectionsByActiveEquelsTo(boolean active) {
		Iterator<SObjectLayoutSection> iterator = sections.iterator();
		while(iterator.hasNext()){
			SObjectLayoutSection section = iterator.next();
			if(section.isActive() != active){
//				sections.remove(section);
				iterator.remove();
			}
		}
	}

	public void setSections(List<SObjectLayoutSection> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		return "SObjectLayout [id=" + id + ", sObjectApiName=" + sObjectApiName
				+ ", title=" + title + ", type=" + type + ", custom=" + custom
				+ ", active=" + active + ", sections=" + sections + "]";
	}
	
}
