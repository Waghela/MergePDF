package com.akritiv.esm.data.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akritiv.esm.data.entities.SObjectLayout;

@Repository
public interface SObjectLayoutRepository extends JpaRepository<SObjectLayout, Serializable>{

	List<SObjectLayout> findByActive(boolean active);
	SObjectLayout findBysObjectApiName(String sObjectApiName);

}
