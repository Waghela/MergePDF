package com.akritiv.esm.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akritiv.esm.data.entities.Role;
import com.akritiv.esm.data.entities.User;
import com.akritiv.esm.data.repositories.RoleRepository;
import com.akritiv.esm.data.repositories.UserRepository;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	
	public List<User> findAll(){
		return userRepository.findAll();
	}
	
	public User create(User user){
		return userRepository.save(user);
	}
	
	public User findUserById(Long id){
		return userRepository.findOne(id);
	}
	
	public User login(String email,String password){
		return userRepository.login(email, password);
	}
	
	public User update(User user){
		return userRepository.save(user);
	}
	
	public void deleteUser(Long id){
		userRepository.delete(id);
	}
	
	public User findUserByUsername(String username){
		return userRepository.findUserByUsername(username);
	}
	
	public User findUserByFederationId(String federationId){
		return userRepository.findUserByFederationId(federationId);
	}
	
	public User findUserByEmail(String email){
		return userRepository.findUserByEmail(email);
	}
	
	public User findUserBySaleforceId(String saleforceId){
		return userRepository.findUserBySaleforceId(saleforceId);
	}
	
	public List<User> findUserBySearchStringExcludingSomeEmail(String searchText, List<String> email){
		return userRepository.findUserBySearchStringExcludingSomeEmail(searchText, email);
	}
	public List<User> findAllUserExcludingSomeEmail(List<String> email){
		return userRepository.findAllUserExcludingSomeEmail(email);
	}
//	public List<User> findDelegateUser(String saleforceId){
//		return userRepository.findDelegateUser(saleforceId);
//	}
//	public List<String> findDelegateUserEmail(String saleforceId){
//		return userRepository.findDelegateUserEmail(saleforceId);
//	}
//	
//	public List<String> findSuperVisorUserEmail(String saleforceId){
//		return userRepository.findSuperVisorUserEmail(saleforceId);
//	}
	public Role findRole(String roleName){
		return roleRepository.findByroleName(roleName);
	}
}
