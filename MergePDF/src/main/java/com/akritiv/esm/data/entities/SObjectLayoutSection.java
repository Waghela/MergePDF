package com.akritiv.esm.data.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "sobject_layout_section")
public class SObjectLayoutSection{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String title;
	private String type;	// [ DETAILS/EDIT ]
	private boolean custom;
	private boolean component;
	private String componentName;
	@Type(type="text")
	private String componentMetadata;
	private double sequence;
	private int columns;
	@Type(type="text")
	private String sectionFieldsJsonArray;

//	@ManyToOne(fetch = FetchType.LAZY, cascade = javax.persistence.CascadeType.ALL)
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	@JoinColumn(name = "sobject_layout_id", nullable = false)
	private SObjectLayout sObjectLayout;
	@Column(columnDefinition = "boolean default false")
	private boolean active;
	@Column(columnDefinition = "boolean default false")
	private boolean deleted;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isCustom() {
		return custom;
	}

	public void setCustom(boolean custom) {
		this.custom = custom;
	}

	public boolean isComponent() {
		return component;
	}

	public void setComponent(boolean component) {
		this.component = component;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	
	public JsonNode getComponentMetadata() {
		if(this.componentMetadata == null)
			return null;
		
		JsonNode node = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readTree(this.componentMetadata);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}

	public void setComponentMetadata(String componentMetadata) {
		this.componentMetadata = componentMetadata;
	}

	public double getSequence() {
		return sequence;
	}

	public void setSequence(double sequence) {
		this.sequence = sequence;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}
	
	public JsonNode getSectionFieldsJsonArray() {
		if(this.sectionFieldsJsonArray == null)
			return null;
		
		JsonNode node = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readTree(this.sectionFieldsJsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}

	public void setSectionFieldsJsonArray(String sectionFieldsJsonArray) {
		this.sectionFieldsJsonArray = sectionFieldsJsonArray;
	}

	public SObjectLayout getsObjectLayout() {
		return sObjectLayout;
	}

	public void setsObjectLayout(SObjectLayout sObjectLayout) {
		this.sObjectLayout = sObjectLayout;
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "SObjectLayoutSection [id=" + id + ", title=" + title
				+ ", type=" + type + ", custom=" + custom + ", component="
				+ component + ", componentName=" + componentName
				+ ", sequence=" + sequence + ", columns=" + columns
				+ ", sectionFieldsJsonArray=" + sectionFieldsJsonArray
				+ ", sObjectLayout=" + sObjectLayout + ", active=" + active
				+ ", deleted=" + deleted + "]";
	}
	
}
