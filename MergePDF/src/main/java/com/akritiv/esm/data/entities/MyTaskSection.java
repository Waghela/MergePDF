package com.akritiv.esm.data.entities;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "mytasksection")
public class MyTaskSection {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private String title;
	private String sObjectApiName;
	@Type(type="text")
	private String tableFieldJsonArray;
	@Type(type="text")
	private String whereClause;
	private double sequence;
	private boolean active;
	
	public MyTaskSection(Long id, String title, String sObjectApiName,String tableFieldJsonArray,
			String whereClause, double sequence, boolean active) {
		this.id = id;
		this.title = title;
		this.sObjectApiName = sObjectApiName;
		this.tableFieldJsonArray = tableFieldJsonArray;
		this.whereClause = whereClause;
		this.sequence = sequence;
		this.active = active;
	}
	
	public MyTaskSection() {
		super();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getsObjectApiName() {
		return sObjectApiName;
	}
	public void setsObjectApiName(String sObjectApiName) {
		this.sObjectApiName = sObjectApiName;
	}
//	public String getTableFieldJsonArray() {
//		return tableFieldJsonArray;
//	}
	public void setTableFieldJsonArray(String tableFieldJsonArray) {
		this.tableFieldJsonArray = tableFieldJsonArray;
	}
	public String getWhereClause() {
		return whereClause;
	}
	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}
	public double getSequence() {
		return sequence;
	}
	public void setSequence(double sequence) {
		this.sequence = sequence;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Override
	public String toString() {
		return "MyTaskSection [id=" + id + ", title=" + title
				+ ", sObjectApiName=" + sObjectApiName
				+ ", tableFieldJsonArray=" + tableFieldJsonArray.replace('\'', '\"')
				+ ", whereClause=" + whereClause + ", active=" + active + "]";
	}
	
//	public JsonNode getTableFieldsArray() {
	public JsonNode getTableFieldJsonArray() {
		if(this.tableFieldJsonArray == null)
			return null;
		
		JsonNode node = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readTree(tableFieldJsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}
	
	public String createSOQL(User user) throws JsonProcessingException, IOException{
		StringBuilder soql = new StringBuilder();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode fields = mapper.readTree(this.tableFieldJsonArray);
//		JsonNode fields = mapper.readTree(this.tableFieldJsonArray.replace('\'', '\"'));
		if (fields!=null && fields.isArray() && fields.size() > 0) {
			soql.append(" SELECT Id,");
//			soql.append(" SELECT ");
			int i=1;
			for (JsonNode field : fields) {
				String fieldName = field.get("soqlname").textValue();
//				String fieldName = field.get("name").textValue();
				if(!fieldName.equalsIgnoreCase("Id")){
					soql.append(fieldName);
					if(i < fields.size()){
						soql.append(",");
					}
				}
				i++;
			}
			soql.append(" FROM ").append(this.sObjectApiName);
			soql = getConditionalWhereClause(soql, user);
		}
		return soql.toString();
	}
	
	private StringBuilder getConditionalWhereClause(StringBuilder soql, User user){
		if(!user.getRole().getRoleName().equalsIgnoreCase(Role.ROLE_ADMIN)){
			soql.append(" WHERE ").append(this.getWhereClause().replace("{LE_CODES}", user.getLeCodesAsStringArray()));
		}
		return soql;
	}
	
	public Map<String, String> tableFields(){
//		Map<String, String> tableFields = new HashMap<String, String>();
		Map<String, String> tableFields = new LinkedHashMap<String, String>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode fields = mapper.readTree(this.tableFieldJsonArray);
//			JsonNode fields = mapper.readTree(this.tableFieldJsonArray.replace('\'', '\"'));
			if (fields!=null && fields.isArray() && fields.size() > 0) {
				for (JsonNode field : fields) {
//					String fieldName = field.get("name").textValue();
					String fieldName = field.get("soqlname").textValue();
					String fieldLabel = field.get("label").textValue();
					boolean hidden = field.has("hidden") && field.hasNonNull("hidden") && field.get("hidden").asBoolean();
					if(!hidden){
						tableFields.put(fieldName, fieldLabel);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return tableFields;
	}
	
	public Map<String, String> hrefFields(){
//		Map<String, String> hrefFields = new HashMap<String, String>();
		Map<String, String> hrefFields = new LinkedHashMap<String, String>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode fields = mapper.readTree(this.tableFieldJsonArray);
//			JsonNode fields = mapper.readTree(this.tableFieldJsonArray.replace('\'', '\"'));
			if (fields!=null && fields.isArray() && fields.size() > 0) {
				for (JsonNode field : fields) {
					if(field.has("href") && field.hasNonNull("href")){
//						String fieldName = field.get("name").textValue();
						String fieldName = field.get("soqlname").textValue();
						String fieldHref = field.get("href").textValue();
						
						hrefFields.put(fieldName, fieldHref);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return hrefFields;
	}
	
	public String createAngularJsField(String recordPrefix,String fieldName){
		String ngField = "";
		if(fieldName.contains("__r")){
			String referenceField = fieldName.substring(0, fieldName.indexOf("."));
			String apiField = fieldName.substring(fieldName.indexOf(".")+1);
			ngField = recordPrefix + ".relationshipSubObjects."+referenceField+".allFields."+apiField;
		}else{
			ngField = recordPrefix + ".allFields." + fieldName; 
		}
//		System.out.println(ngField);
		return ngField;
	}
	public String createAngularJsHrefField(String recordPrefix,String fieldName,String hrefValue){
		String ngField = createAngularJsField(recordPrefix, fieldName);
		String hrefField = "<a href = \"" + hrefValue + "\"><b>{{ " + ngField + " }}</b></a>";
//		System.out.println(hrefField);
		return hrefField;
	}
}
