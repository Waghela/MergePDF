package com.akritiv.esm.data.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.palominolabs.crm.sf.rest.FieldDescription;
import com.palominolabs.crm.sf.rest.PicklistEntry;

@Entity
@Table(name = "sobject")
public class SObject {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "sobject_id")
	private Long id;
	@Column(unique = true)
	private String name;
	private String label;
	private boolean custom;
	private String keyPrefix;
	private String labelPlural;
	private boolean customSetting;
	private boolean active;
	
	@OneToMany(mappedBy = "sObject", cascade = CascadeType.ALL)
	@JsonManagedReference
	@JsonProperty("fields")
	private List<SObjectField> fields;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isCustom() {
		return custom;
	}

	public void setCustom(boolean custom) {
		this.custom = custom;
	}

	public String getKeyPrefix() {
		return keyPrefix;
	}

	public void setKeyPrefix(String keyPrefix) {
		this.keyPrefix = keyPrefix;
	}

	public String getLabelPlural() {
		return labelPlural;
	}

	public void setLabelPlural(String labelPlural) {
		this.labelPlural = labelPlural;
	}

	public boolean isCustomSetting() {
		return customSetting;
	}

	public void setCustomSetting(boolean customSetting) {
		this.customSetting = customSetting;
	}

	public List<SObjectField> getFields() {
		return fields;
	}

	public void setFields(List<SObjectField> fields) {
		this.fields = fields;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "SObject [id=" + id + ", name=" + name + ", label=" + label
				+ ", custom=" + custom + ", keyPrefix=" + keyPrefix
				+ ", labelPlural=" + labelPlural + ", customSetting="
				+ customSetting + ", active=" + active + ", fields=" + fields
				+ "]";
	}
	
	public List<SObjectField> generateFields(List<FieldDescription> fields){
		List<SObjectField> list = new ArrayList<SObjectField>();
		
		for (FieldDescription desc : fields) {
			SObjectField field = new SObjectField();
			
			if(!desc.getName().equalsIgnoreCase("Id")){
				
				String fieldLabel = (!desc.isCustom() && desc.getType().equalsIgnoreCase("reference")) ? desc.getLabel().replace("ID", "").trim() : desc.getLabel(); 
				
				field.setAutoNumber(desc.isAutoNumber());
				field.setCalculatedFormula(desc.getCalculatedFormula());
				field.setCustom(desc.isCustom());
				field.setDefaultValue(desc.getDefaultValue());
				field.setDefaultValueFormula(desc.getDefaultValueFormula());
				field.setDependentPicklist(desc.isDependentPicklist());
				field.setDigits(desc.getDigits());
				field.setExternalId(desc.isExternalId());
				field.setHtmlFormatted(desc.isHtmlFormatted());
				field.setIdLookup(desc.isIdLookup());
				field.setInlineHelpText(desc.getInlineHelpText());
//				field.setLabel(desc.getLabel());
				field.setLabel(fieldLabel);
				field.setLength(desc.getLength());
				field.setName(desc.getName());
				field.setNameField(desc.isNameField());
				field.setNamePointing(desc.isNamePointing());
				field.setNillable(desc.isNillable());
				field.setPicklistValues(generatePickListValues(desc.getPicklistValues()));
				field.setReferenceTo(generateReferenceTo(desc.getReferenceTo()));
				field.setRelationshipName(desc.getRelationshipName());
				field.setRelationshipOrder(desc.getRelationshipOrder());
				field.setRestrictedPicklist(desc.isRestrictedPicklist());
				field.setsObject(this);
				field.setType(desc.getType());
				field.setControllerName(desc.getControllerName());
				field.setDependentPicklistValues(generateDependentPickListValues(desc, fields));
				
				list.add(field);
			}
		}
		return list;
	}
	
	private JsonNode generateDependentPickListValues(FieldDescription depField,List<FieldDescription> fields){
		ObjectNode node = null;
//		String dependentPickListValues = null;
		if(depField.isDependentPicklist() && (depField.getType().equalsIgnoreCase("picklist") || depField.getType().equalsIgnoreCase("multipicklist"))){
			FieldDescription ctrlField = null;
			for (FieldDescription field : fields) {
				if(field.getName().equals(depField.getControllerName())){
					ctrlField = field;
					break;
				}
			}
			
			try {
				ObjectMapper mapper = new ObjectMapper();
				node = mapper.createObjectNode();
				for (int i = 0; i < ctrlField.getPicklistValues().size(); i++) {
					PicklistEntry entry = ctrlField.getPicklistValues().get(i);
					StringBuilder picklistValues = new StringBuilder();
					for (PicklistEntry depEntry : depField.getPicklistValues()) {
						if(depEntry.getValidFor().get(i)){
							picklistValues.append(depEntry.getLabel()).append(",");
						}
					}
					node.put(entry.getLabel(), picklistValues.toString());
				}
//				dependentPickListValues = ((JsonNode)node).toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return node;
//		return dependentPickListValues;
	}
	
	private String generatePickListValues(List<PicklistEntry> list){
		StringBuilder sb = new StringBuilder();
		
		int i=0;
		for (PicklistEntry picklistEntry : list) {
			sb.append(picklistEntry.getLabel());
			if(i < list.size())
				sb.append(",");
			i++;
		}
		
		return sb.toString();
	}
	private String generateReferenceTo(List<String> list){
		StringBuilder sb = new StringBuilder();
		
		int i=0;
		for (String string : list) {
			sb.append(string);
			if(i < list.size())
				sb.append(",");
			i++;
		}
		
		return sb.toString();
	}
}
