package com.akritiv.esm.rest.util.actionmodel;

public class ActionModel{
	
	private SObjectID sObjectID;
	private Comments comments;
	private RejectionReason rejectionreason;
	private Route route;
	private UserAction useraction;
	private CurrentState currentState;
	private LastModifiedBy lastModifiedBy;
	
	public SObjectID getsObjectID() {
		return sObjectID;
	}
	public void setsObjectID(SObjectID sObjectID) {
		this.sObjectID = sObjectID;
	}
	public Comments getComments() {
		return comments;
	}
	public void setComments(Comments comments) {
		this.comments = comments;
	}
	public RejectionReason getRejectionreason() {
		return rejectionreason;
	}
	public void setRejectionreason(RejectionReason rejectionreason) {
		this.rejectionreason = rejectionreason;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	public UserAction getUseraction() {
		return useraction;
	}
	public void setUseraction(UserAction useraction) {
		this.useraction = useraction;
	}
	public CurrentState getCurrentState() {
		return currentState;
	}
	public void setCurrentState(CurrentState currentState) {
		this.currentState = currentState;
	}
	public LastModifiedBy getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(LastModifiedBy lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
}
