package com.akritiv.esm.rest.util.actionmodel;

public class UserAction{
	String apiname;
	String herokupicklistapiname;
	String label;
	String value;
	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	public String getHerokupicklistapiname() {
		return herokupicklistapiname;
	}
	public void setHerokupicklistapiname(String herokupicklistapiname) {
		this.herokupicklistapiname = herokupicklistapiname;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
