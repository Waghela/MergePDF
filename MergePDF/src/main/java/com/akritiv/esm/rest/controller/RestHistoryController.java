package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.palominolabs.crm.sf.rest.FieldDescription;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.SObjectDescription;

@RestController
@RequestMapping("/rest")
public class RestHistoryController extends AbstractRestController {

	@RequestMapping(value = "/history/{object}/{id}", method = RequestMethod.GET)
	public RestResponse historyComponent(@PathVariable("object") String object, @PathVariable("id") String id) {
		RestResponse response = new RestResponse();
		List<RestSObject> resultReocordHistoryConfig = null;
		List<HistoryObject> historyObjectList = null;
		try {
			resultReocordHistoryConfig = isCustomeHistoryAssignToObject(object);
			if (resultReocordHistoryConfig.size() > 0) {
				historyObjectList = getCustomHistoryData(
						resultReocordHistoryConfig.get(0).getField("akritivesm__Look_Up_Api_Name__c"), id);
			} else {
				historyObjectList = getStandardHistoryData(object, id);
			}
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			e.printStackTrace();
		}
		response.addData("historyData", historyObjectList);
		return response;
	}

	public List<RestSObject> isCustomeHistoryAssignToObject(String object) {

		List<RestSObject> queryResultsAttach = null;

		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.RECORD_HISTORY_CONFIGURATION);
			String soql = "SELECT " + queryFields + " FROM " + ESM_SObjectType.RECORD_HISTORY_CONFIGURATION
					+ " WHERE Name='" + object + "'  AND akritivesm__Custom_History__c=True";
			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();

		} catch (Exception e) {
			e.printStackTrace();

		}
		return queryResultsAttach;
	}

	public List<HistoryObject> getCustomHistoryData(String lookUpApiName, String id) {

		List<RestSObject> queryResultsAttach = null;
		List<HistoryObject> historyObjectList = new ArrayList<HistoryObject>();
		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.RECORD_HISTORY);
			String soql = "SELECT " + queryFields + " FROM " + ESM_SObjectType.RECORD_HISTORY + " WHERE "
					+ lookUpApiName + "='" + id
					+ "' Order By akritivesm__CreatedDate__c Desc, akritivesm__CreatedById__c";

			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();
			String lastCreatedBy = "";
			String lastCreatedDate = "";

			for (RestSObject sobject : queryResultsAttach) {

				HistoryObject historyObject = new HistoryObject();
				if (!lastCreatedDate.equals(sobject.getField("akritivesm__CreatedDate__c"))) {
					historyObject.setCreatedDate(sobject.getField("akritivesm__CreatedDate__c"));
				}

				historyObject.setFieldName(sobject.getField("akritivesm__FieldLabel__c"));
				historyObject.setOldValue(sobject.getField("akritivesm__oldValue__c"));
				historyObject.setNewValue(sobject.getField("akritivesm__NewValue__c"));

				if (!(lastCreatedDate.equals(sobject.getField("akritivesm__CreatedDate__c")) && lastCreatedBy.equals(
						sobject.getRelationshipSubObjects().get("akritivesm__CreatedById__r").getField("Name")))) {

					historyObject.setCreatedBy(
							sobject.getRelationshipSubObjects().get("akritivesm__CreatedById__r").getField("Name"));
				}

				lastCreatedDate = sobject.getField("akritivesm__CreatedDate__c");
				lastCreatedBy = sobject.getRelationshipSubObjects().get("akritivesm__CreatedById__r").getField("Name");
				historyObjectList.add(historyObject);
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return historyObjectList;
	}

	public List<HistoryObject> getStandardHistoryData(String object, String id) throws IOException {

		SObjectDescription description = restConnection.describeSObject(object);

		Map<String, String> fieldsMap = new HashMap<>();
		Map<String, Boolean> isReferenceFieldsMap = new HashMap<>();
		for (FieldDescription fieldDescription : description.getFields()) {
			fieldsMap.put(fieldDescription.getName(), fieldDescription.getLabel());
			isReferenceFieldsMap.put(fieldDescription.getName(),
					fieldDescription.getType().equalsIgnoreCase("reference"));

		}

		List<RestSObject> queryResultsAttach = null;
		List<HistoryObject> historyObjectList = new ArrayList<HistoryObject>();
		object = object.replaceAll("__c", "__History");

		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(object);
			String soql = "SELECT " + queryFields + " FROM " + object + " WHERE ParentId='" + id
					+ "' Order By CreatedDate Desc, CreatedById, Id Desc";

			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();
			String lastCreatedBy = "";
			String lastCreatedDate = "";
			String fieldLable = "";
			boolean isRefrenceField = false;

			for (int i = 0; i < queryResultsAttach.size(); i++) {
				RestSObject sobject = queryResultsAttach.get(i);
				HistoryObject historyObject = new HistoryObject();

				if ((sobject.getField("OldValue") == null && sobject.getField("NewValue") == null
						&& !sobject.getField("Field").equals("created"))) {
					continue;
				}

				isRefrenceField = isReferenceFieldsMap.get(sobject.getField("Field")) != null
						? isReferenceFieldsMap.get(sobject.getField("Field"))
						: isReferenceFieldsMap.get(sobject.getField("Field") + "Id") != null
								? isReferenceFieldsMap.get(sobject.getField("Field") + "Id") : false;

				if (isRefrenceField) {
					sobject = queryResultsAttach.get(++i);
				}

				fieldLable = fieldsMap.get(sobject.getField("Field"));
				if (fieldLable == null) {
					fieldLable = fieldsMap.get(sobject.getField("Field") + "Id");
				}

				if (!lastCreatedDate.equals(sobject.getField("CreatedDate"))) {
					historyObject.setCreatedDate(sobject.getField("CreatedDate").toString());
				}

				historyObject.setFieldName(fieldLable);
				historyObject.setOldValue(sobject.getField("OldValue"));
				historyObject.setNewValue(sobject.getField("NewValue"));
				
				if (!(lastCreatedDate.equals(sobject.getField("CreatedDate")) && lastCreatedBy
						.equals(sobject.getRelationshipSubObjects().get("CreatedBy").getField("Name")))) {
					historyObject.setCreatedBy(sobject.getRelationshipSubObjects().get("CreatedBy").getField("Name"));
				}
				
				
				lastCreatedDate = sobject.getField("CreatedDate");
				lastCreatedBy = sobject.getRelationshipSubObjects().get("CreatedBy").getField("Name");
				historyObjectList.add(historyObject);

			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return historyObjectList;
	}

	class HistoryObject {
		private int id;
		private String createdBy;
		private String createdDate;
		private String fieldName;
		private String oldValue;
		private String newValue;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getOldValue() {
			return oldValue;
		}

		public void setOldValue(String oldValue) {
			this.oldValue = oldValue;
		}

		public String getNewValue() {
			return newValue;
		}

		public void setNewValue(String newValue) {
			this.newValue = newValue;
		}
	}
}
