package com.akritiv.esm.rest.util.actionmodel;

import java.util.Map;

import com.akritiv.esm.data.entities.SObject;

public class RestRequestModel {
	Map<String, String> dataModel;
	SObject sObject;
	ActionModel actionModel;
	public Map<String, String> getDataModel() {
		return dataModel;
	}
	public void setDataModel(Map<String, String> dataModel) {
		this.dataModel = dataModel;
	}
	public SObject getsObject() {
		return sObject;
	}
	public void setsObject(SObject sObject) {
		this.sObject = sObject;
	}
	public ActionModel getActionModel() {
		return actionModel;
	}
	public void setActionModel(ActionModel actionModel) {
		this.actionModel = actionModel;
	}
}
