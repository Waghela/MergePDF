package com.akritiv.esm.rest.util;

import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectLayout;

public class RestSObjectMetadata {
	private SObject sObject;
	private SObjectLayout sObjectLayout;
	
	
	public RestSObjectMetadata() {
		super();
	}
	public RestSObjectMetadata(SObject sObject, SObjectLayout sObjectLayout) {
		super();
		this.sObject = sObject;
		this.sObjectLayout = sObjectLayout;
	}
	
	public SObject getsObject() {
		return sObject;
	}
	public void setsObject(SObject sObject) {
		this.sObject = sObject;
	}
	public SObjectLayout getsObjectLayout() {
		return sObjectLayout;
	}
	public void setsObjectLayout(SObjectLayout sObjectLayout) {
		this.sObjectLayout = sObjectLayout;
	}
	
}
