package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.RestSObject;

@RestController
@RequestMapping("/rest/costcode")
public class RestCostCodeController extends AbstractRestController{
	
	@RequestMapping(value = "/search",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse search(@RequestBody JsonNode costCodeModel){
		RestResponse response = new RestResponse();
		
		try {
			String searchParam = costCodeModel.get("searchStr").textValue();
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.COST_CODE); 
			String soql = "SELECT " + queryFields + " FROM " + ESM_SObjectType.COST_CODE ;
			if(!searchParam.isEmpty() || searchParam != null)
				soql += " WHERE Name LIKE '%"+searchParam+"%' OR akritivesm__Description__c LIKE '%"+searchParam+"%' ";
			System.err.println(soql);
			List<RestSObject> costCodes = restConnection.query(soql).getSObjects();
			
			response.addData("costCodes", costCodes);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
}
