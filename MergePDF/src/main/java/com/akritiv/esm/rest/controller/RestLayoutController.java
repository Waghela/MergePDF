package com.akritiv.esm.rest.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.MyTaskSection;
import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectLayout;
import com.akritiv.esm.data.entities.SObjectLayoutSection;
import com.akritiv.esm.data.services.SObjectLayoutSectionService;
import com.akritiv.esm.data.services.SObjectLayoutService;
import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping("/rest/admin/layout")
public class RestLayoutController extends AbstractRestController{
	
	@Autowired
	SObjectLayoutService service;
	@Autowired
	SObjectService sObjectService;
	@Autowired
	SObjectLayoutSectionService sectionService;
	
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public RestResponse list(){
		RestResponse response = new RestResponse();
		try {
			List<SObjectLayout> sObjectLayouts = service.findAll();
			response.addData("sObjectLayouts", sObjectLayouts);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
	
	@RequestMapping(value = "/details/{id}",method = RequestMethod.GET)
	public RestResponse details(@PathVariable("id") Long layoutId){
		RestResponse response = new RestResponse();
		try {
			SObjectLayout sObjectLayoutDetails = service.find(layoutId);
//			SObjectLayout sObjectLayoutDetails = service.findByActiveSections(layoutId,true);
			SObject sObject = sObjectService.findByName(sObjectLayoutDetails.getsObjectApiName());
			response.addData("sObjectLayoutDetails", sObjectLayoutDetails);
			response.addData("sObject", sObject);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
	
	@RequestMapping(value = "/save",method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public RestResponse save(@RequestBody SObjectLayout layout){
		RestResponse response = new RestResponse();
		try{
			layout.setSections(saveSObjectLayoutSections(layout.getSections(),layout));
			layout = service.update(layout);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		
		return response;
	}
	
	private List<SObjectLayoutSection> saveSObjectLayoutSections(List<SObjectLayoutSection> sections,SObjectLayout layout){
		List<SObjectLayoutSection> savedSections = new ArrayList<SObjectLayoutSection>();
		
		for (SObjectLayoutSection section : sections) {
			if(section.isDeleted()){
				sectionService.delete(section.getId());
			}else{
				SObjectLayoutSection savedSection = sectionService.save(section);
				savedSections.add(savedSection);
			}
		}
		return savedSections;
	}
}
