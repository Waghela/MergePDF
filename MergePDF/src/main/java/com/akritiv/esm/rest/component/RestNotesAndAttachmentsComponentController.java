package com.akritiv.esm.rest.component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.config.SalesforceApiConfig;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.palominolabs.crm.sf.rest.RestSObject;

@RestController
@RequestMapping("/rest/layout-component/notes-and-attachments-component")
public class RestNotesAndAttachmentsComponentController extends AbstractRestController {

	@RequestMapping(value = "/viewAttachment", method = RequestMethod.GET)
	public void viewAttachment(HttpServletRequest request, HttpServletResponse response) {

		try {
			String name = request.getParameter("name");
			String body = request.getParameter("body");

			if (name != null && !name.equals("") && body != null && !body.equals("")) {
				response = getAttachmentFileResponse(response, body, name);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "note/{id}", method = RequestMethod.GET)
	public RestResponse getNote(@PathVariable("id") String id) {
		RestResponse response = new RestResponse();

		try {
			String soql = "SELECT Body FROM " + ESM_SObjectType.NOTE + " WHERE Id='" + id + "'";

			List<RestSObject> notes = restConnection.query(soql).getSObjects();
			if (notes.size() > 0) {

				response.addData("note", notes.get(0));
				response.setStatus(ResponseStatus.SUCCESS);
			}
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/loadAttachmentsDetails/{id}", method = RequestMethod.GET)
	public RestResponse loadAttachmentsDetails(@PathVariable("id") String id) {
		RestResponse response = new RestResponse();
		try {

			List<RestSObject> queryResultsAttach = getAttachmentDetails("ParentId", id);
			response.addData("attachmentsDetails", queryResultsAttach);
			response.setStatus(ResponseStatus.SUCCESS);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}
	
	@RequestMapping(value = "/loadNotesDetails/{id}", method = RequestMethod.GET)
	public RestResponse getNotesDetails(@PathVariable("id") String id) {
		RestResponse response = new RestResponse();
		List<RestSObject> queryResultsAttach = null;

		try {
			String soql = "SELECT Id , Title FROM " + ESM_SObjectType.NOTE + " WHERE ParentId='" + id
					+ "'";
			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();
			response.addData("notesDetails", queryResultsAttach);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	protected List<RestSObject> getAttachmentDetails(String fieldName, String filedValue) {

		List<RestSObject> queryResultsAttach = null;

		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.ATTACHMENT);
			String soql = "SELECT " + queryFields + " FROM " + ESM_SObjectType.ATTACHMENT + " WHERE " + fieldName + "='"
					+ filedValue + "'";
			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return queryResultsAttach;
	}
	protected HttpServletResponse getAttachmentFileResponse(HttpServletResponse response, String body, String name) {
		try {
			HttpGet get = new HttpGet(SalesforceApiConfig.getSF_Url().split("services")[0] + body);

			get.addHeader("Authorization", "Bearer " + SalesforceApiConfig.getSF_Token());
			HttpClient c = HttpClients.createDefault();
			HttpResponse re = c.execute(get);
			HttpEntity he = re.getEntity();

			InputStream in = he.getContent();

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			byte[] buf = new byte[2048];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			out.close();
			in.close();

			byte[] responseFile = out.toByteArray();

			File path = new File(System.getProperty("java.io.tmpdir") + "/" + "download");
			if (!path.exists()) {
				path.mkdirs();
			}

			File downloadFile = new File(path + "/" + name);
			FileOutputStream fos = new FileOutputStream(downloadFile);
			fos.write(responseFile);
			fos.close();

			FileInputStream fis = null;
			fis = new FileInputStream(downloadFile);

			response.setHeader("Content-length", "");
			response.setHeader("Content-type", "application/octet-stream");

			response.setHeader("Content-Disposition", "inline; filename=\"" + name + "\"");

			response.setContentType("application/octet-stream");

			response.setContentLength((int) downloadFile.length());
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fis.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			fis.close();
			responseOutputStream.flush();
			responseOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

}