package com.akritiv.esm.rest.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectLayoutSection;
import com.akritiv.esm.data.services.SObjectLayoutSectionService;
import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.rest.util.actionmodel.RestRequestModel;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.RestSObjectImpl;
import com.palominolabs.crm.sf.rest.SaveResult;

@RestController
@RequestMapping("/rest/layout-component/cost-allocation-component")
public class RestCostAllocationComponentController extends AbstractRestController {

	@Autowired
	SObjectService service;
	@Autowired
	SObjectLayoutSectionService sectionService;

	@RequestMapping(value = "/invoicelineitems", method = RequestMethod.POST, headers = {
			"Content-type=application/json" })
	public RestResponse invoiceLineItems(@RequestBody JsonNode jsonNode) {

		RestResponse response = new RestResponse();
		RestSObject user = getLoggedInUserObject();
		try {
			String invoiceId = jsonNode.get("invoiceId").textValue();
			Long sectionId = jsonNode.get("sectionId").asLong();

			SObjectLayoutSection section = sectionService.find(sectionId);

			JsonNode configuration = (section.getComponentMetadata() != null)
					? section.getComponentMetadata().get("configurations") : null;
			if (configuration != null) {
				String invoiceLineItemApiName = configuration.hasNonNull("invoiceLineItemApiName")
						? configuration.get("invoiceLineItemApiName").asText() : null;
				JsonNode invoiceLineItemFields = configuration.hasNonNull("invoiceLineItemFields")
						? configuration.get("invoiceLineItemFields") : null;
				String invoiceLineItemAmountApiName = configuration.hasNonNull("invoiceLineItemAmountApiName")
						? configuration.get("invoiceLineItemAmountApiName").asText() : null;
				String whereClause = configuration.hasNonNull("whereClause") ? configuration.get("whereClause").asText()
						: null;
				String recordLimit = configuration.hasNonNull("recordLimit") ? configuration.get("recordLimit").asText()
						: null;
				String invoiceAmoutApiName = configuration.hasNonNull("invoiceAmoutApiName")
						? configuration.get("invoiceAmoutApiName").asText() : null;

				StringBuilder queryFields = new StringBuilder();
				for (JsonNode fieldNode : invoiceLineItemFields) {
					queryFields.append(fieldNode.get("name").asText() + ",");
					if (fieldNode.get("type").asText().equals("reference")) {
						queryFields.append(fieldNode.get("name").asText().replace("__c", "__r") + ".Name" + ",");
					}
				}
				queryFields.append(invoiceLineItemAmountApiName);
				String soql = "SELECT Id," + queryFields.toString() + " FROM " + invoiceLineItemApiName + " WHERE "
						+ ESM_SObjectType.INVOICE + "='" + invoiceId + "' "
						+ (whereClause != null && !whereClause.equals("") ? " AND " + whereClause : "")
						+ (recordLimit != null && !recordLimit.equals("") ? " LIMIT " + recordLimit : "' LIMIT 200");
				System.err.println(soql);
				List<RestSObject> invoiceLineItems = restConnection.query(soql).getSObjects();
				List<Map<String, String>> dataModelList = createDataModel(invoiceLineItems, user);
				response.addData("dataModelList", dataModelList);
				soql = "SELECT " + invoiceAmoutApiName + " FROM " + ESM_SObjectType.INVOICE + " WHERE Id='" + invoiceId
						+ "'";
				List<RestSObject> soqlResult = restConnection.query(soql).getSObjects();
				if (soqlResult.size() > 0) {
					RestSObject invoiceDetails = soqlResult.get(0);
					response.addData("invoiceAmount", invoiceDetails.getField(invoiceAmoutApiName));
				}

			}
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/saveInvoiceLineItem", method = RequestMethod.POST, headers = {
			"Content-type=application/json" })
	public RestResponse saveInvoiceLineItem(@RequestBody JsonNode jsonNode) {
		RestResponse response = new RestResponse();
		String invoiceId = jsonNode.get("invoiceId").textValue();
		Long sectionId = jsonNode.get("sectionId").asLong();
		JsonNode costAllocationInvoiceLineItems = jsonNode.get("dataModelList");
		SObjectLayoutSection section = sectionService.find(sectionId);
		try {
			JsonNode configuration = (section.getComponentMetadata() != null)
					? section.getComponentMetadata().get("configurations") : null;
			if (configuration != null) {
				String invoiceLineItemApiName = configuration.hasNonNull("invoiceLineItemApiName")
						? configuration.get("invoiceLineItemApiName").asText() : null;
				JsonNode invoiceLineItemFields = configuration.hasNonNull("invoiceLineItemFields")
						? configuration.get("invoiceLineItemFields") : null;
				String invoiceLineItemAmountApiName = configuration.hasNonNull("invoiceLineItemAmountApiName")
						? configuration.get("invoiceLineItemAmountApiName").asText() : null;
				
				if (costAllocationInvoiceLineItems.isArray()) {
					for (JsonNode item : costAllocationInvoiceLineItems) {

						RestSObject lineItem = null;

						String id = (item.hasNonNull("ID_FullId")) ? item.get("ID_FullId").textValue() : null;
						boolean isRemoved = (item.hasNonNull("isRemoved")) ? item.get("isRemoved").asBoolean() : false;
						boolean isPersisted = (item.hasNonNull("isPersisted")) ? item.get("isPersisted").asBoolean()
								: false;

						if (id != null && isRemoved && isPersisted) {
							Id sfdcId = new Id(id);
							restConnection.delete(invoiceLineItemApiName, sfdcId);
						} else if (id != null && !isRemoved && isPersisted) {
							Id sfdcId = new Id(id);
							lineItem = RestSObjectImpl.getNewWithId(invoiceLineItemApiName, sfdcId);
							lineItem.setField("akritivesm__Invoice__c", invoiceId);
							for (JsonNode fieldNode : invoiceLineItemFields) {
								lineItem.setField(fieldNode.get("name").asText(),
										(item.get(fieldNode.get("name").asText()).asText()));
							}
							lineItem.setField(invoiceLineItemAmountApiName,
									(item.get(invoiceLineItemAmountApiName).asText()));

							restConnection.update(lineItem);

						} else if (!isPersisted && !isRemoved) {
							lineItem = RestSObjectImpl.getNew(invoiceLineItemApiName);
							lineItem.setField("akritivesm__Invoice__c", invoiceId);
							for (JsonNode fieldNode : invoiceLineItemFields) {
								lineItem.setField(fieldNode.get("name").asText(),
										(item.get(fieldNode.get("name").asText()).asText()));
							}
							lineItem.setField(invoiceLineItemAmountApiName,
									(item.get(invoiceLineItemAmountApiName).asText()));

							SaveResult result = restConnection.create(lineItem);
							if (!result.isSuccess())
								throw new Exception(result.getErrors().get(0).getMessage());
						}
					}
				}
				
			}
			System.out.println(jsonNode);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public List<Map<String, String>> createDataModel(List<RestSObject> restSObjectList, RestSObject loggedInUser) {
		List<Map<String, String>> listDataModel = new ArrayList<Map<String, String>>();
		for (RestSObject restSObject : restSObjectList) {
			Map<String, String> dataModel = sObjectHelper.createDataModel(restSObject);
			dataModel.put("USER_ID", loggedInUser.getId().getFullId());
			dataModel.put("isRemovable", "true");
			dataModel.put("isPersisted", "true");
			dataModel.put("isRemoved", "false");
			listDataModel.add(dataModel);
		}

		return listDataModel;
	}

	/**
	 * SETTINGS
	 */

	@RequestMapping(value = "/sobjects", method = RequestMethod.GET)
	public RestResponse list() {
		RestResponse response = new RestResponse();
		try {
			List<SObject> sObjects = service.allActiveSObjects();
			response.addData("sObjects", sObjects);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/validateQuery", method = RequestMethod.POST, headers = {
			"Content-type=application/json" })
	public RestResponse validateQuery(@RequestBody String soql) {
		RestResponse response = new RestResponse();

		try {
			List<RestSObject> invoiceLineItems = restConnection.query(soql).getSObjects();
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}

}