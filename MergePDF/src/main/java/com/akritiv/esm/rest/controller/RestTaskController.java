package com.akritiv.esm.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.MyTaskSection;
import com.akritiv.esm.data.services.MyTaskSectionService;
import com.akritiv.esm.data.services.UserService;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.RestQueryResult;

@RestController
@RequestMapping("/rest/mytask")
public class RestTaskController extends AbstractRestController{
	
	@Autowired
	MyTaskSectionService taskService;
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/loadsectiondata",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse loadSectionData(@RequestBody JsonNode sectionNode){
		RestResponse response = new RestResponse();
		
		try {
			long sectionId = sectionNode.get("sectionId").asLong();
			MyTaskSection section = taskService.find(sectionId);
			String soql = section.createSOQL(userService.findUserByUsername(getCurrentUserDetails().getUsername()));
			System.out.println("SOQL[" + section.getTitle() +"] :: " + soql);
			
			RestQueryResult result = restConnection.query(soql);
			
			response.addData("records", result.getSObjects());
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		
		return response;
	}
}
