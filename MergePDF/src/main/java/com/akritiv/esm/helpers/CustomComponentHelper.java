package com.akritiv.esm.helpers;

import com.akritiv.esm.util.SearchComponent;

public interface CustomComponentHelper {
	SearchComponent getSearchComponent(String componentId);
	SearchComponent getInvoiceSearchComponent();
	SearchComponent getSupplierMaintenanceSearchComponent();
}
