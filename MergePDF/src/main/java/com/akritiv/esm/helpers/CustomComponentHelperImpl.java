package com.akritiv.esm.helpers;

import java.util.Map;
import java.util.Set;

import com.akritiv.esm.util.SearchComponent;

public class CustomComponentHelperImpl implements CustomComponentHelper {
	
	private Map<String, SearchComponent> searchComponentsMap;
	
	public CustomComponentHelperImpl(Map<String, SearchComponent> searchComponentsMap){
		this.searchComponentsMap = searchComponentsMap;
		if(searchComponentsMap!=null && searchComponentsMap.size()>0){
			Set<String> keys = searchComponentsMap.keySet();
			System.out.println();
			StringBuilder builder = new StringBuilder("=============================================================================================\n");
			for (String key : keys) {
				builder.append("["+key+"] = " + this.searchComponentsMap.get(key)).append("\n");
			}
			builder.append("=============================================================================================");
			System.err.println(builder.toString());
		}else{
			System.err.println("NO COMPONENTS FOUND!!");
		}
	}

	@Override
	public SearchComponent getSearchComponent(String componentId) {
		SearchComponent component = this.searchComponentsMap.get(componentId);
		return component;
	}

	@Override
	public SearchComponent getInvoiceSearchComponent() {
		SearchComponent component = this.getSearchComponent("InvoiceSearchComponent");
		return component;
	}

	@Override
	public SearchComponent getSupplierMaintenanceSearchComponent() {
		SearchComponent component = this.getSearchComponent("SupplierMaintenanceSearchComponent");
		return component;
	}
}
