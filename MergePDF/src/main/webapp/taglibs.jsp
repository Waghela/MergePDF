<%@ taglib prefix="c" 		uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" 	uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" 	uri="http://www.springframework.org/security/tags" %>

<%-- CUSTOM TAGS --%>
<%@ taglib prefix="cc"		tagdir="/WEB-INF/tags" %>

<%-- SPRING URLS --%>
<spring:url var="logoutUrl" value="/logout" scope="application" ></spring:url>

<%-- SPRING REST URLS --%>
<spring:url var="restInvoiceUrl" value="/rest/invoice" scope="application"></spring:url>

<%-- LOCAL RESOURCES  --%>
<c:url var="local_css" value="/resources/css" scope="application" ></c:url>
<c:url var="local_js" value="/resources/js" scope="application" ></c:url>
<c:url var="local_angular_datatables" value="/resources/angular/datatables/dist" scope="application" ></c:url>
<c:url var="local_angular_modalService" value="/resources/angular/modalService/dist" scope="application" ></c:url>
<c:url var="local_angular_file_upload" value="/resources/angular/file-upload" scope="application" ></c:url>
<c:url var="local_angular_sticky" value="/resources/angular/ngSticky/dist" scope="application" ></c:url>

<c:url var="client_src" value="/resources/angular/src" scope="application" ></c:url>
<c:url var="client_src_components" value="/resources/angular/src/components" scope="application" ></c:url>
<c:url var="client_src_controllers" value="/resources/angular/src/controllers" scope="application" ></c:url>
<c:url var="client_src_services" value="/resources/angular/src/services" scope="application" ></c:url>
<c:url var="client_src_factories" value="/resources/angular/src/factories" scope="application" ></c:url>

<%-- LOCAL PLUGINS  --%>
<c:url var="local_plugins" value="/resources/plugins" scope="application" ></c:url>
<c:url var="local_plugins_datetimepicker" value="/resources/plugins/datetimepicker" scope="application" ></c:url>

<%-- BOWER WEBJARS URLS  --%>
<%-- <c:url var="webjars_angularjs" value="/webjars/angularjs/1.4.4" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_ui_router" value="/webjars/angular-ui-router/0.2.15/release" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_animate" value="/webjars/angular-animate/1.4.4" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_block_ui" value="/webjars/angular-block-ui/0.2.0/dist" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_ui_notofication" value="/webjars/angular-ui-notification/0.0.11/dist" scope="application" ></c:url> --%>

<%-- <c:url var="webjars_angularjs" value="/webjarslocator/angularjs" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_ui_router" value="/webjarslocator/angular-ui-router/release" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_animate" value="/webjarslocator/angular-animate" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_block_ui" value="/webjarslocator/angular-block-ui/dist" scope="application" ></c:url> --%>
<%-- <c:url var="webjars_angularjs_ui_notofication" value="/webjarslocator/angular-ui-notification/dist" scope="application" ></c:url> --%>

<c:url var="webjars_angularjs" value="/resources/bower/angularjs/1.4.4" scope="application" ></c:url>
<c:url var="webjars_angularjs_ui_router" value="/resources/bower/angular-ui-router/0.2.15/release" scope="application" ></c:url>
<c:url var="webjars_angularjs_animate" value="/resources/bower/angular-animate/1.4.4" scope="application" ></c:url>
<c:url var="webjars_angularjs_block_ui" value="/resources/bower/angular-block-ui/0.2.0/dist" scope="application" ></c:url>
<c:url var="webjars_angularjs_ui_notofication" value="/resources/bower/angular-ui-notification/0.0.11/dist" scope="application" ></c:url>

