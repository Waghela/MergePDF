<%@ include file="taglibs.jsp" %>
<!DOCTYPE html>
<html lang="en" ng-app="ESM-APP">
<head>
	<meta charset="utf-8">
	<title>Akritiv ESM</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<link href="${local_css }/bootstrap.min.css" rel="stylesheet">
	<link href="${local_css }/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
	<link href="${local_css }/font-awesome.css" rel="stylesheet">
	<link href="${local_css }/style.css" rel="stylesheet">
	<link href="${local_css }/custom.css" rel="stylesheet">
	<link href="${local_css }/pages/dashboard.css" rel="stylesheet">
	<link href="${local_plugins_datetimepicker }/jquery.datetimepicker.css" rel="stylesheet">
	<link href="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-block-ui/0.2.0/dist/angular-block-ui.min.css" rel="stylesheet">

	<link href="${local_angular_datatables}/plugins/bootstrap/datatables.bootstrap.min.css" rel="stylesheet">
	<link href="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-ui-notification/0.0.11/dist/angular-ui-notification.min.css" rel="stylesheet">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
      <script src="${local_js }/html5.js"></script>
    <![endif]-->
    <!--[if IE]>
		<link rel="stylesheet" type="text/css" href="${local_css }/custom-ie.css" />
	<![endif]-->
</head>
<body block-ui="main">
	<jsp:include page="WEB-INF/template/header_fixed.jsp"></jsp:include>
	<jsp:include page="WEB-INF/template/header.jsp"></jsp:include>
	<div class="main" style="min-height: calc(100vh - 226px)">
		<div class="main-inner">
			<div class="container">
				<div ui-view></div>
			</div>
			<!-- /container -->
		</div>
		<!-- /main-inner -->
	</div>
	<jsp:include page="WEB-INF/template/footer.jsp"></jsp:include>
	
	<sec:authorize access="isFullyAuthenticated()">
		<a id="logoutFromSSO" href="http://genpact-sso-sp.herokuapp.com/logout" style="display: none;"></a>
		<form id="logoutForm" action="${logoutUrl }" method="POST" style="margin: 0; display: none;">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
	</sec:authorize>
	<!-- common	scripts -->
	<script src="${local_js}/jquery-1.7.2.min.js" ></script>
	<script src="${local_plugins_datetimepicker}/build/jquery.datetimepicker.full.js" ></script>
	<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<script src="${local_js}/bootstrap.js" ></script>
	<script src="${local_js}/base.js" ></script>
	<script src="${local_js}/secure.js" ></script>
	<c:url value="/" var="appUrl"></c:url>
	<script>
		$("#logoutUrl").click(function() {
// 		  $("#logoutFromSSO").click();
		  $("#logoutForm").submit();
		});
		var appUrl = "${appUrl}";
	</script>
	
	<!-- ANGULARJS -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angularjs/1.4.4/angular.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-cookies/1.4.4/angular-cookies.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-ui-router/0.2.15/release/angular-ui-router.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-animate/1.4.4/angular-animate.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-block-ui/0.2.0/dist/angular-block-ui.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-ui-notification/0.0.11/dist/angular-ui-notification.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars.bower/angular-drag-and-drop-lists/1.3.0/angular-drag-and-drop-lists.min.js"></script>
<%-- 	<script type="text/javascript" src="${webjars_angularjs}/angular.min.js"></script> --%>
<%-- 	<script type="text/javascript" src="${webjars_angularjs_ui_router}/angular-ui-router.min.js"></script> --%>
<%-- 	<script type="text/javascript" src="${webjars_angularjs_animate}/angular-animate.min.js"></script> --%>
<%-- 	<script type="text/javascript" src="${webjars_angularjs_block_ui}/angular-block-ui.min.js"></script> --%>
<%-- 	<script type="text/javascript" src="${webjars_angularjs_ui_notofication}/angular-ui-notification.min.js"></script> --%>
	
	<!-- ANGULARJS DATATABLES -->
	<script type="text/javascript" src="${local_angular_datatables}/angular-datatables.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/bootstrap/angular-datatables.bootstrap.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/colreorder/angular-datatables.colreorder.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/columnfilter/angular-datatables.columnfilter.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/colvis/angular-datatables.colvis.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/scroller/angular-datatables.scroller.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/tabletools/angular-datatables.tabletools.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/fixedcolumns/angular-datatables.fixedcolumns.min.js"></script>
	<script type="text/javascript" src="${local_angular_datatables}/plugins/fixedheader/angular-datatables.fixedheader.min.js"></script>
	
	<!-- ANGULARJS MODAL SERVICE -->
	<script type="text/javascript" src="${local_angular_modalService}/angular-modal-service.js"></script>

	<!-- ANGULARJS FILE UPLOAD -->
	<script type="text/javascript" src="${local_angular_file_upload}/ng-file-upload.js"></script>
	<script type="text/javascript" src="${local_angular_file_upload}/ng-file-upload-all.js"></script>
	<script type="text/javascript" src="${local_angular_file_upload}/ng-file-upload-shim.js"></script>
	
	<!-- ANGULARJS STICKY -->
	<script type="text/javascript" src="${local_angular_sticky}/sticky.min.js"></script>

	<sec:authorize access="hasAnyRole('ADMIN')" >
	<!-- 	CLIENT ANGULAR ADMIN-APP -------------------------------------------------------------------------------------------->
	<script type="text/javascript" src="${client_src}/admin-app.js"></script>
	
	<script type="text/javascript" src="${client_src_controllers}/SObjectControllers.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/MyTaskSectionControllers.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/LayoutControllers.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/SettingsModalControllers.js"></script>
	
	<script type="text/javascript" src="${client_src_services}/SObjectService.js"></script>
	<script type="text/javascript" src="${client_src_services}/MyTaskSectionService.js"></script>
	<script type="text/javascript" src="${client_src_services}/SObjectLayoutService.js"></script>
	
	<script type="text/javascript" src="${client_src_components}/Repository.js"></script>
	<script type="text/javascript" src="${client_src_components}/Service.js"></script>
	

	<script type="text/javascript" src="${client_src_components}/invoice/settings/CostAllocationComponentSettings.js"></script>
    <script type="text/javascript" src="${client_src_components}/settings/ComponentSettings.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/settings/SObjectListComponentSettings.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/settings/RecordHistoryComponentSettings.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/settings/UploadAttachmentsComponentSettings.js"></script>
    <script type="text/javascript" src="${client_src_components}/public/settings/DownloadPrimaryDocumentComponentSettings.js"></script>
    
	<!-- 	CLIENT ANGULAR ADMIN-APP -------------------------------------------------------------------------------------------->
	</sec:authorize>
	
	<!-- 	CLIENT ANGULAR APP -->
	<script type="text/javascript" src="${client_src}/app.js"></script>
	<script type="text/javascript" src="${client_src}/config.js"></script>
	<script type="text/javascript" src="${client_src}/component-directives.js"></script>
	<script type="text/javascript" src="${client_src}/directives.js"></script>
	<script type="text/javascript" src="${client_src}/filters.js"></script>
	
	<script type="text/javascript" src="${client_src_components}/invoice/invoiceActionComponent.js"></script>
	<script type="text/javascript" src="${client_src_components}/invoice/CostAllocationComponent.js"></script>
	
	<script type="text/javascript" src="${client_src_components}/public/SObjectListComponent.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/RecordHistoryComponent.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/NotesAndAttachmentsComponent.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/UploadAttachmentsComponent.js"></script>
	<script type="text/javascript" src="${client_src_components}/public/DownloadPrimaryDocumentComponent.js"></script>
	

	<script type="text/javascript" src="${client_src_components}/ActionComponentController.js"></script>
	<script type="text/javascript" src="${client_src_components}/ActionValidationService.js"></script>

	<script type="text/javascript" src="${client_src_controllers}/TaskController.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/InvoiceControllers.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/SupplierMaintenanceControllers.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/ModalControllers.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/SectionComponentController.js"></script>
	<script type="text/javascript" src="${client_src_controllers}/UserControllers.js"></script>
	
	<script type="text/javascript" src="${client_src_services}/InvoiceService.js"></script>
	<script type="text/javascript" src="${client_src_services}/SupplierMaintenanceService.js"></script>
	<script type="text/javascript" src="${client_src_services}/LookupService.js"></script>
	<script type="text/javascript" src="${client_src_services}/InvoiceLineItemServices.js"></script>
	<script type="text/javascript" src="${client_src_services}/HistoryService.js"></script>
	<script type="text/javascript" src="${client_src_services}/NotesAndAttachmentsService.js"></script>
	<script type="text/javascript" src="${client_src_services}/AttachmentService.js"></script>
	<script type="text/javascript" src="${client_src_services}/UserService.js"></script>
	
	<script type="text/javascript" src="${client_src_factories}/esm-cache.js"></script>
	<script type="text/javascript" src="${client_src_factories}/RecursionHelper.js"></script>
	<script type="text/javascript" src="${client_src_factories}/CriteriaHelper.js"></script>
	<script type="text/javascript" src="${client_src_factories}/SessionInjector.js"></script>

</body>
</html>