'use strict';

App.config(
		function(
				$stateProvider, 
				$urlRouterProvider, 
				$httpProvider, 
				$locationProvider, 
				blockUIConfig,
				NotificationProvider) {
	
	/**
	 *  BLOCK-UI CONFIGURATION
	 */
	var blockUiConfiguration = function(){
//		// Change the default overlay message
		blockUIConfig.message = 'Loading . . .'; // i.e. Default
//		// delay time in ms
		blockUIConfig.delay = 0;
		// Enable browser navigation blocking
		blockUIConfig.blockBrowserNavigation = true;
//		// Disable automatically blocking of the user interface
		blockUIConfig.autoBlock = true;
		// Disable auto body block
		blockUIConfig.autoInjectBodyBlock = true;
//		// Disable clearing block whenever an exception has occurred
		blockUIConfig.resetOnException = true;
		
		// Tell the blockUI service to ignore certain requests
		blockUIConfig.requestFilter = function(config) {
		  // If the request starts with 'rest/**' ...
		  if(config.url.match(/^rest($|\/).*/)) {
		    return false; // ... don't block it.
		  }
		};
	};
	blockUiConfiguration();
	
	/**
	 * UI-NOTIFICATION CONFIGURATION
	 */
	var uiNotificationConfiguration = function(){
		NotificationProvider.setOptions({
            delay: 5000,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: 'right',
            positionY: 'top'
        });
	};
	uiNotificationConfiguration();
	
	/**
	 * TOOLTIPS CONFIGURATION
	 */
//	var toolTipConfiguration = function(){
//		tooltipsConfigProvider.options({
//	      lazy: false,
//	      size: 'large',
//	      side:	'top'
//	    });
//	};
//	toolTipConfiguration();
	
	
	/**
	 * UI-ROUTER CONFIGURATION
	 */
	$stateProvider
		/******************************************************************************************** INDEX ***/
		.state("index",{ 
			url 			: "/views", 
			templateUrl 	: "mytasks", 
			controller		: "TaskController" 
		})
		/****************************************************************************************** INVOICE ***/
		.state("invoices", { 
			url 			: "/", 
			templateUrl 	: "invoice/list", 
			controller		: "InvoiceListController"
		})
//		.state("invoice-details", { 
//			url 			: "/invoice/{id}", 
//			templateUrl 	: "invoice/details",
//			controller		: "InvoiceDetailsController" 
//		})
		.state("invoice-details", { 
			url 			: "/invoice/{id}", 
			templateUrl 	: "invoice/details2",
			controller		: "InvoiceController" 
		})
		.state("change_password", { 
			url 			: "/changePassword", 
			templateUrl 	: "user/changePassword",
			controller		: "ChangePasswordController" 
		})
		// TESTING...
//		.state("invoice-details-testing", { 
//			url 			: "/invoice/details/{id}", 
//			templateUrl 	: "invoice/details2",
//			controller		: "InvoiceController" 
//		})
		;
	$urlRouterProvider.otherwise("/");
//	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	$httpProvider.interceptors.push('SessionInjector');
});
