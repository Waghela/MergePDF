'use strict';

App.service('HistoryService',
		[	'$http',
 	function($http){
			
		return {
			loadHistoryData			: loadHistoryData
		};
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadHistoryData(object,id){
			return $http.get("rest/history/"+object+"/"+id);
		};
}]);

