App.service('AttachmentService',
		['Upload', '$http',
 	function(Upload, $http){
			
		return {
			uploadAttachments						: uploadAttachments,
			deleteFile								: deleteFile,
			uploadPrimary							: uploadPrimary
		};
		
		function uploadAttachments(file){
            file.upload = Upload.upload({
                url: 'rest/component/upload',
                data: {file: file}
            })
            return file.upload;
		};
		function uploadPrimary(file){
            file.upload = Upload.upload({
                url: 'rest/component/uploadPrimary',
                data: 	{file: file}
            });
            return file.upload;
		};
		function deleteFile(name){
			return $http.post("rest/component/deleteFile", name);
		};
}]);