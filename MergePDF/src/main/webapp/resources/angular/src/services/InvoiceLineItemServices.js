'use strict';

App.service('NonPOInvoiceLineItemService',
		[	'$http','Notification','$filter','$log',
 	function($http , Notification , $filter , $log){
		var numberFilter = $filter('number');
		var currencyFilter = $filter('currencyFilter');
		// Instantiate initial object
		var NonPOInvoiceLineItemService = function(invoiceAmount,invoiceLineItems){
			this.items = [];
			var self = this;
			this.invoiceAmount = (!invoiceAmount) ? 0 : currencyFilter(invoiceAmount);

			if(invoiceLineItems && invoiceLineItems.length > 0){
				angular.forEach(invoiceLineItems,function(item){
					self.items.push({
						id			: item.id.idStr, 
						amount		: item.allFields.akritivesm__Amount__c,
						costCode	: {
							id		:item.allFields.akritivesm__Cost_Code__c,
							name	:(item.relationshipSubObjects.akritivesm__Cost_Code__r) ? item.relationshipSubObjects.akritivesm__Cost_Code__r.allFields.Name : null
						},
						glCode		: {
							id		:item.allFields.akritivesm__GL_Code__c,
							name	:(item.relationshipSubObjects.akritivesm__GL_Code__r) ? item.relationshipSubObjects.akritivesm__GL_Code__r.allFields.Name : null
						},
						isRemovable	: true,
						isRemoved	: false,
						isPersisted	: true
					});
				});
			}else{
				this.items.push({
					amount		: this.invoiceAmount,
					costCode	: {id:null,name:null},
					glCode		: {id:null,name:null},
					isRemovable	: false,
					isRemoved	: false
				});
			}
			
			
		};
			
		/*
		 * PROTOTYPE METHODS
		 */ 
		NonPOInvoiceLineItemService.prototype.getValue = function(value){
			return angular.isUndefined(value) ? null : value;
		}
		NonPOInvoiceLineItemService.prototype.getTotal = function(){			// getTotal()
			var itemsTotalAmount 	= 0;
			angular.forEach(this.items, function(item) {
//				itemsTotalAmount += parseFloat(item.amount);
				if(!item.isRemoved){
					itemsTotalAmount += parseFloat(item.amount);
				}
			});
//			return isNaN(itemsTotalAmount) ? 0 : currencyFilter(itemsTotalAmount);
			return isNaN(itemsTotalAmount) ? 0 : (isNaN(currencyFilter(itemsTotalAmount)) ? 0 : currencyFilter(itemsTotalAmount));
		};
		
		NonPOInvoiceLineItemService.prototype.validate = function(){			// validate()
			var total = this.getTotal();
			var valid = (total > 0)&&(total <= this.invoiceAmount);
			return valid;
		};
		
		NonPOInvoiceLineItemService.prototype.getItems = function(){			// getItems()
			var self = this;
			return self.items;
		};
		
		NonPOInvoiceLineItemService.prototype.setItems = function(items){			// setItems()
			this.items = [];
			var self = this;
			$log.debug(JSON.stringify(items));
			angular.forEach(items,function(item){
				if(!item.isRemoved){
					self.items.push({
						amount			: item.amount,
						costCode		: {id:item.costCode.id,name:item.costCode.name},
						glCode			: {id:item.glCode.id,name:item.glCode.name},
						isRemovable		: false,
						isPersisted		: true,
						isRemoved		: false
					});
				}
			});
		};

		NonPOInvoiceLineItemService.prototype.removeItem = function(index,item){		// removeItem(index)
//			var self = this;
//			self.items.splice(index, 1);
			var self = this;
			if(item){
				if(item.isPersisted){
					item.isRemoved	= true;
					$log.warn(JSON.stringify(self.items));
					return;
				}
			}
			self.items.splice(index, 1);
		};
		
		NonPOInvoiceLineItemService.prototype.isAllPersisted = function(){		// isAllPersisted()
			var persisted = 0;
			
			angular.forEach(this.items,function(item){
				if(item.isPersisted && !item.isRemoved){
					persisted++;
				}
			});
			
			return this.items.length === persisted;
		}

		NonPOInvoiceLineItemService.prototype.isValid = function(checkTotalAmount){				// isValid()
			var self = this;
			var errorMessages = [];
			var errorStage = 0;
			angular.forEach(this.items, function(item) {
				if(errorStage != 1 && !item.isRemoved) {
					item.amount = parseFloat(item.amount);
//					alert(JSON.stringify(item));
					if(!item.amount || item.amount == "null" || item.amount <= 0 || item.amount > self.invoiceAmount){
						errorStage = 1;
						errorMessages.push("Line item amount must not be less than 0 or greater than " + currencyFilter(self.invoiceAmount));
					}
					if(!item.costCode || !item.costCode.id || !item.costCode.name){
						errorStage = 2;
						errorMessages.push("Cost Code is required!");
					}
					if(!item.glCode || !item.glCode.id || !item.glCode.name){
						errorStage = 3;
						errorMessages.push("GL Code is required!");
					}
				}
			});
			if(checkTotalAmount && self.getTotal() != self.invoiceAmount){
				errorMessages.push("Cost allocation total amount must be " + currencyFilter(self.invoiceAmount));
			}
			if(errorMessages.length == 0)
				return true;
			else{
				var errorMessagesStr = "<ul>";
				angular.forEach(errorMessages, function(error) {
					errorMessagesStr += "<li>" + error + "</li>";
				});
				errorMessagesStr += "</ul>";
				Notification.error({
					message	: errorMessagesStr,
					title	:'Errors'
				});
			}
			return false;
		};
		
		NonPOInvoiceLineItemService.prototype.addItem = function(){				// addItem()
			var self 				= this;
			var itemsTotalAmount 	= this.getTotal();
			if(self.invoiceAmount < itemsTotalAmount){
				Notification.error({
					message	:'Total of Invoice line items must not be greater than <b>' + numberFilter(self.invoiceAmount,2) + '</b> !!',
					title	:'Error'
				});
				return;
			}
			if(!self.isValid()){
				return;
			}
			if(currencyFilter(self.invoiceAmount) === currencyFilter(itemsTotalAmount)){
				Notification.warning({
					message	:'You are not allowed to add more line items !!',
					title	:'Warning'
				});
				return;
			}
			
			self.items.push({
				amount		: currencyFilter((self.invoiceAmount - itemsTotalAmount)),
				costCode	: {id:null,name:null},
				glCode		: {id:null,name:null},
				isRemovable	: true,
				isRemoved	: false
			});
		};
		
		return NonPOInvoiceLineItemService;
}]);