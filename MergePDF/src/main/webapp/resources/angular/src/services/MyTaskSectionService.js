'use strict';

AdminApp.service('MyTaskSectionService',
		[	'$http',
 	function($http){
			
		return {
			loadMyTaskSections				: loadMyTaskSections,
			loadMyTaskSectionDetails		: loadMyTaskSectionDetails,
			saveMyTaskSection				: saveMyTaskSection,
			deleteMyTaskSection				: deleteMyTaskSection
		};
		
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadMyTaskSections(){
			return $http.get("rest/admin/mytasksection/list");
		};
		function loadMyTaskSectionDetails(id){
			return $http.get("rest/admin/mytasksection/details/"+id);
		};
		function saveMyTaskSection(myTaskSectionModel){
			return $http.post("rest/admin/mytasksection/save",myTaskSectionModel);
		};
		function deleteMyTaskSection(id){
			return $http.post("rest/admin/mytasksection/delete",id);
		};
}]);
