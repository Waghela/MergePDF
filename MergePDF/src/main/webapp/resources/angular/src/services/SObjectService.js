'use strict';

AdminApp.service('SObjectService',
		[	'$http',
 	function($http){
			
		return {
			loadSObjects				: loadSObjects,
			loadSfdcSObjects			: loadSfdcSObjects,
			loadSfdcSObjectFields		: loadSfdcSObjectFields,
			saveSObjects				: saveSObjects
		};
		
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadSObjects(){
			return $http.get("rest/admin/sobject/list");
		};
		function loadSfdcSObjects(){
			return $http.get("rest/admin/sobject/sfdcobjects");
		};
		function loadSfdcSObjectFields(sfdcSObjectNames){
			return $http.post("rest/admin/sobject/sfdcobjectfields",sfdcSObjectNames);
		};
		function saveSObjects(dataToSave){
			return $http.post("rest/admin/sobject/save",dataToSave);
		};
}]);
