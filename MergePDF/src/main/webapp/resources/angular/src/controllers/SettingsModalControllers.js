'use strict';

/************************* LAYOUT SECTION SETTINGS LOOKUP MODAL CONTROLLER *************************/
AdminApp.controller('LayoutSectionSettingsModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService','Notification','section',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService , Notification , section ){
		$scope.title = title;
		$scope.section = section;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
		};
		
		$scope.saveAndClose = function(){
//			section = angular.copy($scope.section);
//			close(null,500);
			$element.modal('hide');
			close($scope.section, 500); // close, but give 500ms for bootstrap to animate
		};
}]);

AdminApp.controller('LayoutSectionCriteriaModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$compile','$element','title','close','LookupService','Notification','section','sobject','CriteriaHelper',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $compile , $element , title , close , LookupService , Notification , section , sobject , CriteriaHelper){
			
			$scope.init = function(){
				$scope.title = title;
				$scope.section = section;
				var fields = [];
				angular.forEach(sobject.fields,function(field){
					if(field.custom && field.type != 'textarea' && field.type != 'date' && field.type != 'datetime' && field.type != 'formula' && field.type != 'multipicklist'){
						fields.push(field);
					}
				});
				$scope.sobject = sobject;
				$scope.sobject.fields = fields;
				
				$scope.criteria = $scope.section.componentMetadata.criteria;
				if($scope.section.componentMetadata.criteria == null){
					$scope.section.componentMetadata.criteria = CriteriaHelper.groupNode();
				}
				
				//  This close function doesn't need to use jQuery or bootstrap, because
				//  the button has the 'data-dismiss' attribute.
				$scope.close = function(){
					$element.modal('hide');
				};

				$scope.saveAndClose = function(){
					try{
						if($scope.section.componentMetadata.criteria){
							var result = CriteriaHelper.validate($scope.section.componentMetadata.criteria,[],true);
							if(result == null){
								delete $scope.section.componentMetadata.criteria
							}
							$element.modal('hide');
							close($scope.section, 500); // close, but give 500ms for bootstrap to animate
						}
					}catch(e){
						alert(e);
						return;
					}
				};
			};
			$scope.init();
		}]);
/************************* LAYOUT SECTION FIELD SETTINGS LOOKUP MODAL CONTROLLER *************************/
AdminApp.controller('LayoutSectionFieldSettingsModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService','Notification','field',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService , Notification , field){
			$scope.title = title;
			$scope.field = field;
			if($scope.field.type=='picklist' || $scope.field.type == 'multipicklist'){
				$scope.field.picklistValues = $scope.field.picklistValues.replace(new RegExp(',','g'),'\n');
			}
			//  This close function doesn't need to use jQuery or bootstrap, because
			//  the button has the 'data-dismiss' attribute.
			$scope.close = function(){
				$element.modal('hide');
			};
			
			$scope.saveAndClose = function(){
//			section = angular.copy($scope.section);
//			close(null,500);
				$element.modal('hide');
				if($scope.field.type=='picklist' || $scope.field.type == 'multipicklist'){
					$scope.field.picklistValues = $scope.field.picklistValues.replace(new RegExp('\n','g'),',');
				}
				close($scope.field, 500); // close, but give 500ms for bootstrap to animate
			};
		}]);

/************************* LAYOUT SECTION SETTINGS LOOKUP MODAL CONTROLLER *************************/
AdminApp.controller('InvoiceComponentSettingsModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService','Notification','section',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService , Notification , section){
		$scope.title = title;
		$scope.section = section;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
		};
		
		$scope.saveAndClose = function(){
			$element.modal('hide');
			close($scope.section, 500); // close, but give 500ms for bootstrap to animate
		};
}]);