'use strict';

/************************* INVOICE DETAIL CONTROLLER *************************/
App.controller('InvoiceController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','InvoiceService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , InvoiceService , esmCache){
		
		// FUNCTION :: LoadInvoiceMetadata
		$scope.loadInvoiceMetadata = function(){
			var invoiceDetailsBlock = blockUI.instances.get('invoiceDetailsBlock');
			invoiceDetailsBlock.start("Loading layout...");
			InvoiceService.loadInvoiceMetadata()
				.success(function(response){
					invoiceDetailsBlock.stop();
					if(response.status === "SUCCESS"){
						
						$scope.metadata = response.data;
						$scope.loadInvoiceDetails();
					}
				})
				.error(function(response){
					invoiceDetailsBlock.stop();
				});
		};
		
		// FUNCTION :: LoadInvoiceDetails()
		$scope.loadInvoiceDetails = function(){
			var invoiceDetailsBlock = blockUI.instances.get('invoiceDetailsBlock');
			invoiceDetailsBlock.start("Loading Invoice details...");
			
			InvoiceService.loadInvoiceDetails($stateParams.id)
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.metadata.dataModel = response.data.dataModel;
					}else{
						$scope.errors = response.errors;
					}
					invoiceDetailsBlock.stop();
				})
				.error(function(){
					invoiceDetailsBlock.stop();
				});
		};
		
		// FUNCTION :: SaveInvoiceDetails()
		$scope.save = function(invoiceModel){
			console.info(JSON.stringify(invoiceModel));
			$scope.blockUiInstances.layout.start("Submitting Invoice details...");
			InvoiceService.saveInvoice(invoiceModel)
				.success(function(response){
					if(response.status === "SUCCESS"){
						
					}else{
						
					}
					$scope.blockUiInstances.layout.stop();
				})
				.error(function(response){
					$scope.blockUiInstances.layout.stop();
					alert("Error occured!!");
				});
		};
		
		$scope.blockUiInstances = {
			layout : blockUI.instances.get('invoiceDetailsBlock')
		};
		$scope.init = function(){
			$scope.ctrl = $scope;
			$scope.metadata = {};
			$scope.loadInvoiceMetadata();
		};
		$scope.init();
}]);

/************************* INVOICES LIST CONTROLLER *************************/
App.controller('InvoiceListController',
	['$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','InvoiceService','esmCache', 'DTOptionsBuilder',
	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , InvoiceService , esmCache, DTOptionsBuilder){
		$scope.invoicesCacheId = 'InvoiceListController_Invoices';
		
		$scope.searchInvoices = function(componentData){
			var searchInvoiceBlock = blockUI.instances.get('searchInvoiceBlock');
			searchInvoiceBlock.start("Searching...");
			
			InvoiceService.searchInvoices(componentData)
			.success(function(response){
				if(response.status === "SUCCESS"){
					$scope.invoices = response.data.searchResult;
					esmCache.put($scope.invoicesCacheId,$scope.invoices);
					$scope.hasCachedData = false;
					if(response.data.searchResult.length > 0)
						$scope.disableCSVExport = false;
				}
				searchInvoiceBlock.stop();
			})
			.error(function(){
				searchInvoiceBlock.stop();
			});
		}
//		$scope.searchInvoices();
		
		$scope.init = function(){
			$scope.invoices = esmCache.get($scope.invoicesCacheId);
			$scope.hasCachedData = ($scope.invoices && $scope.invoices.length > 0) ? true : false;
			$scope.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers');
			$scope.columnHeader = ['ESM Ref No#','Invoice No.','LE Code','Invoice Type','Supplier Name','Total Amount','Payment Date','Payment Document No./Check No.','Invoice Received Date','Status'];
			$scope.columnApiNameArr = ['Name','Invoice_Number__c','LE_Code_Text__c','Invoice_Type_Temp__c','Supplier_Name_formula__c','Akritiv_ESM__Total_Amount__c','Payment_Date__c','Payment_Document_No_Check_No__c','Invoice_Recieved_Date__c','Akritiv_ESM__Status__c'];
			$scope.disableCSVExport = true;
		};
		$scope.init();
	}]
);