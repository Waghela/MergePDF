'use strict';

/************************* LAYOUT LIST CONTROLLER *************************/
AdminApp.controller('LayoutListController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','SObjectLayoutService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , SObjectLayoutService , esmCache){

		$scope.reload = ($stateParams.reload) ? true : false;
		
		$scope.loadSObjectLayouts = function(){
			var sObjectLayoutsBlock = blockUI.instances.get('sObjectLayoutsBlock');
			sObjectLayoutsBlock.start("Loading layouts...");
			
			SObjectLayoutService.loadSObjectLayouts()
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.sObjectLayouts = response.data.sObjectLayouts;
						esmCache.put($scope.sObjectLayoutsCacheId,$scope.sObjectLayouts);
						$scope.hasCachedData = false;
					}
					sObjectLayoutsBlock.stop();
				})
				.error(function(){
					sObjectLayoutsBlock.stop();
				});
		};
		
		$scope.navigateToEditPage = function(layoutDetails){
			$state.go("admin-layouts-edit",{ id : layoutDetails.id ,details : layoutDetails });
		}
		
		$scope.init = function(){
			$scope.sObjectLayoutsCacheId = 'LayoutListController_sObjectLayouts';

			$scope.sObjectLayouts = esmCache.get($scope.sObjectLayoutsCacheId);
			$scope.hasCachedData = ($scope.sObjectLayouts && $scope.sObjectLayouts.length > 0) ? true : false;
			if(!$scope.hasCachedData || $scope.reload){
				$scope.loadSObjectLayouts();
			}
		};
		$scope.init();
}]);

/************************* LAYOUT EDIT CONTROLLER *************************/
AdminApp.controller('LayoutEditController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','$compile','blockUI','ModalService','Notification','SObjectLayoutService','esmCache','$timeout','ComponentService',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , $compile , blockUI , ModalService , Notification , SObjectLayoutService , esmCache , $timeout , ComponentService){
		var layoutId 		= $stateParams.id;
		var layoutDetails 	= $stateParams.details;
		$scope.ctrlScope	= $scope;
		
		$scope.loadSObjectLayoutDetails = function(){
			if(layoutId){
				var sObjectLayoutsEditBlock = blockUI.instances.get('sObjectLayoutsEditBlock');
				sObjectLayoutsEditBlock.start("Loading layout contents...");
				
				SObjectLayoutService.loadSObjectLayoutDetails(layoutId)
					.success(function(response){
						if(response.status === "SUCCESS"){
							$scope.layoutDetails = response.data.sObjectLayoutDetails;
							$scope.sObject = response.data.sObject;
							$scope.sections = $scope.layoutDetails.sections;
							$scope.componentList = ComponentService.list($scope.layoutDetails.sObjectApiName);
//							$scope.createDataModel($scope.layoutDetails);
						}
						sObjectLayoutsEditBlock.stop();
					})
					.error(function(){
						sObjectLayoutsEditBlock.stop();
					});
			}
		};
		
		$scope.backToList = function(){
			$state.go("admin-layouts");
		};
		
		$scope.log = function(item){
			console.log(JSON.stringify(item));
		}
		
		angular.extend($scope,{
			componentList 				:ComponentService.list(),
			sections					:[],
			untitledSection	: {
				id						:null,
				title					:'Untitled Section',
				sectionFieldsJsonArray	:[],
				columns					:2,
				custom					:true,
				component				:false,
				componentName			:null,
				sequence				:1,
				active					:false,
				type					:'DETAIL',
				deleted					:false,
				componentMetadata		:{}
			},
			dndSection 	: function(){
//				alert("New Section coppied!");
//				$scope.openSectionSettingsModal(angular.copy($scope.untitledSection));
			},
			dndField	: function(field){
//				alert(field.label + " coppied!");
			},
			openSectionSettingsModal: function(section,index){
				var sectionRef = section;
				ModalService.showModal({
					templateUrl		: "modal/layoutsectionsettings",
					controller		: "LayoutSectionSettingsModalController",
					inputs			: {
						title	: (section.component) ? "Component Settings" : "Section Settings",
						section	: angular.copy(section)
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						sectionRef = angular.copy(result);
						$scope.sections[index] = sectionRef;
					});
				});
			},
			openComponentSettingsModal: function(section,index){
				var sectionRef = section;
				ModalService.showModal({
					templateUrl		: "layout-component/settings/component-settings-modal",
					controller		: "ComponentSettingsController",
					inputs			: {
						title	: "Configuration Settings",
						section	: angular.copy(section)
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						sectionRef = angular.copy(result);
						$scope.sections[index] = sectionRef;
					});
				});
			},
			openSectionCriteriaModal: function(section,index){
				var sectionRef = section;
				ModalService.showModal({
					templateUrl		: "modal/layoutsectioncriteria",
					controller		: "LayoutSectionCriteriaModalController",
					inputs			: {
						title	: (section.component) ? "Component Criteria" : "Section Criteria",
						section	: angular.copy(section),
						sobject	: angular.copy($scope.sObject)
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						sectionRef = angular.copy(result);
						$scope.sections[index] = sectionRef;
					});
				});
			},
			openSectionFieldSettingsModal: function(section,field){
				var fieldRef = field;
				var fieldIndex = section.sectionFieldsJsonArray.indexOf(field);
				ModalService.showModal({
					templateUrl		: "modal/layoutsectionfieldsettings",
					controller		: "LayoutSectionFieldSettingsModalController",
					inputs			: {
						title	: "Edit field [ " + field.label + " ]",
						field	: angular.copy(field)
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						fieldRef = angular.copy(result);
						section.sectionFieldsJsonArray[fieldIndex] = fieldRef;
					});
				});
			},
			openActionComponentSettingsModal: function(settings){
				var refSettings = settings;
				ModalService.showModal({
					templateUrl		: settings.lookup.templateUrl,
					controller		: settings.lookup.controller,
					inputs			: {
						title		: settings.lookup.title,
						section		: angular.copy(settings.section)
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						refSettings.section = angular.copy(result);
					});
				});
			},
			dropCallback : function(event, index, item, external, type, allowedType){
				if(allowedType === 'sectionType'){
					$scope.openSectionSettingsModal(item);
				}
				return item;
			},
			deleteSection : function(section,index){
				if(section.id && section.id!=null){
					section.deleted = true;
				}else{
					$scope.sections.splice(index, 1);
				}
			}
		});
		
		$scope.saveLayout = function(){
			var sObjectLayoutsEditBlock = blockUI.instances.get('sObjectLayoutsEditBlock');
			sObjectLayoutsEditBlock.start("Saving changes...");
			
			var layout = angular.copy($scope.layoutDetails);
			layout.actionComponentSettings = JSON.stringify(layout.actionComponentSettings);
			angular.forEach(layout.sections,function(section,index){
				section.sequence = index + 1;
				section.sectionFieldsJsonArray = JSON.stringify(section.sectionFieldsJsonArray);
				section.componentMetadata = JSON.stringify(section.componentMetadata);
			});
			
			SObjectLayoutService.saveSObjectLayout(layout)
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.layoutDetails = response.data.savedSObjectLayout;
						$scope.loadSObjectLayoutDetails();
					}
					sObjectLayoutsEditBlock.stop();
				})
				.error(function(){
					sObjectLayoutsEditBlock.stop();
				});
		};
		
		$scope.init = function(){
			$scope.loadSObjectLayoutDetails();
			
			$scope.$watch('sections',function(section){
				$scope.sectionsAsJson = angular.toJson(section,true);
			},true);
		};
		$scope.init();
}]);