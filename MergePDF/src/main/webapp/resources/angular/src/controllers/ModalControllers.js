'use strict';

/************************* COST CODE LOOKUP MODAL CONTROLLER *************************/
App.controller('CostCodeLookupModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService){
		$scope.title = title;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
//			close({},500); // close, but give 500ms for bootstrap to animate
		};
		$scope.selectAndClose = function(id,name){
			close({
				id		: id,
				name	: name
			}, 500); // close, but give 500ms for bootstrap to animate
		};
		
		//  This cancel function must use the bootstrap, 'modal' function because
		//  the doesn't have the 'data-dismiss' attribute.
		$scope.cancel = function(){
			//  Manually hide the modal.
			$element.modal('hide');
			
			//  Now call close, returning control to the caller.
			close({},500); // close, but give 500ms for bootstrap to animate
		};
		
		// CUSTOM CODING FOR COST CODES
		$scope.costCodeModel = {
			searchStr : ""
		};
		$scope.loadCostCodes = function(){
			var costCodeModalBlock = blockUI.instances.get('costCodeModalBlock');
			costCodeModalBlock.start("Searching Cost Codes...");
			
			LookupService.loadCostCodes($scope.costCodeModel)
				.success(function(response){
					$scope.costCodeErrors = response.errors;
					
					if(response.status == 'SUCCESS'){
						$scope.costCodes = response.data.costCodes;
					}else{
						// SHOW ERROR
					}
					costCodeModalBlock.stop();
				})
				.error(function(){
					alert('Unexpected error occured while searching cost codes !!');
					costCodeModalBlock.stop();
				});
		};
		$timeout($scope.loadCostCodes(),500);
}]);

/************************* GL CODE LOOKUP MODAL CONTROLLER *************************/
App.controller('GLCodeLookupModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService){
		$scope.title = title;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
//			close({},500); // close, but give 500ms for bootstrap to animate
		};
		$scope.selectAndClose = function(id,name){
			close({
				id		: id,
				name	: name
			}, 500); // close, but give 500ms for bootstrap to animate
		};
		
		//  This cancel function must use the bootstrap, 'modal' function because
		//  the doesn't have the 'data-dismiss' attribute.
		$scope.cancel = function(){
			//  Manually hide the modal.
			$element.modal('hide');
			
			//  Now call close, returning control to the caller.
			close($scope.result,500); // close, but give 500ms for bootstrap to animate
		};
		
		// CUSTOM CODING FOR GL CODES
		$scope.glCodeModel = {
			searchStr : ""
		};
		$scope.loadGLCodes = function(){
			var glCodeModalBlock = blockUI.instances.get('glCodeModalBlock');
			glCodeModalBlock.start("Searching GL Codes...");
			
			LookupService.loadGLCodes($scope.glCodeModel)
				.success(function(response){
					$scope.glCodeErrors = response.errors;
					
					if(response.status == 'SUCCESS'){
						$scope.glCodes = response.data.glCodes;
					}else{
						// SHOW ERROR
					}
					glCodeModalBlock.stop();
				})
				.error(function(){
					alert('Unexpected error occured while searching GL codes !!');
					glCodeModalBlock.stop();
				});
		};
		$timeout($scope.loadGLCodes(),500);
}]);

/************************* APPROVER USER LOOKUP MODAL CONTROLLER *************************/
App.controller('ApproverUserLookupModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService','selectedApproverUserIds','Notification',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService , selectedApproverUserIds , Notification){
			$scope.title = title;
			$scope.selectedApproverUserIds = selectedApproverUserIds;
			//  This close function doesn't need to use jQuery or bootstrap, because
			//  the button has the 'data-dismiss' attribute.
			$scope.close = function(){
				$element.modal('hide');
//			close({},500); // close, but give 500ms for bootstrap to animate
			};
			$scope.selectAndClose = function(id,name){
				var isNewUser = true;
				// check whether the user is already selected or not.
				angular.forEach($scope.selectedApproverUserIds,function(userId){
					if(id == userId){
//						alert("This approver user is already selected.");
						Notification.warning({
							title	: "Warning!",
							message	: "Approver user <b>" + name + "</b> is already selected."
						});
						isNewUser = false;
					}
				});
				
				if(isNewUser){
					close({
						id		: id,
						name	: name
					}, 500); // close, but give 500ms for bootstrap to animate
				}
			};
			
			//  This cancel function must use the bootstrap, 'modal' function because
			//  the doesn't have the 'data-dismiss' attribute.
			$scope.cancel = function(){
				//  Manually hide the modal.
				$element.modal('hide');
				
				//  Now call close, returning control to the caller.
				close($scope.result,500); // close, but give 500ms for bootstrap to animate
			};
			
			// CUSTOM CODING FOR GL CODES
			$scope.approverUserModel = {
					searchStr : ""
			};
			$scope.loadApproverUsers = function(){
				var approverUserModalBlock = blockUI.instances.get('approverUserModalBlock');
				approverUserModalBlock.start("Searching approver users...");
				
				LookupService.loadApproverUsers($scope.approverUserModel)
				.success(function(response){
					$scope.approverUserErrors = response.errors;
					
					if(response.status == 'SUCCESS'){
						$scope.buyerUsers = response.data.buyerUsers;
					}else{
						// SHOW ERROR
					}
					approverUserModalBlock.stop();
				})
				.error(function(){
					alert('Unexpected error occured while searching !!');
					approverUserModalBlock.stop();
				});
			};
			$timeout($scope.loadApproverUsers(),500);
		}]);
/************************* NOTE MODAL CONTROLLER *************************/
App.controller('NoteLookupModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService','noteId',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close,LookupService ,noteId ){
		$scope.title = title;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
//			close({},500); // close, but give 500ms for bootstrap to animate
		};
		$scope.selectAndClose = function(id,name){
			close({
				id		: id,
				name	: name
			}, 500); // close, but give 500ms for bootstrap to animate
		};
		
		//  This cancel function must use the bootstrap, 'modal' function because
		//  the doesn't have the 'data-dismiss' attribute.
		$scope.cancel = function(){
			//  Manually hide the modal.
			$element.modal('hide');
			
			//  Now call close, returning control to the caller.
			close({},500); // close, but give 500ms for bootstrap to animate
		};
		

		$scope.loadNotes = function(){
			var noteModalBlock = blockUI.instances.get('noteModalBlock');
			noteModalBlock.start("Loading Note Body...");
			
			LookupService.loadNote(noteId)
				.success(function(response){
					$scope.noteErrors = response.errors;
					
					if(response.status == 'SUCCESS'){
						$scope.note = response.data.note;
					}else{
						// SHOW ERROR
					}
					noteModalBlock.stop();
				})
				.error(function(){
					alert('Unexpected error occured while searching cost codes !!');
					noteModalBlock.stop();
				});
		};
		$timeout($scope.loadNotes(),500);
}]);
/************************* GENERIC LOOKUP MODAL CONTROLLER *************************/
App.controller('GenericLookUpModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','LookupService','Notification', 'referenceTo',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , LookupService , Notification, referenceTo){
	$scope.title = title;
	$scope.close = function(){
		$element.modal('hide');
	};
	$scope.selectAndClose = function(id,name){
		close({
			id		: id,
			name	: name
		}, 500);
	};
	
	$scope.cancel = function(){
		$element.modal('hide');
		close($scope.result,500);
	};
	
	$scope.genericModel = {
			searchStr 	: "",
			referenceTo	: referenceTo
	};
	$scope.loadGenericLookUpResult = function(){
		var genericModalBlock = blockUI.instances.get('genericModalBlock');
		genericModalBlock.start("Searching "+ $scope.title +"s . . .");
		LookupService.loadGenericLookUpResult($scope.genericModel)
			.success(function(response){
				if(response.status == 'SUCCESS'){
					$scope.searchResult = response.data.searchResult;
				}else{
					// SHOW ERROR
					$scope.modelErrors = response.errors;
				}
				genericModalBlock.stop();
			})
			.error(function(){
				genericModalBlock.stop();
				alert('Unexpected error occured while searching !!');
			});
	};
	$timeout($scope.loadGenericLookUpResult(),500);
}]);
