'use strict';

/************************* SUPPLIER MAINTENANCE LIST CONTROLLER *************************/
App.controller('SupplierMaintenanceListController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','SupplierMaintenanceService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , SupplierMaintenanceService , esmCache){
		$scope.recordsCacheId = 'SupplierMaintenanceListController_Records';
			
		$scope.searchSupplierMaintenance = function(componentData){
			var searchSupplierBlock = blockUI.instances.get('searchSupplierBlock');
			searchSupplierBlock.start("Searching...");
			
			SupplierMaintenanceService.searchSupplierMaintenance(componentData)
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.records = response.data.searchResult;
						esmCache.put($scope.recordsCacheId,$scope.records);
						$scope.hasCachedData = false;
					}
					searchSupplierBlock.stop();
				})
				.error(function(){
					searchSupplierBlock.stop();
				});
		};
		
		$scope.init = function(){
			$scope.records = esmCache.get($scope.recordsCacheId);
			$scope.hasCachedData = ($scope.records && $scope.records.length > 0) ? true : false;
		};
		$scope.init();
}]);

/************************* SUPPLIER MAINTENANCE DETAILS CONTROLLER *************************/
App.controller('SupplierMaintenanceDetailsController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','SupplierMaintenanceService','HistoryService','NotesAndAttachmentsService',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , SupplierMaintenanceService,HistoryService,NotesAndAttachmentsService){
			
			$scope.supplierMaintenanceHistoryComponentModel = {
					title		: "Supplier Maintenance History",
					loadData	: function(){
						$scope.loadHistoryData();
					},
					data		: null,
			};
			
			// FUNCTION :: loadNotesAndAttachemntsData()
			$scope.loadHistoryData = function(){
				var historyDetailBlock = blockUI.instances.get('historyDetailBlock');
				var historyDetails = null;
				historyDetailBlock.start("Loading Supplier Maintenance History Details...");
				HistoryService.loadHistoryData("akritivesm__Supplier_Maintenance__c",$stateParams.id).success(function(response){
					if(response.status === "SUCCESS"){
						$scope.supplierMaintenanceHistoryComponentModel.data = response.data.historyData;
					}else{
						$scope.supplierMaintenanceHistoryComponentModel.errors = response.errors;
					}
					historyDetailBlock.stop();
				})
				.error(function(){
					historyDetailBlock.stop();
				});
			};
			

			$scope.notesAndAttachemntsComponentModel = {
				loadData	: function(){
						$scope.loadNotesAndAttachemntsData();
					},
					notesDetails		: null,
					attachmentsDetails : null,
			};
			// FUNCTION :: loadNotesAndAttachemntsData()
			$scope.loadNotesAndAttachemntsData = function(){
				var notesAndAttachmentBlock = blockUI.instances.get('notesAndAttachmentBlock');
				notesAndAttachmentBlock.start("Loading Notes & Attachments...");
				NotesAndAttachmentsService.getNotesDetails($stateParams.id).success(function(response){
					if(response.status === "SUCCESS"){
						$scope.notesAndAttachemntsComponentModel.notesDetails = response.data.notesDetails;	
					}else{
						$scope.notesAndAttachemntsComponentModel.errors = response.errors;
					}
					notesAndAttachmentBlock.stop();
				})
				.error(function(){
					notesAndAttachmentBlock.stop();
				});
				NotesAndAttachmentsService.loadAttachmentsDetails($stateParams.id).success(function(response){
					if(response.status === "SUCCESS"){
						$scope.notesAndAttachemntsComponentModel.attachmentsDetails = response.data.attachmentsDetails;	
					}else{
						$scope.notesAndAttachemntsComponentModel.errors = response.errors;
					}
					notesAndAttachmentBlock.stop();
				})
				.error(function(){
					notesAndAttachmentBlock.stop();
				});
			}; 
			// FUNCTION :: loadSupplierMaintenanceDetails()
			$scope.loadSupplierMaintenanceDetails = function(){
				var supplierMaintenanceDetailsBlock = blockUI.instances.get('supplierMaintenanceDetailsBlock');
				supplierMaintenanceDetailsBlock.start("Loading Supplier Maintenance details...");
				
				SupplierMaintenanceService.loadSupplierMaintenanceDetails($stateParams.id)
					.success(function(response){
						if(response.status === "SUCCESS"){
							$scope.sm = response.data.supplierMaintenanceDetails;
							$scope.isEnableEdit = response.data.isEnableEdit;
						}else{
							$scope.errors = response.errors;
						}
						
						supplierMaintenanceDetailsBlock.stop();
					})
					.error(function(){
						supplierMaintenanceDetailsBlock.stop();
					});
			};
			
			$scope.userActionItems = [
      			{label : 'Approve',value : 'Approve'},
      			{label : 'Reject',value : 'Reject'}
      		];
      		
      		// FUNCTION :: showDependant()
      		$scope.showDependant = function(value){
      			if(value=='Reject'){
      				$scope.rejectionReasonItems = ['Duplicate Request',
      							                   'Invalid / Missing Information.',
      							                   'Others'];	
      			}else{
      				$scope.rejectionReasonItems = null;
      			}
      		}
      		
      	// FUNCTION :: submitInvoice()
    		$scope.submitSupplierMaintenanceDetails = function(){
    			$scope.submitSupplierMaintenanceErrors = [];
    			if(!$scope.sm.New_Comments__c){
    				$scope.submitSupplierMaintenanceErrors.push("Comments is required!");
    			}
    			if(!$scope.sm.useraction){
    				$scope.submitSupplierMaintenanceErrors.push("User action is required!");
    			}
    			if($scope.sm.useraction == 'Reject' && !$scope.sm.rejectionReason){
    				$scope.submitSupplierMaintenanceErrors.push("Rejection reason is required!");
    			}
    			if($scope.submitSupplierMaintenanceErrors.length > 0){
    				return;
    			}
    			
    			var supplierMaintenanceDetailsBlock = blockUI.instances.get('supplierMaintenanceDetailsBlock');
				supplierMaintenanceDetailsBlock.start("Submitting Supplier Maintenance details...");

				SupplierMaintenanceService.submitSupplierMaintenanceDetails($scope.sm)
	    			.success(function(response){
						if(response.status === "SUCCESS"){
							Notification.success({
								title 	:'Success',
								message	:'Supplier Maintenance details saved successfully.'});
							$state.go("supplier-maintenanace-list");												// REDIRECT TO SM LISTING...
						}else{
							Notification.error({
								title	:'Error',
								message	:'Error occured while saving supplier maintenance details!!'
							});
							$scope.errors = response.errors;
						}
						supplierMaintenanceDetailsBlock.stop();
					})
					.error(function(){
						$scope.errors = [{"ERROR" : "Unexpected error occured !!!"}];
						supplierMaintenanceDetailsBlock.stop();
					});
    		}
    		
    		// FUNCTION :: cancelSupplierMaintenance()
    		$scope.cancelSupplierMaintenance = function(){
    			$state.go("supplier-maintenanace-list");												// REDIRECT TO SM LISTING...
    		}
			
			$scope.init = function(){
				$scope.loadSupplierMaintenanceDetails();
			};
			$scope.init();
}]);
/************************* SUPPLIER MAINTENANCE CREATE CONTROLLER *************************/
App.controller('SupplierMaintenanceCreateController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','SupplierMaintenanceService', '$timeout', 'Upload',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , SupplierMaintenanceService, $timeout, Upload){
			$scope.baseScope = $scope;
			$scope.uploaded = false;
			$scope.sm = {};
			$scope.files=[];
			// FUNCTION :: loadSupplierMaintenanceFields()
			$scope.loadSupplierMaintenanceFields = function(){
				var supplierMaintenanceCreateBlock = blockUI.instances.get('supplierMaintenanceDetailsBlock');
				supplierMaintenanceCreateBlock.start("Loading Supplier Maintenance...");
				
				SupplierMaintenanceService.loadSupplierMaintenanceFields()
					.success(function(response){
						if(response.status === "SUCCESS"){
							$scope.pickList = response.data.supplierMaintenanceFields;
						} else {
							$scope.errors = response.errors;
						}
						supplierMaintenanceCreateBlock.stop();
					})
					.error(function(){
						supplierMaintenanceCreateBlock.stop();
					});
			};
			
			$scope.loadSupplierMaintenanceFields();
			
			// FUNCTION :: showRequestorUserLookup()
			$scope.showRequestorUserLookup = function(sm){
				ModalService.showModal({
					templateUrl		: "modal/buyeruser",
					controller		: "ApproverUserLookupModalController",
					inputs			: {
						title						: "Approver Users Lookup",
						selectedApproverUserIds		: []
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						sm.akritivesm__Requestor__c = result;
					});
				});
			};
			
		    $scope.saveSupplierMaintenance = function(){
		    	$scope.supplierMaintenanceErrors=[];
		    	var uploadedFiles=0;
		    	var notPersistedFileList="";
		    	if(!$scope.sm.Vendor_Type__c)
	    		{
		    		$scope.supplierMaintenanceErrors.push("Vendor Type is required");
	    		}
		    	if(!$scope.sm.Vendor_Number_S__c)
	    		{
		    		$scope.supplierMaintenanceErrors.push("Vendor Number is required");
	    		}
		    	if(!$scope.sm.Vendor_Name__c)
	    		{
		    		$scope.supplierMaintenanceErrors.push("Vendor Name is required");
	    		}
		    	if(!$scope.sm.Request_Type__c)
		    	{
		    		$scope.supplierMaintenanceErrors.push("Request Type mail is required");
		    	}
		    	if(!$scope.sm.akritivesm__Comments__c)
		    	{
		    		$scope.supplierMaintenanceErrors.push("Comments street is required");
		    	}
		    	if(!$scope.sm.akritivesm__User_Actions__c)
		    	{
		    		$scope.supplierMaintenanceErrors.push("User Actions city is required");
		    	}
		    	if($scope.supplierMaintenanceErrors.length > 0)
	    		{
		    		return;
	    		}
		    	else
	    		{
//		    		$scope.finalSaveSupplierMaintenance();
		    		angular.forEach($scope.files,function(file){
			    		if(!file.isPersisted)
		    			{
			    			notPersistedFileList += file.name + ",";
		    			}
			    		else
		    			{
			    			uploadedFiles++;
		    			}
			    	});
	    		}
		    	if(notPersistedFileList.length > 0)
	    		{
			    	Notification.error({
			    		title	:'Error',
			    		message	: notPersistedFileList + ' not uploaded. Please upload it either remove it.'
			    	});
	    		}
		    	if(uploadedFiles == $scope.files.length)
		    		$scope.finalSaveSupplierMaintenance();
		    };
		    
		    $scope.finalSaveSupplierMaintenance = function(){
		    	var supplierMaintenanceCreateBlock = blockUI.instances.get('supplierMaintenanceDetailsBlock');
		    	
		    	supplierMaintenanceCreateBlock.start("Saving Supplier Maintenance...");
		    	$scope.sm.files = $scope.files; 
		    	SupplierMaintenanceService.saveSupplierMaintenance($scope.sm)
					.success(function(response){
						if(response.status === "SUCCESS"){
							Notification.success({
								title 	:'Success',
								message	:'Supplier maintenance saved successfully.'});
							$state.go("supplier-maintenanace-list");
						}else{
							Notification.error({
								title	:'Error',
								message	:'Error occured while saving SupplierMaintenanceService!!'
							});
							$scope.errors = response.errors;
						}
						supplierMaintenanceCreateBlock.stop();
					})
					.error(function(){
						$scope.errors = [{"ERROR" : "Unexpected error occured !!!"}];
						supplierMaintenanceCreateBlock.stop();
					});
			}
		    
		    $scope.cancelSupplierMaintenance = function(){
				$state.go("supplier-maintenanace-list");
			}
}]);