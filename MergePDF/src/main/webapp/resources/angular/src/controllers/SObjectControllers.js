'use strict';

/************************* SOBJECT LIST CONTROLLER *************************/
AdminApp.controller('SObjectListController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','SObjectService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , SObjectService , esmCache){
			
		$scope.reload = ($stateParams.reload) ? true : false;
			
		$scope.loadSObjects = function(){
			var searchSObjectBlock = blockUI.instances.get('searchSObjectBlock');
			searchSObjectBlock.start("Loading sObjects...");
			
			SObjectService.loadSObjects()
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.sObjects = response.data.sObjects;
						esmCache.put($scope.sObjectsCacheId,$scope.sObjects);
						$scope.hasCachedData = false;
					}
					searchSObjectBlock.stop();
				})
				.error(function(){
					searchSObjectBlock.stop();
				});
		};
		$scope.manageSObjects = function(){
			$state.go("admin-sobjects-manage");
		};
			
		$scope.init = function(){
			$scope.sObjectsCacheId = 'SObjectListController_sObjects';

			$scope.sObjects = esmCache.get($scope.sObjectsCacheId);
			$scope.hasCachedData = ($scope.sObjects && $scope.sObjects.length > 0) ? true : false;
			if(!$scope.hasCachedData || $scope.reload){
				$scope.loadSObjects();
			}
		};
		$scope.init();
}]);

/************************* SOBJECT MANAGE CONTROLLER *************************/
AdminApp.controller('SObjectManageController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','SObjectService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , SObjectService , esmCache){
			
		$scope.loadSfdcSObjects = function(){
			var sfdcSObjectBlock = blockUI.instances.get('sfdcSObjectBlock');
			sfdcSObjectBlock.start("Loading Salesforce sObjects...");
			
			SObjectService.loadSfdcSObjects()
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.sfdcSObjects = response.data.sfdcSObjects;
						esmCache.put($scope.sfdcSObjectsCacheId,$scope.sfdcSObjects);
						$scope.hasSfdcCachedData = false;
					}
					sfdcSObjectBlock.stop();
				})
				.error(function(){
					sfdcSObjectBlock.stop();
				});
		};
		$scope.loadHerokuSObjects = function(){
			var herokuSObjectBlock = blockUI.instances.get('herokuSObjectBlock');
			herokuSObjectBlock.start("Loading Heroku sObjects...");
			
			SObjectService.loadSObjects()
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.herokuSObjects = response.data.sObjects;
						$scope.hasHerokuCachedData = false;
					}
					herokuSObjectBlock.stop();
				})
				.error(function(){
					herokuSObjectBlock.stop();
				});
		};
		$scope.save = function(){
			var dataToSave = {
				objectsToSave 	: $scope.herokuSObjects,
				objectsToDelete	: $scope.herokuSObjectsToDelete
			};
			
			var manageSObjectBlock = blockUI.instances.get('manageSObjectBlock');
			manageSObjectBlock.start("Saving Changes...");
			
			SObjectService.saveSObjects(dataToSave)
				.success(function(response){
					manageSObjectBlock.stop();
					if(response.status === "SUCCESS"){
//						$scope.reloadAll();
						$state.go("admin-sobjects",{reload : true});
					}
				})
				.error(function(response){
					manageSObjectBlock.stop();
				});
		}
		$scope.addToHeroku = function(sObject){
			var manageSObjectBlock = blockUI.instances.get('manageSObjectBlock');
			manageSObjectBlock.start("Adding to Heroku...");
				
			var isDuplicate = false;
			angular.forEach($scope.herokuSObjects,function(herokuObj){
				if(isDuplicate === false){
					isDuplicate = (sObject.name === herokuObj.name);
				}
			});
			if(isDuplicate){
				manageSObjectBlock.stop();
				Notification.warning({
					message	: sObject.label + ' is already exists in heroku objects.',
					title	:'Duplicate'
				});
			}else{
				var isDeleted = false;
				var deletedIndex = 0;
				var deletedObj = {};
				var i = 0;
				angular.forEach($scope.herokuSObjectsToDelete,function(obj){
					if(isDeleted === false){
						isDeleted = (sObject.name === obj.name && obj.id && obj.id != null);
					}
					if(isDeleted){
						deletedIndex = i;
						deletedObj = angular.copy(obj);
					}
					i++;
				});
				if(!isDeleted){
					$scope.herokuSObjects.push(sObject);						// ADD NEW OBJECT
				}else{
					$scope.herokuSObjectsToDelete.splice(deletedIndex,1);		// REMOVE FROM DELETED LIST
					$scope.herokuSObjects.push(deletedObj);						// RESTORED DELETED OBJECT
				}
				
				manageSObjectBlock.stop();
				Notification.success({
					message	: sObject.label + ' is added.',
					title	:'Success'
				});
			}
		};
		$scope.removeFromHeroku = function(sObject,index){
			if(sObject.id && sObject.id != null){
				$scope.herokuSObjectsToDelete.push(sObject);
			};
			$scope.herokuSObjects.splice(index,1);
		};
		$scope.backToList = function(){
			$state.go("admin-sobjects");
		};
		$scope.reloadAll = function(){
			$scope.loadSfdcSObjects();
			$scope.loadHerokuSObjects();
			$scope.herokuSObjectsToDelete = [];
		};
			
		$scope.init = function(){
			$scope.sfdcSObjectsCacheId = 'SObjectManageController_SFDC_SObjects';
			$scope.herokuSObjectsCacheId = 'SObjectManageController_HEROKU_SObjects';
			$scope.herokuSObjectsToDelete = [];

			$scope.sfdcSObjects = esmCache.get($scope.sfdcSObjectsCacheId);
			$scope.hasSfdcCachedData = ($scope.sfdcSObjects && $scope.sfdcSObjects.length > 0) ? true : false;
			if(!$scope.hasSfdcCachedData){
				$scope.loadSfdcSObjects();
			}
//			
//			$scope.herokuSObjects = esmCache.get($scope.herokuSObjectsCacheId);
//			$scope.hasHerokuCachedData = ($scope.herokuSObjects && $scope.herokuSObjects.length > 0) ? true : false;
//			if(!$scope.hasHerokuCachedData){
				$scope.loadHerokuSObjects();
//			}
		};
		$scope.init();
}]);