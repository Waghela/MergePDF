'use strict';

var modules = [ 'ngCookies','ui.router',
                'ngAnimate',
                'blockUI',
                'datatables',
                'angularModalService',
                'ui-notification',
      		  	'ngFileUpload'];

try{
	// use module 'ESM-ADMIN' if it exist
	angular.module('ESM-ADMIN'); // throws error if "ESM-ADMIN" doesn't exist
	modules.push('ESM-ADMIN');
//	alert("ADMIN Module Injected!");
}catch(e){
//	alert("ADMIN Module not found!");
}

var App = angular.module('ESM-APP',modules);

App.run(function($rootScope,$location,blockUI,$filter,$http){
	$rootScope.locationPath = $location.path();
	$rootScope.isActive = function(path){
		var pos = $location.path().lastIndexOf("/");
		var subPath = $location.path().substr(0,pos);
		
		if($location.path() == path || subPath == path)
			return true;
		else
			return false;
	};
	// To show value for any element with default value
	$rootScope.showValue = function(element,defVal){
		defVal = (defVal == null) ? '--' : defVal ;
		//return ($scope.loading) ? 'loading...' : ((element) ? element : '--');
		return (element) ? element : defVal;
	};

	$rootScope.showDateValue = function(element,defVal,format,timezone){
		defVal = (defVal == null) ? '--' : defVal ;
		format = (format == null) ? 'MMM d, y h:mm:ss a' : format ;
		return (element) ? $filter('date')(element, format, timezone) : defVal;
	};
	
	$rootScope.showNumberValue = function(element,fractionSize){
		fractionSize = (fractionSize == null) ? 2 : fractionSize;
		return (element) ? $filter('number')(element, fractionSize) : '--';
	};
	
	$rootScope.showCheckBoxValue = function(element){
		return (element && (element === 'true' || element === true)) ? "icon-check" : "icon-check-empty"
	};
});