'use strict';

App.controller('ActionComponentController',
		[	'$scope','$log','ActionValidationService','$q',
	function($scope , $log , ActionValidationService , $q ){
			
		$scope.ifValid = function(){
			var deferred = $q.defer();
			
			if(ActionValidationService.registeredItems.length == 0){
				if($scope.isActionModelValid()){
					deferred.resolve();
				}
			}
			
			ActionValidationService.triggerValidation();
			
			$scope.layoutBlockUI.start("Validating...");
			
			$scope.watchValidating = $scope.$watch(
				function(scope){
					var validatedCount = 0;
					var validCount = 0;
					angular.forEach(ActionValidationService.registeredItems,function(item){
						if(item.status == "VALIDATED"){
							validatedCount++;
						}
						if(item.valid){
							validCount++;
						}
					});
					return {validatedCount: validatedCount,validCount: validCount, count: ActionValidationService.registeredItems.length};
				},
				function(result){
					if(result.validatedCount == result.count){
						$scope.watchValidating();
						if((result.count == result.validCount) && $scope.isActionModelValid()){
							$scope.layoutBlockUI.stop();
							deferred.resolve();
						}else{
							$scope.layoutBlockUI.stop();
							deferred.reject();
						}
					}
				}
			);
			
			return deferred.promise;
		};
}]);