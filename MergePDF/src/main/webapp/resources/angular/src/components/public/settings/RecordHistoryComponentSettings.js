'use strict';

AdminApp.controller('RecordHistoryComponentSettingsController',
		[	'$scope','SObjectService','blockUI','$http','Notification',
 	function($scope , SObjectService , blockUI , $http,Notification){
		// Save your configuration in $scope.configurations

	  
	   $scope.isComponentValid = function(){
				
	   }
	  
	   $scope.onSObjectChange = function(sObject){
			$scope.configurations.sObjectApiName = sObject.name;
		}
	   $scope.onCustomSettingSObjectChange = function(customSettingSObject){
			$scope.configurations.customSettingSObjectApiName = customSettingSObject.name;
		}
	   $scope.onCustomHistorySObjectChange = function(customHistorySObject){
			$scope.configurations.customHistorySObjectApiName = customHistorySObject.name;
		}
	   $scope.onCustomSettingSObjectLookupApiFieldChange = function(customSettingSObjectLookupApiField){
			$scope.configurations.customSettingSObjectLookupApiField = customSettingSObjectLookupApiField;
		}
	   $scope.onCustomSettingSObjectCustomHistoryStatusFieldChange = function(customSettingSObjectCustomHistoryStatusField){
			$scope.configurations.customSettingSObjectCustomHistoryStatusField = customSettingSObjectCustomHistoryStatusField;
		}
	   $scope.onCustomHistorySObjectOldValueFieldChange = function(customHistorySObjectOldValueField){
			$scope.configurations.customHistorySObjectOldValueField = customHistorySObjectOldValueField;
		}
	   $scope.onCustomHistorySObjectNewValueFieldChange= function(customHistorySObjectNewValueField){
			$scope.configurations.customHistorySObjectNewValueField = customHistorySObjectNewValueField;
		}
	   $scope.onCustomHistorySObjectFieldLabelFieldChange = function(customHistorySObjectFieldLabelField){
			$scope.configurations.customHistorySObjectFieldLabelField = customHistorySObjectFieldLabelField;
		}
	   $scope.onCustomHistorySObjectCreatedDateFieldChange = function(customHistorySObjectCreatedDateField){
			$scope.configurations.customHistorySObjectCreatedDateField = customHistorySObjectCreatedDateField;
		}
	   $scope.onCustomHistorySObjectCreatedByIdFieldChange = function(customHistorySObjectCreatedByIdField){
			$scope.configurations.customHistorySObjectCreatedByIdField = customHistorySObjectCreatedByIdField;
		}
	   
      $scope.loadRecordHistoryLookupFields = function(){
		var RecordHistoryComponentSettingsBlockUi = blockUI.instances.get('RecordHistoryComponentSettingsBlockUi');
		RecordHistoryComponentSettingsBlockUi.start("Loading sObjects...");
		
		$http.get("rest/layout-component/record-history-component/sobjects")
			.success(function(response){
				if(response.status=="SUCCESS"){
					$scope.sObjects = response.data.sObjects;
					angular.forEach($scope.sObjects,function(sObject){
						if($scope.configurations.sObjectApiName == sObject.name){
								$scope.sObject = sObject;
						}	
						if($scope.configurations.customSettingSObjectApiName == sObject.name){
							$scope.customSettingSObject = sObject;
							angular.forEach(sObject.fields,function(field){
								if($scope.configurations.customSettingSObjectLookupApiField.name == field.name){
									$scope.customSettingSObjectLookupApiField = field;
								}	
								if($scope.configurations.customSettingSObjectCustomHistoryStatusField.name == field.name){
									$scope.customSettingSObjectCustomHistoryStatusField = field;
								}
								
							  });
					    }
						if($scope.configurations.customHistorySObjectApiName == sObject.name){
							$scope.customHistorySObject = sObject;
							angular.forEach(sObject.fields,function(field){
								if($scope.configurations.customHistorySObjectOldValueField.name == field.name){
									$scope.customHistorySObjectOldValueField = field;
								}	
								if($scope.configurations.customHistorySObjectNewValueField.name == field.name){
									$scope.customHistorySObjectNewValueField = field;
								}
								if($scope.configurations.customHistorySObjectFieldLabelField.name == field.name){
									$scope.customHistorySObjectFieldLabelField = field;
								}
								if($scope.configurations.customHistorySObjectCreatedDateField.name == field.name){
									$scope.customHistorySObjectCreatedDateField = field;
								}
								if($scope.configurations.customHistorySObjectCreatedByIdField.name == field.name){
									$scope.customHistorySObjectCreatedByIdField = field;
								}
								
							  });
					    }
					  });
				}else{
					Notification.error({
						message	:"Error occured while record history lookup fields!!",
						title	:'Error'
					});	
				}
				RecordHistoryComponentSettingsBlockUi.stop();
				})
				.error(function(response){
					RecordHistoryComponentSettingsBlockUi.stop();
					Notification.error({
						message	:"Unexpected error occured while record history lookup fields!",
						title	:'Error'
					});	
				});
		};
		
		
		$scope.configurations = ($scope.configurations) ? $scope.configurations :{
			sObjectApiName 								: null,
			customSettingSObjectApiName 				: null,
			customSettingSObjectLookupApiField 			: null,
			customSettingSObjectCustomHistoryStatusField: null,
			customHistorySObjectApiName       			: null,
			customHistorySObjectOldValueField        	: null,
			customHistorySObjectNewValueField        	: null,
			customHistorySObjectFieldLabelField        	: null,
			customHistorySObjectCreatedDateField        : null,
			customHistorySObjectCreatedByIdField        : null,

		};
		$scope.init = function(){
			$scope.loadRecordHistoryLookupFields();
		};
		$scope.init();
		console.log("RecordHistoryComponentSettingsController initialized...");
}]);