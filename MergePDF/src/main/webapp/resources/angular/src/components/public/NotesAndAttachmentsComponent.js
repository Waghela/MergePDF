'use strict';

App.controller('NotesAndAttachmentsComponentController',
		[	'$scope','$log','$http','blockUI','$stateParams','ModalService',
	function($scope , $log , $http , blockUI , $stateParams,ModalService){
			
		
		/** OVERRIDE */
		$scope.isComponentValid = function(){
			return false;
		}
		$scope.getNote = function getNote(id) {
			return $http.get("rest/layout-component/notes-and-attachments-component/note/" + id);
		}
		$scope.loadNotesAndAttachemntsData = function(){
			var notesAndAttachmentBlock = blockUI.instances.get('notesAndAttachmentBlock');
			notesAndAttachmentBlock.start("Loading Notes & Attachments...");
			 $http.get("rest/layout-component/notes-and-attachments-component/loadNotesDetails/"
						+ $stateParams.id).success(function(response){
				if(response.status === "SUCCESS"){
					$scope.notesDetails = response.data.notesDetails;								
				}else{
					$scope.errors = response.errors;
				}
				notesAndAttachmentBlock.stop();
			})
			.error(function(){
				notesAndAttachmentBlock.stop();
			});
			 $http
				.get("rest/layout-component/notes-and-attachments-component/loadAttachmentsDetails/"
						+ $stateParams.id).success(function(response){
				if(response.status === "SUCCESS"){
					$scope.attachmentsDetails = response.data.attachmentsDetails;	
				}else{
					$scope.errors = response.errors;
				}
				notesAndAttachmentBlock.stop();
			})
			.error(function(){
				notesAndAttachmentBlock.stop();
			});
		};
		// FUNCTION :: showNoteModal()
		$scope.showNoteModal = function(id){
		ModalService.showModal({
				templateUrl		: "modal/note",
				controller		: "NoteLookupModalController",
				inputs			: {
					title	: "Notes",
					noteId		: id,
				}
			}).then(function(modal){
				modal.element.modal();
				modal.close.then(function(result){
					//note.costCode = result;
				});
			});
		};
		/** OVERRIDE */
		$scope.init = function(){
			console.log( $scope.section.title +" NotesAndAttachmentsComponentController Initializing...");
			 $scope.loadNotesAndAttachemntsData();
		
		};
		$scope.init();
	}
]);