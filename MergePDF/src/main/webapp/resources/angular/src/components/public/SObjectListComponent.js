'use strict';

App.controller('SObjectListComponentController',
		[	'$scope','$log','blockUI','$stateParams','SObjectListComponentService',
	function($scope , $log , blockUI , $stateParams , SObjectListComponentService){
			
		/**
		 * GLOBAL VARIABLES
		 * ------------------
		 * @section						: component section details
		 * @sectionIndex				: position of Component in layout
		 * @model						: data model for SObject Types
		 * 
		 * INSTANCE METHODS
		 * ----------------
		 * @doValidate					: validation phase [ NOTE : Must be Defined ]
		 * 
		 */
			
		$scope.loadInvoiceLineItems = function(){
			var componentBlock = blockUI.instances.get("componentBlock");
			componentBlock.start('Loading invoice line items...');
			
			var config = {
					id					: $stateParams.id,
					configurations		: $scope.section.componentMetadata.configurations
			};
			
			SObjectListComponentService.loadLineIems(config)
				.success(function(response){
					if(response.status == "SUCCESS"){
						$scope.lineItems = response.data.lineItems;
					}else{
						alert(response.error[0].message);
					}
					componentBlock.stop();
				})
				.error(function(response){
					componentBlock.stop();
					alert("Error occured!");
				});
		}
	
		/** OVERRIDE */
		$scope.isComponentValid = function(){
			return true;
		};
		
		/** OVERRIDE */
		$scope.init = function(){
			$scope.configurations = $scope.section.componentMetadata.configurations;
			console.log( $scope.section.title +" SObjectListComponentController Initializing...");
			$scope.loadInvoiceLineItems();
		};
		$scope.init();
	}
]);
App.service('SObjectListComponentService',
		['$http',
	    function($http){
			return {
				loadLineIems		:loadLineIems
			};
			
			/* -------------------------------------------------------------------
			 * PUBLIC FUNCTIONS 
			 * -------------------------------------------------------------------*/
			function loadLineIems(configObj){
				return $http.post("rest/layout-component/sobject-list-component/lineitems", configObj);
			};
}]);