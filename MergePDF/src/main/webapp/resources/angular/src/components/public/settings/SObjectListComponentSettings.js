'use strict';

AdminApp.controller('SObjectListComponentSettingsController',
		[	'$scope','blockUI','$http','Notification',
 	function($scope , blockUI , $http, Notification){
		
		$scope.loadSObjects = function(){
			var listComponentSettingsBlock = blockUI.instances.get('listComponentSettingsBlock');
			listComponentSettingsBlock.start("Loading sObjects...");
			$http.get("rest/layout-component/sobject-list-component/sobjects")
				.success(function(response){
					if(response.status == "SUCCESS"){
						$scope.sObjects = response.data.sObjects;
						angular.forEach($scope.sObjects,function(sobject){
							if(sobject.name == $scope.configurations.sobjectApiName){
								$scope.selectedSObject = sobject;
							}
						});
					}else{
						alert("Error occured while loading sObjects!");
					}
					listComponentSettingsBlock.stop();
				})
				.error(function(response){
					listComponentSettingsBlock.stop();
					alert("Unexpected error occured while loading sObjects!");
				});
		};
		
		$scope.onSObjectSelectChange = function(sobject){
			$scope.configurations.sobjectFields = [];
			$scope.configurations.sobjectApiName = sobject.name;
		};
		
		$scope.addField = function(field){
			var count = 0;
			angular.forEach($scope.configurations.sobjectFields,function(sobjectField){
				if(sobjectField.name == field.name){
					count++;
				}
			});
			if(count==0){
				$scope.configurations.sobjectFields.push(field);
			}
		};
		
		$scope.changeSOQLQuery = function () {
			var fields = '';
			$scope.configurations.isSOQLValidate = false;
			angular.forEach($scope.configurations.sobjectFields,function(sobjectField, key){
				fields += sobjectField.name;
				if(sobjectField.type == 'reference')
					fields += ', ' + sobjectField.relationshipName + '.Name';
				if($scope.configurations.sobjectFields.length-1 != key)
					fields += ", ";
			});
			$scope.configurations.soqlQuery = "SELECT " + fields + " FROM " + $scope.configurations.sobjectApiName ;
			if($scope.configurations.whereClause != null && $scope.configurations.whereClause != ''){
				$scope.configurations.soqlQuery += ' WHERE ' + $scope.configurations.whereClause;
			}
			if($scope.configurations.recordLimit != null && $scope.configurations.recordLimit != ''){
				$scope.configurations.soqlQuery += ' LIMIT ' + $scope.configurations.recordLimit;
			}
		}
		
		$scope.validateQuery = function () {
			   var CostAllocationComponentSettingsBlockUi = blockUI.instances.get('CostAllocationComponentSettingsBlockUi');
				CostAllocationComponentSettingsBlockUi.start("Validating SOQL...");
			   $http.post("rest/layout-component/sobject-list-component/validateQuery",$scope.configurations.soqlQuery)
				.success(function(response){
					if(response.status=="SUCCESS"){
						$scope.configurations.isSOQLValidate = true;
//						$scope.configurations.soqlQuery = $scope.configurations.soqlQuery ;
						Notification.success({
							message	:"SOQL query is correct.",
							title	:'Sucess'
						});
					}else{
						$scope.configurations.isSOQLValidate = false;
						Notification.error({
							message	:response.errors[0].message,
							title	:'Error In Soql'
						});
					}
					CostAllocationComponentSettingsBlockUi.stop();
				})
				.error(function(response){
					CostAllocationComponentSettingsBlockUi.stop();
					$scope.configurations.isSOQLValidate = true;
					Notification.error({
						message	:"Unexpected error occured while validating soql sObjects!",
						title	:'Error'
					});				
				});
	    };
		
		// Save your configuration in $scope.configurations
		$scope.configurations = ($scope.configurations) ? $scope.configurations :{
			sobjectApiName 	: null,
			sobjectFields	: [],
			whereClause		: null,
			recordLimit		: 0,
			soqlQuery		: '',
			isSOQLValidate	: false
		};
		
		$scope.init = function(){
			$scope.sObjects = [];
			$scope.loadSObjects();
		};
		
		$scope.init();
		
		console.log("SObjectListComponentSettingsController initialized...");
}]);