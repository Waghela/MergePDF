'use strict';

/******************************************************************************************************* [ DIRECTIVE ]*/
App.directive('invoiceActionComponent',
		[	'$compile','$http','blockUI','$log','esmCache','$timeout','$controller',
	function($compile , $http , blockUI , $log , esmCache , $timeout , $controller) {
		
		return {
			restrict	:'E',
			scope		:{ 
							ctrl			:'='
						 },
			templateUrl	:"layout-component/invoice-action-component",
			controller	:function($scope){
				$scope.layoutBlockUI = $scope.ctrl.blockUiInstances.layout;
				angular.extend(this,$controller("ActionComponentController",{$scope: $scope}));
				angular.extend(this,$controller("InvoiceActionComponentController",{$scope: $scope}));
			}
		};
}]);
/******************************************************************************************************* [ CONTROLLER ]*/
App.controller('InvoiceActionComponentController',
		[	'$scope','$log','ActionValidationService','ModalService','$filter',
	function($scope , $log , ActionValidationService , ModalService , $filter){
			
		var ctrl = $scope.ctrl;
		$scope.ActionValidationService = ActionValidationService;
		
		// COMPONENT VARIABLES
		angular.extend($scope,{
			errorMessages					: {}
		});
		
		var userActionField 		= null;
		var rejectionReasonField 	= null;
		var currentState 			= null;
		$scope.section 				= null;
		// WATCH
		$scope.$watch('ctrl.metadata', function (value) {
			
			if(ctrl.metadata.sObjectLayout && $scope.section == null){
				$scope.section =  ctrl.metadata.sObjectLayout.actionComponentSettings.section;
				$scope.actionModel = $scope.section.fields;
			}
			
			if(ctrl.metadata.sObject && userActionField == null){
				angular.forEach(ctrl.metadata.sObject.fields,function(field){
					if(field.name == $scope.section.fields.useraction.herokupicklistapiname){
						userActionField = field;
					}
				});
			};
			if(ctrl.metadata.sObject && rejectionReasonField == null){
				angular.forEach(ctrl.metadata.sObject.fields,function(field){
					if(field.name == $scope.section.fields.rejectionreason.apiname){
						rejectionReasonField = field;
					}
				});
			};
			
	    });
		
		$scope.$watch('ctrl.metadata.dataModel', function (value) {
			// LOADING USER ACTION PICKLIST VALUES DEPENDING ON CURRENT STATE
			if(userActionField && currentState == null){
				currentState = ctrl.metadata.dataModel[userActionField.controllerName];
				if(currentState){
					var picklistValues = userActionField.dependentPicklistValues[currentState];
					if(picklistValues.length == 0){
						$scope.ActionValidationService.hasUserAction = false;
//						console.log("NO USER ACTION DEFINED!");
					}else{
						$scope.ActionValidationService.hasUserAction = true;
//						console.log("USER ACTION DEFINED!");
						$scope.userActionPicklistValues = picklistValues.substring(0,picklistValues.lastIndexOf(',')).split(',');
					}
				}
			}
			// REJECTION REASON PICKLIST VALUES
			if(rejectionReasonField){
				var picklistValues = rejectionReasonField.picklistValues;
				$scope.rejectionReasonPicklistValues = picklistValues.substring(0,picklistValues.lastIndexOf(',')).split(',');
			}
		});
		
		// ACTION MODEL
		angular.extend($scope,{
			isActionModelValid				:function(){
				var model = $scope.actionModel;
				if(model.comments.required == true && (model.comments.value == '' || model.comments.value == null)){
					return false;
				}
				if(model.useraction.value == '' || model.useraction.value == null){
					return false;
				}
				if(model.useraction.value == $scope.section.fields.rejectionreason.useractionvalue 
						&& ( model.rejectionreason.value == '' || model.rejectionreason.value == null )){
					return false;
				}
				if(model.useraction.value == $scope.section.fields.route.useractionvalue 
						&& ( model.route.value == '' || model.route.value == null )){
					return false;
				}
				
				// SET ID FOR SOBJECT
				$scope.actionModel.sObjectID = {
					idStr 		: ctrl.metadata.dataModel.ID_IdStr,
					fullId		: ctrl.metadata.dataModel.ID_FullId,
					keyPrefix	: ctrl.metadata.dataModel.ID_KeyPrefix
				};
				
				return true;
			},
			parseDataModel					: function(){
				var objectDataModel 	= ctrl.metadata.dataModel;
				var dataModel 			= {};
				var layoutSections 		= ctrl.metadata.sObjectLayout.sections;
				
				angular.forEach(layoutSections,function(section){
					console.log("SECTION :: " + section.title);
					if(section.type == 'EDIT' && section.custom && section.sectionFieldsJsonArray){
						
						angular.forEach(section.sectionFieldsJsonArray,function(field){
							if(field.custom){
								var value = objectDataModel[field.name];
								if(field.type == 'datetime'){
									value = $filter('date')(new Date(value),'yyyy-MM-ddTHH:mm:ss.sssZ');
								}else if(field.type == 'date'){
									value = $filter('date')(new Date(value),'yyyy-MM-dd');
								}
								dataModel[field.name] = value;
							}
						});
					}
				});
				$scope.actionModel.currentState.value = objectDataModel[$scope.actionModel.currentState.apiname]; 
				$scope.actionModel.comments.historyValue = objectDataModel[$scope.actionModel.comments.apiname];
				return dataModel;
			}
		});
		
		// BUTTON ACTIONS
		angular.extend($scope,{
			save							: function(){
				$scope.errorMessages = null;
				$scope.ifValid().then(
						function(){			// VALID							
							var saveModel = {
									sObject		: angular.copy(ctrl.metadata.sObject),
									dataModel	: $scope.parseDataModel(),//ctrl.metadata.dataModel,
									actionModel	: $scope.actionModel
								}
							saveModel.sObject.fields = []; // clear fields to reduce load on request
//							ctrl.save(saveModel);
							$log.info(JSON.stringify(saveModel));
						},function(){ 		// INVALID
							$scope.errorMessages = {};
							$scope.errorMessages.required = true;
						});
			},
			cancel							: function(){
				
			}
		});
		
		// Route Lookup modal
		$scope.showLookupModal = function(){
			ModalService.showModal({
				templateUrl		: "modal/genericModal",
				controller		: "GenericLookUpModalController",
				inputs			: {
					title		: "Route",
					referenceTo	: $scope.actionModel.route.referenceTo
				}
			}).then(function(modal){
				modal.element.modal();
				modal.close.then(function(result){
//					console.log(result);
					$scope.actionModel.route['value'] = result.id;
					$scope.actionModel.route['value__Name'] = result.name;
//					console.log($scope.actionModel.route);
				});
			});
		};
}]);