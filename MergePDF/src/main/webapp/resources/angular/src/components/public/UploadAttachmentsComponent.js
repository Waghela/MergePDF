'use strict';

App.controller('UploadAttachmentsComponentController',
		[	'$scope','$log','Upload','$http','blockUI','$stateParams','ModalService','Notification',
	function($scope , $log , Upload,$http , blockUI , $stateParams,ModalService,Notification){
			
		
		/** OVERRIDE */
		$scope.isComponentValid = function(){
			return false;
		}
		

		var AttachmentBlock = blockUI.instances.get('AttachmentBlock');
		$scope.errFiles = [];
		$scope.files=[];
		$scope.allowedExtentions = [];
		$scope.disablePrimaryDocuent = false;
		$scope.primaryDocument;
		$scope.primaryFileName = "";
		$scope.attachmentDetails = {};
	
		
		$scope.selectFile = function(files, errFiles) {
			var blackListedFiles="";
			var sizeExceededFiles="";
			if($scope.primaryDocument)
			{
				$scope.allowedExtentions=[];
				$scope.allowedExtentions.push($scope.allowedExtForPrime);
			}
			else
			{
				$scope.allowedExtentions=[];
				angular.forEach($scope.allowedExt.split(','),function(ext){
					$scope.allowedExtentions.push(ext);
				});
			}
			angular.forEach(files,function(file){
        		if(($scope.allowedExtentions.indexOf(file.name.toLowerCase().substr(file.name.indexOf("."),file.name.length - 1)) > -1) && (file.size/(1024*1024) <= 10))
        		{
        			file.isPersisted = false;
	        		var pushOnScope = true;
	        		angular.forEach($scope.files,function(fileInScope){
	        			if(fileInScope.name == file.name)
        				{
	        				pushOnScope = false;
	        				if($scope.primaryDocument  && $scope.primaryFileName == "" && fileInScope.name == file.name)
	        					fileInScope.primaryDocument = true;
		        			else
		        				fileInScope.primaryDocument = false;
	        				return;
        				}
	        			return;
		        	});
	        		if($scope.primaryDocument && $scope.primaryFileName == "")
	        		{
	        			$scope.primaryFileName = file.name;
	        			$scope.disablePrimaryDocuent = true;
	        			$scope.primaryDocument = false;
	        		}
	        		if(pushOnScope)
        			{
	        			if($scope.primaryDocument && $scope.primaryFileName == file.name)
	        				file.primaryDocument = true;
	        			else
	        				file.primaryDocument = false;
	        			$scope.files.push(file);
        			}
        		}
        		else
    			{
        			blackListedFiles += file.name + ",";
    			}
        	});

			if(blackListedFiles.length > 0)
			{
				Notification.error({
					title	:'Error',
					message	:'Extention is black listed for ' + blackListedFiles
				});
			}
	        	
	        	
        	angular.forEach(errFiles,function(errFile){
        		sizeExceededFiles += errFile.name + ","
        	});
        	
        	if(sizeExceededFiles.length > 0)
        	{
        		Notification.error({
	        		title	:'Error',
	        		message	:'Error occured, may be size limit exceeded for files' + sizeExceededFiles 
        		});
        	}
		};
	    
		$scope.primaryDocumentClicked = function(primaryDoc){
			$scope.primaryDocument = primaryDoc;
		};
		
	    $scope.attachFiles = function(files)
	    {
	    	AttachmentBlock.start("Attaching File...");
	        angular.forEach($scope.files, function(file) {
	        	if(files == file)
        		{
	        		var response;
		        	if(file.name != $scope.primaryFileName)
	        		{
		        		$scope.uploadAttachments(file)
			        		.then(function(response){
			        			if(response.data.status == "SUCCESS")
								{
					        		file.fileName = response.data.data.fileName;
					        		file.isPersisted = true;
						        	Notification.success({
											title 	:'Success',
											message	: file.name + ' File Uploaded successfully.'
									});
								}
			        			else
								{
									Notification.error({
										title 	:'Error',
										message	: file.name + ' upload failed.'
									});
								}
			        			AttachmentBlock.stop();
			        		});
	        		}
		        	else
	        		{
		        		$scope.uploadPrimary(file)
			        		.then(function(response){
			        			if(response.data.status == "SUCCESS")
								{
					        		file.fileName = response.data.data.fileName;
					        		file.isPersisted = true;
						        	Notification.success({
											title 	:'Success',
											message	: file.name + ' File Uploaded successfully.'
									});
								}
			        			else
								{
									Notification.error({
										title 	:'Error',
										message	: file.name + ' upload failed.'
									});
								}
			        			AttachmentBlock.stop();
			        		});
	        		}
        		}
	        });
	    };
	    
	    $scope.deleteFile = function(file)
	    {
	    	AttachmentBlock.start("Deleting File...");
	    	var index = $scope.files.indexOf(file);
	    	var dataObj = {
	    				fileName : file.fileName,
	    		};
	    	
	    	$http.post("rest/layout-component/upload-attachments-component/deleteFile", dataObj)
	    		.success(function(response){
						if(response.status === "SUCCESS"){
							Notification.success({
								title 	:'Success',
								message	: file.name + ' deleted successfully.'
							});
						} else {
							$scope.errors = response.errors;
						}
						AttachmentBlock.stop();
					});
	    	if(file.primaryDocument)
    		{
	    		$scope.disablePrimaryDocuent = false;
	    		$scope.primaryFileName = "";
	    		$scope.primaryDocument = false;
    		}
	    	if(index > -1)
    		{
	    		$scope.files.splice(index,1);
    		}
	    }
	    $scope.saveAttachments = function(){
	    	$scope.uploadAttachmentErrors=[];
	    	var uploadedFiles=0;
	    	var notPersistedFileList="";
            var AttachmentBlock = blockUI.instances.get('AttachmentBlock');
	    	
            AttachmentBlock.start("Saving Attachments ...");
	    		angular.forEach($scope.files,function(file){
		    		if(!file.isPersisted)
	    			{
		    			notPersistedFileList += file.name + ",";
	    			}
		    		else
	    			{
		    			uploadedFiles++;
	    			}
		    	});
    		
	    	if(notPersistedFileList.length > 0)
    		{
		    	Notification.error({
		    		title	:'Error',
		    		message	: uploadedFiles + ' not uploaded. Please upload it either remove it.'
		    	});
		    	AttachmentBlock.stop();
    		}
	    	if(uploadedFiles > 0 && uploadedFiles == $scope.files.length)
	    	{
	    		$scope.attachmentDetails.files = $scope.files; 
		    	$scope.attachmentDetails.id= $stateParams.id;
		    	$http.post("rest/layout-component/upload-attachments-component/saveAttachments", $scope.attachmentDetails)
					.success(function(response){
						if(response.status === "SUCCESS"){
							Notification.success({
								title 	:'Success',
								message	:'Attachments saved successfully.'});
							window.location.reload();
							
						}else{
							Notification.error({
								title	:'Error',
								message	:'Error occured while saving Attachmetns!!'
									
							});
							
							$scope.errors = response.errors;
						}
						AttachmentBlock.stop();
					})
					.error(function(){
						$scope.errors = [{"ERROR" : "Unexpected error occured !!!"}];
						AttachmentBlock.stop();
							});
	    	}	
	    	else{
	    		Notification.error({
					title	:'Error',
					message	:'No files uploaded!!'
						
				});
	    		AttachmentBlock.stop();
	    	}
	    };
	    $scope.uploadAttachments = function(file){
            file.upload = Upload.upload({
                url: 'rest/layout-component/upload-attachments-component/upload',
                data: {file: file}
            })
            return file.upload;
		};
		 $scope.uploadPrimary = function (file){
            file.upload = Upload.upload({
                url: 'rest/layout-component/upload-attachments-component/uploadPrimary',
                data: 	{file: file}
            });
            return file.upload;
		};
		/** OVERRIDE */
		$scope.init = function(){
			$scope.allowedSize = $scope.section.componentMetadata.configurations.allowedSize;
			$scope.allowedExt = $scope.section.componentMetadata.configurations.allowedExt;
			$scope.allowedExtForPrime = $scope.section.componentMetadata.configurations.allowedExtForPrime;
			$scope.allowAttachPrime = $scope.section.componentMetadata.configurations.allowAttachPrime;
			console.log( $scope.section.title +" UploadAttachmentsComponentController Initializing...");
		
		};
		$scope.init();
	}
]);