'use strict';

App.controller('CostAllocationComponentController',
		[	'$scope','$log','$http','$rootScope','blockUI','$stateParams','ModalService','$filter','Notification',
			function($scope , $log , $http ,$rootScope, blockUI , $stateParams,ModalService,$filter,Notification){
			
	/**
	 * GLOBAL VARIABLES
	 * ------------------
	 * @section						: component section details
	 * @sectionIndex				: position of Component in layout
	 * @model						: data model for SObject Types
	 * 
	 * INSTANCE METHODS
	 * ----------------
	 * @doValidate					: validation phase [ NOTE : Must be Defined ]
	 * 
	 */
		    $scope.configurations	= $scope.section.componentMetadata.configurations;
		    var errorMessages = [];
			var currencyFilter = $filter('currencyFilter');
		    $scope.loadInvoiceLineItems = function(){
			var componentBlock = blockUI.instances.get("componentBlock");
			componentBlock.start('Loading invoice line items...');
			$scope.jsonNode = {};
			$scope.jsonNode = {
					invoiceId :$stateParams.id,
					sectionId :$scope.section.id,
					
			};
            $scope.invoiceAmount = 0.0;
			$http.post("rest/layout-component/cost-allocation-component/invoicelineitems",$scope.jsonNode)
				.success(function(response){
					if(response.status == "SUCCESS"){
						$scope.dataModelList = response.data.dataModelList;
						if(response.data.invoiceAmount)
						{
							  $scope.invoiceAmount = parseFloat(parseFloat(response.data.invoiceAmount).toFixed(3));
						}
						  if($scope.getTotal() <  $scope.invoiceAmount){
								$scope.addItems();
						  }
					}else{
						Notification.error({
							message	:response.error[0].message,
							title	:'Error'
						});
					}
					componentBlock.stop();
				})
				.error(function(response){
					console.log(response);
					componentBlock.stop();
					Notification.error({
						message	:'Error Occured in loading invoice line items !!',
						title	:'Error'
					});					
				});
		}
		$scope.getTotal = function(){			// getTotal()
			var invoiceLineItemTotalAmount 	= 0;
			angular.forEach($scope.dataModelList , function(model) {
//				itemsTotalAmount += parseFloat(item.amount);
				if(model.isRemoved == 'false')
				   invoiceLineItemTotalAmount += parseFloat(model[$scope.configurations.invoiceLineItemAmountApiName]);
				
			});
//			return isNaN(itemsTotalAmount) ? 0 : currencyFilter(itemsTotalAmount);
			return isNaN(invoiceLineItemTotalAmount) ? 0 : (isNaN(currencyFilter(invoiceLineItemTotalAmount)) ? 0 : currencyFilter(invoiceLineItemTotalAmount));
		};
		$scope.addItems = function(){	// addItem()
		  if($scope.getTotal() <  $scope.invoiceAmount){
			   $scope.newfields = {};
			
			angular.forEach($scope.configurations.invoiceLineItemFields,function(sObject){
				$scope.newfields[sObject.name] = '';
			});
			$scope.newfields[$scope.configurations.invoiceLineItemAmountApiName] = $scope.invoiceAmount - $scope.getTotal();
			$scope.newfields["isRemovable"] = 'true';
			$scope.newfields["isPersisted"] = 'false';
			$scope.newfields["isRemoved"] = 'false';
		    console.log($scope.newfields);
			$scope.dataModelList.push($scope.newfields);
		  }
		  else{
			  Notification.error({
					message	:"You are not allowed to add more cost allocation with total amout more than  " + currencyFilter($scope.invoiceAmount),
					title	:'Error'
				});		
				return false;
		  }
		};
		$scope.isValid = function () {
			errorMessages = [];
			if($scope.getTotal() <= 0 || $scope.getTotal() !=  $scope.invoiceAmount){
				errorMessages.push("Cost allocation total amount must be " + currencyFilter($scope.invoiceAmount));
			}
			angular.forEach($scope.dataModelList,function(model){
				angular.forEach($scope.configurations.invoiceLineItemFields,function(sObject){
					if(sObject.required && (model[sObject.name] == null || model[sObject.name] == '' )){
						errorMessages.push("Please provide " + sObject.label );
					}
				});
			
			});
			
			if(errorMessages.length == 0)
				return true;
			else
			    return false;
			
		}
		/** OVERRIDE */
		$scope.isComponentValid = function(){
			return $scope.isValid();
		}
		// FUNCTION :: SaveInvoiceLineItemDetails()
		$scope.save = function(){
			  if($scope.isValid()){
			     var componentBlock = blockUI.instances.get("componentBlock");
			     componentBlock.start('Loading invoice line items...');
			     $scope.jsonNode.dataModelList = 	$scope.dataModelList;
			     $http.post("rest/layout-component/cost-allocation-component/saveInvoiceLineItem",$scope.jsonNode)
			     .success(function(response){
			       if(response.status === "SUCCESS"){
			    	   Notification.success({
							title 	:'Success',
							message	:'Invoice Line Item saved successfully.'});
			    	   $scope.init();
			       }
			       else{
			    	   Notification.error({
							message	:'Error Occured in saving invoice line items !!',
							title	:'Error'
						});		
			       }
				   componentBlock.stop();
			    })
			    .error(function(response){
					componentBlock.stop();
					Notification.error({
						message	:'Error Occured in saving invoice line items !!',
						title	:'Error'
					});		
			    });
			  }
			  else
			  {
					var errorMessagesStr = "<ul>";
					angular.forEach(errorMessages, function(error) {
						errorMessagesStr += "<li>" + error + "</li>";
					});
					errorMessagesStr += "</ul>";
					Notification.error({
						message	: errorMessagesStr,
						title	:'Errors'
					});
					return false;
				}
		};
		
		/** OVERRIDE */
		$scope.init = function(){
		    $scope.loadInvoiceLineItems();
		};
		$scope.init();
	}
]);