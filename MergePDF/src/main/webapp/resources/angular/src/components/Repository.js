'use strict';

AdminApp.service('ComponentRepository', function(){
	
		var componentType = {
			public 		: "PUBLIC",
			private		: "PRIVATE",
			dashboard	: "DASHBOARD"
		}
		this.componentType = componentType; 
	
		// COMPONENTS ---

		this.componentList = [
              /** SOBJECT LIST COMPONENT  ***/
              {
            	  type				:componentType.public,
            	  sObjectApiName	:null,
            	  name				:"SObjectListComponent",
            	  title				:"SObject List Component",
            	  directive			:{
            		  templateUrl		:"layout-component/sobject-list-component",
            		  controller		:"SObjectListComponentController",
            		  settings			:{
                		  templateUrl		: "layout-component/settings/sobject-list-component-settings",
                		  controller		: "SObjectListComponentSettingsController"
                	  }
            	  },
              },
		     /** COST ALLOCATION COMPONENT  ***/
//             {
//            	 type				:componentType.private,
//            	 sObjectApiName		:"Akritiv_ESM__Invoice__c",
//            	 
//            	 name				:"CostAllocationComponent",
//            	 title				:"Cost Allocation Component",
//            	 directive			:{
//            		 templateUrl		: "layout-component/cost-allocation-component",
//                	 controller			: "CostAllocationComponentController",
//                	 settings			:{
//                		 	templateUrl		: "layout-component/settings/cost-allocation-component-settings",
//                		 	controller		: "CostAllocationComponentSettingsController",
//               	  		}
//            	 }
//             },
             /** RECORD HISTORY COMPONENT  ***/
//             {
//           	  type				:componentType.public,
//           	  sObjectApiName	:null,
//           	  name				:"RecordHistoryComponent",
//           	  title				:"Record History Component",
//           	  directive			:{
//           		  templateUrl		: "layout-component/record-history-component",
//           		  controller		: "RecordHistoryComponentController",
//           		  settings			:{
//              		  templateUrl		: "layout-component/settings/record-history-component-settings",
//              		  controller		: "RecordHistoryComponentSettingsController"
//              	  }
//           	  },
//           
//             },
             /** NOTES AND ATTACHMENTS COMPONENT  ***/
             {
           	  type				:componentType.public,
           	  sObjectApiName	:null,
           	  name				:"NotesAndAttachmentsComponent",
           	  title				:"Notes And Attachments Component",
           	  directive			:{
           		  templateUrl		: "layout-component/notes-and-attachments-component",
           		  controller		: "NotesAndAttachmentsComponentController"	
           	  },
           
             },
             /** UPLOAD ATTACHMENTS COMPONENT  ***/
//             {
//           	  type				:componentType.public,
//           	  sObjectApiName	:null,
//           	  name				:"UploadAttachmentsComponent",
//           	  title				:"Upload Attachments Component",
//           	  directive			:{
//           		  templateUrl		: "layout-component/upload-attachments-component",
//           		  controller		: "UploadAttachmentsComponentController",
//           		  settings			:{
//            		  templateUrl		: "layout-component/settings/upload-attachments-component-settings",
//            		  controller		: "UploadAttachmentsComponentSettingsController"
//            	  }
//           	  },
//           
//             },
             /** DOWNLOAD PRIMARY DOCUMENT COMPONENT  ***/
//             {
//           	  type				:componentType.public,
//           	  sObjectApiName	:null,
//           	  name				:"DownlaodPrimaryDocumentComponent",
////           	  title				:"Download Priamry Document Component",
//           	  title				:"Priamry Document Component",
//           	  directive			:{
//           		  templateUrl		: "layout-component/download-primary-document-component",
//           		  controller		: "DownloadPrimaryDocumentComponentController",
//           		  settings			:{
//              		  templateUrl		: "layout-component/settings/download-primary-document-component-settings",
//              		  controller		: "DownloadPrimaryDocumentComponentSettingsController"
//              	  }
//           	  },
//           
//             },
             
             /** MULTIPLE APPROVER COMPONENT  ***/
//             {
//            	 type				:componentType.public,
//            	 sObjectApiName		:"History__c",
//            	 name				:"MultipleApproverComponent",
//            	 title				:"Multiple Approver Component",
//            	 directive			:{
//            		 templateUrl		: "layout-component/multiple-approver-component",
//            		 controller			: "MultipleApproverComponentController"
//            	 }
//             },
         ];
		
});