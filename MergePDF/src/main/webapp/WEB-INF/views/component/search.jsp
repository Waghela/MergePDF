<%@ include file="../../../taglibs.jsp" %>

<div class="accordion" id="{{ 'accordionSearchComponent_'+ componentId}}">
	<div class="accordion-group">
		<div class="accordion-heading" style="background: #1294D5;">
			<a 	class			="accordion-toggle" 
				data-toggle		="collapse"
				data-parent		="{{ '#accordionSearchComponent_'+ componentId}}" 
				href			="{{ '#collapseSearchComponent_'+ componentId}}"
				style="color: whitesmoke;">
				{{ model.title }}
			</a>
		</div>
		<div 	block-ui		="{{ componentId }}" 
				id				="{{ 'collapseSearchComponent_'+ componentId}}" 
				class			="accordion-body collapse in" >
			<div class="accordion-inner" style="padding: 17px 0px 0px 0px;" >
				
				<div class="form-horizontal">
					<div class="layout-section">
						<form ng-submit="executeSearchAction()" style="margin: 0">
							<div class="layout-section-content layout-section-column{{2}}" style="border-bottom : 0px;">
								<div class="layout-section-field" style="border-bottom : 0px;" ng-repeat-start="field in model.fields track by $index">
									<div class="control-label">{{ field.label }}</div>
									<div class="field-control">
										<div ng-if="field.type == 'datetime'" class="input-append" style="margin-bottom: 0;">
											<input 	id 				= "{{field.id}}"
													ui-date-format	= "MMM d, y hh:mm:ss a"
													ng-model		="field.value"
													type			="text" 
												   	class			="span2 m-wrap" 
												   	style			="margin-bottom: 0;" 
												   	ng-disabled		="true" />
										   	<button type			="button" 
													class			="btn"
													ng-click		="showDateTimePicker(field, true)">
												<i class="icon-calendar"></i>
											</button>
										</div>
										<div ng-if="field.type == 'date'" class="input-append" style="margin-bottom: 0;">
											<input 	id 				= "{{field.id}}"
													ui-date-format	="MMM d, y"
													ng-model		="field.value"
													type			="text" 
												   	class			="span2 m-wrap" 
												   	style			="margin-bottom: 0;" 
												   	ng-disabled		="true" />
										   	<button type			="button" 
													class			="btn"
													ng-click		="showDateTimePicker(field, false)">
												<i class="icon-calendar"></i>
											</button>
										</div>
										<input ng-if="field.type == 'string'" type="text" class="span3" id="{{ field.id }}" ng-model="field.value">
										<select ng-if="field.type == 'picklist'" ng-model="field.value" id="{{ field.id }}" >
											<option value="">--All--</option>
											<option value="{{item}}" ng-selected="field.value==item" ng-repeat = "item in field.picklistValues">{{item}}</option>
										</select>
										<input ng-if="field.type == 'currency'" id="{{ field.id }}" type="number" string-to-number ng-model="field.value" style="margin-bottom: 0px;">
									</div>
									
								</div>
								<div ng-repeat-end class="layout-section-field-hr-{{$even}}" style="border-bottom:0px"></div>
							</div>
							<button type="submit" style="display: none;"></button>
						</form>
					</div>
					<div class="form-actions search-form-actions">
						<button type		="button" 
								ng-click	="executeSearchAction()"
								class		="btn btn-primary" >
							<i class="icon-search">&nbsp;&nbsp;Search</i>
						</button>
						<button  
								ng-click	="reset()"
								class		="btn">
							<i class="icon-refresh">&nbsp;&nbsp;Reset</i>
						</button>
						<button type		="button"
								class		="btn"
								data-toggle	="collapse"
								data-parent	="{{ '#accordionSearchComponent_'+ componentId}}"
								href		="{{ '#collapseSearchComponent_'+ componentId}}">
							<i class="icon-chevron-up">&nbsp;&nbsp;Collapse</i>
						</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
