<%@ include file="../../../taglibs.jsp" %>

<div ng-switch on="mode">
	<div ng-switch-when="DETAIL">
		<div ng-switch on ="field.type">
			<div ng-switch-when = "reference">
				{{ $root.showValue(model[field.name+'__Name']) }}
			</div>
			<div ng-switch-when="double">
				{{ $root.showNumberValue(model[field.name]) }}
			</div>
			<div ng-switch-when="datetime">
				{{ $root.showDateValue(model[field.name]) }}
			</div>
			<div ng-switch-when="date">
				{{ $root.showDateValue(model[field.name],'--','MMM d, y') }}
			</div>
			<div ng-switch-default style="white-space: pre-line;">{{ $root.showValue(model[field.name]) }}</div>
		</div>
	</div>
	<div ng-switch-when="EDIT">
		<div ng-switch on="field.type">
			<div ng-switch-when="boolean"> 				<!-- CHECKBOX -->
				<input id="{{ field.id }}" type="checkbox" ng-model="model[field.name]"> 
			</div>
			<div ng-switch-when="double"> 				<!-- NUMBER TEXTBOX -->
				<input id="{{ field.id }}" ng-required="field.required" type="number" min="0" string-to-number ng-model="model[field.name]" style="margin-bottom: 0px;"> 
			</div>
			<div ng-switch-when="currency"> 			<!-- CURRENCY NUMBER TEXTBOX -->
				<input id="{{ field.id }}" ng-required="field.required" type="number" string-to-number ng-model="model[field.name]" style="margin-bottom: 0px;">
			</div>
			<div ng-switch-when="string">				<!-- TEXTBOX -->
				<input id="{{ field.id }}" name="{{field.name}}" ng-required="field.required" type="text" ng-model="model[field.name]" style="margin-bottom: 0px;">
			</div>
			<div ng-switch-when="email">				<!-- EMAIL TEXTBOX -->
				<input id="{{ field.id }}" ng-required="field.required" type="email" ng-model="model[field.name]" style="margin-bottom: 0px;">
			</div>
			<div ng-switch-when="textarea">				<!-- TEXTAREA -->
				<textarea id="{{ field.id }}" ng-required="field.required" rows="4" class="span3" ng-model="model[field.name]" style="resize:none;margin-bottom: 0px;"></textarea>
			</div>
			<div ng-switch-when="reference">			<!-- LOOKUP -->
				<div class="input-append" style="margin-bottom: 0;" ng-show	="field.custom">
					<input 	type 			= "hidden"  
							ng-model 		= "model[field.name]" />
					<input 	ng-required		="field.required"
							type			="text" 
						   	class			="span2 m-wrap" 
						   	style			="margin-bottom: 0;" 
						   	ng-disabled		="true" 
						   	ng-model		="model[field.name+'__Name']" />
				   	<button type			="button" 
							ng-click		="showLookupModal()" 
							class			="btn" 
							title			="{{ field.label }} Lookup">
						<i class="icon-search"></i>
					</button>
				</div>
				<div ng-show ="!field.custom">
					{{ $root.showValue(model[field.name+'__Name']) }}
				</div>
			</div>
			<div ng-switch-when="datetime">				<!-- DATE & TIME PICKER -->
				<div class="input-append" style="margin-bottom: 0;">
					<input 	id 				= "{{field.id}}"
							ui-date-format	= "MMM d, y hh:mm:ss a"
							ng-model		="model[field.name]"
							ng-required		="field.required"
							type			="text" 
						   	class			="span2 m-wrap" 
						   	style			="margin-bottom: 0;" 
						   	ng-disabled		="true" />
				   	<button type			="button" 
							class			="btn"
							ng-click		="showDateTimePicker(true)">
						<i class="icon-calendar"></i>
					</button>
				</div>
			</div>
			<div ng-switch-when="date">					<!-- DATE PICKER -->
				<div class="input-append" style="margin-bottom: 0;">
					<input 	id 				= "{{field.id}}"
							ui-date-format	="MMM d, y"
							ng-model		="model[field.name]"
							ng-required		="field.required"
							type			="text" 
						   	class			="span2 m-wrap" 
						   	style			="margin-bottom: 0;" 
						   	ng-disabled		="true" />
				   	<button type			="button" 
							class			="btn"
							ng-click		="showDateTimePicker(false)">
						<i class="icon-calendar"></i>
					</button>
				</div>
			</div>
			<div ng-switch-when="picklist">				<!-- COMPOBOX / SELECT -->
				<select id="{{ field.id }}" ng-required="field.required" ng-model="model[field.name]" class="span3" style="margin-bottom: 0px;" ng-options="value for value in picklistvalues">
					<option value="" ng-disabled="field.required">--None--</option>
				</select>
			</div>
			<div ng-switch-when="multipicklist" ng-init="multipicklistModel = (model[field.name]) ? model[field.name].split(';') : []">		<!-- COMPOBOX / MULTI SELECT -->
				<select id="{{ field.id }}" ng-required="field.required" ng-model="multipicklistModel" class="span3" style="margin-bottom: 0px;" ng-options="value for value in picklistvalues" multiple="multiple" ng-change="model[field.name] = multipicklistModel.join('; ')">
				</select>
			</div>
		</div>
	</div>
	<div ng-switch-when="MANAGE">
		{{ 'Sample ' + field.label }}
		<div class="draggable-actions">
			<input type="checkbox" ng-model="field.required" ng-if="section.type=='EDIT'" title="Turn {{ field.required ? 'off' : 'on' }} required">
			<a ng-click="ctrlScope.openSectionFieldSettingsModal(section,field)" ng-if="section.type=='EDIT' && (field.type=='picklist' || field.type=='multipicklist')" title="{{ field.label }} Settings"><i class="icon-wrench"></i></a>
			<a ng-click="removeField()" title="Remove {{ field.label }} field"><i class="icon-remove"></i></a>
		</div>
	</div>
	<div ng-switch-default></div>
</div>

