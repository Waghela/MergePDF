<%@ include file="../../../taglibs.jsp" %>
<div class="layout-section" ng-if="rendered"> 
	<div class="layout-section-title">{{ section.title }} 
		<i ng-if="(!isAllSectionFieldsValid() || !isComponentValid()) && section.type == 'EDIT'" class="icon-exclamation-sign icon-color-red"></i>
		<div ng-if="sectionIndex == 0 && section.type == 'EDIT'" class="draggable-actions">
			<div class="required-fields-label">= Required information</div>
		</div>
	</div>
	<div ng-if="!section.component" class="layout-section-content layout-section-column{{ section.columns }}">
		<div class="layout-section-field" ng-repeat-start="field in section.sectionFieldsJsonArray">
			<div class="field-label">{{ field.label }}</div>
			<div class="field-control">
				<layout-section-component-field 
					section		="section"
					field		="field"
					model		="model"
					mode		="section.type">
				</layout-section-component-field>
			</div>
		</div>
		<div ng-repeat-end class="layout-section-field-hr-{{$even}}"></div>
	</div>
	<div ng-if		="section.component && section.componentMetadata.templateUrl" 
		 ng-include	="section.componentMetadata.templateUrl"
		 class		="layout-section-content"></div>
	<div ng-if		="section.component && !section.componentMetadata.templateUrl"
		 class		="layout-section-content">
		<div class="alert alert-block alert-danger">
			<h4>Developer Error!</h4>
			TemplateUrl is not defined !
		</div>
	</div>
</div>