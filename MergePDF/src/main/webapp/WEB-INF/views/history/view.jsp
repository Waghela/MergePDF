<%@ include file="../../../taglibs.jsp"%>
<div block-ui="historyDetailBlock">
	<table style="margin-bottom: 0;"
		class="table table-spriped table-hover table-condensed details-table ">
		<tbody>
			<tr>
				<th>{{ model.title }}</th>
			</tr>
		</tbody>
	</table>
	<table class="table table-spriped table-condensed">
		<thead>
			<tr>
				<th>Date</th>
				<th>User</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="history in model.data">
				<td>{{ $root.showDateValue(history.createdDate,'') }}</td>
				<td>{{ history.createdBy }}</td>
				<td>
				  <span	ng-show="history.fieldName != null && history.oldValue != null">
				       Changed <b>{{history.fieldName }}</b> From {{ history.oldValue }} To <b>{{history.newValue }}</b>.
				  </span>
				  <span ng-show="history.fieldName != null && history.oldValue == null">
				       Insert  <b>{{ history.fieldName }}</b> To <b>{{ history.newValue }}</b>.
				  </span>
				  <span ng-show="history.fieldName == null">
				       New Record Inserted.
				  </span>
				</td>
			</tr>
		</tbody>
	</table>
</div>