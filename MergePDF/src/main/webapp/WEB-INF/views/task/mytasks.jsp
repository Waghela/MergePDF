<%@ include file="../../../taglibs.jsp" %>
<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header">
<!-- 				<i class="icon-check"></i> -->
				<h3>Views</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
				<div class="control-group">
					<div class="controls">
						<div class="accordion" id="accordion2">
							<c:forEach items="${sections}" var="section" varStatus="loop">
								<c:set var="tableFields" value="${section.tableFields() }"></c:set>
								<c:set var="hrefFields" value="${section.hrefFields() }"></c:set>
								<c:choose>
									<c:when test="${loop.index == 0 }">
										<c:set var="accordionBodyClass" value="accordion-body collapse in"></c:set>
									</c:when>
									<c:otherwise>
										<c:set var="accordionBodyClass" value="accordion-body collapse"></c:set>
									</c:otherwise>
								</c:choose>
								
								<div 	class	="accordion-group" 
										ng-init	="loadData('section_${section.id }_records',${section.id },'myTaskSection_${section.id }_Block','${section.title }')">
									<div class="accordion-heading">
										<a 	class		="accordion-toggle" 
											data-toggle	="collapse"
											data-parent	="#accordion2" 
											href		="#collapseSection_${section.id }">
											${section.title } [ Total : {{ (section_${section.id }_records) ? section_${section.id }_records.length : 0 }} ]
										</a>
									</div>
									<div 	block-ui	="myTaskSection_${section.id }_Block" 
											id			="collapseSection_${section.id }" 
											class		="${accordionBodyClass}" >
<!-- 											class		="accordion-body collapse in" > -->
										<div class="accordion-inner datatable-mytask-accordion-inner" >
											<button type	="button" 
													class	="btn btn-small btn-primary" 
													ng-click="loadData('section_${section.id }_records',${section.id },'myTaskSection_${section.id }_Block','${section.title }')">
												<i 	class	="icon-small icon-refresh">&nbsp;&nbsp;Refresh</i>
											</button>
											<table datatable="ng" class="table table-spriped table-hover table-condensed" >
												<thead>
											        <tr class="list-table">
											        	<c:forEach items="${tableFields }" var="field">
											        		<th>${field.value }</th>
											        	</c:forEach>
											        </tr>
										        </thead>
										        <tbody>
<%-- 										        	<tr ng-show="!section_${section.id }_records || section_${section.id }_records.length === 0 "> --%>
<%-- 										        		<th colspan="${tableFields.size()}"><center style="color:red;">No Records found.</center></th> --%>
<!-- 										        	</tr> -->
											        <tr	ng-show		="section_${section.id }_records.length > 0 "
											        	ng-repeat	="record in section_${section.id }_records">
											        	<c:forEach items="${tableFields }" var="field">
											        		<c:choose>
											        			<c:when test="${hrefFields.containsKey(field.key)}">
											        				<td>${section.createAngularJsHrefField('record',field.key,hrefFields.get(field.key))}</td>
												        		</c:when>
												        		<c:otherwise>
												        			<td>{{ $root.showValue(${section.createAngularJsField('record',field.key)}) }}</td>
												        		</c:otherwise>
											        		</c:choose>
											        	</c:forEach>
											        </tr>
										        </tbody>
											</table>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
					<!-- /controls -->
				</div>
				<!-- /control-group -->
			</div>
		</div>
	</div>
</div>