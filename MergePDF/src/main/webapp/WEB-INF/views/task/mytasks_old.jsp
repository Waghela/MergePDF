<div class="row">
	<div class="span12">
		<div class="widget">
			<div class="widget-header">
				<i class="icon-check"></i>
				<h3>Items to Approve</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content">
				<div class="control-group">
					<div class="controls">
						<div class="accordion" id="accordion2">
							<jsp:include page="invoices.jsp"></jsp:include>
							<jsp:include page="exceptionQueueInvoices.jsp"></jsp:include>
							<jsp:include page="supplierMaintenance.jsp"></jsp:include>
						</div>
					</div>
					<!-- /controls -->
				</div>
				<!-- /control-group -->
			</div>
		</div>
	</div>
</div>