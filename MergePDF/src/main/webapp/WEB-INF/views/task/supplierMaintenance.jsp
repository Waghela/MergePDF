
<div class="accordion-group" >
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
			data-parent="#accordion2" href="#collapseSupplierMaintenance"> Supplier Maintenance (Total
			Records: {{ supplierMaintenanceRecords.length }}) </a>
	</div>
	<div block-ui="myTaskSupplierMaintenanceBlock" id="collapseSupplierMaintenance" class="accordion-body collapse " >
		<div class="accordion-inner mytask-accordion-inner" >
			<button type="button" class="btn btn-small btn-primary" ng-click="loadSupplierMaintenanceRecords()"><i class="icon-small icon-refresh">&nbsp;&nbsp;Refresh</i></button>
			<table class="table table-spriped table-hover table-condensed" >
				<thead>
			        <tr class="list-table">
			            <th>ESM Ref No#</th>
			            <th>Vendor Name</th>
			            <th>Current State</th>
			            <th>Address 1</th>
			            <th>Vendor Currency</th>
			            <th>Requestor Name</th>
			        </tr>
		        </thead>
		        <tbody>
		        	<tr ng-show="supplierMaintenanceRecords.length === 0 ">
		        		<th colspan="8"><center style="color:red;">No Supplier Maintenance Records found.</center></th>
		        	</tr>
			        <tr ng-show="supplierMaintenanceRecords.length > 0 " ng-repeat="record in supplierMaintenanceRecords">
			        	<td><a href = "#/supplier-maintenance/{{ record.id.idStr }}"><b>{{ record.allFields.Name }}</b></a></td>
			            <td>{{ record.allFields.Vendor_Name__c }}</td>
			            <td>{{ record.allFields.akritivesm__Current_State__c }}</td>
			            <td>{{ record.allFields.akritivesm__Address1__c }}</td>
			            <td>{{ record.allFields.Currency__c }}</td>
			            <td>{{ record.allFields.Requestor_Name__c }}</td>
			        </tr>
		        </tbody>
			</table>
		</div>
	</div>
</div>