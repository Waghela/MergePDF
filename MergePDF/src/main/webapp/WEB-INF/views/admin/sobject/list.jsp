<%@ include file="../../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-cloud"></i>
				<h3>SObjects</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="searchSObjectBlock">
			
				<div class="form-actions" style="margin-top: 0px;text-align: center;">
					<button type="button" class="btn btn-primary" ng-click="manageSObjects()"><i class="icon-cog"></i>&nbsp; Manage</button>
					<button type="button" class="btn" ng-click="loadSObjects()"><i class="icon-refresh"></i>&nbsp; Reload</button>
				</div>
			
				<div class="controls" >
				
					<div ng-show = "hasCachedData" class="alert">
						Showing cached data, Please <strong>Reload</strong> search to get fresh data.
					</div>
	
					<table datatable="ng" class="table table-spriped table-hover table-condensed">
						<thead>
							<tr class="list-table">
					            <th>Label</th>
					            <th>Plural Label</th>
					            <th>Name</th>
					            <th style="width: 50px;text-align: center;">Custom</th>
					            <th style="width: 50px;text-align: center;">Active</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="sObj in sObjects">
								<td><b>{{ sObj.label }}</b></td>
					            <td>{{ sObj.labelPlural }}</td>
					            <td>{{ sObj.name }}</td>
					            <td style="text-align: center;"><i class="icon-large {{ $root.showCheckBoxValue(sObj.custom) }}"></i></td>
					            <td style="text-align: center;"><i class="icon-large {{ $root.showCheckBoxValue(sObj.active) }}"></i></td>
							</tr>
						</tbody>
					</table>
					
				</div>
				<div class="form-actions" style="margin-top : 45px;margin-bottom:0px;text-align: center;">
					<button type="button" class="btn btn-primary" ng-click="manageSObjects()"><i class="icon-cog"></i>&nbsp; Manage</button>
					<button type="button" class="btn" ng-click="loadSObjects()"><i class="icon-refresh"></i>&nbsp; Reload</button>
				</div>
			</div>
			<!-- /widget-content -->
		</div>
	</div>
</div>