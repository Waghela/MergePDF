<%@ include file="../../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>Edit Layout <text ng-if="layoutDetails.title">{{ '[ ' + layoutDetails.title + ' ]' }}</text></h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="sObjectLayoutsEditBlock">
				<div class="form-actions" style="margin-top: 0px;text-align: center;">
					<button type="button" class="btn btn-success" ng-click="saveLayout()"><i class="icon-save"></i>&nbsp; Save</button>
					<button type="button" class="btn" ng-click="loadSObjectLayoutDetails()"><i class="icon-refresh"></i>&nbsp; Reload</button>
					<button type="button" class="btn" ng-click="backToList()"><i class="icon-circle-arrow-left"></i>&nbsp; Back to list</button>
				</div>
				<div class="controls">
				 
				 	<div class="sticky-section-container" >
						<div class="layout-section-sticky" sticky>
							<div class="layout-section-title" >{{ layoutDetails.title + ' Layout' }}</div>
							<div class="layout-section-content draggable-elements-container">
								<span 	class				="draggable-element draggable-element-section" 
										dnd-draggable		="untitledSection" 
										dnd-effect-allowed	="copy" 
										draggable			="true"
										dnd-type			="'sectionType'"
										>Section</span>
									<!-- dnd-copied			="dndSection()"										 -->
									
								<span 	class				="draggable-element draggable-element-component"
										ng-repeat			="component in componentList" 
										dnd-draggable		="component" 
										dnd-effect-allowed	="copy" 
										draggable			="true"
										dnd-type			="'componentType'"
										>{{ component.title }}</span>
									
								<span 	class				="draggable-element draggable-element-field" 
										ng-repeat			="field in sObject.fields"
										draggable			="true"
										dnd-draggable		="field"
										dnd-effect-allowed	="copy" 
										dnd-type			="'fieldType'"
										dnd-copied			="dndField(field)">{{field.label}}</span>
							</div>
						</div>
					</div>
					
<!-- 					<div 	class				="layout-section"> -->
<!-- 						<pre> -->
<!-- 							{{ sectionsAsJson }} -->
<!-- 						</pre> -->
<!-- 					</div>  -->
					
					<div 	class					="layout-section-container" 
							dnd-list				="sections" 
							dnd-allowed-types		="['sectionType','componentType']" >
							
						<div 	ng-class			="{'layout-section': true,'custom-component': section.component}" 
								ng-repeat 			="section in sections | filter : { deleted : false }"
								dnd-draggable		="section"
								dnd-effect-allowed	="move"
								dnd-type			="'sectionType'"
								dnd-moved			="sections.splice($index, 1)">
								
							<div class="layout-section-title">
								{{ section.title }} 
								<i class="icon-eye-open" ng-class="{'icon-color-green': !section.component,'icon-color-white': section.component}" ng-show="section.active"></i>
								<i class="icon-edit" 	 ng-class="{'icon-color-green': !section.component,'icon-color-white': section.component}" ng-show="section.type == 'EDIT'"></i>
								<div class="draggable-actions">
									<a ng-click="openComponentSettingsModal(section,$index)" title="Configuration Settings" ng-if="section.componentMetadata.settings && section.component"><i class="icon-cog"></i></a>
									<a ng-click="openSectionCriteriaModal(section,$index)" title="Criteria"><i class="icon-cogs"></i></a>
									<a ng-click="openSectionSettingsModal(section,$index)" title="Settings"><i class="icon-wrench"></i></a>
									<a ng-click="deleteSection(section,$index)" title="{{ (section.component) ? 'Remove Component' : 'Remove Section' }}"><i class="icon-remove"></i></a>
								</div>
							</div>
							<div ng-if="section.component" class="layout-section-content layout-section-column1">
								<center>{{ section.title + ' contents' }}</center>
							</div>
							<div 	ng-if				="!section.component"
									class				="layout-section-content layout-section-column{{ (section.columns) ? section.columns : 1 }}"
									dnd-list			="section.sectionFieldsJsonArray"
									dnd-allowed-types	="['fieldType']">
								<div 	class				="layout-section-field" 
										ng-repeat-start		="field in section.sectionFieldsJsonArray"
										dnd-draggable		="field"
										dnd-effect-allowed	="move"
										dnd-horizontal-list	="{{section.columns == 2}}"
										dnd-type			="'fieldType'"
										dnd-moved			="section.sectionFieldsJsonArray.splice($index,1)">
									<div class="field-label">{{ field.label }}</div>
									<div class="field-control">
										<layout-section-component-field 
											section		="section"
											field		="field" 
											mode		="'MANAGE'" 
											ctrl-scope	="ctrlScope"
											remove-action="section.sectionFieldsJsonArray.splice($index,1)">
										</layout-section-component-field>
									</div>
								</div>
								<div ng-repeat-end class="layout-section-field-hr-{{$even}}"></div>
							</div>
						</div>
					</div>
					
					<div class="layout-section standard-component" ng-if="layoutDetails.actionComponentSettings"> 
						<div class="layout-section-title">
							{{ layoutDetails.actionComponentSettings.section.title }} 
							<small style="font-size: smaller;font-style: italic;">[ {{ layoutDetails.actionComponentSettings.name }} ]</small>
							<div class="draggable-actions">
								<a ng-click="openActionComponentSettingsModal(layoutDetails.actionComponentSettings)" title="{{ layoutDetails.actionComponentSettings.lookup.title }}"><i class="icon-cog"></i></a>
							</div>
						</div>
						<div class="layout-section-content layout-section-column1">
							<center>{{ layoutDetails.actionComponentSettings.name + ' contents' }}</center>
						</div>
					</div>
					
				</div>
				<div class="form-actions" style="margin-bottom:0px; text-align: center;">
<!-- 					<button type="button" class="btn btn-success" ng-click="log(sections)"><i class="icon-save"></i>&nbsp; Log</button> -->
					<button type="button" class="btn btn-success" ng-click="saveLayout()"><i class="icon-save"></i>&nbsp; Save</button>
					<button type="button" class="btn" ng-click="loadSObjectLayoutDetails()"><i class="icon-refresh"></i>&nbsp; Reload</button>
					<button type="button" class="btn" ng-click="backToList()"><i class="icon-circle-arrow-left"></i>&nbsp; Back to list</button>
				</div>
			</div>
		</div>
	</div>
</div>