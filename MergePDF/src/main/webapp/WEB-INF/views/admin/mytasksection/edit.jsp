<%@ include file="../../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-tasks"></i>
				<h3><text ng-if="!title">MyTask Section</text>{{ title }}</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="myTaskSectionBlock">
				<div class="form-actions" style="margin-top: 0px;text-align: center;">
					<button type="button" class="btn btn-danger" ng-click="deleteSection(sectionModel)" ng-show="enableDelete"><i class="icon-remove"></i>&nbsp; Delete</button>
					<button type="button" class="btn btn-success" ng-click="save(selectedSObject)"><i class="icon-save"></i>&nbsp; Save</button>
					<button type="button" class="btn" ng-click="backToList()"><i class="icon-circle-arrow-left"></i>&nbsp; Back to list</button>
				</div>
				<div class="controls">
					<Table class="table table-spriped table-hover table-condensed">
						<thead>
							<tr>
								<th colspan="8" style="line-height: 25px;">Section Details</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="8">
									<form class="form-horizontal" style="margin-bottom: 0px;">
										<fieldset>
											<div class="control-group" style="margin-top: 18px;">
												<label class="control-label form-label">Title</label>
												<div class="controls">
													<input type="text" class="span6" required="required" placeholder="Title" ng-model="sectionModel.title">
													<p class="help-block">Section Title on collapsible panel.</p>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label form-label">SObject</label>
												<div class="controls">
													<select ng-model="selectedSObject" required="required" class="span6" ng-options="sObj.label for sObj in herokuSObjects" ng-change="manageConfigureFieldsButton(selectedSObject)">
														<option value="">--None--</option>
													</select>
													<p class="help-block">Salesforce object to query data.</p>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label form-label">SOQL Where Clause</label>
												<div class="controls">
													<textarea rows="4" class="span6" required="required" style="resize:none;" ng-model="sectionModel.whereClause"></textarea>
													<p class="help-block">Where clause without <b>WHERE</b> keyword.</p>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label form-label">Sequence</label>
												<div class="controls">
													<input type="number" required="required" placeholder="Sequence" ng-model="sectionModel.sequence">
													<p class="help-block">Sequence order in My Tasks page sections.</p>
												</div>
											</div>
											<div class="control-group">
												<label class="control-label form-label">Active</label>
												<div class="controls">
													<label class="checkbox inline" style="cursor: pointer;">
														<input type="checkbox" ng-model="sectionModel.active"> Show on <b>My Tasks</b> page
													</label>
												</div>
											</div>
										</fieldset>
									</form>
								</td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th class="section-header" colspan="8" style="line-height: 25px;">Section Table Details
									<button style="float: right;" type="button" class="btn btn-mini btn-primary" ng-disabled = "disableConfigureFieldsButton" ng-click="showSObjectFieldLookup(selectedSObject)">
										<i class="icon-cog"></i>&nbsp;
										Configure SObject Fields
									</button>
								</th>
							</tr>
							<tr>
								<th style="width: 20px;">#</th>
								<th>Label</th>
								<th>Name</th>
								<th>Lookup</th>
								<th>Hyper link</th>
								<th style="width: 20px;">Hidden</th>
								<th style="width: 55px; text-align: center;">Index</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="field in sectionModel.tableFieldJsonArray">
								<td>{{ $index+1 }}</td>
								<td><b>{{ field.label }}</b></td>
								<td>{{ field.name }}</td>
								<td>
									<select ng-model="field.relativeField" ng-show="field.relationshipName" class="span3" style="margin-bottom: 0px;" ng-options="refField.value for refField in field.refFields" ng-change="setSoqlName(field,field.relativeField)" >
										<option value="">--None--</option>
									</select>
								</td>
								<td>
									<input type="text" style="margin-bottom: 0px;" ng-model="field.href">
								</td>
								<td style="text-align: center;">
									<input type="checkbox" ng-model="field.hidden">
								</td>
								<td style="text-align: center;">
									<button class="btn btn-mini" ng-click="move($index,$index-1)" ng-disabled="$index == 0"><i class="icon-chevron-up"></i></button>
									<button class="btn btn-mini" ng-click="move($index,$index+1)" ng-disabled="$index == sectionModel.tableFieldJsonArray.length-1"><i class="icon-chevron-down"></i></button>
								</td>
							</tr>
						</tbody>
					</Table>
				</div>
				<div ng-show="errors.length > 0" style="font-weight: bold;color: red;">
					<ul style="margin-bottom: 0px;">
						<li ng-repeat="error in errors" >
							{{ error }}
						</li>
					</ul>
				</div>
				<div class="form-actions" style="margin-bottom:0px; text-align: center;">
					<button type="button" class="btn btn-danger" ng-click="deleteSection(sectionModel)" ng-show="enableDelete"><i class="icon-remove"></i>&nbsp; Delete</button>
					<button type="button" class="btn btn-success" ng-click="save(selectedSObject)"><i class="icon-save"></i>&nbsp; Save</button>
					<button type="button" class="btn" ng-click="backToList()"><i class="icon-circle-arrow-left"></i>&nbsp; Back to list</button>
				</div>
			</div>
		</div>
	</div>
</div>