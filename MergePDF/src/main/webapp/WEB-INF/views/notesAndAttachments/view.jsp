<%@ include file="../../../taglibs.jsp"%>
<div block-ui="notesAndAttachmentBlock" ng-show="model.notesDetails.length > 0 || model.attachmentsDetails.length > 0">
	<table style="margin-bottom: 0;"
		class="table table-spriped table-hover table-condensed details-table">
		<tbody>
			<tr>
				<th>Notes & Attachments</th>
			</tr>
		</tbody>
	</table>
	<table class="table table-spriped table-condensed">
		<tr>
			<th>Title</th>
			<th>Size</th>
			<th>Description</th>
			<th style="text-align: center;">View</th>
		</tr>
		<tr ng-repeat="note in model.notesDetails">
			<td>{{ $root.showValue(note.allFields.Title) }}</td>
			<td>-</td>
			<td>-</td>
			<td style="text-align: center;"><button type="button"
					ng-click="showNoteModal(note.id.idStr)" class="btn btn-primary">
					<i class="icon icon-eye-open"></i>
				</button></td>
		</tr>
		<tr ng-repeat="attachment in model.attachmentsDetails">
			<td>{{ $root.showValue(attachment.allFields.Name) }}</td>
			<td>{{ $root.showValue(attachment.allFields.BodyLength/1024) |
				number : 2}} KB</td>
			<td>{{ $root.showValue(attachment.allFields.Description) }}</td>
			<td style="text-align: center;"><a class="btn btn-primary"
				href="rest/notesAndAttachments/viewAttachment?name={{attachment.allFields.Name}}&body={{attachment.allFields.Body}}">
					<i class="icon-large icon-download-alt"></i>
			</a></td>
		</tr>
	</table>
</div>