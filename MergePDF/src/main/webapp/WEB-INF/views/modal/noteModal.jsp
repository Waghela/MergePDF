<jsp:include page="../../../taglibs.jsp"></jsp:include>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" block-ui="noteModalBlock">
				<form ng-submit="loadNotes()" style="margin: 0;min-height: 200px;">
					<fieldset>
						<div class="control-group">
							<div class="controls">
								<div class="input-append">
								<textarea disabled="disabled" rows="10" style="margin-bottom: 0;resize:none;height: 100%;width:100%;">{{ $root.showValue(note.allFields.Body) }}</textarea>
								</div>
							</div>
						</div>
					
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
		        <button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</button>
		      </div>
		</div>
	</div>
</div>