<%@ include file="../../taglibs.jsp" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Authenticating...</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	</head>
	<body>
		<script src="${local_js}/jquery-1.7.2.min.js" ></script>
		<script>
			$(document).ready(function(){
				$("#submitButton").click();
			});
		</script>
		Please wait...
		<form id="loginForm" name="loginForm" action="<c:url value="/login" />" method="POST">
			<input type="hidden" id="username" name="username" value="${SSOUser.username}"/>
			<input type="hidden" id="password" name="password" value="${SSOUser.password}" />
			<button style="display: none" id="submitButton" type="submit">Sign In</button>
		</form>
	</body>
</html>
