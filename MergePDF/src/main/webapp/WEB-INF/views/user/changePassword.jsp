<jsp:include page="../../../taglibs.jsp"></jsp:include>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>Change Password</h3>
			</div>
			<div class="widget-content" block-ui="changePasswordModalBlock">

				<table style="margin-bottom: 0;"
					class="table table-spriped table-hover table-condensed details-table ">
					<tbody>

						<tr>
							<td>Old Password:</td>
							<td><input type="password" ng-model="password.oldPassword" class="required" ng-required="true"
								maxlength="255" /></td>


						</tr>
						<tr>
							<td>New Password:</td>
							<td><input type="password" ng-model="password.newPassword" class="required" ng-required="true"
								maxlength="255" /></td>


						</tr>
						<tr>
							<td>Confirm New Password:</td>
							<td><input type="password" class="required" ng-required="true"
								ng-model="password.confirmNewPassword" maxlength="255" /></td>


						</tr>
						<tr ng-show="submitChangePasswordErrors.length > 0">
							<td colspan="2"
								style="text-align: left; font-weight: bold; color: red;">
								<ul style="margin-bottom: 0px;">
									<li ng-repeat="error in submitChangePasswordErrors">{{
										error }}</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="text-align: center;">
								<div class="form-actions">
									<button type="button" ng-click="savePassword()"
										class="btn btn-primary">
										<i class="icon icon-ok"></i> Save
									</button>
									<button type="button" ng-click="canceChangePassword()" class="btn">
										<i class="icon-remove"></i> Cancel
									</button>
								</div>
							</td>
						</tr>
					</tbody>
				</table>

			</div>

		</div>
	</div>
</div>
<
