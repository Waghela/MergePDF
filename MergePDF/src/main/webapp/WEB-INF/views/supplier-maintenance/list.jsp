<%@ include file="../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-search"></i>
				<h3>Supplier Maintenance Search</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="searchSupplierBlock">
				<div class="controls" >
				
				<div class="form-actions search-form-actions ">
					<a 	href="#/supplier-maintenance/create" 
						style="background-color: #1294D5"
						class="btn btn-primary center">
							<span class="icon-plus"></span> Create Supplier Request
					</a>
				</div>
				
				<search-component 	component-id 	= "${SearchComponent.id }"
									search-action	= "searchSupplierMaintenance" >
				</search-component>
				
				<div ng-show = "hasCachedData" class="alert">
<!-- 					<strong>Warning!</strong>  -->
					Showing cached data, Please <strong>Refresh</strong> search to get fresh data.
				</div>
				
				<table datatable="ng" class="table table-spriped table-hover table-condensed">
					<thead>
						<tr class="list-table">
							<th>ESM Ref No#</th>
				            <th>Vendor Name</th>
				            <th>Current State</th>
				            <th>Address 1</th>
				            <th>Vendor Currency</th>
				            <th>Requestor Name</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="record in records">
							<td><a href = "#/supplier-maintenance/{{ record.id.idStr }}"><b>{{ record.allFields.Name }}</b></a></td>
							<td>{{ record.allFields.Vendor_Name__c }}</td>
				            <td>{{ record.allFields.akritivesm__Current_State__c }}</td>
				            <td>{{ record.allFields.akritivesm__Address1__c }}</td>
				            <td>{{ record.allFields.Currency__c }}</td>
				            <td>{{ record.allFields.Requestor_Name__c }}</td>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
			<!-- /widget-content -->
		</div>
	</div>
</div>