<%-- <jsp:include page="../../../taglibs.jsp"></jsp:include> --%>
<%@ include file="../../../taglibs.jsp" %>
<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-file"></i>
				<h3>Supplier Maintenance Details {{ '[ ' + $root.showValue(sm.allFields.Name) + ' ]' }}</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content" block-ui="supplierMaintenanceDetailsBlock">
<!-- 				SUPPLIER MAINTENANCE DETAILS... -->
				<div class="alert alert-danger" ng-show="errors">
					<strong>Error!</strong> {{ errors[0].message }}
                </div>
                <table style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " >
                	<tbody>
                		<tr>
							<th colspan="4" >Requestor Information</th>
						</tr>
						<tr>
							<td>Requestor</td><td>{{ $root.showValue(sm.relationshipSubObjects.akritivesm__Requestor__r.allFields.Name) }}</td>
							<td>Request Info</td><td>{{ $root.showValue(sm.allFields.akritivesm__Request_Info__c) }}</td>
						</tr>
						<tr>
							<td>Reason For Change</td><td>{{ $root.showValue(sm.allFields.Reason_for_change__c) }}</td>
							<td>Vendor Type</td><td>{{ $root.showValue(sm.allFields.Vendor_Type__c) }}</td>
						</tr>
						<tr>
							<td>Description</td><td>{{ $root.showValue(sm.allFields.ReasonForChangeDescription__c) }}</td>
							<td>Current State</td><td>{{ $root.showValue(sm.allFields.akritivesm__Current_State__c) }}</td>
						</tr>
						<tr>
							<td>Current Approver</td><td>{{ $root.showValue(sm.relationshipSubObjects.akritivesm__Current_Approver__r.allFields.Name) }}</td>
							<td>Owner</td><td>{{ $root.showValue(sm.relationshipSubObjects.Owner.allFields.Name) }}</td>
						</tr>
						<tr>
							<th colspan="4" >Vendor Information</th>
						</tr>
						<tr>
							<td>E-one Vendor #</td><td>{{ $root.showValue(sm.allFields.E_one_Vendor_Number__c) }}</td>
							<td>BPCS/Customer #</td><td>{{ $root.showValue(sm.allFields.BPCS_Customer__c) }}</td>
						</tr>
						<tr>
							<td>Vendor Number (S)</td><td>{{ $root.showValue(sm.allFields.Vendor_Number_S__c) }}</td>
							<td>Vendor Name</td><td>{{ $root.showValue(sm.allFields.Vendor_Name__c) }}</td>
						</tr>
						<tr>
							<td>ESM Ref No</td><td>{{ $root.showValue(sm.allFields.Name) }}</td>
							<td>Request Type</td><td>{{ $root.showValue(sm.allFields.Request_Type__c) }}</td>
						</tr>
						<tr>
							<td>Attention</td><td>{{ $root.showValue(sm.allFields.Attention__c) }}</td>
							<td>Address1</td><td>{{ $root.showValue(sm.allFields.akritivesm__Address1__c) }}</td>
						</tr>
						<tr>
							<td>City</td><td>{{ $root.showValue(sm.allFields.City__c) }}</td>
							<td>Address2</td><td>{{ $root.showValue(sm.allFields.akritivesm__Address2__c) }}</td>
						</tr>
						<tr>
							<td>State</td><td>{{ $root.showValue(sm.allFields.State__c) }}</td>
							<td>Address3</td><td>{{ $root.showValue(sm.allFields.akritivesm__Address3__c) }}</td>
						</tr>
						<tr>
							<td>Zip Code</td><td>{{ $root.showValue(sm.allFields.Zip_Code__c) }}</td>
							<td>Vendor Country</td><td>{{ $root.showValue(sm.allFields.Vendor_Country__c) }}</td>
						</tr>
						<tr>
							<td>Vendor Contact</td><td>{{ $root.showValue(sm.allFields.Vendor_Contact__c) }}</td>
							<td>Vendor Phone</td><td>{{ $root.showValue(sm.allFields.Vendor_Phone__c) }}</td>
						</tr>
						<tr>
							<td>Vendor Fax</td><td>{{ $root.showValue(sm.allFields.Vendor_Fax__c) }}</td>
							<td>Vendor email</td><td>{{ $root.showValue(sm.allFields.Vendor_email__c) }}</td>
						</tr>
						<tr>
							<td>Remmitance Email Address</td><td>{{ $root.showValue(sm.allFields.Remmitance_Email_Address__c) }}</td>
							<td></td><td></td>
						</tr>
						<tr>
							<th colspan="4" >Payment Information</th>
						</tr>
						<tr>
							<td>Tax Explaination code</td><td>{{ $root.showValue(sm.allFields.Tax_Explaination_code__c) }}</td>
							<td>Hold payment flag</td><td>{{ $root.showValue(sm.allFields.Hold_payment_flag__c) }}</td>
						</tr>
						<tr>
							<td>Payment Instrument</td><td>{{ $root.showValue(sm.allFields.Payment_Instrument__c) }}</td>
							<td>Tax ID</td><td>{{ $root.showValue(sm.allFields.Tax_ID_Encr__c) }}</td>
						</tr>
						<tr>
							<td>Payment Terms</td><td>{{ $root.showValue(sm.allFields.Payment_Terms__c) }}</td>
							<td>Payment by voucher</td><td>{{ $root.showValue(sm.allFields.Payment_by_voucher__c) }}</td>
						</tr>
						<tr>
							<td>ABA #</td><td>{{ $root.showValue(sm.allFields.ABA__c) }}</td>
							<td>Swift Code</td><td>{{ $root.showValue(sm.allFields.Swift_Code__c) }}</td>
						</tr>
						<tr>
							<td>Vendor Currency</td><td>{{ $root.showValue(sm.allFields.Currency__c) }}</td>
							<td>IBAN</td><td>{{ $root.showValue(sm.allFields.IBAN__c) }}</td>
						</tr>
						<tr>
							<th colspan="4" >Governance Information</th>
						</tr>
						<tr>
							<td>Person / Corp Code</td><td>{{ $root.showValue(sm.allFields.Person_Corp_Code__c) }}</td>
							<td>Sub Cot</td><td>{{ $root.showValue(sm.allFields.Sub_Cot__c) }}</td>
						</tr>
						<tr>
							<td>Company code</td><td>{{ $root.showValue(sm.allFields.Company_code__c) }}</td>
							<td>Special Factor Payee</td><td>{{ $root.showValue(sm.allFields.Special_Factor_Payee__c) }}</td>
						</tr>
						<tr>
							<td>Freight handling Code</td><td>{{ $root.showValue(sm.allFields.Freight_handling_Code__c) }}</td>
							<td>CostaRica</td><td><i class="icon-large {{ $root.showCheckBoxValue(sm.allFields.CostaRica__c) }}"></i></td>
						</tr>
						<tr>
							<td>Account Number</td><td>{{ $root.showValue(sm.allFields.Account_Number__c) }}</td>
							<td>1099 Reporting</td><td>{{ $root.showValue(sm.allFields.X1099_Reporting__c) }}</td>
						</tr>
						<tr>
							<td>Cate code 29</td><td>{{ $root.showValue(sm.allFields.Cate_code_29__c) }}</td>
							<td>GL Offset</td><td>{{ $root.showValue(sm.allFields.GL_Offset__c) }}</td>
						</tr>
						<tr>
							<td>Pre Note Indicator</td><td><i class="icon-large {{ $root.showCheckBoxValue(sm.allFields.Pre_Note_Indicator__c) }}"></i></td>
							<td>CAD Tax Rates</td><td>{{ $root.showValue(sm.allFields.CAD_Tax_Rates__c) }}</td>
						</tr>
						<tr>
							<th colspan="4" >Closure Fields</th>
						</tr>
						<tr>
							<td>Comment History</td><td>{{ $root.showValue(sm.allFields.akritivesm__Comment_History__c) }}</td>
							<td>TIN Mapping Completed</td><td><i class="icon-large {{ $root.showCheckBoxValue(sm.allFields.TIN_Mapping_Completed__c) }}"></i></td>
						</tr>
						<tr ng-show="isEnableEdit && !errors">
							<td>Comments</td>
							<td class="esm-input">
								<textarea rows="3" style="resize:none;" ng-model = "sm.New_Comments__c"></textarea>
							</td>
							<td>User Action</td>
							<td class="esm-input">
								<select id="userAction" ng-model="sm.useraction" ng-change="showDependant(sm.useraction)">
									<option value="">--None--</option>
									<option value="{{item.value}}" ng-repeat="item in userActionItems">{{item.label}}</option>
								</select>
							</td>
						</tr>
						<tr ng-show="rejectionReasonItems && !errors">
							<td >Rejection Reason</td>
							<td class="esm-input">
								<select id="rejectionReason" ng-model="sm.rejectionReason">
									<option value="">--None--</option>
									<option value="{{item}}" ng-repeat = "item in rejectionReasonItems">{{item}}</option>
								</select>
							</td>
							<td></td><td></td>
						</tr>
						<tr ng-show="submitSupplierMaintenanceErrors.length > 0">
							<td colspan="4" style="text-align: left;font-weight: bold;color: red;">
								<ul style="margin-bottom: 0px;">
									<li ng-repeat="error in submitSupplierMaintenanceErrors" >
										{{ error }}
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="text-align: center;">
								<button ng-disabled="errors" ng-show="isEnableEdit"
									type="submit" class="btn btn-primary"
									ng-click="submitSupplierMaintenanceDetails()">Save</button> 
								 <!-- 	<a	class="btn btn-primary" ng-show="sm.allFields.akritivesm__Primary_Document_ID__c != null && sm.allFields.akritivesm__Primary_Document_ID__c != ''"
								href="rest/notesAndAttachments/viewPrimaryDocument?id={{sm.allFields.akritivesm__Primary_Document_ID__c}}">
									<i class="icon-large icon-download-alt"></i> View Supplier
									Maintenance</a>   -->
							
								<button ng-click="cancelSupplierMaintenance()" type="button"
									class="btn">Cancel</button>
							</td>
						</tr>
					</tbody>
				</table>
				<attachments-component model="notesAndAttachemntsComponentModel">
				</attachments-component>
				<history-component model="supplierMaintenanceHistoryComponentModel">
				</history-component> 
			</div>
		</div>
	</div>
</div>