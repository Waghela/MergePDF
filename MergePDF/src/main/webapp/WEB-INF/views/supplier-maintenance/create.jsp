<%-- <jsp:include page="../../../taglibs.jsp"></jsp:include> --%>
<%@ include file="../../../taglibs.jsp" %>
<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-file"></i>
				<h3>Supplier Maintenance Create</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content" block-ui="supplierMaintenanceCreateBlock">
<!-- 				SUPPLIER MAINTENANCE DETAILS... -->
                <table style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " >
                	<tbody>
						<tr>
							<attach-file 	allowed-size			="10MB" 
											allowed-ext				=".pdf,.xls,.xlsx,.txt,.png,.jpeg,.csv" 
											allowed-ext-for-prime	=".pdf"
											allow-attach-prime		='true'
											files					="files"
											base-scope				="baseScope">
							</attach-file>
						</tr>
                		<tr>
							<th colspan="4" >Requestor Information</th>
						</tr>
						<tr>
							<td>Request Info</td>
							<td class="esm-input">
								<select ng-model="sm.akritivesm__Request_Info__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="requestInfo in pickList.akritivesm__Request_Info__c" 
											value="{{requestInfo.value}}">{{requestInfo.label}}</option>
								</select>
							</td>
							<td>Current Working User</td><td></td>
						</tr>
						<tr>
							<td>Requestor</td>
							<td class="esm-input">
								<div class="input-append" style="margin-bottom: 0;">
									<input type="hidden" ng-model="sm.akritivesm__Requestor__c.id" />
									<input 	type			="text" 
											class			="span2 m-wrap" 
											style			="margin-bottom: 0;" 
											name			="Requestor__c" 
											ng-disabled		="true" 
											ng-model		="sm.akritivesm__Requestor__c.name" />
									<button type			="button" 
											ng-click		="showRequestorUserLookup(sm)" 
											class			="btn" 
											title			="Requestor Lookup">
										<i class="icon-search"></i>
									</button>
								</div>
							</td>
							<td>Reason For Change</td>
							<td class="esm-input">
								<select ng-model="sm.Reason_for_change__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="reasonForChange in pickList.Reason_for_change__c" 
											value="{{reasonForChange.value}}">{{reasonForChange.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Vendor Type</td>
							<td class="esm-input">
								<select ng-model="sm.Vendor_Type__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="vendorType in pickList.Vendor_Type__c" 
											value="{{vendorType.value}}">{{vendorType.label}}</option>
								</select>
							</td>
							<td>Supplier Profile</td>
							<td class="esm-input">
							</td>
						</tr>
						<tr>
							<td>Description</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.ReasonForChangeDescription__c">
							</td>
							<td>Current State</td>
							<td class="esm-input" ng-model="sm.akritivesm__Current_State__c">
								Start
							</td>
						</tr>
                		<tr>
							<th colspan="4" >Vendor Information</th>
						</tr>
						<tr>
							<td>E-one Vendor #</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.E_one_Vendor_Number__c" style="margin-bottom:0px">
							</td>
							<td>BPCS/Customer #</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.BPCS_Customer__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Vendor Number (S)</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_Number_S__c" style="margin-bottom:0px">
							</td>
							<td>Vendor Name</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_Name__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>ESM Ref No</td>
							<td></td>
							<td>Request Type</td>
							<td class="esm-input">
								<select ng-model="sm.Request_Type__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="requestType in pickList.Request_Type__c" 
											value="{{requestType.value}}">{{requestType.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Attention</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Attention__c" style="margin-bottom:0px">
							</td>
							<td>Address1</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.akritivesm__Address1__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>City</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.City__c" style="margin-bottom:0px">
							</td>
							<td>Address2</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.akritivesm__Address2__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>State</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.State__c" style="margin-bottom:0px">
							</td>
							<td>Address3</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.akritivesm__Address3__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Zip Code</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Zip_Code__c" style="margin-bottom:0px">
							</td>
							<td>Vendor Country</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_Country__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Vedor Contact</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_Contact__c" style="margin-bottom:0px">
							</td>
							<td>Vendor Phone</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_Phone__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Vedor Fax</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_Fax__c" style="margin-bottom:0px">
							</td>
							<td>Vendor Email</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Vendor_email__c" style="margin-bottom:0px">	
							</td>
						</tr>
						<tr>
							<td>Remmitance Email Address</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Remmitance_Email_Address__c" style="margin-bottom:0px">
							</td>
							<td></td><td></td>
						</tr>
						<tr>
							<th colspan="4" >Payment Information</th>
						</tr>
						<tr>
							<td>Tax Explaination code</td>
							<td class="esm-input">
								<select ng-model="sm.Tax_Explaination_code__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="taxExplainationCode in pickList.Tax_Explaination_code__c" 
											value="{{taxExplainationCode.value}}">{{taxExplainationCode.label}}</option>
								</select>
							</td>
							<td>Hold payment flag</td>
							<td class="esm-input">
								<select ng-model="sm.Hold_payment_flag__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="holdPaymentFlag in pickList.Hold_payment_flag__c" 
											value="{{holdPaymentFlag.value}}">{{holdPaymentFlag.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Payment Instrument</td>
							<td class="esm-input">
								<select ng-model="sm.Payment_Instrument__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="paymentInstrument in pickList.Payment_Instrument__c" 
											value="{{paymentInstrument.value}}">{{paymentInstrument.label}}</option>
								</select>
							</td>
							<td>Tax ID</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Tax_ID_Encr__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Payment Terms</td>
							<td class="esm-input">
								<select ng-model="sm.Payment_Terms__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="paymentTerms in pickList.Payment_Terms__c" 
											value="{{paymentTerms.value}}">{{paymentTerms.label}}</option>
								</select>
							</td>
							<td>Payment by voucher</td>
							<td class="esm-input">
								<select ng-model="sm.Payment_by_voucher__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="paymentByVoucher in pickList.Payment_by_voucher__c" 
											value="{{paymentByVoucher.value}}">{{paymentByVoucher.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>ABA #</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.ABA__c" style="margin-bottom:0px">
							</td>
							<td>Swift Code</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Swift_Code__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Vendor Currency</td>
							<td class="esm-input">
								<select ng-model="sm.Currency__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="currency in pickList.Currency__c" 
											value="{{currency.value}}">{{currency.label}}</option>
								</select>
							</td>
							<td>IBAN</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.IBAN__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<th colspan="4" >Governance Information</th>
						</tr>
						<tr>
							<td>Person / Corp Code</td>
							<td class="esm-input">
								<select ng-model="sm.Person_Corp_Code__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="personCorpCode in pickList.Person_Corp_Code__c" 
											value="{{personCorpCode.value}}">{{personCorpCode.label}}</option>
								</select>
							</td>
							<td>Sub Cot</td>
							<td class="esm-input">
								<select ng-model="sm.Sub_Cot__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="subCot in pickList.Sub_Cot__c" 
											value="{{subCot.value}}">{{subCot.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Company code</td>
							<td class="esm-input">
								<select ng-model="sm.Company_code__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="companyCode in pickList.Company_code__c" 
											value="{{companyCode.value}}">{{companyCode.label}}</option>
								</select>
							</td>
							<td>Special Factor Payee</td>
							<td class="esm-input">
								<select ng-model="sm.Special_Factor_Payee__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="specialFactorPayee in pickList.Special_Factor_Payee__c" 
											value="{{specialFactorPayee.value}}">{{specialFactorPayee.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Freight handling Code</td>
							<td class="esm-input">
								<select ng-model="sm.Freight_handling_Code__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="freightHandlingCode in pickList.Freight_handling_Code__c" 
											value="{{freightHandlingCode.value}}">{{freightHandlingCode.label}}</option>
								</select>
							</td>
							<td>CostaRica</td>
							<td class="esm-input">
								<input type="checkbox" ng-model="sm.CostaRica__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Account Number</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.Account_Number__c" style="margin-bottom:0px">
							</td>
							<td>1099 Reporting</td>
							<td class="esm-input">
								<select ng-model="sm.X1099_Reporting__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="x1099Reporting in pickList.X1099_Reporting__c" 
											value="{{x1099Reporting.value}}">{{x1099Reporting.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Cate code 29</td>
							<td class="esm-input">
								<select ng-model="sm.Cate_code_29__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="cateCode29 in pickList.Cate_code_29__c" 
											value="{{cateCode29.value}}">{{cateCode29.label}}</option>
								</select>
							</td>
							<td>GL Offset</td>
							<td class="esm-input">
								<input type="text" ng-model="sm.GL_Offset__c" style="margin-bottom:0px">
							</td>
						</tr>
						<tr>
							<td>Pre Note Indicator</td>
							<td class="esm-input">
								<input type="checkbox" ng-model="sm.Pre_Note_Indicator__c" style="margin-bottom:0px">
							</td>
							<td>CAD Tax Rates</td>
							<td class="esm-input">
								<select ng-model="sm.CAD_Tax_Rates__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="cadTaxRates in pickList.CAD_Tax_Rates__c" 
											value="{{cadTaxRates.value}}">{{cadTaxRates.label}}</option>
								</select>
							</td>
						</tr>
						<tr>
							<th colspan="4" >Closure Fields</th>
						</tr>
						<tr>
							<td>TIN Mapping Completed</td>
							<td class="esm-input">
								<input type="checkbox" ng-model="sm.TIN_Mapping_Completed__c" style="margin-bottom:0px">
							</td>
							<td>Comment History</td>
							<td></td>
						</tr>
						<tr>
							<td>Comments</td>
							<td class="esm-input">
								<textarea 	rows		="3" 
											style		="resize:none;" 
											name		="akritivesm__Comments__c"
											ng-model	="sm.akritivesm__Comments__c">
								</textarea>
							</td>
							<td>User Actions</td>
							<td class="esm-input">
								<select ng-model="sm.akritivesm__User_Actions__c" style="margin-bottom:0px">
									<option value="">--None--</option>
									<option ng-repeat="userActions in pickList.akritivesm__User_Actions__c" 
											value="{{userActions.value}}">{{userActions.label}}</option>
								</select>
							</td>
						</tr>
						<tr ng-show="supplierMaintenanceErrors.length > 0">
							<td colspan="4" style="text-align: left;font-weight: bold;color: red;">
								<ul style="margin-bottom: 0px;">
									<li ng-repeat="error in supplierMaintenanceErrors" >
										{{ error }}
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="text-align: center;">
								<button ng-disabled			=""
										type				="submit" 
										class				="btn btn-primary" 
										ng-click			="saveSupplierMaintenance()">Save</button>
								<button ng-click="cancelSupplierMaintenance()" type="button" class="btn" >Cancel</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>