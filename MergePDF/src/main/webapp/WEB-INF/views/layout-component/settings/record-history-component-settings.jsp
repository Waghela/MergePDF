<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div block-ui="RecordHistoryComponentSettingsBlockUi">
	<div class="layout-section-title">Record History SObject</div>
	</br>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectSObjects">SObject</label>
		<div class="controls">
			 <select id         = "selectSObjects"
				     ng-model   = "sObject"
				     ng-options = "sObj as sObj.label for sObj in sObjects | filter: {customSetting: false}" 
				     class      = "span3"
				     ng-change  = "onSObjectChange(sObject);">
				         <option value="">--Select SObject --</option>
			</select>
		</div>
	</div>
	<div class="layout-section-title">Record History Custom Setting</div>
	</br>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomSettingSObject">Custom Setting SObject</label>
		<div class="controls">
			 <select id         = "selectCustomSettingSObject"
				     ng-model   = "customSettingSObject"
				     ng-options = "sObj as sObj.label for sObj in sObjects | filter: {customSetting: true}" 
				     class      = "span3"
				     ng-change  = "onCustomSettingSObjectChange(customSettingSObject);">
				         <option value="">--Select Custom Setting SObject --</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomSettingSObjectLookupField">Look Up API Field</label>
		<div class="controls">
			 <select ng-disabled = "!customSettingSObject" id = "selectCustomSettingSObjectLookupField"
						        ng-model    = "customSettingSObjectLookupApiField"
						        ng-options  = "field as field.label for field in customSettingSObject.fields | filter: {custom: true,type:'string'}"
						        class       = "span3"	  ng-change  = "onCustomSettingSObjectLookupApiFieldChange(customSettingSObjectLookupApiField);"					         
						        >
							<option value="" >--Select Look Up API Field--</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomSettingSObjectStatusField">Custom History Status </label>
		<div class="controls">
			 <select ng-disabled = "!customSettingSObject" id= "selectCustomSettingSObjectStatusField"
						        ng-model    = "customSettingSObjectCustomHistoryStatusField"
						        ng-options  = "field as field.label for field in customSettingSObject.fields | filter: {custom: true,type:'boolean'}"
						        class       = "span3"    ng-change  = "onCustomSettingSObjectCustomHistoryStatusFieldChange(customSettingSObjectCustomHistoryStatusField);" >
							<option value="" >--Select Custom HistoyrField--</option>
			</select>
		</div>
	</div>
	<div class="layout-section-title">Record History Custom History</div>
	</br>
	<div class="control-group">
		<label class = "control-label " 
		       for   = "selectCustomHistorySObject">Custom History SObject</label>
		<div class="controls">
			 <select id         = "selectCustomHistorySObject"
				     ng-model   = "customHistorySObject"
				     ng-options = "sObj as sObj.label for sObj in sObjects | filter: {customSetting: false}" 
				     class      = "span3"
				     ng-change  = "onCustomHistorySObjectChange(customHistorySObject);">
				         <option value="">--Select Custom History SObject --</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomHistorySObjectOldValueField">Old Value Field </label>
		<div class="controls">
			 <select ng-disabled = "!customHistorySObject" id= "selectCustomHistorySObjectOldValueField"
						        ng-model    = "customHistorySObjectOldValueField"
						        ng-options  = "field as field.label for field in customHistorySObject.fields | filter: {custom: true,type:'string'}"
						        class       = "span3"    ng-change  = "onCustomHistorySObjectOldValueFieldChange(customHistorySObjectOldValueField);" >
							<option value="" >--Select Old Value Field--</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomHistorySObjectNewValueField">New Value Field </label>
		<div class="controls">
			 <select ng-disabled = "!customHistorySObject" id= "selectCustomHistorySObjectNewValueField"
						        ng-model    = "customHistorySObjectNewValueField"
						        ng-options  = "field as field.label for field in customHistorySObject.fields | filter: {custom: true,type:'string'}"
						        class       = "span3"    ng-change  = "onCustomHistorySObjectNewValueFieldChange(customHistorySObjectNewValueField);" >
							<option value="" >--Select New Value Field--</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomHistorySObjectFieldLabelField">Field Label Field </label>
		<div class="controls">
			 <select ng-disabled = "!customHistorySObject" id= "selectCustomHistorySObjectFieldLabelField"
						        ng-model    = "customHistorySObjectFieldLabelField"
						        ng-options  = "field as field.label for field in customHistorySObject.fields | filter: {custom: true,type:'string'}"
						        class       = "span3"    ng-change  = "onCustomHistorySObjectFieldLabelFieldChange(customHistorySObjectFieldLabelField);" >
							<option value="" >--Select Label Field--</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomHistorySObjectCreatedDateField">Created Date </label>
		<div class="controls">
			 <select ng-disabled = "!customHistorySObject" id= "selectCustomHistorySObjectCreatedDateField"
						        ng-model    = "customHistorySObjectCreatedDateField"
						        ng-options  = "field as field.label for field in customHistorySObject.fields | filter: {custom: true,type:'datetime'}"
						        class       = "span3"    ng-change  = "onCustomHistorySObjectCreatedDateFieldChange(customHistorySObjectCreatedDateField);" >
							<option value="" >--Select Created Date Field--</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectCustomHistorySObjectCreatedByIdField">Created By</label>
		<div class="controls">
			 <select ng-disabled = "!customHistorySObject" id= "selectCustomHistorySObjectCreatedByIdField"
						        ng-model    = "customHistorySObjectCreatedByIdField"
						        ng-options  = "field as field.label for field in customHistorySObject.fields | filter: {custom: true,type:'reference'}"
						        class       = "span3"    ng-change  = "onCustomHistorySObjectCreatedByIdFieldChange(customHistorySObjectCreatedByIdField);" >
							<option value="" >--Select Created By Field--</option>
			</select>
		</div>
	</div>
</div>


