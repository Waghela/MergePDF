<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div block-ui="DownloadPrimaryDocumentComponentSettingsBlockUi">
	<div class="layout-section-title">Primary Document SObject</div>
	</br>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectSObjects">SObject</label>
		<div class="controls">
			 <select id         = "selectSObjects"
				     ng-model   = "sObject"
				     ng-options = "sObj as sObj.label for sObj in sObjects | filter: {customSetting: false}" 
				     class      = "span3"
				     ng-change  = "onSObjectChange(sObject);">
				         <option value="">--Select SObject --</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectTrackerIdLookUpField">Tracker Id Look Up Field</label>
		<div class="controls">
			 <select id         = "selectTrackerIdLookUpField"
				     ng-model   = "trackerIdLookupFiled"
				     ng-options = "field as field.label for field in sObject.fields | filter: {type: 'reference'}" 
				     class      = "span3"
				     ng-change  = "getTrackerSObjectFields(trackerIdLookupFiled);">
				         <option value="">--Select Field --</option>
			</select>
		</div>
	</div>
	
	<div class="control-group">
		<label class = "control-label "
		       for   = "selectAttachmentIdField">Tracker Id Look Up Field</label>
		<div class="controls">
			 <select id         = "selectAttachmentIdField"
				     ng-model   = "attachmentIdField"
				     ng-options = "field as field.label for field in trackerSObject.fields" 
				     class      = "span3"
				     ng-change  = "onAttachmentIdFieldChange(attachmentIdField);">
				         <option value="">--Select Field --</option>
			</select>
		</div>
	</div>

</div>


