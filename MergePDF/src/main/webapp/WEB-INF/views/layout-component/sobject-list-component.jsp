<%@ include file="../../../taglibs.jsp" %>

<!-- DETAILS VIEW AND EDIT VIEW-->
<table datatable="ng" class="table table-spriped table-hover table-condensed">
	<thead>
		<tr class="list-table">
			<th ng-repeat="field in section.componentMetadata.configurations.sobjectFields">
				{{field.label}}
			</th>
		</tr>
	</thead>
	<tbody>
		<tr ng-repeat="item in lineItems">
			<td ng-repeat="field in section.componentMetadata.configurations.sobjectFields">
				{{ field.type == 'reference' ? $root.showValue(item.relationshipSubObjects[field.relationshipName].allFields.Name) : $root.showValue(item.allFields[field.name]) }}
			</td>
		</tr>
	</tbody>
</table>
