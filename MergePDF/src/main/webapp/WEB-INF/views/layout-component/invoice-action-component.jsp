<%@ include file="../../../taglibs.jsp" %>

<div class="controls">
	<div class="layout-section" ng-if="section && ActionValidationService.hasUserAction"> 
		<div class="layout-section-title">
			{{ section.title }} 
			<i ng-if="!isActionModelValid()" class="icon-exclamation-sign icon-color-red"></i>
		</div>
		<div class="layout-section-content layout-section-column2">
			<div class="layout-section-field">
				<div class="field-label">{{ section.fields.comments.label }}</div>
				<div class="field-control">
					<textarea 	ng-required		="section.fields.comments.required" 
								rows			="4" 
								class			="span3" 
								ng-model		="actionModel.comments.value" 
								style="resize:none;margin-bottom: 0px;">
					</textarea>
				</div>
			</div>
			<div class="layout-section-field">
				<div class="field-label">{{ section.fields.useraction.label }}</div>
				<div class="field-control">
					<select 	ng-required		="'true'" 
								ng-model		="actionModel.useraction.value" 
								class			="span3" 
								style			="margin-bottom: 0px;" ng-options="value for value in userActionPicklistValues">
						<option value="" ng-disabled="'true'">--None--</option>
					</select>
				</div>
			</div>
			<div class="layout-section-field-hr"></div>
			<div class="layout-section-field" ng-if="actionModel.useraction.value == section.fields.rejectionreason.useractionvalue">
				<div class="field-label">{{ section.fields.rejectionreason.label }}</div>
				<div class="field-control">
					<select 	ng-required		="'true'" 
								ng-model		="actionModel.rejectionreason.value" 
								class			="span3" 
								style			="margin-bottom: 0px;" ng-options="value for value in rejectionReasonPicklistValues">
						<option value="" ng-disabled="'true'">--None--</option>
					</select>
				</div>
			</div>
			<div class="layout-section-field" ng-if="actionModel.useraction.value == section.fields.route.useractionvalue">
				<div class="field-label">{{ section.fields.route.label }}</div>
				<div class="field-control">
					<div class="input-append" style="margin-bottom: 0;">
						<input 	type 			= "hidden"  
								ng-model 		= "actionModel.route.value" />
						<input 	ng-required		="'true'"
								type			="text" 
							   	class			="span2 m-wrap" 
							   	style			="margin-bottom: 0;" 
							   	ng-disabled		="true" 
							   	ng-model		="actionModel.route.value__Name" />
					   	<button type			="button" 
								ng-click		="showLookupModal()" 
								class			="btn" >
							<i class="icon-search"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<ul style="margin: 0;" ng-if="errorMessages">
		<li ng-if="errorMessages.required" class="required">Please provide valid <strong>required</strong> information!</li>
	</ul>
</div>

<div class="form-actions" style="margin-bottom:0px; text-align: center;">
	<button type="submit" class="btn btn-primary" ng-click="save()" ng-if="ActionValidationService.hasUserAction"><i class="icon-ok"></i>&nbsp; Save</button>
	<button type="button" class="btn" ng-click="cancel()"><i class="icon-remove"></i>&nbsp; Cancel</button>
	<button type="button" class="btn" ng-click="ctrl.loadInvoiceDetails()"><i class="icon-refresh"></i>&nbsp; Reload</button>
</div>