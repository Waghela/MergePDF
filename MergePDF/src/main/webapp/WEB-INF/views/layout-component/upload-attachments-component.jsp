<%@ include file="../../../taglibs.jsp"%>
<table style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " block-ui="AttachmentBlock">
	<tbody>
    	<tr>
			<th colspan="4" >Add Attachment</th>
		</tr>
		<tr>
			
			<td>
				<span 	data-ng-show	="!primaryDocument"
						class		="btn btn-mini btn-success fileinput-button dz-clickable" 
						style		="background-color: #00BA8B"
						name		="file"
						enctype		="multipart/form-data"
						data-ngf-select	="selectFile($files, $invalidFiles)"
						ngf-max-size="{{allowedSize}}"
						accept		="{{allowedExt}}"
						multiple>
						<span>Add </span><i class="icon-file"></i> 
				</span>
				<span 	data-ng-show	="primaryDocument"
						class		="btn btn-mini btn-success fileinput-button dz-clickable" 
						style		="background-color: #00BA8B"
						name		="file"
						enctype		="multipart/form-data"
						data-ngf-select	="selectFile($files, $invalidFiles)"
						ngf-max-size="{{allowedSize}}"
						accept		="{{allowedExtForPrime}}"
						single>
						<span>Add </span><i class="icon-file"></i>
				</span> 
			</td><td >Select the File to be attached</td>
			<td data-ng-if="allowAttachPrime">
				<input 	type				="checkbox" 
						ng-model			="primaryDocument"
						data-ng-click		="primaryDocumentClicked(primaryDocument);"
						data-ng-disabled	="disablePrimaryDocuent"
						tooltip 			="Primary Document">
						Upload Primary Document
			</td>
			<td data-ng-if="!allowAttachPrime"></td><td></td>
		</tr>
		<tr>
		   <td colspan="4" style="text-align: left;color:red;">
		       <span data-ng-show	="!primaryDocument" >{{ allowedExt }} format are sported.</span>
		       <span data-ng-show	="primaryDocument">{{ allowedExtForPrime }} format are sported.</span>
		   </td>
		</tr>
		<tr>
			<td colspan="4" style="text-align: center;">Note : Please limit your files to a maximum of 10 MB in size.</td>
		</tr>
		<tr>
			<th colspan="4">Attachment Section</th>
		</tr>
		<tr ng-if			="files"
			data-ng-show	="files.length > 0">
			<td colspan="4">
				<table style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " >
					<thead><th></th><th>File</th><th>Size in Bytes</th><th>Upload</th></thead>
					<tbody>
						<tr data-ng-model 	= "files"
							data-ng-repeat	="file in files">
							<td class="vender-maintenance-attachment-section">
									<a href="" data-ng-click="deleteFile(file)">delete</a>
							</td>
							<td class="esm-input">{{file.name}}</td>
							<td class="vender-maintenance-attachment-section">{{file.size}}</td>
							<td class="esm-input" ng-show="!file.isPersisted"><a href="" data-ng-click="attachFiles(file)">Upload</a> <label data-ng-if="primaryFileName == file.name" style="margin-bottom: 0;">Primary Document</label></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr data-ng-show	="files.length == 0">
			<td colspan="4">
				<center style="color:red;">No attachment found.</center>
			</td>
		</tr>
		<tr>
		<td colspan='4' align="center" style="text-align: center;">
		  <button
										type		="submit" 
										class		="btn btn-mini btn-primary" 
										ng-click	="saveAttachments();">
									<i class="icon icon-ok"></i> Save
								</button>
		</td>
		</tr>
	</tbody>
</table>