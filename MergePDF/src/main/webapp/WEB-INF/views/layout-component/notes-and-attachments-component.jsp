<%@ include file="../../../taglibs.jsp"%>
<div block-ui = "notesAndAttachmentBlock">
<!-- 	 ng-show  = "notesDetails.length > 0 || attachmentsDetails.length > 0"> -->
	<table class="table table-spriped table-condensed">
		<thead>
			<tr>
				<th>Title</th>
				<th>Size</th>
				<th>Description</th>
				<th style="text-align: center;">View</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-if="notesDetails.length === 0 && attachmentsDetails.length === 0">
				<td colspan="4">No data available in table</td>
			</tr>
			<tr ng-repeat="note in notesDetails">
				<td>{{ $root.showValue(note.allFields.Title) }}</td>
				<td>-</td>
				<td>-</td>
				<td style="text-align: center;">
				    <button type	 = "button"
						    ng-click = "showNoteModal(note.id.idStr)" 
						    class	 = "btn btn-mni btn-primary">
						<i class="icon icon-eye-open"></i>
					</button>
				</td>
			</tr>
			<tr ng-repeat="attachment in attachmentsDetails">
				<td>{{ $root.showValue(attachment.allFields.Name) }}</td>
				<td>{{ $root.showValue(attachment.allFields.BodyLength/1024) |
					number : 2}} KB</td>
				<td>{{ $root.showValue(attachment.allFields.Description) }}</td>
				<td style="text-align: center;">
				   <a class = "btn btn-mini btn-primary"
					  href  = "rest/layout-component/notes-and-attachments-component/viewAttachment?name={{attachment.allFields.Name}}&body={{attachment.allFields.Body}}">
						<i class="icon-large icon-download-alt"></i>
				   </a>
				</td>
			</tr>
		</tbody>
	</table>
</div>