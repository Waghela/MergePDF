<%@ include file="../../taglibs.jsp" %>
<div class="footer "><!-- fixed-bottom -->
	<div class="footer-inner">
		<div class="container">
			<div class="row">
    			<div class="span12">
    				&copy; 2015 <a href="https://www.akritiv.com/home/" target="_blank">Akritiv Technologies Pvt. Ltd.</a>.
    			</div> <!-- /span12 -->
    		</div> <!-- /row -->
		</div> <!-- /container -->
	</div> <!-- /footer-inner -->
</div> <!-- /footer -->