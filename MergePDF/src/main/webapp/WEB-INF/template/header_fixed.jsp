<%@ include file="../../taglibs.jsp" %>
<div class="navbar navbar-fixed-top">
	<div class="navbar-inner" style="background: #1294D5 !important">
		<div class="container">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".nav-collapse"><span class="icon-bar"></span><span
				class="icon-bar"></span><span class="icon-bar"></span> </a>
			<a class="brand" href="#">Akritiv ESM</a>
			<div class="nav-collapse">
				<sec:authorize access="isFullyAuthenticated()" >
				<ul class="nav pull-right">
					<li class="dropdown"><a href="javascript::;" class="dropdown-toggle"
						data-toggle="dropdown"><i class="icon-user"></i>&nbsp; <sec:authentication property="principal.fullname" />
							<b class="caret"></b></a>
						<ul class="dropdown-menu">
<!-- 							<li><a href="javascript:;">Profile</a></li> -->
<%-- 							<li><a style="cursor: pointer;" ng-click="$root.logout('${logoutUrl }')">Logout</a></li> --%>
							<li><a id="changePassword" style="cursor: pointer;"   href="#/changePassword">Change Password</a></li>
							<li><a id="logoutUrl" style="cursor: pointer;">Logout</a></li>
						</ul>
					</li>
				</ul>
				</sec:authorize>
				<sec:authorize access="hasAnyRole('ADMIN')" >
					<ul class="nav pull-right">
						<li class="dropdown">
							<a href="javascript::;" class="dropdown-toggle" data-toggle="dropdown">
								<i class="icon-cogs"></i>&nbsp;<b>SETUP</b>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="#/sobjects"><i class="icon-cloud"></i>&nbsp; SObjects</a></li>
								<li><a href="#/mytasksections"><i class="icon-tasks"></i>&nbsp; My Task Sections</a></li>
								<li><a href="#/layouts"><i class="icon-list-alt"></i>&nbsp; Layouts</a></li>
							</ul>
						</li>
					</ul>
				</sec:authorize>
			</div>
			<!--/.nav-collapse -->
		</div>
		<!-- /container -->
	</div>
	<!-- /navbar-inner -->
</div>