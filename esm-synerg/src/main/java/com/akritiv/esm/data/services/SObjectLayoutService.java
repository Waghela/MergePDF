package com.akritiv.esm.data.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akritiv.esm.data.entities.SObjectLayout;
import com.akritiv.esm.data.entities.SObjectLayoutSection;
import com.akritiv.esm.data.repositories.SObjectLayoutRepository;

@Service
@Transactional
public class SObjectLayoutService {

	@Autowired
	SObjectLayoutRepository repository;
	
	public List<SObjectLayout> findAll(){
		List<SObjectLayout> layouts = repository.findAll();
		for (SObjectLayout sObjectLayout : layouts) {
			sObjectLayout.setSections(null);
		}
		return layouts; 
	}
	public List<SObjectLayout> findAllByAllSections(){
		return repository.findAll();
	}
	public List<SObjectLayout> findAllByActiveSections(boolean active){
		List<SObjectLayout> layouts = repository.findAll();
		for (SObjectLayout sObjectLayout : layouts) {
			sObjectLayout.setSectionsByActiveEquelsTo(active);
		}
		return layouts;
	}
	
	public SObjectLayout find(Long id){
		SObjectLayout layout = repository.findOne(id);
		Map<Double, SObjectLayoutSection> map = new TreeMap<Double, SObjectLayoutSection>();
		for (SObjectLayoutSection section : layout.getSections()) {
			map.put(section.getSequence(), section);
		}
		layout.setSections(new ArrayList<SObjectLayoutSection>(map.values()));
		return layout;
	}
	public SObjectLayout findBySObjectApiName(String sObjectApiName){
		SObjectLayout layout = repository.findBysObjectApiName(sObjectApiName);
		Map<Double, SObjectLayoutSection> map = new TreeMap<Double, SObjectLayoutSection>();
		for (SObjectLayoutSection section : layout.getSections()) {
			map.put(section.getSequence(), section);
		}
		layout.setSections(new ArrayList<SObjectLayoutSection>(map.values()));
		return layout;
	}
	public SObjectLayout findBySObjectApiName(String sObjectApiName,boolean activeStatus){
		SObjectLayout layout = repository.findBysObjectApiName(sObjectApiName);
		Map<Double, SObjectLayoutSection> map = new TreeMap<Double, SObjectLayoutSection>();
		for (SObjectLayoutSection section : layout.getSections()) {
			if(section.isActive() == activeStatus){
				map.put(section.getSequence(), section);
			}
		}
		layout.setSections(new ArrayList<SObjectLayoutSection>(map.values()));
		return layout;
	}
	
	public SObjectLayout findByActiveSections(Long id,boolean active){
		SObjectLayout layout = repository.findOne(id);
		layout.setSectionsByActiveEquelsTo(active);
		return layout;
	}
	
	public List<SObjectLayout> allActiveSObjectLayouts(){
		return this.findByActiveWithSObjectLayoutSections(true);
//		return repository.findByActive(true);
	}
	
	public List<SObjectLayout> allInactiveSObjectLayouts(){
		return this.findByActiveWithSObjectLayoutSections(false);
//		return repository.findByActive(false);
	}
	
	private List<SObjectLayout> findByActiveWithSObjectLayoutSections(boolean value){
		List<SObjectLayout> layouts = repository.findByActive(value);
		for (SObjectLayout sObjectLayout : layouts) {
			sObjectLayout.setSectionsByActiveEquelsTo(value);
		}
		return layouts;
	}
	
	public SObjectLayout create(SObjectLayout SObjectLayout){
		return repository.save(SObjectLayout);
	}
	
	public SObjectLayout update(SObjectLayout SObjectLayout){
		return repository.save(SObjectLayout);
	}
	
	public void deleteSObjectLayout(Long id){
		repository.delete(id);
	}
	
	public void delete(SObjectLayout SObjectLayout){
		repository.delete(SObjectLayout);
	}
	
	public void delete(List<SObjectLayout> SObjectLayoutsToDelete){
		repository.delete(SObjectLayoutsToDelete);
	}
	
	public SObjectLayout save(SObjectLayout SObjectLayout){
		return repository.save(SObjectLayout);
	}
	
	public List<SObjectLayout> save(List<SObjectLayout> SObjectLayouts){
		return repository.save(SObjectLayouts);
	}
}
