package com.akritiv.esm.data.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.akritiv.esm.data.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Serializable> {

	@Query("select u from User u where u.email=?1 and u.password=?2")
	User login(String email, String password);

	@Query("select u from User u where u.email=?1 and u.password=?2 and upper(u.status)='ACTIVE'")
	User findByEmailAndPassword(String email, String password);

	@Query("select u from User u where upper(u.username)=upper(?1) and upper(u.status)='ACTIVE'")
	User findUserByUsername(String email);
	
	@Query("select u from User u where upper(u.federationid)=upper(?1) and upper(u.status)='ACTIVE'")
	User findUserByFederationId(String federationId);
	
	User findUserByEmail(String email);

	@Query("select u from User u where u.saleforceId=?1")
	User findUserBySaleforceId(String saleforceId);

	
	@Query("SELECT u FROM User u WHERE  (u.saleforceId LIKE %?1% OR u.firstName LIKE %?1% OR u.lastName LIKE %?1% OR u.email LIKE %?1%) AND  u.email NOT IN ( ?2 ) AND upper(u.status) LIKE 'ACTIVE' AND upper(u.role.roleName) LIKE 'BUYER' ")
	List<User> findUserBySearchStringExcludingSomeEmail(String searchText, List<String> email);

	@Query("SELECT u FROM User u WHERE u.email NOT IN ( ?1 ) AND upper(u.status) LIKE 'ACTIVE' AND upper(u.role.roleName) LIKE 'BUYER'")
	List<User> findAllUserExcludingSomeEmail(List<String> email);
	
//	@Query("SELECT u FROM User u WHERE u.delegateUserId LIKE ?1 AND u.enableDelegation = true AND upper(u.status) LIKE 'ACTIVE' AND upper(u.role.roleName) LIKE 'BUYER'")
//	List<User> findDelegateUser(String saleforceId);
	
//	@Query("SELECT u.email FROM User u WHERE u.delegateUserId LIKE ?1 AND u.enableDelegation = true AND upper(u.status) LIKE 'ACTIVE' AND upper(u.role.roleName) LIKE 'BUYER'")
//	List<String> findDelegateUserEmail(String saleforceId);
	
//	@Query("SELECT u.email FROM User u WHERE u.supervisorId LIKE ?1 AND upper(u.status) LIKE 'ACTIVE' AND upper(u.role.roleName) LIKE 'BUYER' ")
//	List<String> findSuperVisorUserEmail(String saleforceId);
	
	
	

}