package com.akritiv.esm.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akritiv.esm.data.entities.SObjectLayoutSection;
import com.akritiv.esm.data.repositories.SObjectLayoutSectionRepository;

@Service
@Transactional
public class SObjectLayoutSectionService {
	
	@Autowired
	SObjectLayoutSectionRepository repository;
	
	public List<SObjectLayoutSection> findAll(){
		return repository.findAll();
	}
	public SObjectLayoutSection find(Long id){
		return repository.findOne(id);
	}
	public SObjectLayoutSection create(SObjectLayoutSection section){
		return repository.save(section);
	}
	public SObjectLayoutSection update(SObjectLayoutSection section){
		return repository.save(section);
	}
	public void delete(SObjectLayoutSection section){
		repository.delete(section);
	}
	public void delete(Long id){
		repository.delete(id);
	}
	public void delete(List<SObjectLayoutSection> sections){
		repository.delete(sections);
	}
	public SObjectLayoutSection save(SObjectLayoutSection section){
		return repository.save(section);
	}
	public List<SObjectLayoutSection> save(List<SObjectLayoutSection> sections){
		return repository.save(sections);
	}
}
