package com.akritiv.esm.data.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akritiv.esm.data.entities.SObjectLayoutSection;

//@Repository
public interface SObjectLayoutSectionRepository extends JpaRepository<SObjectLayoutSection, Serializable>{

}
