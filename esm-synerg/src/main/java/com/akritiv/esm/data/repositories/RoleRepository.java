package com.akritiv.esm.data.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.akritiv.esm.data.entities.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Serializable>{
	Role findByroleName(String roleName);
}
