package com.akritiv.esm.data.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectField;
import com.akritiv.esm.data.repositories.SObjectFieldRepository;
import com.akritiv.esm.data.repositories.SObjectRepository;

@Service
@Transactional
public class SObjectService {

	@Autowired
	SObjectRepository sObjectRepository;
	@Autowired
	SObjectFieldRepository fieldRepository;
	
	public List<SObject> findAll(){
		return sObjectRepository.findAll();
	}
	
	public SObject find(Long id){
		return sObjectRepository.findOne(id);
	}
	
	public SObject findByName(String name){
		return sObjectRepository.findByName(name);
	}
	
	public List<SObject> allActiveSObjects(){
		return sObjectRepository.findByActive(true);
	}
	
	public List<SObject> allInactiveSObjects(){
		return sObjectRepository.findByActive(false);
	}
	
	public SObject create(SObject sObject){
		return sObjectRepository.save(sObject);
	}
	
	public SObject update(SObject sObject){
		return sObjectRepository.save(sObject);
	}
	
	public void deleteSObject(Long id){
		sObjectRepository.delete(id);
	}
	
	public void delete(SObject sObject){
		sObjectRepository.delete(sObject);
	}
	
	public void delete(List<SObject> sObjectsToDelete){
		sObjectRepository.delete(sObjectsToDelete);
	}
	
	public SObject save(SObject sObject){
		return sObjectRepository.save(sObject);
	}
	
	public List<SObject> save(List<SObject> sObjects){
		return sObjectRepository.save(sObjects);
	}
	
	public SObjectField saveSObjectField(SObjectField field){
		return fieldRepository.save(field);
	}
	
	public List<SObjectField> saveSObjectFields(List<SObjectField> fields){
		return fieldRepository.save(fields);
	}

	public List<SObjectField> getSobjectFieldsBySobjectId(Long sObjectId){
		return fieldRepository.getSobjectFieldsBySobjectId(sObjectId);
	}
	
	public List<SObjectField> getSobjectReferenceFieldsBySobjectName(String name){
		return fieldRepository.getSobjectReferenceFieldsBySobjectName(name);
	}
	
	public List<SObjectField> getSobjectFieldByName(String sObjectFieldName){
		return fieldRepository.getSobjectFieldByName(sObjectFieldName);
	}
}
