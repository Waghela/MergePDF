package com.akritiv.esm.data.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akritiv.esm.data.entities.MyTaskSection;

public interface MyTaskSectionRepository extends JpaRepository<MyTaskSection, Serializable>{

	List<MyTaskSection> findByActive(boolean active);
	List<MyTaskSection> findByActiveOrderBySequenceAsc(boolean active);
}
