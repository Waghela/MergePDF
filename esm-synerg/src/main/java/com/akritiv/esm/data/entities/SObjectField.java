package com.akritiv.esm.data.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "sobjectfield")
public class SObjectField {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	private int length;
	private String name;
	private String type;
	private String defaultValue;
	private String label;
	private boolean nillable;
	@Type(type="text")
	private String inlineHelpText;
	private boolean nameField;
    private boolean externalId;
    private boolean idLookup;
    private boolean autoNumber;
    @Type(type="text")
    private String calculatedFormula;
    @Type(type="text")
    private String defaultValueFormula;
    private int digits;
    @Type(type="text")
    private String picklistValues;					// LIST OF STRINGS
    @Type(type="text")
    private String dependentPicklistValues;			// LIST OF STRINGS
    @Type(type="text")
    private String referenceTo;						// LIST OF STRINGS
    private String relationshipName;
    private String relationshipOrder;
    private boolean restrictedPicklist;
    private boolean namePointing;
    private boolean custom;
    private boolean htmlFormatted;
    private boolean dependentPicklist;
    private String controllerName;

	@ManyToOne(fetch = FetchType.LAZY,cascade = javax.persistence.CascadeType.ALL)
//    @Cascade(value = { CascadeType.DELETE,CascadeType.SAVE_UPDATE })
    @JsonBackReference
    @JoinColumn(name = "sobject_id",nullable = false)
    private SObject sObject;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isNillable() {
		return nillable;
	}

	public void setNillable(boolean nillable) {
		this.nillable = nillable;
	}

	public String getInlineHelpText() {
		return inlineHelpText;
	}

	public void setInlineHelpText(String inlineHelpText) {
		this.inlineHelpText = inlineHelpText;
	}

	public boolean isNameField() {
		return nameField;
	}

	public void setNameField(boolean nameField) {
		this.nameField = nameField;
	}

	public boolean isExternalId() {
		return externalId;
	}

	public void setExternalId(boolean externalId) {
		this.externalId = externalId;
	}

	public boolean isIdLookup() {
		return idLookup;
	}

	public void setIdLookup(boolean idLookup) {
		this.idLookup = idLookup;
	}

	public boolean isAutoNumber() {
		return autoNumber;
	}

	public void setAutoNumber(boolean autoNumber) {
		this.autoNumber = autoNumber;
	}

	public String getCalculatedFormula() {
		return calculatedFormula;
	}

	public void setCalculatedFormula(String calculatedFormula) {
		this.calculatedFormula = calculatedFormula;
	}

	public String getDefaultValueFormula() {
		return defaultValueFormula;
	}

	public void setDefaultValueFormula(String defaultValueFormula) {
		this.defaultValueFormula = defaultValueFormula;
	}

	public int getDigits() {
		return digits;
	}

	public void setDigits(int digits) {
		this.digits = digits;
	}

	public String getPicklistValues() {
		return picklistValues;
	}

	public void setPicklistValues(String picklistValues) {
		this.picklistValues = picklistValues;
	}

	public JsonNode getDependentPicklistValues() {
		if(this.dependentPicklistValues == null)
			return null;
		
		JsonNode node = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			node = mapper.readTree(dependentPicklistValues);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return node;
	}

	public void setDependentPicklistValues(JsonNode dependentPicklistValues) {
		this.dependentPicklistValues = (dependentPicklistValues!=null) ? dependentPicklistValues.toString() : null;
	}
	
	public String getReferenceTo() {
		return referenceTo;
	}

	public void setReferenceTo(String referenceTo) {
		this.referenceTo = referenceTo;
	}

	public String getRelationshipName() {
		return relationshipName;
	}

	public void setRelationshipName(String relationshipName) {
		this.relationshipName = relationshipName;
	}

	public String getRelationshipOrder() {
		return relationshipOrder;
	}

	public void setRelationshipOrder(String relationshipOrder) {
		this.relationshipOrder = relationshipOrder;
	}

	public boolean isRestrictedPicklist() {
		return restrictedPicklist;
	}

	public void setRestrictedPicklist(boolean restrictedPicklist) {
		this.restrictedPicklist = restrictedPicklist;
	}

	public boolean isNamePointing() {
		return namePointing;
	}

	public void setNamePointing(boolean namePointing) {
		this.namePointing = namePointing;
	}

	public boolean isCustom() {
		return custom;
	}

	public void setCustom(boolean custom) {
		this.custom = custom;
	}

	public boolean isHtmlFormatted() {
		return htmlFormatted;
	}

	public void setHtmlFormatted(boolean htmlFormatted) {
		this.htmlFormatted = htmlFormatted;
	}

	public boolean isDependentPicklist() {
		return dependentPicklist;
	}

	public void setDependentPicklist(boolean dependentPicklist) {
		this.dependentPicklist = dependentPicklist;
	}
	
	public String getControllerName() {
		return controllerName;
	}

	public void setControllerName(String controllerName) {
		this.controllerName = controllerName;
	}

	public SObject getsObject() {
		return sObject;
	}

	public void setsObject(SObject sObject) {
		this.sObject = sObject;
	}

	@Override
	public String toString() {
		return "SObjectField [id=" + id + ", length=" + length + ", name="
				+ name + ", type=" + type + ", defaultValue=" + defaultValue
				+ ", label=" + label + ", nillable=" + nillable
				+ ", inlineHelpText=" + inlineHelpText + ", nameField="
				+ nameField + ", externalId=" + externalId + ", idLookup="
				+ idLookup + ", autoNumber=" + autoNumber
				+ ", calculatedFormula=" + calculatedFormula
				+ ", defaultValueFormula=" + defaultValueFormula + ", digits="
				+ digits + ", picklistValues=" + picklistValues
				+ ", dependentPicklistValues=" + dependentPicklistValues
				+ ", referenceTo=" + referenceTo + ", relationshipName="
				+ relationshipName + ", relationshipOrder=" + relationshipOrder
				+ ", restrictedPicklist=" + restrictedPicklist
				+ ", namePointing=" + namePointing + ", custom=" + custom
				+ ", htmlFormatted=" + htmlFormatted + ", dependentPicklist="
				+ dependentPicklist + ", controllerName=" + controllerName
				+ ", sObject=" + sObject + "]";
	}

}
