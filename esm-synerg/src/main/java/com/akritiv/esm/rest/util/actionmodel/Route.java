package com.akritiv.esm.rest.util.actionmodel;

public class Route{
	String apiname;
	String label;
	String referenceTo;
	String useractionvalue;
	String value;
	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getReferenceTo() {
		return referenceTo;
	}
	public void setReferenceTo(String referenceTo) {
		this.referenceTo = referenceTo;
	}
	public String getUseractionvalue() {
		return useractionvalue;
	}
	public void setUseractionvalue(String useractionvalue) {
		this.useractionvalue = useractionvalue;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}