package com.akritiv.esm.rest.component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.config.SalesforceApiConfig;
import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectLayoutSection;
import com.akritiv.esm.data.services.SObjectLayoutSectionService;
import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.rest.component.RestRecordHistoryComponentController.HistoryObject;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.RestSObject;

@RestController
@RequestMapping("/rest/layout-component/download-primary-document-component")
public class RestDownloadPrimaryDocumentComponentController extends AbstractRestController {

	@Autowired
	SObjectService service;

	@Autowired
	SObjectLayoutSectionService sectionService;

	@RequestMapping(value = "/primaryDocumentDetails", method = RequestMethod.POST, headers = {
			"Content-type=application/json" })
	public RestResponse primaryDocumentDetails(@RequestBody JsonNode jsonNode) {

		RestResponse response = new RestResponse();
//		RestSObject user = getLoggedInUserObject();
		try {
			String id = jsonNode.get("id").textValue();
			Long sectionId = jsonNode.get("sectionId").asLong();

			SObjectLayoutSection section = sectionService.find(sectionId);

			JsonNode configuration = (section.getComponentMetadata() != null)
					? section.getComponentMetadata().get("configurations") : null;
			if (configuration != null) {
				String sObjectApiName = configuration.hasNonNull("sObjectApiName")
						? configuration.get("sObjectApiName").asText() : null;
				String trackerSObjectField = configuration.hasNonNull("trackerSObjectField")
						? configuration.get("trackerSObjectField").get("relationshipName").asText() : null;
				String attachmentIdField = configuration.hasNonNull("attachmentIdField")
						? configuration.get("attachmentIdField").get("name").asText() : null;
			
				String soql = "SELECT " + trackerSObjectField+"."+attachmentIdField + " FROM " + sObjectApiName + " WHERE Id='" + id + "'";
					
				System.out.println(soql);
				List<RestSObject> queryResults = restConnection.query(soql).getSObjects();
				if(queryResults != null && !queryResults.isEmpty()){
					String attachmentId = queryResults.get(0).getRelationshipSubObjects().get(trackerSObjectField).getField(attachmentIdField);
					response.addData("attachmentId",attachmentId);
				}

				

			}
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/viewPrimaryDocument", method = RequestMethod.GET)
	public void viewPrimaryDocument(HttpServletRequest request, HttpServletResponse response) {

		try {
			String id = request.getParameter("id");
			List<RestSObject> queryResultsAttach = getAttachmentDetails("Id", id);
			if (queryResultsAttach.size() > 0) {
				RestSObject attachment = queryResultsAttach.get(0);
				response = getAttachmentFileResponse(response, attachment.getField("Body"),
						attachment.getField("Name"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected List<RestSObject> getAttachmentDetails(String fieldName, String filedValue) {

		List<RestSObject> queryResultsAttach = null;

		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.ATTACHMENT);
			String soql = "SELECT " + queryFields + " FROM " + ESM_SObjectType.ATTACHMENT + " WHERE " + fieldName + "='"
					+ filedValue + "'";
			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return queryResultsAttach;
	}

	protected HttpServletResponse getAttachmentFileResponse(HttpServletResponse response, String body, String name) {
		try {
			HttpGet get = new HttpGet(SalesforceApiConfig.getSF_Url().split("services")[0] + body);

			get.addHeader("Authorization", "Bearer " + SalesforceApiConfig.getSF_Token());
			HttpClient c = HttpClients.createDefault();
			HttpResponse re = c.execute(get);
			HttpEntity he = re.getEntity();

			InputStream in = he.getContent();

			ByteArrayOutputStream out = new ByteArrayOutputStream();

			byte[] buf = new byte[2048];
			int n = 0;
			while (-1 != (n = in.read(buf))) {
				out.write(buf, 0, n);
			}
			out.close();
			in.close();

			byte[] responseFile = out.toByteArray();

			File path = new File(System.getProperty("java.io.tmpdir") + "/" + "download");
			if (!path.exists()) {
				path.mkdirs();
			}

			File downloadFile = new File(path + "/" + name);
			FileOutputStream fos = new FileOutputStream(downloadFile);
			fos.write(responseFile);
			fos.close();

			FileInputStream fis = null;
			fis = new FileInputStream(downloadFile);

			response.setHeader("Content-length", "");
			response.setHeader("Content-type", "application/octet-stream");

			response.setHeader("Content-Disposition", "inline; filename=\"" + name + "\"");

			response.setContentType("application/octet-stream");

			response.setContentLength((int) downloadFile.length());
			OutputStream responseOutputStream = response.getOutputStream();
			int bytes;
			while ((bytes = fis.read()) != -1) {
				responseOutputStream.write(bytes);
			}
			fis.close();
			responseOutputStream.flush();
			responseOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/sobjects", method = RequestMethod.GET)
	public RestResponse list() {
		RestResponse response = new RestResponse();
		try {
			List<SObject> sObjects = service.allActiveSObjects();
			response.addData("sObjects", sObjects);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}

}