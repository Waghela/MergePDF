package com.akritiv.esm.rest.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectField;
import com.akritiv.esm.data.entities.SObjectLayoutSection;
import com.akritiv.esm.data.services.SObjectLayoutSectionService;
import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.FieldDescription;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.SObjectDescription;

@RestController
@RequestMapping("/rest/layout-component/record-history-component")
public class RestRecordHistoryComponentController extends AbstractRestController {

	@Autowired
	SObjectService service;
	@Autowired
	SObjectLayoutSectionService sectionService;

	@RequestMapping(value = "/recordHistory", method = RequestMethod.POST, headers = {
			"Content-type=application/json" })
	public RestResponse recordHistory(@RequestBody JsonNode jsonNode) {

		RestResponse response = new RestResponse();
//		RestSObject user = getLoggedInUserObject();
		try {
			String id = jsonNode.get("id").textValue();
			Long sectionId = jsonNode.get("sectionId").asLong();

			SObjectLayoutSection section = sectionService.find(sectionId);

			JsonNode configuration = (section.getComponentMetadata() != null)
					? section.getComponentMetadata().get("configurations") : null;
			if (configuration != null) {
				String recorHistoryLookupField = configuration.hasNonNull("sObjectApiName")
						? configuration.get("sObjectApiName").asText() : null;

				String customSettingSObjectLookupApiField = configuration
						.hasNonNull("customSettingSObjectLookupApiField")
								? configuration.get("customSettingSObjectLookupApiField").get("name").asText() : null;

				List<RestSObject> resultReocordHistoryConfig = null;
				List<HistoryObject> historyObjectList = null;
				try {
					resultReocordHistoryConfig = isCustomeHistoryAssignToObject(recorHistoryLookupField, configuration);
					if (resultReocordHistoryConfig.size() > 0) {
						historyObjectList = getCustomHistoryData(
								resultReocordHistoryConfig.get(0).getField(customSettingSObjectLookupApiField), id,
								configuration);
					} else {
						historyObjectList = getStandardHistoryData(recorHistoryLookupField, id);
					}
					response.setStatus(ResponseStatus.SUCCESS);
				} catch (IOException e) {
					e.printStackTrace();
				}
				response.addData("historyData", historyObjectList);

			}
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public List<RestSObject> isCustomeHistoryAssignToObject(String object, JsonNode configuration) {

		List<RestSObject> queryResultsAttach = null;

		try {
			String customSettingSObjectApiName = configuration.hasNonNull("customSettingSObjectApiName")
					? configuration.get("customSettingSObjectApiName").asText() : null;
			String customSettingSObjectCustomHistoryStatusField = configuration
					.hasNonNull("customSettingSObjectCustomHistoryStatusField")
							? configuration.get("customSettingSObjectCustomHistoryStatusField").get("name").asText()
							: null;
			String queryFields = sObjectHelper.getAllFieldsForQuery(customSettingSObjectApiName);
			String soql = "SELECT " + queryFields + " FROM " + customSettingSObjectApiName + " WHERE Name='" + object
					+ "'  AND " + customSettingSObjectCustomHistoryStatusField + "=True";
			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();

		} catch (Exception e) {
			e.printStackTrace();

		}
		return queryResultsAttach;
	}

	public List<HistoryObject> getCustomHistoryData(String lookUpApiName, String id, JsonNode configuration) {

		List<RestSObject> queryResultsAttach = null;
		String customHistorySObjectApiName = configuration.hasNonNull("customHistorySObjectApiName")
				? configuration.get("customHistorySObjectApiName").asText() : null;

		String customHistorySObjectOldValueField = configuration.hasNonNull("customHistorySObjectOldValueField")
				? configuration.get("customHistorySObjectOldValueField").get("name").asText() : null;
		String customHistorySObjectNewValueField = configuration.hasNonNull("customHistorySObjectNewValueField")
				? configuration.get("customHistorySObjectNewValueField").get("name").asText() : null;
		String customHistorySObjectFieldLabelField = configuration.hasNonNull("customHistorySObjectFieldLabelField")
				? configuration.get("customHistorySObjectFieldLabelField").get("name").asText() : null;
		String customHistorySObjectCreatedDateField = configuration.hasNonNull("customHistorySObjectCreatedDateField")
				? configuration.get("customHistorySObjectCreatedDateField").get("name").asText() : null;
		String customHistorySObjectCreatedByIdField = configuration.hasNonNull("customHistorySObjectCreatedByIdField")
				? configuration.get("customHistorySObjectCreatedByIdField").get("name").asText() : null;
		String customHistorySObjectCreatedByIdReferenceField = customHistorySObjectCreatedByIdField.replace("__c","__r");
		List<HistoryObject> historyObjectList = new ArrayList<HistoryObject>();
		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(customHistorySObjectApiName);
			String soql = "SELECT " + queryFields + " FROM " + customHistorySObjectApiName + " WHERE "
					+ lookUpApiName + "='" + id
					+ "' Order By "+customHistorySObjectCreatedDateField+" Desc, "+customHistorySObjectCreatedByIdField;

			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();
			String lastCreatedBy = "";
			String lastCreatedDate = "";

			for (RestSObject sobject : queryResultsAttach) {

				HistoryObject historyObject = new HistoryObject();
				if (!lastCreatedDate.equals(sobject.getField(customHistorySObjectCreatedDateField))) {
					historyObject.setCreatedDate(sobject.getField(customHistorySObjectCreatedDateField));
				}

				historyObject.setFieldName(sobject.getField(customHistorySObjectFieldLabelField));
				historyObject.setOldValue(sobject.getField(customHistorySObjectOldValueField));
				historyObject.setNewValue(sobject.getField(customHistorySObjectNewValueField));

				if (!(lastCreatedDate.equals(sobject.getField(customHistorySObjectCreatedDateField)) && lastCreatedBy.equals(
						sobject.getRelationshipSubObjects().get(customHistorySObjectCreatedByIdReferenceField).getField("Name")))) {

					historyObject.setCreatedBy(
							sobject.getRelationshipSubObjects().get(customHistorySObjectCreatedByIdReferenceField).getField("Name"));
				}

				lastCreatedDate = sobject.getField(customHistorySObjectCreatedDateField);
				lastCreatedBy = sobject.getRelationshipSubObjects().get(customHistorySObjectCreatedByIdReferenceField).getField("Name");
				historyObjectList.add(historyObject);
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return historyObjectList;
	}

	public List<HistoryObject> getStandardHistoryData(String object, String id) throws IOException {

		SObjectDescription description = restConnection.describeSObject(object);

		Map<String, String> fieldsMap = new HashMap<>();
		Map<String, Boolean> isReferenceFieldsMap = new HashMap<>();
		for (FieldDescription fieldDescription : description.getFields()) {
			fieldsMap.put(fieldDescription.getName(), fieldDescription.getLabel());
			isReferenceFieldsMap.put(fieldDescription.getName(),
					fieldDescription.getType().equalsIgnoreCase("reference"));

		}

		List<RestSObject> queryResultsAttach = null;
		List<HistoryObject> historyObjectList = new ArrayList<HistoryObject>();
		object = object.replaceAll("__c", "__History");

		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(object);
			String soql = "SELECT " + queryFields + " FROM " + object + " WHERE ParentId='" + id
					+ "' Order By CreatedDate Desc, CreatedById, Id Desc";

			System.out.println(soql);
			queryResultsAttach = restConnection.query(soql).getSObjects();
			String lastCreatedBy = "";
			String lastCreatedDate = "";
			String fieldLable = "";
			boolean isRefrenceField = false;

			for (int i = 0; i < queryResultsAttach.size(); i++) {
				RestSObject sobject = queryResultsAttach.get(i);
				HistoryObject historyObject = new HistoryObject();

				if ((sobject.getField("OldValue") == null && sobject.getField("NewValue") == null
						&& !sobject.getField("Field").equals("created"))) {
					continue;
				}

				isRefrenceField = isReferenceFieldsMap.get(sobject.getField("Field")) != null
						? isReferenceFieldsMap.get(sobject.getField("Field"))
						: isReferenceFieldsMap.get(sobject.getField("Field") + "Id") != null
								? isReferenceFieldsMap.get(sobject.getField("Field") + "Id") : false;

				if (isRefrenceField) {
					sobject = queryResultsAttach.get(++i);
				}

				fieldLable = fieldsMap.get(sobject.getField("Field"));
				if (fieldLable == null) {
					fieldLable = fieldsMap.get(sobject.getField("Field") + "Id");
				}

				if (!lastCreatedDate.equals(sobject.getField("CreatedDate"))) {
					historyObject.setCreatedDate(sobject.getField("CreatedDate").toString());
				}

				historyObject.setFieldName(fieldLable);
				historyObject.setOldValue(sobject.getField("OldValue"));
				historyObject.setNewValue(sobject.getField("NewValue"));

				if (!(lastCreatedDate.equals(sobject.getField("CreatedDate")) && lastCreatedBy
						.equals(sobject.getRelationshipSubObjects().get("CreatedBy").getField("Name")))) {
					historyObject.setCreatedBy(sobject.getRelationshipSubObjects().get("CreatedBy").getField("Name"));
				}

				lastCreatedDate = sobject.getField("CreatedDate");
				lastCreatedBy = sobject.getRelationshipSubObjects().get("CreatedBy").getField("Name");
				historyObjectList.add(historyObject);

			}

		} catch (Exception e) {
			e.printStackTrace();

		}
		return historyObjectList;
	}

	class HistoryObject {
		private int id;
		private String createdBy;
		private String createdDate;
		private String fieldName;
		private String oldValue;
		private String newValue;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getOldValue() {
			return oldValue;
		}

		public void setOldValue(String oldValue) {
			this.oldValue = oldValue;
		}

		public String getNewValue() {
			return newValue;
		}

		public void setNewValue(String newValue) {
			this.newValue = newValue;
		}
	}

	/**
	 * SETTINGS
	 */

	@RequestMapping(value = "/sobjects", method = RequestMethod.GET)
	public RestResponse list() {
		RestResponse response = new RestResponse();
		try {
			List<SObject> sObjects = service.allActiveSObjects();
			response.addData("sObjects", sObjects);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}

}