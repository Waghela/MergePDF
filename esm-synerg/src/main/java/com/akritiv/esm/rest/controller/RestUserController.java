package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.security.sasl.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.config.SalesforceApiConfig;
import com.akritiv.esm.data.entities.Role;
import com.akritiv.esm.data.entities.User;
import com.akritiv.esm.data.services.UserService;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/user")
public class RestUserController extends AbstractRestController{
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/create-or-update", method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public ResponseEntity<String> createHerokuUserDetails(HttpServletRequest request, @RequestBody JsonNode userDetailArray){
		
		ResponseEntity<String> response;
		System.out.println("Encoded :: " + encode(env.getProperty("sfdc.organizationid")));
		String headerValue = request.getHeader("Authorization");
		try
		{
			if(decode(headerValue.substring(headerValue.indexOf(" ")+1, headerValue.length())).equalsIgnoreCase(SalesforceApiConfig.getSF_OrgId())){
				if(userDetailArray.isArray()){
					for(JsonNode userDetail : userDetailArray){
						User newUser = new User();
						User resultFromDB = userService.findUserBySaleforceId(userDetail.get("Id").asText().length() > 15 ? userDetail.get("Id").asText().substring(0, 15) : userDetail.get("Id").asText());
						if(resultFromDB == null){
							newUser = createAndMapUserObject(newUser, new ObjectMapper().readValue(userDetail.toString(), HashMap.class));
							newUser.setPassword("akritiv*123");
							newUser.setRole(userService.findRole(Role.ROLE_BUYER));
						}
						else{
							newUser = createAndMapUserObject(resultFromDB, new ObjectMapper().readValue(userDetail.toString(), HashMap.class));
						}
						userService.create(newUser);
					}
					response = new ResponseEntity<String>(userDetailArray.toString(), HttpStatus.OK);
				}
				else{
					throw new IOException("Invalid JSON Array Format");
				}
			}
			else{
				throw new AuthenticationException("Unable to perform rest call.");
			}
		}
		catch(AuthenticationException e)
		{
			e.printStackTrace();
			response = new ResponseEntity<String>(e.getClass().getSimpleName() +" :: "+e.getMessage(), HttpStatus.UNAUTHORIZED);
		}
		catch (IllegalArgumentException e) {
			e.printStackTrace();
			response = new ResponseEntity<String>(e.getClass().getSimpleName() +" :: "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		catch (IOException e) {
			e.printStackTrace();
			response = new ResponseEntity<String>(e.getClass().getSimpleName() +" :: "+e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	private User createAndMapUserObject(User user, Map<String, String> userDetail){
		
		user.setSaleforceId(userDetail.get("Id") != null && !userDetail.get("Id").isEmpty() ? userDetail.get("Id").length() > 15 ? userDetail.get("Id").substring(0, 15) : userDetail.get("Id") : null);
		user.setEmail(userDetail.get("Email__c") != null && !userDetail.get("Email__c").isEmpty() ? userDetail.get("Email__c") : null);
		user.setFirstName(userDetail.get("First_Name__c") != null && !userDetail.get("First_Name__c").isEmpty() ? userDetail.get("First_Name__c") : null);
		user.setLastName(userDetail.get("Last_Name__c") != null && !userDetail.get("Last_Name__c").isEmpty() ? userDetail.get("Last_Name__c") : null);
		user.setMiddleInitial(userDetail.get("Middle_Initial__c") != null && !userDetail.get("Middle_Initial__c").isEmpty() ? userDetail.get("Middle_Initial__c") : null);
		user.setStatus(userDetail.get("Status__c") != null && !userDetail.get("Status__c").isEmpty() ? userDetail.get("Status__c") : null);
		user.setLeCodes(userDetail.get("LE_Codes__c") != null && !userDetail.get("LE_Codes__c").isEmpty() ? userDetail.get("LE_Codes__c") : null);
		user.setUsername(userDetail.get("User_Name__c") != null && !userDetail.get("User_Name__c").isEmpty() ? userDetail.get("User_Name__c") : null);
		
//		user.setEnableDelegation(userDetail.get("Enable_Delegation__c") != null && !userDetail.get("Enable_Delegation__c").isEmpty() ? Boolean.parseBoolean(userDetail.get("Enable_Delegation__c")) : null);
//		user.setDelegateUserId(userDetail.get("Delegate_User__c") != null && !userDetail.get("Delegate_User__c").isEmpty() ? userDetail.get("Delegate_User__c") : null);
//		user.setSupervisorId(userDetail.get("Supervisor__c") != null && !userDetail.get("Supervisor__c").isEmpty() ? userDetail.get("Supervisor__c") : null);
		return user;
	}

	private static String decode(String string){
		return StringUtils.newStringUtf8(Base64.decodeBase64(string));
	}
	
	private static String encode(String string){
		return Base64.encodeBase64String(StringUtils.getBytesUtf8(string));
	}
	
	@RequestMapping(value = "/savePassword", method = RequestMethod.POST, headers = { "Content-type=application/json" })
	public RestResponse savePassword(@RequestBody JsonNode password) {
		RestResponse response = new RestResponse();
		try {
			String oldPassword = password.get("oldPassword").textValue();
			String newPassword = password.get("newPassword").textValue();
		
			User user = userService.findUserByUsername(getCurrentUserDetails().getUsername());
			if( user.getPassword().equals(oldPassword)){
						    user.setPassword(newPassword);	
			    userService.update(user);
			    response.setStatus(ResponseStatus.SUCCESS);
			}
			else{
				response.addError("Invalid Password", "Invalid Old Password");
				response.setStatus(ResponseStatus.FAIL);
			}
			

		} catch (NullPointerException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			response.setStatus(ResponseStatus.FAIL);
			e.printStackTrace();
		}
		
		return response;
	}
}
