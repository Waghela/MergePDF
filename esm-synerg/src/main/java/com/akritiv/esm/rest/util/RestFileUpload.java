package com.akritiv.esm.rest.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.security.core.context.SecurityContextHolder;

import com.akritiv.esm.config.security.SecurityUser;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.rest.RestConnection;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.RestSObjectImpl;
import com.palominolabs.crm.sf.rest.SaveResult;

public class RestFileUpload extends HttpServlet {
	private final Logger log = Logger.getLogger(RestFileUpload.class.getName());
	
	 private static final String SAVE_DIR = "uploadFiles";
	 private static final String nameSeprator = "__";
	 private static final String PRIMARY = "Primary";
	 private static final String SECONDARY = "Secondary";
		
	 private static String savePath;
	 private static String loggedInUserName;
	 private static String filePath;
	 private static String fileType;
	 private static String uploadedFileName;
	 private static String contextPath;
	 private static ResponseStatus status;
	 
	 public RestFileUpload() {}
	 
	 @Override
	 protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		setLoggedInUserName(((SecurityUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()); 
        setSavePath(getContextPath() + SAVE_DIR);
        setFilePath(getSavePath() + File.separator + getLoggedInUserName());
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        
        System.out.println(getFilePath());
        
        if(getLoggedInUserName() == null)
        	throw new NullPointerException();

        if (isMultipart) {

			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
//			String filename = "";
			try {

				List items = upload.parseRequest(request);
				Iterator iterator = items.iterator();

				while (iterator.hasNext()) {
					FileItem item = (FileItem) iterator.next();
					if (!item.isFormField()) {
						String fileName = item.getName();
						String ext = fileName.substring(fileName.lastIndexOf("."), fileName.length()); 
						fileName = fileName.substring(0, fileName.lastIndexOf(".")) + nameSeprator + getFileType() + nameSeprator + String.valueOf(new Timestamp(new Date().getTime()));
//						filename = new File(item.getName()).getName();
						File path = new File(getSavePath());

						if (!path.exists()) {
							path.mkdirs();
						}

						File newpath = new File(getFilePath());
						if (!newpath.exists()) {
							newpath.mkdirs();
						}
						// else {
						// if
						// (!request.getParameterMap().containsKey("uploadtype"))
						// {
						// delete(newpath);
						// newpath.mkdirs();
						// }
						// }

						File uploadedFile = new File(newpath + "/" + fileName.replace(":", "").replace(".", "").replace(" ", "") + ext);
						setUploadedFileName(fileName.replace(":", "").replace(".", "").replace(" ", "") + ext);
						System.out.println("the files uploaded are:" + uploadedFile.getAbsolutePath());
						item.write(uploadedFile);
					}
				}
				setStatus(ResponseStatus.SUCCESS);
			} catch (FileUploadException e) {
				setStatus(ResponseStatus.FAIL);
				e.printStackTrace();
			} catch (Exception e) {
				setStatus(ResponseStatus.FAIL);
				e.printStackTrace();
			}
		}
	 }

	 public SaveResult uploadFile(String parentId,String trackerId, RestConnection restConnection, List<String> uploadedFileList) throws IOException
	 {
		ArrayList<String> filePathLst = new ArrayList<String>();
		ArrayList<String> primaryFileLst = new ArrayList<String>();
		File folder = new File(getFilePath());
		File[] listOfFiles = folder.listFiles();
		String primaryFile = null;
		SaveResult saveFileResult = null;
		try 
		{
			if(uploadedFileList != null && uploadedFileList.size() > 0)
			{
				for(String uploadedFileName : uploadedFileList)
				{
					for (int i = 0; i < listOfFiles.length; i++) {
						if (listOfFiles[i].isFile() && listOfFiles[i].getName().contains(uploadedFileName)) {
							if(listOfFiles[i].getName().contains(SECONDARY)){
								filePathLst.add(listOfFiles[i].getAbsolutePath());
							}else{
								primaryFile = listOfFiles[i].getName();
								primaryFileLst.add(listOfFiles[i].getAbsolutePath());
							}
						}
					}
				}
		
				if (!filePathLst.isEmpty() && filePathLst.size() > 0){
					for (String uploadedfilepath : filePathLst) {
						saveFileResult = uploadFileOnSalesForce(uploadedfilepath, parentId, restConnection,"");
					}
				}
				if(primaryFileLst.size() > 0){
					for(String primaryFilePath : primaryFileLst){
						saveFileResult = uploadFileOnSalesForce(primaryFilePath, parentId, restConnection,"This is Primary Document");
						if(saveFileResult.isSuccess() && primaryFile != null){
							RestSObject trackerSObject = RestSObjectImpl.getNewWithId(ESM_SObjectType.TRACKER, new Id(trackerId));
							trackerSObject.setField("akritivtlm__Primary_Document_ID__c", saveFileResult.getId().getIdStr());
							trackerSObject.setField("akritivtlm__Primary_Document_Name__c", primaryFile);
							
							restConnection.update(trackerSObject);
						}
					}
				}
				
				if(saveFileResult.isSuccess()){
					setStatus(ResponseStatus.SUCCESS);
					
					// for deleting the folder and the files in folder
					String file = getFilePath();
					delete(new File(file));
				}
				else{
					throw new Exception("File Upload Failed.");
				}
			} 
		} 
		catch (Exception ex){
			ex.printStackTrace();
			setStatus(ResponseStatus.FAIL);
		}
		return saveFileResult;
	 }
	 
	 private SaveResult uploadFileOnSalesForce(String filePath, String parentId, RestConnection restConnection,String desciption) throws IOException
	 {
		File fileToBeUploaded = new File(filePath);
		RestSObject attachmentSObject = RestSObjectImpl.getNew("Attachment");
		MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		InputStream inputStream = new FileInputStream(fileToBeUploaded);
		byte[] inputBuffer = new byte[(int) fileToBeUploaded.length()];
		inputStream.read(inputBuffer);
		
		attachmentSObject.setField("Body", Base64.encodeBase64String(inputBuffer));
		String contentType = mimeTypesMap.getContentType(fileToBeUploaded);
		attachmentSObject.setField("ContentType", contentType);
		String fileName = fileToBeUploaded.getName();
		fileName = fileName
				.substring(0,
						fileName.contains("_Primary") ? fileName.indexOf("_Primary") - 1
								: fileName.contains("_Secondary") ? fileName.indexOf("_Secondary") - 1
										: fileName.indexOf("_") - 1) + fileName.substring(fileName.lastIndexOf("."));
		attachmentSObject.setField("Name", fileName.replaceAll("\\%", "").replaceAll("\\$", "").replaceAll("\\#", "").replaceAll("\\@", "").replaceAll("\\~", ""));
		attachmentSObject.setField("ParentId", parentId);
		attachmentSObject.setField("Description",desciption);
		SaveResult saveFileResult = restConnection.create(attachmentSObject);
		inputStream.close();
		return saveFileResult;
	 }
	 
	 public static void delete(File file) {
		try 
		{
			if (file != null) 
			{
				if (file.isDirectory()) 
				{
					File[] files = file.listFiles();
					for (int i = 0; i < files.length; i++)
					{
						delete(files[i]);
					}
					file.delete();
				} 
				else 
				{
					if (file.delete()) 
					{
						setStatus(ResponseStatus.SUCCESS);
					} 
					else 
					{
						throw new FileNotFoundException();
					}
				}

			}
		} 
		catch (FileNotFoundException e) 
		{
			setStatus(ResponseStatus.FAIL);
			e.printStackTrace();
		}
    }

	public static String getSavePath() {
		return savePath;
	}

	private static void setSavePath(String savePath) {
		RestFileUpload.savePath = savePath;
	}

	public static String getLoggedInUserName() {
		return loggedInUserName;
	}

	private static void setLoggedInUserName(String loggedInUserName) {
		RestFileUpload.loggedInUserName = loggedInUserName;
	}

	public static String getFilePath() {
		return filePath;
	}

	private static void setFilePath(String filePath) {
		RestFileUpload.filePath = filePath;
	}

	public static String getFileType() {
		return fileType;
	}

	public static void setFileType(String fileType) {
		RestFileUpload.fileType = fileType;
	}

	public static String getUploadedFileName() {
		return uploadedFileName;
	}

	public static void setUploadedFileName(String uploadedFileName) {
		RestFileUpload.uploadedFileName = uploadedFileName;
	}

	public static ResponseStatus getStatus() {
		return status;
	}

	public static void setStatus(ResponseStatus status) {
		RestFileUpload.status = status;
	}

	public static String getContextPath() {
		return contextPath;
	}

	public static void setContextPath(String contextPath) {
		RestFileUpload.contextPath = contextPath;
	}

}
