package com.akritiv.esm.rest.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.rest.util.RestFileUpload;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.fasterxml.jackson.databind.JsonNode;

@RestController
@RequestMapping("/rest/component")
public class RestComponentController extends AbstractRestController{

	final RestFileUpload fileUpload = new RestFileUpload();
	final String PRIMARY = "Primary";
	final String SECONDARY = "Secondary";
	
	@RequestMapping(value = "/search",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse searchComponent(@RequestBody JsonNode component){
		RestResponse response = new RestResponse();
		if(component!=null && component.hasNonNull("componentId")){
			response.setStatus(ResponseStatus.SUCCESS);
			response.addData("SearchComponent", customComponentHelper.getSearchComponent(component.get("componentId").textValue()));
		}else{
			response.addError("NoSuchSearchComponentFound", "Invalid search component id !!");
		}
		return response;
	}
	
	@RequestMapping(value = "/attachment",method = RequestMethod.GET)
	public RestResponse createSupplierMaintenance(){
		RestResponse response = new RestResponse();
		response.setStatus(ResponseStatus.SUCCESS);
		return response;
	}
	
	@RequestMapping(value = "/upload")
	public RestResponse uploadFile(HttpServletRequest request, HttpServletResponse res){
		return upload(request, res, SECONDARY);
	}
	
	@RequestMapping(value = "/uploadPrimary")
	public RestResponse uploadPrimaryFile(HttpServletRequest request, HttpServletResponse res){
		return upload(request, res, PRIMARY);
	}

	private RestResponse upload(HttpServletRequest request, HttpServletResponse res, String fileType){
		RestResponse response = new RestResponse();
		
		try {
			fileUpload.setFileType(fileType);
			fileUpload.setContextPath(request.getServletContext().getRealPath(""));
			fileUpload.service(request, res);
			response.setStatus(fileUpload.getStatus());
			response.addData("fileName", fileUpload.getUploadedFileName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/deleteFile", method = RequestMethod.POST)
	public RestResponse deleteFile(@RequestBody JsonNode fileData){
		RestResponse response 	= new RestResponse();
		if(!fileData.hasNonNull("fileName"))
		{
			response.setStatus(ResponseStatus.SUCCESS);
			return response;
		}
		String filePath = fileUpload.getFilePath() + File.separator + fileData.get("fileName").asText();;
		File fileToBeDeleted 	= new File(filePath);
		try {
			fileUpload.delete(fileToBeDeleted);
			response.setStatus(fileUpload.getStatus());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}
