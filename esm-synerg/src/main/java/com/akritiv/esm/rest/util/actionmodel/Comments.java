package com.akritiv.esm.rest.util.actionmodel;

public class Comments{
	String apiname;
	String label;
	boolean required;
	String value;
	String historyValue;
	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getHistoryValue() {
		return historyValue;
	}
	public void setHistoryValue(String historyValue) {
		this.historyValue = historyValue;
	}
}