package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.Role;
import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectField;
import com.akritiv.esm.data.entities.SObjectLayout;
import com.akritiv.esm.data.entities.User;
import com.akritiv.esm.data.services.SObjectLayoutService;
import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.data.services.UserService;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.rest.util.RestSObjectMetadata;
import com.akritiv.esm.rest.util.actionmodel.RestRequestModel;
import com.akritiv.esm.util.CommonFunctions;
import com.akritiv.esm.util.ESM_SObjectType;
import com.akritiv.esm.util.SearchComponent;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.rest.ApiException;
import com.palominolabs.crm.sf.rest.RestQueryResult;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.RestSObjectImpl;
import com.palominolabs.crm.sf.rest.SaveResult;

@RestController
@RequestMapping("/rest/invoice")
public class RestInvoiceController extends AbstractRestController{
	
	@Autowired
	SObjectLayoutService service;
	@Autowired
	SObjectService sObjectService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/search",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse searchInvoices(@RequestBody JsonNode componentNode){
		RestResponse response = new RestResponse();
		RestQueryResult result = null;
		try {
			System.err.println("COMPONENT PARAM :: " + componentNode.size());
			SearchComponent component = SearchComponent.parseSearchComponent(componentNode);
			System.err.println("JSON REQUEST :: " + component.toString());
			
			System.err.println("SEARCHING.........");
			User currentUser = userService.findUserByUsername( getCurrentUserDetails().getUsername());
			
//			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.INVOICE);
			String whereClause = component.loadWhereFilterQueryString(sObjectService);
			String soql= "SELECT Id,Name,Invoice_Number__c,LE_Code_Text__c,Invoice_Type_Temp__c,Supplier_Name_formula__c,Akritiv_ESM__Total_Amount__c,Payment_Date__c,Payment_Document_No_Check_No__c,Invoice_Recieved_Date__c,Akritiv_ESM__Status__c" + 
//			String soql= "SELECT " + queryFields +
					" FROM " + ESM_SObjectType.INVOICE + 
					" " + whereClause + getConditionalWhereClause(currentUser, whereClause);
//					" LIMIT 2000";
					
			System.out.println("SEARCH INVOICE :: "+soql);
			result = restConnection.query(soql);
			
			response.setStatus(ResponseStatus.SUCCESS);
			response.addData("searchResult", result.getSObjects());
		} catch (IOException e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.err.println("RestInvoicesController->searchInvoices() :: " + result.getTotalSize() + " RECORDS FOUND...");
		return response;
	}
	
	private String getConditionalWhereClause(User user, String whereClause){
		if(!user.getRole().getRoleName().equalsIgnoreCase(Role.ROLE_ADMIN)){
			return (whereClause!= null && !whereClause.trim().equals("") ? " AND LE_Code_Text__c IN("+ user.getLeCodesAsStringArray()+")" : " WHERE LE_Code_Text__c IN("+ user.getLeCodesAsStringArray()+")" );
		}
		return "";
	}
	
	@RequestMapping(value = "/toapprove",method = RequestMethod.GET)
	public List<RestSObject> invoicesToApprove(){
		
		RestQueryResult result = null;
		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.INVOICE);
//			String soql= "SELECT "+queryFields+"  FROM "+ESM_SObjectType.INVOICE+" WHERE akritivesm__Current_Approver__r.akritivesm__Email__c='"+ getCurrentUserDetails().getUsername() + "' AND akritivesm__Current_State__c IN ('Pending Approval')";
			String soql= "SELECT " + queryFields + 
					" FROM " + ESM_SObjectType.INVOICE + 
					" WHERE akritivesm__Current_Approver__r.akritivesm__Email__c='"+ getCurrentUserDetails().getUsername() + 
					"' AND akritivesm__Current_State__c IN ('Pending Approval','Parked Invoice','Awaiting Approval')";
			System.out.println(soql);
			result = restConnection.query(soql);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println("RestInvoicesController->invoicesToApprove() :: " +result.getTotalSize() + " RECORDS FOUND...");
		return result.getSObjects();
	}

	@RequestMapping(value = "/pendingapproval/toapprove",method = RequestMethod.GET)
	public List<RestSObject> pendingApprovalToApprove(){
		
		RestQueryResult result = null;
		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.INVOICE);
			String soql= "SELECT " + queryFields + 
					" FROM " + ESM_SObjectType.INVOICE + 
					" WHERE akritivesm__Current_Approver__r.akritivesm__Email__c='"+ getCurrentUserDetails().getUsername() + 
					"' AND akritivesm__Current_State__c IN ('Pending Approval','Awaiting Approval')";
			System.out.println(soql);
			result = restConnection.query(soql);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println("RestInvoicesController->pendingApprovalToApprove() :: " +result.getTotalSize() + " RECORDS FOUND...");
		return result.getSObjects();
	}
	
	@RequestMapping(value = "/parkedinvoices/toapprove",method = RequestMethod.GET)
	public List<RestSObject> parkedInvoicesToApprove(){
		
		RestQueryResult result = null;
		try {
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.INVOICE);
			String soql= "SELECT " + queryFields + 
					" FROM " + ESM_SObjectType.INVOICE + 
					" WHERE akritivesm__Current_Approver__r.akritivesm__Email__c='"+ getCurrentUserDetails().getUsername() + 
					"' AND akritivesm__Current_State__c IN ('Parked Invoice')";
			System.out.println(soql);
			result = restConnection.query(soql);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.err.println("RestInvoicesController->parkedInvoicesToApprove() :: " +result.getTotalSize() + " RECORDS FOUND...");
		return result.getSObjects();
	}
	
	@RequestMapping(value = "/details/{id}",method = RequestMethod.GET)
	public RestResponse details(@PathVariable("id") String invoiceId){
		RestResponse response = new RestResponse();
		RestSObject invoiceDetails = null;
		
		try {
//			RestSObject user = getLoggedInUserObject();
			
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.INVOICE);//,"akritivesm__Tracker_Id__r");
		
			String soql = "SELECT " + 
							queryFields +
						 " FROM "+
							ESM_SObjectType.INVOICE +
						 " WHERE Id='" + invoiceId + "'";
			System.err.println(soql);
			
			List<RestSObject> soqlResult = restConnection.query(soql).getSObjects();
			if(soqlResult.size() > 0){
				invoiceDetails = soqlResult.get(0); //restConnection.query(soql).getSObjects().get(0);
				response.addData("invoiceDetails", invoiceDetails);
				Map<String, String> dataModel = sObjectHelper.createDataModel(invoiceDetails);
				response.addData("dataModel", dataModel);
				response.setStatus(ResponseStatus.SUCCESS);
			}else{
				response.addError("NoSuchRecordFoundException", "No such invoice found!");
//				response.addError("Error", "Invalid invoice id '" + invoiceId + "'!");
			}
		}catch(ApiException e1){
			response.addError(e1.getClass().getSimpleName(), "Invalid invoice id '" + invoiceId + "'!");
			e1.printStackTrace();
		}catch(IOException e2){
			response.addError(e2.getClass().getName(), e2.getMessage());
			e2.printStackTrace();
		}catch (Exception e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	private String invoiceNames(RestSObject invoice) throws IOException{
		// Select Id, (Select Id,akritivesm__Purchase_Order__r.Name From akritivesm__InvoicePOMatrix__r) From akritivesm__Invoice__c
		StringBuilder builder = new StringBuilder();
		boolean isPo = (invoice.getField("akritivesm__Document_Type__c").equals("PO Invoice")) ? true : false;
		if(isPo){
			String soql = "SELECT Id, "
					+ "		( SELECT Id, "
					+ "				 akritivesm__Purchase_Order__r.Name "
					+ "		  FROM akritivesm__InvoicePOMatrix__r) "
					+ "	   FROM akritivesm__Invoice__c "
					+ "	   WHERE Id = '" + invoice.getId().getFullId() + "'";
			System.err.println(soql);
			List<RestSObject> soqlResult = restConnection.query(soql).getSObjects();
			if(soqlResult.size() > 0){
				
				RestSObject object = soqlResult.get(0);
				if(object.getRelationshipQueryResults()!=null && object.getRelationshipQueryResults().size() > 0 && object.getRelationshipQueryResults().containsKey("akritivesm__InvoicePOMatrix__r")){
					List<RestSObject> invoices = object.getRelationshipQueryResults().get("akritivesm__InvoicePOMatrix__r").getSObjects();
					int i = 1;
					for (RestSObject inv : invoices) {
						String invName = inv.getRelationshipSubObjects().get("akritivesm__Purchase_Order__r").getField("Name");
						builder.append(invName);
						if(i < invoices.size()){
							i++;
							builder.append(",\n");
						}
					}
				}
			}
		}
		return builder.toString();
	}
	
	private Map<String, String> invoiceDataModel(RestSObject invoice) throws Exception{
		Map<String, String> dataModel = new HashMap<String, String>();
		SObject sObject = sObjectService.findByName(ESM_SObjectType.INVOICE);
		
		for (SObjectField field :sObject.getFields()) {
			dataModel.put(field.getName(), invoice.getField(field.getName()));
			if(field.getType().equalsIgnoreCase("reference")){
//				dataModel.put(field.getName()+"__Name", invoice.get);
			}
		}
		return dataModel;
	}
	
	@RequestMapping(value = "/metadata",method = RequestMethod.GET)
	public RestResponse metadata(){
		RestResponse response = new RestResponse();
		try{
			SObjectLayout sObjectLayoutDetails = service.findBySObjectApiName(ESM_SObjectType.INVOICE,true);
			SObject sObject = sObjectService.findByName(ESM_SObjectType.INVOICE);
			response.addData("sObject", sObject);
			response.addData("sObjectLayout", sObjectLayoutDetails);
			Map<String, String> dataModel = new HashMap<String, String>();
			for (SObjectField field :sObject.getFields()) {
				dataModel.put(field.getName(), null);
				if(field.getType().equalsIgnoreCase("reference")){
					dataModel.put(field.getName()+"__Name", null);
				}
			}
			response.addData("dataModel", dataModel);
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	private RestSObjectMetadata getInvoiceMetadata(RestSObject invoice){
		SObjectLayout sObjectLayoutDetails = service.findBySObjectApiName(invoice.getType());
		SObject sObject = sObjectService.findByName(invoice.getType());
		RestSObjectMetadata metadata = new RestSObjectMetadata(sObject, sObjectLayoutDetails);
		return metadata;
	}
	
	@RequestMapping(value = "/save",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse saveInvoiceDetails(@RequestBody RestRequestModel model){
		RestResponse response = new RestResponse();
		
		try {
			RestSObject restSObject = sObjectHelper.createRestSObject(model, getLoggedInUserObject());
			restConnection.update(restSObject);
			response.addData("restSObject", restSObject);
			response.addData("restRequestModel", model);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		
		return response;
	}
	
	/*
	 * REQUEST METHOD : /loadinvoicelineitems
	 */
	@RequestMapping(value = "/loadinvoicelineitems",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse loadInvoiceLineItems(@RequestBody JsonNode invoice){
		RestResponse response = new RestResponse();
		RestSObject restInvoice = sObjectHelper.createRestSObject(invoice);
		
		try {
			response.addData("invoiceLineItems", invoiceLineItems(restInvoice.getId().getFullId()));
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		
		return response;
	}
	
	private List<RestSObject> invoiceLineItems(String invoiceId) throws IOException{
		String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.INVOICE_LINE_ITEM);
		String soql = "SELECT " + queryFields + 
				     " FROM " + ESM_SObjectType.INVOICE_LINE_ITEM + 
				     " WHERE akritivesm__Invoice__c='" + invoiceId + "'";
		
		List<RestSObject> invoiceLineItems = restConnection.query(soql).getSObjects();
		return invoiceLineItems;
	}
	
	@RequestMapping(value = "/submit",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse submitInvoice(@RequestBody JsonNode invoice){
		RestResponse response = new RestResponse();
		
		RestSObject restInvoice = sObjectHelper.createRestSObject(invoice);
		
		// SET CURRENT STATE 
		restInvoice.setField("akritivesm__Current_State__c", 
				invoice.get("allFields").get("akritivesm__Current_State__c").textValue());
		
		JsonNode CostAllocationItems = (invoice.hasNonNull("CostAllocationItems")) ? invoice.get("CostAllocationItems") : null;
		JsonNode costAllocationInvoiceLineItems = (invoice.hasNonNull("costAllocationInvoiceLineItems")) ? invoice.get("costAllocationInvoiceLineItems") : null;
		
		Map<String, String> whereFieldsMap =new HashMap<String, String>();
		whereFieldsMap.put("akritivesm__Email__c", "'" + getCurrentUserDetails().getUsername() + "'");
		RestSObject user = sObjectHelper.getRecordsUsingWhere(ESM_SObjectType.BUYER_USER, whereFieldsMap).get(0);
		
		String newComment = "",oldComment="";
		if(invoice.has("New_Comments__c") && !invoice.get("New_Comments__c").textValue().isEmpty()){
//			JsonNode comment = invoice.get("allFields").get("akritivesm__Comments__c"); 			// akritivesm__Comments__c
			JsonNode comment = invoice.get("allFields").get("akritivesm__Comment_History__c"); 		// akritivesm__Comment_History__c
			oldComment = (comment.textValue()!=null) ? comment.textValue() : "";
			newComment = invoice.get("New_Comments__c").textValue()
					+ " by "
					+ user.getField("Name")
					+ " on "
					+ CommonFunctions.getAmericanNewYorkTimeComments()
					+ "\r\n" + " " + oldComment; // SET NEW COMMENT
		}
		
//		restInvoice.setField("akritivesm__Comments__c", newComment);							// SET COMMENT  
		restInvoice.setField("akritivesm__Comment_History__c", newComment);						// SET COMMENT HISTORY 
		restInvoice.setField("akritivesm__Last_Modified_By__c", user.getId().getIdStr());		// SET LAST MODIFIED BY FIELD
		
		if(invoice.get("useraction").textValue().equalsIgnoreCase("Approve")){
			restInvoice.setField("akritivesm__User_Actions__c", "Approve"); 		// SET USER ACTION "STATUS"
		}else if(invoice.get("useraction").textValue().equalsIgnoreCase("Resolve")){
			restInvoice.setField("akritivesm__User_Actions__c", "Resolve"); 				// SET USER ACTION "STATUS"
		}else if(invoice.get("useraction").textValue().equalsIgnoreCase("Reject")){
			restInvoice.setField("akritivesm__User_Actions__c", "Reject"); 			// SET USER ACTION "REJECT"
			restInvoice.setField("akritivesm__Reject_Reasons__c", 
					invoice.get("rejectionReason").textValue()); 					// SET USER ACTION "REJECTION REASON"
		}
		
		/**********************************/
		try {
			if (CostAllocationItems!=null){
				//submitCostAllocationItems(CostAllocationItems,restInvoice.getId().getIdStr());
				response.addData("CostAllocationItems", CostAllocationItems);
			}
			if (costAllocationInvoiceLineItems!=null){
				List<RestSObject> submittedCostAllocationInvoiceLineItems 
					= submitCostAllocationInvoiceLineItems(costAllocationInvoiceLineItems,restInvoice.getId().getIdStr());
				response.addData("submittedCostAllocationInvoiceLineItems", submittedCostAllocationInvoiceLineItems);
			}
			restConnection.update(restInvoice);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		/**********************************/
//		response.setStatus(ResponseStatus.SUCCESS);
		response.addData("submittedInvoice", restInvoice);
		return response;
	}
	
//	private List<RestSObject> submitCostAllocationInvoiceLineItems(JsonNode costAllocationInvoiceLineItems, String invoiceId) throws IOException,Exception{
//		List<RestSObject> submittedCostAllocationInvoiceLineItems = null;
//		if(costAllocationInvoiceLineItems.isArray()){
//			submittedCostAllocationInvoiceLineItems = new ArrayList<RestSObject>();
//			for (JsonNode item : costAllocationInvoiceLineItems) {
//				RestSObject lineItem = RestSObjectImpl.getNew(ESM_SObjectType.INVOICE_LINE_ITEM);
//				lineItem.setField("akritivesm__Invoice__c", invoiceId);
//				lineItem.setField("akritivesm__Amount__c", String.valueOf(item.get("amount").asDouble(0)));
//				lineItem.setField("akritivesm__Cost_Code__c", item.get("costCode").get("id").textValue());
//				lineItem.setField("akritivesm__GL_Code__c", item.get("glCode").get("id").textValue());
//				
//				SaveResult result = restConnection.create(lineItem);
//				if(!result.isSuccess())
//					throw new Exception(result.getErrors().get(0).getMessage());
//				else
//					submittedCostAllocationInvoiceLineItems.add(lineItem);
//			}
//		}
//		return submittedCostAllocationInvoiceLineItems;
//	}

	private void submitCostAllocationItems(JsonNode node,String invoiceId) throws IOException,Exception{
		if(node.isArray()){
			for (JsonNode jsonNode : node) {
				RestSObject costAllocation = RestSObjectImpl.getNew(ESM_SObjectType.COST_ALLOCATION);
				costAllocation.setField("akritivesm__Invoice__c", invoiceId);
				costAllocation.setField("akritivesm__Invoice_Line_Item__c", jsonNode.get("invoiceLineItemId").textValue());
				costAllocation.setField("akritivesm__Cost_Code__c", jsonNode.get("costCode").get("id").textValue());
				costAllocation.setField("akritivesm__GL_Code__c", jsonNode.get("glCode").get("id").textValue());
				costAllocation.setField("akritivesm__GL_Code__c", jsonNode.get("glCode").get("id").textValue());
				costAllocation.setField("akritivesm__Total_Amount__c", jsonNode.get("totalAllocatedAmt").textValue());
				
				SaveResult result = restConnection.create(costAllocation);
				if(!result.isSuccess())
					throw new Exception(result.getErrors().get(0).getMessage());
			}
		}
	}
	
	/*
	 * REQUEST METHOD : /savenonpolineitems
	 */
	@RequestMapping(value = "/savenonpolineitems",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse saveNonPoLineItems(@RequestBody JsonNode request){
		RestResponse response = new RestResponse();
		
		try {
			JsonNode costAllocationInvoiceLineItems = (request.hasNonNull("costAllocationInvoiceLineItems")) ? request.get("costAllocationInvoiceLineItems") : null;
			String invoiceId = (request.hasNonNull("invoiceId")) ? request.get("invoiceId").textValue() : null;
			if(costAllocationInvoiceLineItems!=null && invoiceId!=null){
				List<RestSObject> submittedCostAllocationInvoiceLineItems 
					= submitCostAllocationInvoiceLineItems(costAllocationInvoiceLineItems,invoiceId);
			
				response.addData("submittedCostAllocationInvoiceLineItems", submittedCostAllocationInvoiceLineItems);
				response.addData("costAllocationInvoiceLineItems", costAllocationInvoiceLineItems);
				response.setStatus(ResponseStatus.SUCCESS);
			}else{
				response.addError("Error", "Invalid data for cost allocation!");
			}
		} catch (IOException e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		
		return response;
	}
	
	private List<RestSObject> submitCostAllocationInvoiceLineItems(JsonNode costAllocationInvoiceLineItems, String invoiceId) throws IOException,Exception{
		List<RestSObject> submittedCostAllocationInvoiceLineItems = null;
		if(costAllocationInvoiceLineItems.isArray()){
			submittedCostAllocationInvoiceLineItems = new ArrayList<RestSObject>();
			for (JsonNode item : costAllocationInvoiceLineItems) {
				
				RestSObject lineItem = RestSObjectImpl.getNew(ESM_SObjectType.INVOICE_LINE_ITEM);
				lineItem.setField("akritivesm__Invoice__c", 	invoiceId);
				
				String id = (item.hasNonNull("id")) ? item.get("id").textValue() : null;
				boolean isRemoved = (item.hasNonNull("isRemoved")) ? item.get("isRemoved").asBoolean() : false;
				boolean isPersisted = (item.hasNonNull("isPersisted")) ? item.get("isPersisted").asBoolean() : false;
				
				if(id!=null && isRemoved && isPersisted){
					Id sfdcId = new Id(id);
					lineItem.setField("Id", id);
					restConnection.delete(ESM_SObjectType.INVOICE_LINE_ITEM, sfdcId);
				}else if(!isPersisted){
					lineItem.setField("akritivesm__Amount__c", 		String.valueOf(item.get("amount").asDouble(0)));
					lineItem.setField("akritivesm__Cost_Code__c", 	item.get("costCode")	.get("id").textValue());
					lineItem.setField("akritivesm__GL_Code__c", 	item.get("glCode")		.get("id").textValue());
					
					SaveResult result = restConnection.create(lineItem);
					if(!result.isSuccess())
						throw new Exception(result.getErrors().get(0).getMessage());
				}
				submittedCostAllocationInvoiceLineItems.add(lineItem);
			}
		}
		return submittedCostAllocationInvoiceLineItems;
	}
}
