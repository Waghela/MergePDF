package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.RestSObject;

@RestController
@RequestMapping("/rest/buyeruser")
public class RestBuyerUserController extends AbstractRestController{

	@RequestMapping(value = "/search",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse search(@RequestBody JsonNode approverUserModel){
		RestResponse response = new RestResponse();
		
		try {
			String searchParam = approverUserModel.get("searchStr").textValue();
			String queryFields = sObjectHelper.getAllFieldsForQuery(ESM_SObjectType.BUYER_USER); 
			String soql = "SELECT " + queryFields + " FROM " + ESM_SObjectType.BUYER_USER ;
			if(!searchParam.isEmpty() || searchParam != null)
				soql += " WHERE (	Name 				LIKE '%"+searchParam+"%' "
						  + "OR 	akritivesm__Email__c 			LIKE '%"+searchParam+"%' "
						  + "OR 	akritivesm__First_Name__c		LIKE '%"+searchParam+"%' "
						  + "OR 	akritivesm__Last_Name__c		LIKE '%"+searchParam+"%' )"
						  + "AND 	akritivesm__User_Type__c		= 'Requestor'" 
						  + "AND 	akritivesm__Email__c			!='"+getCurrentUserDetails().getUsername()+"'";
						  
			System.err.println(soql);
			List<RestSObject> buyerUsers = restConnection.query(soql).getSObjects();
			
			response.addData("buyerUsers", buyerUsers);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
}
