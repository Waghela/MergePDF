package com.akritiv.esm.rest.util.actionmodel;

public class CurrentState{
	String apiname;
	String value;
	public String getApiname() {
		return apiname;
	}
	public void setApiname(String apiname) {
		this.apiname = apiname;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}