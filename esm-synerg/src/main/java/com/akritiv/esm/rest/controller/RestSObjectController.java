package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.SObject;
import com.akritiv.esm.data.entities.SObjectField;
import com.akritiv.esm.data.repositories.SObjectRepository;
import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.rest.util.KeyValuePOJO;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.palominolabs.crm.sf.rest.DescribeGlobalResult;
import com.palominolabs.crm.sf.rest.FieldDescription;
import com.palominolabs.crm.sf.rest.GlobalSObjectDescription;
import com.palominolabs.crm.sf.rest.SObjectDescription;

@RestController
@RequestMapping("/rest/admin/sobject")
public class RestSObjectController extends AbstractRestController{
	
	@Autowired
	SObjectService service;

	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public RestResponse list(){
		RestResponse response = new RestResponse();
		try {
			List<SObject> sObjects = service.findAll();
			response.addData("sObjects", sObjects);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
	
	@RequestMapping(value = "/sfdcobjects",method = RequestMethod.GET)
	public RestResponse sfdcobjects(){
		RestResponse response = new RestResponse();
		List<SObject> sfdcSObjects = new ArrayList<SObject>();
		try {
			DescribeGlobalResult globalResult = restConnection.describeGlobal();
			List<GlobalSObjectDescription> sObjectDescriptions = globalResult.getBasicSObjectMetadatas();
			if(sObjectDescriptions!=null){
				for (GlobalSObjectDescription desc : sObjectDescriptions) {
//					if(desc.isLayoutable() && !desc.isCustomSetting()){
					if(desc.isLayoutable()){
						SObject object = new SObject();
						object.setActive(true);
						object.setCustom(desc.isCustom());
						object.setCustomSetting(desc.isCustomSetting());
						object.setKeyPrefix(desc.getKeyPrefix());
						object.setLabel(desc.getLabel());
						object.setLabelPlural(desc.getLabelPlural());
						object.setName(desc.getName());
						sfdcSObjects.add(object);
					}
				}
			}
			response.addData("sfdcSObjects", sfdcSObjects);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
	
	@RequestMapping(value = "/sfdcobjectfields",method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public RestResponse sfdcSObjectFields(@RequestBody JsonNode sfdcSObjectNames){
		RestResponse response = new RestResponse();
		try {
			if(sfdcSObjectNames.isArray() && sfdcSObjectNames.size() > 0){
				Map<String, List<KeyValuePOJO>> sfdcSObjectFields = new LinkedHashMap<String, List<KeyValuePOJO>>();
				String sfdcSObjectType = "";
				for (JsonNode sfdcSObjectName : sfdcSObjectNames) {
					sfdcSObjectType = sfdcSObjectName.textValue();
					SObjectDescription description = restConnection.describeSObject(sfdcSObjectType);
//					sfdcSObjectFields.put(sfdcSObjectType, convertFieldListToMap(description.getFields()));
					sfdcSObjectFields.put(sfdcSObjectType, convertToKeyValuePOJOList(description.getFields()));
				}
				response.addData("sfdcSObjectFields", sfdcSObjectFields);
				response.setStatus(ResponseStatus.SUCCESS);
			}
		} catch (IOException e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
	private List<KeyValuePOJO> convertToKeyValuePOJOList(List<FieldDescription> fields){
		List<KeyValuePOJO> pojos = new ArrayList<KeyValuePOJO>();
		for (FieldDescription field : fields) {
			if(!field.getName().equalsIgnoreCase("Id")){
				String fieldLabel = (!field.isCustom() && field.getType().equalsIgnoreCase("reference")) ? field.getLabel().replace("ID", "").trim() : field.getLabel();
				pojos.add(new KeyValuePOJO(field.getName(), fieldLabel));
			}
		}
		return pojos;
	}
	
	
	@RequestMapping(value = "/save",method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public RestResponse save(@RequestBody JsonNode dataToSave){
		RestResponse response = new RestResponse();
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			List<SObject> objectsToSave 	= mapper.readValue(
													dataToSave.get("objectsToSave").toString(), 
													TypeFactory.defaultInstance().constructCollectionType(List.class, SObject.class));
			List<SObject> objectsToDelete 	= mapper.readValue(
													dataToSave.get("objectsToDelete").toString(), 
													TypeFactory.defaultInstance().constructCollectionType(List.class, SObject.class));
			
			response.addData("deletedSObjects", deleteSObjects(objectsToDelete));
			response.addData("savedSObjects", saveSObjects(objectsToSave));
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	private List<SObject> deleteSObjects(List<SObject> sObjects){
		service.delete(sObjects);
		return sObjects;
	}
	private List<SObject> saveSObjects(List<SObject> sObjects) throws IOException{
		List<SObject> result = new ArrayList<SObject>();
		for (SObject sObject : sObjects) {
			if(sObject.getId() == null){
				SObjectDescription desc = restConnection.describeSObject(sObject.getName());
				sObject.setActive(true);
				sObject.setCustom(desc.isCustom());
				sObject.setCustomSetting(desc.isCustomSetting());
				sObject.setKeyPrefix(desc.getKeyPrefix());
				sObject.setLabel(desc.getLabel());
				sObject.setLabelPlural(desc.getLabelPlural());
				sObject.setName(desc.getName());
				
				SObject savedSObject = service.save(sObject); 

				List<SObjectField> fields = savedSObject.generateFields(desc.getFields());
				List<SObjectField> savedFields = service.saveSObjectFields(fields);
				
				savedSObject.setFields(savedFields);
				
				result.add(savedSObject);
			}
		}
		return result;
	}
	
}
