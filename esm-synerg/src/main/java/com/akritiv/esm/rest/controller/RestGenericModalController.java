package com.akritiv.esm.rest.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.RestSObject;

@RestController
@RequestMapping("/rest/genericModal")
public class RestGenericModalController extends AbstractRestController{
	
	@RequestMapping(value = "/search",method = RequestMethod.POST,headers = {"Content-type=application/json"})
	public RestResponse search(@RequestBody JsonNode genericModal){
		RestResponse response = new RestResponse();
		
		try {
			String searchParam = genericModal.get("searchStr").textValue();
			String searchObjectName = genericModal.get("referenceTo").textValue().split(",")[0];
			String soql = "SELECT ID, Name, Owner.Name FROM " + searchObjectName + getWhereClause(searchParam, searchObjectName) + " LIMIT 200";
			 
			System.err.println(soql);
			List<RestSObject> searchResult = restConnection.query(soql).getSObjects();
			
			response.addData("searchResult", searchResult);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (IOException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
			response.addError(e.getClass().getName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	private String getWhereClause(String searchParam, String sObjectName){
		String whereClause = "";
		if(searchParam != null && !searchParam.isEmpty())
			whereClause += " WHERE Name LIKE '%"+searchParam+"%' AND "+ getWhereForBuyerUser(sObjectName);
		else
			whereClause += getWhereForBuyerUser(sObjectName);
		return whereClause;
	}
	private String getWhereForBuyerUser(String sObjectName){
		String whereClause = "";
		if(sObjectName.equalsIgnoreCase(ESM_SObjectType.BUYER_USER)){
			whereClause = " WHERE " + "akritivesm__Email__c !='"+getCurrentUserDetails().getUsername()+"'";
		}
		return whereClause;
	}
}
