package com.akritiv.esm.rest.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class RestResponse {
	String status;
	Map<String, Object> data;
	List<RestError> errors;
	
	public RestResponse(){
		super();
		this.data = new HashMap<String, Object>();
		this.errors = new ArrayList<RestError>();
		this.status = ResponseStatus.FAIL.name();
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(ResponseStatus status) {
		this.status = status.name();
	}
	
	public RestResponse addData(String key, Object value){
		data.put(key, value);
		return this;
	}
	public RestResponse addError(String errorType, String errorMessage){
		errors.add(new RestError(errorType, errorMessage));
		return this;
	}
	
	public Map<String, Object> getData(){
		return this.data;
	}
	public List<RestError> getErrors(){
		return this.errors;
	}
	
	public enum ResponseStatus{
		SUCCESS,
		FAIL
	}
	public class RestError {
		String type;
		String message;
		
		public RestError(String type, String message) {
			super();
			this.type = type;
			this.message = message;
		}

		public String getType() {
			return type;
		}

		public String getMessage() {
			return message;
		};
	}
}
