package com.akritiv.esm.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.entities.MyTaskSection;
import com.akritiv.esm.data.services.MyTaskSectionService;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/rest/admin/mytasksection")
public class RestMyTaskSectionController extends AbstractRestController {

	@Autowired
	MyTaskSectionService service;
	
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public RestResponse list(){
		RestResponse response = new RestResponse();
		try {
			List<MyTaskSection> myTaskSections = service.findAll();
			response.addData("myTaskSections", myTaskSections);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/details/{id}",method = RequestMethod.GET)
	public RestResponse details(@PathVariable("id") Long sectionId){
		RestResponse response = new RestResponse();
		try {
			MyTaskSection myTaskSectionDetails = service.find(sectionId);
			response.addData("myTaskSectionDetails", myTaskSectionDetails);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
	
	@RequestMapping(value = "/save",method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public RestResponse save(@RequestBody MyTaskSection section){
		RestResponse response = new RestResponse();
		
		try{
			MyTaskSection savedMyTaskSection = service.save(section);
			response.addData("savedMyTaskSection", savedMyTaskSection);
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		
		return response;
	}
	
	@RequestMapping(value = "/delete",method = RequestMethod.POST, headers = {"Content-type=application/json"})
	public RestResponse delete(@RequestBody Long sectionId){
		RestResponse response = new RestResponse();
		
		try{
			MyTaskSection deletedMyTaskSection = service.find(sectionId);
			if(deletedMyTaskSection!=null){
				service.deleteMyTaskSection(sectionId);
				response.addData("deletedMyTaskSection", deletedMyTaskSection);
				response.setStatus(ResponseStatus.SUCCESS);
			}else{
				response.addError("NoSuchSectionFound", "Invalid MyTask Section to Delete...!");
			}
		} catch (Exception e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		
		return response;
	}
}
