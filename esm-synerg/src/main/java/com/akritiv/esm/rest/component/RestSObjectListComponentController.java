package com.akritiv.esm.rest.component;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akritiv.esm.data.services.SObjectService;
import com.akritiv.esm.rest.controller.AbstractRestController;
import com.akritiv.esm.rest.util.RestResponse;
import com.akritiv.esm.rest.util.RestResponse.ResponseStatus;
import com.akritiv.esm.util.ESM_SObjectType;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.FieldDescription;
import com.palominolabs.crm.sf.rest.RestSObject;

@RestController 
@RequestMapping("/rest/layout-component/sobject-list-component")
public class RestSObjectListComponentController extends AbstractRestController{
	
	@Autowired
	SObjectService sObjectService;

	
	@RequestMapping(value = "/sobjects",method = RequestMethod.GET)
	public RestResponse sObjects(){
		RestResponse response = new RestResponse();
		try{
			response.addData("sObjects", sObjectService.allActiveSObjects());
			response.setStatus(ResponseStatus.SUCCESS);
		}catch(Exception e){
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/lineitems",method = RequestMethod.POST)
	public RestResponse lineItems(@RequestBody JsonNode configrationObject){
		RestResponse response = new RestResponse();
		String soqlQuery = null, id = null;
		JsonNode configration = null;
		try {
			if(configrationObject != null){
				id = configrationObject.hasNonNull("id") ? configrationObject.get("id").asText() : null;
				configration = configrationObject.hasNonNull("configurations") ? configrationObject.get("configurations") : null;
				if(configration != null)
				{
					soqlQuery = configration.hasNonNull("soqlQuery") ? configration.get("soqlQuery").asText() : null;
				}
			}
			if(id != null && soqlQuery != null)
			{
				soqlQuery = soqlQuery.replace("{SOBJECT_ID}", "'" + id + "'");
//				soqlQuery += " WHERE "+ ESM_SObjectType.INVOICE +"='"+ id +"' LIMIT 200";
				System.err.println("soql :: "+soqlQuery);
				List<RestSObject> lineItems = restConnection.query(soqlQuery).getSObjects();
				response.addData("lineItems", lineItems);
				response.setStatus(ResponseStatus.SUCCESS);
			}
		} catch (IOException e) {
			response.addError(e.getClass().getSimpleName(), e.getMessage());
			e.printStackTrace();
		}
		return response;
	}
	
	@RequestMapping(value = "/validateQuery", method = RequestMethod.POST, headers = {"Content-type=application/json" })
	public RestResponse validateQuery(@RequestBody String soql) {
		RestResponse response = new RestResponse();

		try {
			if(soql.contains("{SOBJECT_ID}")){
				soql = soql.replace("{SOBJECT_ID}", "null");
			}
			restConnection.query(soql).getSObjects();
			response.setStatus(ResponseStatus.SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.addError(e.getClass().getSimpleName(), e.getMessage());
		}
		return response;
	}
//	private String generateQueryFields(JsonNode sObjectFileds){
//		StringBuilder queryFields = new StringBuilder();
//		for (JsonNode field : sObjectFileds) {
//			queryFields.append(field.get("name").asText()).append(",");
//			if(field.get("type").asText().equalsIgnoreCase("reference"))
//				queryFields.append(field.get("relationshipName").asText()).append(".").append("Name").append(",");
//		}
//		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
//	}
}
