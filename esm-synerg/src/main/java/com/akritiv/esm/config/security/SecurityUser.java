package com.akritiv.esm.config.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.akritiv.esm.data.entities.Role;
import com.akritiv.esm.data.entities.User;

public class SecurityUser extends User implements UserDetails{
	
	private User user;
	
	private static final long serialVersionUID = 1L;
	public SecurityUser(User user) {
		if(user!=null){
			this.user = user;
			this.setId(user.getId());
			this.setFirstName(user.getFirstName());
			this.setLastName(user.getLastName());
			this.setUsername(user.getUsername());
			this.setEmail(user.getEmail());
			this.setPassword(user.getPassword());
			this.setRole(user.getRole());
		}
	}
	
	public User getUser() {
		return user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Set<Role> userRoles = new HashSet<Role>();
		userRoles.add(this.getRole());
		
		if(userRoles!=null){
			for (Role role : userRoles) {
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role.getRoleName());
				authorities.add(authority);
			}
		}
		
		return authorities;
	}
	
	@Override
	public String getPassword() {
		return super.getPassword();
	}


	@Override
	public String getUsername() {
		return super.getUsername();
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
}
