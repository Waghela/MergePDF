package com.akritiv.esm.helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.akritiv.esm.config.security.SecurityUser;
import com.akritiv.esm.rest.util.actionmodel.RestRequestModel;
import com.akritiv.esm.util.CommonFunctions;
import com.akritiv.esm.util.ESM_SObjectType;
import com.akritiv.esm.util.SfdcApi;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.core.Id;
import com.palominolabs.crm.sf.core.SObject;
import com.palominolabs.crm.sf.rest.FieldDescription;
import com.palominolabs.crm.sf.rest.RestConnection;
import com.palominolabs.crm.sf.rest.RestQueryResult;
import com.palominolabs.crm.sf.rest.RestSObject;
import com.palominolabs.crm.sf.rest.RestSObjectImpl;
import com.palominolabs.crm.sf.rest.SObjectDescription;

public class SObjectHelperImpl implements SObjectHelper{

	private SfdcApi restConnection;
	
	public SObjectHelperImpl(SfdcApi restConnection) {
		this.restConnection = restConnection;
	}
	
	@Override
	public final String getAllFieldsForQuery(String sObjectType) throws IOException{
//		StringBuilder queryFields = new StringBuilder();
//		
//		List<FieldDescription> fields = restConnection.describeSObject(sObjectType).getFields();
//		for (FieldDescription field : fields) {
//			queryFields.append(field.getName()).append(",");
//			if(field.getType().equalsIgnoreCase("reference")){
//				queryFields.append(field.getRelationshipName()).append(".").append("Name").append(",");
//			}
//		}
//		
//		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
		
		StringBuilder queryFields = new StringBuilder();
		List<FieldDescription> userFields = null;
//		SObjectDescription description = restConnection.describeSObject(sObjectType);
		List<FieldDescription> fields = restConnection.describeSObject(sObjectType).getFields();
		for (FieldDescription field : fields) {
			
			queryFields.append(field.getName()).append(",");
			if(field.getType().equalsIgnoreCase("reference")){
				if(field.getReferenceTo().get(0).equalsIgnoreCase(ESM_SObjectType.BUYER_USER)){
					userFields = (userFields==null) ? restConnection.describeSObject(field.getReferenceTo().get(0)).getFields() : userFields;
					for (FieldDescription rfield : userFields) {
						queryFields.append(field.getRelationshipName()).append(".").append(rfield.getName()).append(",");
					}
				}else{
					queryFields.append(field.getRelationshipName()).append(".").append("Name").append(",");
				}
			}
		}
		
		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
	}
	

	@Override
	public String getAllSimpleFieldsForQuery(String sObjectType)
			throws IOException {
		StringBuilder queryFields = new StringBuilder();
		
		List<FieldDescription> fields = restConnection.describeSObject(sObjectType).getFields();
		for (FieldDescription field : fields) {
			queryFields.append(field.getName()).append(",");
		}
		
		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
	}

	@Override
	public String getAllCustomFieldsForQuery(String sObjectType) throws IOException {
		StringBuilder queryFields = new StringBuilder();
		
		List<FieldDescription> fields = restConnection.describeSObject(sObjectType).getFields();
		for (FieldDescription field : fields) {
			if(field.isCustom())
				queryFields.append(field.getName()).append(",");
		}
		
		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
	}
	@Override
	public String getAllFieldsForQuery(String sObjectType, String relativeFieldApiName) throws IOException {
		StringBuilder queryFields = new StringBuilder();
		List<FieldDescription> userFields = null;
		
		List<FieldDescription> fields = restConnection.describeSObject(sObjectType).getFields();
		for (FieldDescription field : fields) {
			queryFields.append(field.getName()).append(",");
			if(field.getType().equalsIgnoreCase("reference")){
				if(relativeFieldApiName.equals(field.getRelationshipName())){
					List<FieldDescription> rFields = restConnection.describeSObject(field.getReferenceTo().get(0)).getFields();
					for (FieldDescription rfield : rFields) {
						queryFields.append(field.getRelationshipName()).append(".").append(rfield.getName()).append(",");
					}
				}else if(field.getReferenceTo().get(0).equalsIgnoreCase(ESM_SObjectType.BUYER_USER)){
					userFields = (userFields==null) ? restConnection.describeSObject(field.getReferenceTo().get(0)).getFields() : userFields;
					for (FieldDescription rfield : userFields) {
						queryFields.append(field.getRelationshipName()).append(".").append(rfield.getName()).append(",");
					}
				}else{
					queryFields.append(field.getRelationshipName()).append(".").append("Name").append(",");
				}
			}
		}
		
		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
	}
	@Override
	public final String getAdditionalFields(String relationalApiName,String... lookupFields){
		StringBuilder queryFields = new StringBuilder();
		for (String field : lookupFields) {
			queryFields.append(relationalApiName).append(".").append(field).append(",");
		}
		return queryFields.toString().substring(0, queryFields.toString().length() - 1);
	}

	@Override
	public List<RestSObject> getRecordsUsingWhere(String type, Map<String, String> whereFieldsMap) {
		StringBuilder whereFields = new StringBuilder();
		for (String field : whereFieldsMap.keySet()) {
			whereFields.append(field).append("=").append(whereFieldsMap.get(field)).append(",");
		}
		whereFields.deleteCharAt(whereFields.length() - 1);
		
		try {
			RestQueryResult queryResult = restConnection.query("SELECT " + getAllFieldsForQuery(type) + " FROM " + type + " WHERE " + whereFields);
			return queryResult.getSObjects();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public RestSObject createRestSObject(JsonNode jsonNode) {
		RestSObject restSObject;
		if(!jsonNode.get("id").isNull() || !jsonNode.get("id").get("idStr").isNull()){
			Id id = new Id(jsonNode.get("id").get("idStr").textValue());
			restSObject = RestSObjectImpl.getNewWithId(jsonNode.get("type").textValue(), id);
		}else{
			restSObject = RestSObjectImpl.getNew(jsonNode.get("type").textValue());
		}
		return restSObject;
	}

	@Override
	public RestSObject createRestSObject(JsonNode jsonNode, String extraFieldsObjectKey) {
		RestSObject restSObject = createRestSObject(jsonNode);
		if(extraFieldsObjectKey != null && jsonNode.hasNonNull(extraFieldsObjectKey)){
			Iterator<String> it = jsonNode.get(extraFieldsObjectKey).fieldNames();
			while (it.hasNext()) {
				String fieldName = (String) it.next();
				restSObject.setField(fieldName, jsonNode.get(extraFieldsObjectKey).get(fieldName).textValue());
			}
		}
		return restSObject;
	}

	@Override
	public RestSObject createRestSObject(JsonNode jsonNode, String extraFieldsObjectKey, String... extraFieldNames) {
		RestSObject restSObject = createRestSObject(jsonNode);
		if(extraFieldsObjectKey != null && jsonNode.hasNonNull(extraFieldsObjectKey)){
			JsonNode node = jsonNode.get(extraFieldsObjectKey);
			for (String fieldName : extraFieldNames) {
				restSObject.setField(fieldName, node.get(fieldName).textValue());
			}
		}
		return restSObject;
	}

	@Override
	public Map<String, String> createDataModel(RestSObject restSObject, RestSObject loggedInUser) {
		Map<String, String> dataModel = createDataModel(restSObject);
		dataModel.put("USER_ID", loggedInUser.getId().getFullId());
		return dataModel;
	}
	
	@Override
	public Map<String, String> createDataModel(RestSObject restSObject) {
		Map<String, String> dataModel = new HashMap<String, String>();
		if(restSObject.getId()!=null){
			dataModel.put("ID_FullId", restSObject.getId().getFullId());
			dataModel.put("ID_IdStr", restSObject.getId().getIdStr());
			dataModel.put("ID_KeyPrefix", restSObject.getId().getKeyPrefix());
		}
		for(String key : restSObject.getAllFields().keySet()){
			dataModel.put(key, restSObject.getField(key));
//			System.err.println(key + " :: " + restSObject.getField(key));
		}
		for (String iKey : restSObject.getRelationshipSubObjects().keySet()) {
//			System.err.println("==> " + iKey + " :: ");
			dataModel.put(iKey.replace("__r", "__c")+"__Name", restSObject.getRelationshipSubObjects().get(iKey).getField("Name"));
//			for (String key : restSObject.getRelationshipSubObjects().get(iKey).getAllFields().keySet()) {
//				System.err.println("=====> " + key + " :: " + restSObject.getRelationshipSubObjects().get(iKey).getField(key));
//			}
		}
		return dataModel;
	}

	@Override
	public RestSObject createRestSObject(RestRequestModel model, RestSObject user) {
		RestSObject restSObject = null;
		
		if(model.getActionModel().getsObjectID()!=null){
			Id id = new Id(model.getActionModel().getsObjectID().getIdStr());
			restSObject = RestSObjectImpl.getNewWithId(model.getsObject().getName(), id);
		}else{
			restSObject = RestSObjectImpl.getNew(model.getsObject().getName());
		}
		
		if(restSObject!=null){
			restSObject.setAllFields(model.getDataModel());
//			for (String key : model.getDataModel().keySet()) {
//				restSObject.setField(key, model.getDataModel().get(key));
//			}
			restSObject.setField(model.getActionModel().getCurrentState().getApiname(), model.getActionModel().getCurrentState().getValue());
			
			String newComment = "",oldComment="";
			if(model.getActionModel().getComments().getValue()!=null && !model.getActionModel().getComments().getValue().isEmpty()){
				oldComment = model.getActionModel().getComments().getHistoryValue();
				newComment = model.getActionModel().getComments().getValue()
						+ " by "
						+ user.getField("Name")
						+ " on "
						+ CommonFunctions.getAmericanNewYorkTimeComments()
						+ "\r\n" + " " + oldComment; // SET NEW COMMENT
			}
			
			restSObject.setField(model.getActionModel().getComments().getApiname(), newComment);						// SET COMMENT HISTORY 
			restSObject.setField(model.getActionModel().getLastModifiedBy().getApiname(), user.getId().getIdStr());
			
			restSObject.setField(model.getActionModel().getUseraction().getApiname(), model.getActionModel().getUseraction().getValue());
			if(model.getActionModel().getUseraction().getValue().equals(model.getActionModel().getRejectionreason().getUseractionvalue())){  	// REJECT ACTION
				restSObject.setField(model.getActionModel().getRejectionreason().getApiname(), model.getActionModel().getRejectionreason().getValue());
			}else if(model.getActionModel().getUseraction().getValue().equals(model.getActionModel().getRoute().getUseractionvalue())){			// ROUTE ACTION
				restSObject.setField(model.getActionModel().getRoute().getApiname(), model.getActionModel().getRoute().getValue());
			}
		}
		
		return restSObject;
	}


}
