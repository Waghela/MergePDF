package com.akritiv.esm.helpers;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;

import com.akritiv.esm.config.security.SecurityUser;
import com.akritiv.esm.rest.util.actionmodel.RestRequestModel;
import com.fasterxml.jackson.databind.JsonNode;
import com.palominolabs.crm.sf.rest.RestSObject;

public interface SObjectHelper {
	String getAllFieldsForQuery(String sObjectType) throws IOException;
	String getAllFieldsForQuery(String sObjectType,String relativeFieldApiName) throws IOException;
	String getAllSimpleFieldsForQuery(String sObjectType) throws IOException;
	String getAllCustomFieldsForQuery(String sObjectType) throws IOException;
	String getAdditionalFields(String lookupFieldApiName, String... lookupFields);
	List<RestSObject> getRecordsUsingWhere(String type, Map<String, String> whereFields);
	
	RestSObject createRestSObject(JsonNode jsonNode);
	RestSObject createRestSObject(JsonNode jsonNode,String extraFieldsObjectKey);
	RestSObject createRestSObject(JsonNode jsonNode,String extraFieldsObjectKey, String... extraFieldsArray);
	RestSObject createRestSObject(RestRequestModel model, RestSObject user);
	
	Map<String, String> createDataModel(RestSObject restSObject);
	Map<String, String> createDataModel(RestSObject restSObject, RestSObject loggedInUser);
}