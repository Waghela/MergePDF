package com.akritiv.esm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public final class CommonFunctions {
	public static String getAmericanNewYorkTimeComments() {
        Calendar calendar = new GregorianCalendar();
//        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        String IST = df.format(calendar.getTime());
        return IST;
    }
	
	public static String getStandardSalesforceDateTime() {
//		Calendar calendar = Calendar.getInstance();
		Calendar calendar = GregorianCalendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.sss'Z'");
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String IST = df.format(calendar.getTime());
		System.err.println("NEW DATE TIME : " + IST);
		return IST;
	}
	
	public static String getStandardSalesforceDateTime(String time) throws ParseException{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));
		String IST = df.format(DateFormat.getDateInstance().parse(time));
		System.err.println("NEW DATE TIME : " + IST);
		return IST;
	}
}
