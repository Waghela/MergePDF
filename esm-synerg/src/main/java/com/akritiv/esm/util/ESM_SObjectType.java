package com.akritiv.esm.util;

public interface ESM_SObjectType {
	public static final String INVOICE 					        = "Akritiv_ESM__Invoice__c";
	public static final String INVOICE_LINE_ITEM 		        = "akritivesm__Invoice_Line_Item__c";
	public static final String BUYER_USER 				        = "Heroku_Buyer_User__c";
	public static final String COST_ALLOCATION 			        = "akritivesm__Cost_Allocation__c";
	public static final String COST_CODE 				        = "akritivesm__Cost_Center__c";
	public static final String GL_CODE					        = "akritivesm__GL_Code__c";
	public static final String ADDITIONAL_CHARGES 		        = "akritivesm__Additional_Charges__c";
	public static final String SUPPLIER_MAINTENANCE 	        = "akritivesm__Supplier_Maintenance__c";
	public static final String ATTACHMENT	 					= "Attachment";
	public static final String NOTE	 	 						= "Note";
	public static final String RECORD_HISTORY	 				= "akritivesm__Record_History__c";
	public static final String RECORD_HISTORY_CONFIGURATION	 	= "akritivesm__Record_History_Configuration__c";
	public static final String TRACKER							= "akritivtlm__Tracker__c";

}
