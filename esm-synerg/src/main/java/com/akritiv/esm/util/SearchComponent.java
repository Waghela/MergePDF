package com.akritiv.esm.util;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.akritiv.esm.data.entities.SObjectField;
import com.akritiv.esm.data.services.SObjectService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;

@Component
public class SearchComponent {
	
	// PRIVATE VARIABLES
	String id;
	String title;
	List<Field> fields;
	
	// PUBLIC GETTERS & SETTERS
	public String getId() {
		return strValue(id);
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getTitle() {
		return strValue(title);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	
	public void addField(Field field){
		if(this.fields==null)
			this.fields = new ArrayList<Field>();
		this.fields.add(field);
	}
	public void addField(JsonNode node) throws ParseException{
		if(this.fields==null)
			this.fields = new ArrayList<Field>();
		
		Field field = new Field();
		if(node.hasNonNull("label") && node.get("label").textValue()!="null"){
			field.setLabel(node.get("label").textValue());
		}
		if(node.hasNonNull("name") && node.get("name").textValue()!="null"){
			field.setName(node.get("name").textValue());
		}
		if(node.hasNonNull("type") && node.get("type").textValue()!="null"){
			field.setType(node.get("type").textValue());
		}
		if(node.hasNonNull("value") && node.get("value").textValue()!="null"){
			field.setValue(node.get("value").textValue());
		}
		if(node.hasNonNull("rangeOpr") && node.get("rangeOpr").textValue()!="null"){
			field.setRangeOpr(node.get("rangeOpr").textValue());
		}
		if(field.getType().equalsIgnoreCase(FieldType.PICKLIST) && node.get("picklistValues").isArray()){
			for (int i = 0; i < node.get("picklistValues").size(); i++) {
				field.addPicklistValue(node.get("picklistValues").get(i).textValue());
			}
		}
		if(field.getType().equalsIgnoreCase(FieldType.DATE) || field.getType().equalsIgnoreCase(FieldType.DATETIME)){
			field.setValue((!node.get("value").isNull() && !node.get("value").textValue().isEmpty()) ? CommonFunctions.getStandardSalesforceDateTime(node.get("value").textValue()) : node.get("value").textValue());
		}
		this.fields.add(field);
	}
	
	@Override
	public String toString() {
		return "SearchComponent [id=" + id + ", title=" + title + ", fields="
				+ fields + "]";
	}
	
	public String strValue(String value){
		return (value == null) ? "" : value;
	}
	
	public String loadWhereFilterQueryString(SObjectService sobjectService){
		StringBuilder query = new StringBuilder();
		List<SObjectField> sObjectField = null;
		List<Field> queryFields = new ArrayList<Field>();
		for (Field field : fields) {
			if(field.getValue() != null && !field.getValue().isEmpty())
				queryFields.add(field);
		}
		if(queryFields.size() == 0)
			return "";
		
		query.append(" WHERE ");
		
		int index = 1;
		for (Field field : queryFields) {
			
			field.setRangeOpr(field.getRangeOpr().replace("&lt;", "<"));
			field.setRangeOpr(field.getRangeOpr().replace("&gt;", ">"));
			if(sobjectService != null)
				sObjectField = sobjectService.getSobjectFieldByName(field.getName());
			
			switch (field.getType()) {
			case FieldType.STRING:
				if(sobjectService != null && sObjectField != null && sObjectField.size() == 0 && sObjectField.get(0).getType().equalsIgnoreCase("reference")){
					query.append(field.getName() + field.getRangeOpr() + "'" + field.getValue().trim() + "'");
				}
				else{
					query.append(field.getName() + " LIKE '%" + field.getValue().trim() + "%'");
				}
				break;
			case FieldType.PICKLIST:
				if(field.getValue().isEmpty())
					query.append(field.getName() + " LIKE '%" + field.getValue().trim() + "%'");
				else
					query.append(field.getName() + field.getRangeOpr() + "'"  + field.getValue().trim() + "'");
				break;
			case FieldType.DATE : 
			case FieldType.DATETIME :
				if(field.getValue().isEmpty())
					query.append(field.getName() + " LIKE '%" + field.getValue().trim() + "%'");
				else
					query.append(field.getName() + field.getRangeOpr() + field.getValue().trim());
				break;
			case FieldType.CURRENCY :
				if(field.getValue().isEmpty())
					query.append(field.getName() + " LIKE '%" + field.getValue().trim() + "%'");
				else
					query.append(field.getName() + field.getRangeOpr() + field.getValue().trim());
				break;
			}
			
			if(index < queryFields.size()){
				query.append(" AND ");
//				query.append(" OR ");
			}
			index++;
		}
		
		return query.toString();
	}
	
	public static final SearchComponent parseSearchComponent(JsonNode node) throws ParseException{
		SearchComponent component = new SearchComponent();
		
		if (node.hasNonNull("id") && node.get("id").textValue() != "null") {
			component.setId(node.get("id").textValue());
		}
		if (node.hasNonNull("title") && node.get("title").textValue() != "null") {
			component.setTitle(node.get("title").textValue());
		}
		if (node.hasNonNull("fields") && node.get("fields").getNodeType() == JsonNodeType.ARRAY) {
			ArrayNode fields = (ArrayNode) node.get("fields");
			for (int i = 0; i < fields.size(); i++) {
				JsonNode fieldNode = fields.get(i);
				component.addField(fieldNode);
			}
		}
		
		return component;
	}

	//PRIVATE CLASS
	public class Field{
		String name;
		String label;
		String type;
		String value;
		String rangeOpr;
		
		List<String> picklistValues;
		
		public String getName() {
			return strValue(name);
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getLabel() {
			return strValue(label);
		}
		public void setLabel(String label) {
			this.label = label;
		}
		public String getType() {
			return strValue(type);
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getValue() {
			return strValue(value);
		}
		public void setValue(String value) {
			this.value = value;
		}
		public List<String> getPicklistValues() {
			return picklistValues;
		}
		public void setPicklistValues(List<String> picklistValues) {
			this.picklistValues = picklistValues;
		}
		public void addPicklistValue(String pickListValue){
			if(this.picklistValues==null)
				this.picklistValues = new ArrayList<String>();
			this.picklistValues.add(pickListValue);
		}
		public String getRangeOpr() {
			return rangeOpr;
		}
		public void setRangeOpr(String rangeOpr) {
			this.rangeOpr = rangeOpr;
		}
//		@Override
//		public String toString() {
//			return "Field [name=" + name + ", label=" + label + ", type="
//					+ type + ", value=" + value + ", picklistValues="
//					+ picklistValues + "]";
//		}
		@Override
		public String toString() {
			return "Field [name=" + name + ", label=" + label + ", type=" + type + ", value=" + value + ", rangeOpr="
					+ rangeOpr + ", picklistValues=" + picklistValues + "]";
		}
		
	}

	// PRIVATE ENUM
//	public enum FieldType{
//		string,
//		picklist,
//		date,
//		datetime,
//		reference
//	}
	public interface FieldType{
		public static final String STRING = "string";
		public static final String PICKLIST = "picklist";
		public static final String DATE = "date";
		public static final String DATETIME = "datetime";
		public static final String REFERENCE = "reference";
		public static final String CURRENCY = "currency";
	}
}


