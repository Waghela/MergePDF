package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller
@RequestMapping(value = "/modal")
@WebController(name = "ModalController", folder = "modal")
public class ModalController extends AbstractWebController{

	@RequestMapping(value = "/glcode",method = RequestMethod.GET)
	public ModelAndView glCodeModal(Model model){
		return modelAndView(model, "glCodeModal");
	}
	
	@RequestMapping(value = "/costcode",method = RequestMethod.GET)
	public ModelAndView costCodeModal(Model model){
		return modelAndView(model, "costCodeModal");
	}

	@RequestMapping(value = "/sobjectfield",method = RequestMethod.GET)
	public ModelAndView sObjectFieldModal(Model model){
		return modelAndView(model, "sObjectFieldModal");
	}
	@RequestMapping(value = "/note",method = RequestMethod.GET)
	public ModelAndView noteModal(Model model){
		return modelAndView(model, "noteModal");
	}
	
	@RequestMapping(value = "/buyeruser",method = RequestMethod.GET)
	public ModelAndView buyerUserModal(Model model){
		return modelAndView(model, "buyerUserModal");
	}

	@RequestMapping(value = "/genericModal",method = RequestMethod.GET)
	public ModelAndView genericModal(Model model){
		return modelAndView(model, "genericModal");
	}
	
	/**
	 *  SETTINGS LOOKUP MODALS
	 */

	@RequestMapping(value = "/layoutsectionsettings",method = RequestMethod.GET)
	public ModelAndView layoutSectionSettings(Model model){
		return modelAndView(model, "settings/layoutSectionSettings");
	}

	@RequestMapping(value = "/layoutsectioncriteria",method = RequestMethod.GET)
	public ModelAndView layoutSectionCriteria(Model model){
		return modelAndView(model, "settings/layoutSectionCriteria");
	}
	
	@RequestMapping(value = "/layoutsectionfieldsettings",method = RequestMethod.GET)
	public ModelAndView layoutSectionFieldSettings(Model model){
		return modelAndView(model, "settings/layoutSectionFieldSettings");
	}

	@RequestMapping(value = "/invoiceActionComponentSettings",method = RequestMethod.GET)
	public ModelAndView invoiceActionComponentSettings(Model model){
		return modelAndView(model, "settings/invoiceActionComponentSettings");
	}

	@RequestMapping(value = "/sObjectListComponentSettings",method = RequestMethod.GET)
	public ModelAndView sObjectListComponentSettings(Model model){
		return modelAndView(model, "settings/sObjectListComponentSettings");
	}
}
