package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller
@WebController(folder = "history",name = "HistoryController")
public class HistoryController extends AbstractWebController{

	@RequestMapping(value = "/history")
	public ModelAndView historyComponent(Model model){
		return modelAndView(model, "view");
	}
}
