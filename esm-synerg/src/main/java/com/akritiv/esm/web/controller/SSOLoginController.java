package com.akritiv.esm.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.data.entities.User;
import com.akritiv.esm.data.services.UserService;
import com.akritiv.esm.rest.controller.AbstractRestController;

@Controller
@RequestMapping(value = "/sso")
public class SSOLoginController extends AbstractRestController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/login/callback")
	public ModelAndView login(@RequestParam(value="federationid") String federationid){
		ModelAndView model = new ModelAndView("ssologin");
		System.out.println("ssologin controller");
		User SSOUser = userService.findUserByFederationId(federationid);
		model.addObject("SSOUser", SSOUser);
		return model;
	}
}
