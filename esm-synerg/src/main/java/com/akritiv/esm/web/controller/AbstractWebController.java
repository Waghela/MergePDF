package com.akritiv.esm.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;
import com.akritiv.esm.core.AbstractAkritivAppController;
import com.akritiv.esm.helpers.CustomComponentHelper;

public abstract class AbstractWebController extends AbstractAkritivAppController{
	
	@Autowired
	protected CustomComponentHelper componentHelper;
	
	protected ModelAndView modelAndView(Model model, String viewName){
		String controllerName = this.getClass().getAnnotation(WebController.class).name();
		String folderName = this.getClass().getAnnotation(WebController.class).folder();
		model.addAttribute("controllerName", controllerName.trim().toString());
		return new ModelAndView(folderName + "/" + viewName);
	}
}
