package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller 
@RequestMapping(value = "/admin/layout")
@WebController(name = "LayoutController",folder = "admin/layout")
public class LayoutController extends AbstractWebController{
	
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ModelAndView list(Model model){
//		model.addAttribute("SearchComponent", componentHelper.getInvoiceSearchComponent());
		return modelAndView(model,"list");
	}

	@RequestMapping(value = "/edit",method = RequestMethod.GET)
	public ModelAndView edit(Model model){
		return modelAndView(model,"edit");
	}
}
