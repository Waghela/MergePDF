package com.akritiv.esm.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;
import com.akritiv.esm.data.entities.MyTaskSection;
import com.akritiv.esm.data.services.MyTaskSectionService;
import com.akritiv.esm.data.services.UserService;
import com.akritiv.esm.util.ESM_SObjectType;

@Controller
@RequestMapping(value = "/mytasks" )
@WebController(name = "TaskController",folder = "task")
public class TaskController extends AbstractWebController{
	
	@Autowired
	MyTaskSectionService myTaskService;
	@Autowired
	UserService userService;
	
	@RequestMapping
	public ModelAndView index(Model model){
//		createMyTaskSections();
//		displayAllActiveMyTaskSections();
		model.addAttribute("sections", myTaskService.allActiveSectionsOrderBySequenceAsc());
		return modelAndView(model,"mytasks");
	}
	
	private void createMyTaskSections(){
		String title 						= "Invoices to approve";
		String sObjectApiName 				= ESM_SObjectType.INVOICE;
		String tableFieldJsonArray 			= "[{'name':'Name','label':'ESM Ref No#','href':'#/invoice/{{record.id.idStr}}'},{'name':'akritivesm__Invoice_No__c','label':'Invoice No'},{'name':'akritivesm__Document_Type__c','label':'Document Type'},{'name':'akritivesm__Current_State__c','label':'Current State'},{'name':'akritivesm__Invoice_Date__c','label':'Invoice Date'},{'name':'akritivesm__Total_Amount__c','label':'Total Amount'},{'name':'akritivesm__Current_Approver__r.Name','label':'Current Approver'}]".replace("'", "\"");
		String whereClause 					= "akritivesm__Current_Approver__r.akritivesm__Email__c='{LOGGED_IN_USERNAME}' AND akritivesm__Current_State__c IN ('Pending Approval','Parked Invoice','Awaiting Approval')";
		boolean active						= true;
		double sequence						= 1; 
		MyTaskSection section = new MyTaskSection(null, title, sObjectApiName, tableFieldJsonArray, whereClause, sequence, active);
		myTaskService.create(section);
		System.out.println("MY TASK SECTION CREATED :: " + section.toString());
	}
	
	private void displayAllActiveMyTaskSections(){
		try {
			List<MyTaskSection> sections = myTaskService.findAll();
			for (MyTaskSection section : sections) {
				System.err.println(section.getSequence() + " :: " + section.toString());
//				String soql = createSoql(section);
				String soql = section.createSOQL(userService.findUserByUsername(getCurrentUserDetails().getUsername()));
				System.err.println("SOQL :: " + soql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
