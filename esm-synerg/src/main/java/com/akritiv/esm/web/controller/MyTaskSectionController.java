package com.akritiv.esm.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.akritiv.esm.annotations.WebController;

@Controller
@RequestMapping(value = "/admin/mytasksection")
@WebController(name = "MyTaskSectionController", folder = "admin/mytasksection")
public class MyTaskSectionController extends AbstractWebController{

	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ModelAndView list(Model model){
		return modelAndView(model, "list");
	}

	@RequestMapping(value = "/edit",method = RequestMethod.GET)
	public ModelAndView create(Model model){
		return modelAndView(model, "edit");
	}
}
