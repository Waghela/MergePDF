<%@ include file="../../taglibs.jsp" %>
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav" id="header-menu">
        <li ng-class="{active: isActive('/') || isActive('/')}" ><a href="#/"><i class="icon-file"></i><span>Invoices</span> </a></li>
        <li ng-class="{active: isActive('/views') || isActive('/views')}" ><a href="#/views"><i class="icon-th-large"></i><span>Views</span> </a> </li>
        <li></li>
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>