<%@ include file="../../taglibs.jsp" %>
<%@ tag import="com.akritiv.esm.util.SearchComponent"%>
<%@ tag description="Search Component" language="java" pageEncoding="ISO-8859-1"%>
<%@ attribute name="componentModel" required="true" type="com.akritiv.esm.util.SearchComponent" rtexprvalue="true"%>
<%@ attribute name="searchAction" required="true" %>

<%
	SearchComponent component = componentModel;
%>

<div class="accordion" id="accordionSearchComponent">
	<div class="accordion-group">
		<div class="accordion-heading">
			<a 	class			="accordion-toggle" 
				data-toggle		="collapse"
				data-parent		="#accordionSearchComponent" 
				href			="#collapseSearchComponent">
				${component.title}
			</a>
		</div>
		<div 	block-ui		="InvoiceSearchComponentBlock" 
				id				="collapseSearchComponent" 
				class			="accordion-body collapse" >
			<div class="accordion-inner mytask-accordion-inner" >
				
				<div class="form-horizontal">
					<div class="control-group">
						
						<c:forEach var="field" items="component.fields">
							<!-- CONTROL LABEL -->
							<label class="control-label" for="${field.name }">${field.label }</label>
							
							<c:choose>
							
								<c:when test="${field.type == 'string'}">
									<!-- TEXTBOX CONTROL -->
									<div class="controls">
										<input type="text" class="span3" id="${field.name }" ng-model="field.value">
									</div>
								</c:when>
								<c:when test="${field.type == 'picklist'}">
									<!-- PICKLIST CONTROL -->
									<div class="controls">
										<select ng-model="field.value" id="${field.name }">
											<option value="">--None--</option>
											<c:forEach var="pValue" items="field.picklistValues">
												<option value="${pValue}">${pValue}</option>
											</c:forEach>
										</select>
									</div>
								</c:when>
								
							</c:choose>
							
						</c:forEach>
						
					</div>
					
					<div class="form-actions">
						<button type		="button" 
								class		="btn btn-primary" 
								ng-click	="${searchAction}">
							<i class="icon-search">&nbsp;&nbsp;Search</i>
						</button>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>