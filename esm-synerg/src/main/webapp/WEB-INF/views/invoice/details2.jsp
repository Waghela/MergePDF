<%@ include file="../../../taglibs.jsp" %>
<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-file"></i>
				<h3>{{ $root.showValue(metadata.sObjectLayout.title,'Loading...') }}</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content" block-ui="invoiceDetailsBlock" style="min-height: 100px;">
				<div class="controls">
					<layout-section-component
						ng-repeat 		= "section in metadata.sObjectLayout.sections"
						section			= "section"
						section-index 	= "$index"
						model			= "metadata.dataModel"
						layout-block-ui-instance = "blockUiInstances.layout">
					</layout-section-component>
				</div>
<!-- 				<invoice-action-component -->
<!-- 					ctrl		= "ctrl"> -->
<!-- 				</invoice-action-component> -->
			</div>
		</div>
	</div>
</div>