<%@ include file="../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-search"></i>
				<h3>Search Invoices</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="searchInvoiceBlock">
				<div class="controls" >
				
					<search-component 	component-id 	= "${SearchComponent.id }"
										search-action	= "searchInvoices" >
					</search-component>
					
					<div ng-show = "hasCachedData" class="alert">
	<!-- 					<strong>Warning!</strong>  -->
						Showing cached data, Please <strong>Refresh</strong> search to get fresh data.
					</div>
					
					<export-json-to-csv data='{{invoices}}' 
										report-title='search-result' 
										column-header='columnHeader' 
										column-api-name-arr='columnApiNameArr' 
										disabled="disableCSVExport">
					</export-json-to-csv>
					
					<table id="invoiceSearchTable" datatable="ng" dt-options="" class="table table-spriped table-hover table-condensed">
						<thead>
							<tr class="list-table">
								<th>ESM Ref No#</th>
								<th>Invoice No.</th>
					            <th>LE Code</th>
					            <th>Invoice Type</th>
					            <th>Supplier Name</th>
					            <th>Total Amount</th>
					            <th>Payment Date</th>
					            <th>Payment Document No./Check No.</th>
					            <th>Invoice Received Date</th>
					            <th>Status</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="invoice in invoices">
								<td><a href="#/invoice/{{ invoice.id.idStr }}"><b>{{ invoice.allFields.Name }}</b></a></td>
								<td>{{ invoice.allFields.Invoice_Number__c }}</td>
								<td>{{ invoice.allFields.LE_Code_Text__c }}</td>
					            <td>{{ invoice.allFields.Invoice_Type_Temp__c }}</td>
					            <td>{{ invoice.allFields.Supplier_Name_formula__c }}</td>
					            <td>{{ invoice.allFields.Akritiv_ESM__Total_Amount__c | number:2 }}</td>
					            <td>{{ invoice.allFields.Payment_Date__c }}</td>
					            <td>{{ invoice.allFields.Payment_Document_No_Check_No__c }}</td>
					            <td>{{ invoice.allFields.Invoice_Recieved_Date__c }}</td>
					            <td>{{ invoice.allFields.Akritiv_ESM__Status__c }}</td>
							</tr>
						</tbody>
					</table>
					<export-json-to-csv data='{{invoices}}' 
										report-title='search-result' 
										column-header='columnHeader' 
										column-api-name-arr='columnApiNameArr' 
										disabled="disableCSVExport">
					</export-json-to-csv>
				</div>
			</div>
			<!-- /widget-content -->
		</div>
	</div>
</div>