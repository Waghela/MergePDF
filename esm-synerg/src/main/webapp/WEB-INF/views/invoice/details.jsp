<%-- <jsp:include page="../../../taglibs.jsp"></jsp:include> --%>
<%@ include file="../../../taglibs.jsp" %>
<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-file"></i>
				<h3>Invoice Details {{ '[ ' + $root.showValue(invoice.allFields.Name) + ' ]' }}</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content" block-ui="invoiceDetailsBlock">
				<div class="alert alert-danger" ng-show="errors">
					<strong>Error!</strong> {{ errors[0].message }}
                </div>
                <table style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " >
                	<tbody>
                		<tr>
							<th colspan="4" >Invoice Header</th>
						</tr>
						<tr>
							<td>Document Type</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Document_Type__c) }}</td>
							<td>Invoice Type</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Invoice_Type__c) }}</td>
						</tr>
						<tr>
							<td>Invoice No</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Invoice_No__c) }}</td>
							<td>
								<span ng-show = "invoice.allFields.akritivesm__Document_Type__c == 'PO Invoice'">Purchase Order</span>
							</td>
							<td><span ng-show = "invoice.allFields.akritivesm__Document_Type__c == 'PO Invoice'">{{ $root.showValue(invoiceNames) }}</span></td>
						</tr>
						<tr>
							<td>Invoice Date</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Invoice_Date__c) }}</td>
							<td>Supplier Profile</td><td>{{ $root.showValue(invoice.relationshipSubObjects.akritivesm__Supplier_Profile__r.allFields.Name) }}</td>
						</tr>
						<tr>
							<td>Requestor / Buyer</td><td>{{ $root.showValue(invoice.relationshipSubObjects.akritivesm__Requestor_Buyer__r.allFields.Name) }}</td>
							<td>Current State</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Current_State__c) }}</td>
						</tr>
						<tr>
							<th colspan="4" >Invoice Detail</th>
						</tr>
						<tr>
							<td>Currency</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Currency__c) }}</td>
							<td>Amount</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Amount__c) | number:2 }}</td>
						</tr>
						<tr>
							<td>Other Charges/Surcharges</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Other_Charges_Surcharges__c) | number:2 }}</td>
							<td>Total Amount</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Total_Amount__c) | number:2 }}</td>
						</tr>
						<tr>
							<th colspan="4" >Billing Address</th>
						</tr>
						<tr>
							<td>Bill To City</td><td>{{ $root.showValue(invoice.allFields.Bill_To_City__c) }}</td>
							<td>Billing Address 1</td><td>{{ $root.showValue(invoice.allFields.Billing_Address_1__c) }}</td>
						</tr>
						<tr>
							<td>Bill To State/Province</td><td>{{ $root.showValue(invoice.allFields.Bill_To_State_Province__c) }}</td>
							<td>Bill To Address2</td><td>{{ $root.showValue(invoice.allFields.Bill_To_Address2__c) }}</td>
						</tr>
						<tr>
							<td>Bill To Zip/Postal Code</td><td>{{ $root.showValue(invoice.allFields.Bill_To_Zip_Postal_Code__c) }}</td>
							<td>Bill To Address3</td><td>{{ $root.showValue(invoice.allFields.Bill_To_Address3__c) }}</td>
						</tr>
						<tr>
							<td>Bill To Country</td><td>{{ $root.showValue(invoice.allFields.Bill_To_Country__c) }}</td>
							<td></td><td></td>
						</tr>
						<tr>
							<th colspan="4" >Shipping Address</th>
						</tr>
						<tr>
							<td>Ship To Name</td><td>{{ $root.showValue(invoice.allFields.Ship_To_Name__c) }}</td>
							<td>Ship To Address1</td><td>{{ $root.showValue(invoice.allFields.Ship_To_Address1__c) }}</td>
						</tr>
						<tr>
							<td>Ship To City</td><td>{{ $root.showValue(invoice.allFields.Ship_To_City__c) }}</td>
							<td>Ship To Address2</td><td>{{ $root.showValue(invoice.allFields.Ship_To_Address2__c) }}</td>
						</tr>
						<tr>
							<td>Ship To State/Province</td><td>{{ $root.showValue(invoice.allFields.Ship_To_State_Province__c) }}</td>
							<td>Ship To Zip/Postal Code</td><td>{{ $root.showValue(invoice.allFields.Ship_To_Zip_Postal_Code__c) }}</td>
						</tr>
						<tr>
							<td>Ship To Country/Territory</td><td>{{ $root.showValue(invoice.allFields.Ship_To_Country_Territory__c) }}</td>
							<td></td><td></td>
						</tr>
						<tr>
							<th colspan="4" >Remittance Address</th>
						</tr>
						<tr>
							<td>Remit To Name</td><td>{{ $root.showValue(invoice.allFields.Remit_To_Name__c) }}</td>
							<td>Remit To Address1</td><td>{{ $root.showValue(invoice.allFields.Remit_To_Address1__c) }}</td>
						</tr>
						<tr>
							<td>Remit To City</td><td>{{ $root.showValue(invoice.allFields.Remit_To_City__c) }}</td>
							<td>Remit To Address2</td><td>{{ $root.showValue(invoice.allFields.Remit_To_Address2__c) }}</td>
						</tr>
						<tr>
							<td>Remit To Zip/Postal Code</td><td>{{ $root.showValue(invoice.allFields.Remit_To_Zip_Postal_Code__c) }}</td>
							<td>Remit To Address3</td><td>{{ $root.showValue(invoice.allFields.Remit_To_Address3__c) }}</td>
						</tr>
						<tr>
							<td>Remit To State/Province</td><td>{{ $root.showValue(invoice.allFields.Remit_To_State_Province__c) }}</td>
							<td>Remit To Country/Territory</td><td>{{ $root.showValue(invoice.allFields.Remit_To_Country_Territory__c) }}</td>
						</tr>
						<tr>
							<th colspan="4" >Aditional Details</th>
						</tr>
						<tr>
							<td>Reject Reason</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Reject_Reasons__c) }}</td>
							<td>Hold Reason</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Hold_Reason__c) }}</td>
						</tr>
						<tr>
							<td>Exception Reason</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Exception_Reason__c) }}</td>
							<td>Comments History</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Comment_History__c) }}</td>
						</tr>
						<tr>
							<td>Inquiry Comments</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Inquiry_Comments__c) }}</td>
							<td>Current Approver</td><td>{{ $root.showValue(invoice.relationshipSubObjects.akritivesm__Current_Approver__r.allFields.Name) }}</td>
						</tr>
						<tr>
							<td>Owner</td><td>{{ $root.showValue(invoice.relationshipSubObjects.Owner.allFields.Name) }}</td>
							<td>Last Modified By</td><td>{{ $root.showValue(invoice.relationshipSubObjects.akritivesm__Last_Modified_By__r.allFields.Name) }}</td>
						</tr>
						<tr>
							<th colspan="4" >Payment Instruction</th>
						</tr>
						<tr>
							<td>Location</td><td>{{ $root.showValue(invoice.allFields.akritivesm__Location__c) }}</td>
							<td></td><td></td>
						</tr>
						<tr>
							<th colspan="4" >Invoice Line Item</th>
						</tr>
                	</tbody>
				</table>
				<table ng-show="isNonPo && !enableCostAllocation" class = "table table-spriped table-condensed">
					<tr>
						<th>Amount</th>
						<th>Cost Code</th>
						<th>GL Code</th>
					</tr>
					<tr ng-repeat="item in invoiceLineItems">
						<td>{{ $root.showValue(item.allFields.akritivesm__Amount__c) | number:2}}</td>
						<td>{{ $root.showValue(item.relationshipSubObjects.akritivesm__Cost_Code__r.allFields.Name) }}</td>
						<td>{{ $root.showValue(item.relationshipSubObjects.akritivesm__GL_Code__r.allFields.Name) }}</td>
					</tr>
				</table>
				<table ng-hide="isNonPo" class = "table table-spriped table-condensed">
					<tr>
						<th>Invoice Line No</th>
						<th>Product/Service No.</th>
						<th>Product/Service Description</th>
						<th>Rate</th>
						<th>Quantity</th>
						<th>UOM</th>
						<th>Amount</th>
						<th>GRN</th>
						<th>GRN Line Item</th>
					</tr>
					<tr ng-repeat="item in invoiceLineItems">
						<td style="text-align: left;">{{ $root.showValue(item.allFields.akritivesm__Invoice_Line_No__c) }}</td>
						<td>{{ $root.showValue(item.allFields.akritivesm__Product_Service_No__c) }}</td>
						<td>{{ $root.showValue(item.allFields.akritivesm__Product_Service_Description__c) }}</td>
						<td>{{ $root.showValue(item.allFields.akritivesm__Rate__c) | number:2 }}</td>
						<td>{{ $root.showValue(item.allFields.akritivesm__Quantity__c) | number:0 }}</td>
						<td>{{ $root.showValue(item.allFields.akritivesm__UOM__c)  }}</td>
						<td>{{ $root.showValue(item.allFields.akritivesm__Amount__c) | number:2}}</td>
						<td>{{ $root.showValue(item.relationshipSubObjects.akritivesm__GRN__r.allFields.Name)  }}</td>
						<td>{{ $root.showValue(item.relationshipSubObjects.akritivesm__GRN_Line_Item__r.allFields.Name)  }}</td>
					</tr>
				</table>
				<table ng-show="enableCostAllocation && isEnableEdit" class = "table table-spriped table-condensed">
					<tr>
						<th>Amount</th>
						<th>Cost Code</th>
						<th>GL Code</th>
						<th></th>
					</tr>
					<tr ng-repeat = "item in iliService.items | filter : { isRemoved : false}">
						<td class="esm-input">
							<input 	type				="number" 
								   	style				="margin-bottom: 0;" 
								   	string-to-number 
								   	ng-model			="item.amount" 
								   	ng-disabled			="item.isPersisted"/>
						</td>
						<td class="esm-input">
							<div class="input-append" style="margin-bottom: 0;">
								<input 	type			="hidden" 
										ng-model		="item.costCode.id" />
								<input 	type			="text" 
									   	class			="span2 m-wrap" 
									   	style			="margin-bottom: 0;" 
									   	name			="txtCostCode"
									   	ng-disabled		="true" 
									   	ng-model		="item.costCode.name" />
								<button type			="button" 
										ng-click		="showCostCodeModal(item)" 
										class			="btn" 
										ng-disabled		="item.isPersisted" 
										title			="Cost Code Lookup">
									<i class="icon-search"></i>
								</button>
							</div>
						</td>
						<td class="esm-input">
							<div class="input-append" style="margin-bottom: 0;">
								<input 	type			="hidden" 
										ng-model		="item.glCode.id" />
								<input 	type			="text" 
										class			="span2 m-wrap" 
										style			="margin-bottom: 0;" 
										name			="txtGLCode" 
										ng-disabled		="true" 
										ng-model		="item.glCode.name" />
								<button type			="button" 
										ng-click		="showGLCodeModal(item)" 
										class			="btn" 
										ng-disabled		="item.isPersisted" 
										title			="GL Code Lookup">
									<i class="icon-search"></i>
								</button>
							</div>
						</td>
						<td style="text-align: right;">
							<button class			="btn btn-mini btn-danger" 
									type			="button" 
									ng-click		="iliService.removeItem($index,item)">
								<i class="icon-remove"></i>
							</button>
						</td>
					</tr>
					<tr>
						<td style="font-weight: bold;" >TOTAL : {{ iliService.getTotal() | number : 2 }}</td>
						<td colspan="3" style="text-align: center;"></td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: center;">
<!-- 							<button type="button" class="btn btn-primary btn-small" ng-click="iliService.addItem()">Add more</button> -->
							<button type		="button" 
									class		="btn btn-small" 
									ng-click	="iliService.addItem()"><i class="icon-plus-sign"></i>&nbsp;Add more</button>
							<button type		="button" 
									class		="btn btn-small btn-primary" 
									ng-click	="saveCostAllocationInvoiceLineItems()" ><i class="icon-ok"></i>&nbsp;Save</button>
							<button type		="button" 
									class		="btn btn-small btn-primary" 
									ng-click	="loadInvoiceLineItems()"><i class="icon-refresh"></i>&nbsp;Reload</button>
						</td>
					</tr>
				</table>
<!-- 				<table ng-hide="!enableCostAllocation" style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " > -->
<!-- 					<tbody> -->
<!-- 						<tr> -->
<!-- 							<th>Cost Allocation</th> -->
<!-- 						</tr> -->
<!-- 					</tbody> -->
<!-- 				</table> -->
<!-- 				<table ng-hide="!enableCostAllocation" class = "table table-spriped table-condensed"> -->
<!-- 					<tr> -->
<!-- 						<th>Invoice Line Item</th> -->
<!-- 						<th>Total Amount</th> -->
<!-- 						<th>Cost Code</th> -->
<!-- 						<th>GL Code</th> -->
<!-- 						<th></th> -->
<!-- 					</tr> -->
<!-- 					<tr ng-repeat="item in CostAllocationItems.list"> -->
<!-- 						<td style="text-align: left;font-weight: bold;">{{ $root.showValue(item.invoiceLineItemNo) }}</td> -->
<!-- 						<td>{{ $root.showValue(item.totalAllocatedAmt) | number:2 }}</td> -->
<!-- 						<td>{{ $root.showValue(item.costCode.name) }}</td> -->
<!-- 						<td>{{ $root.showValue(item.glCode.name) }}</td> -->
<!-- 						<td style="text-align: right;"><button class="btn btn-mini btn-danger" type="button" ng-disabled="showNewCostAllocationSection" ng-click="CostAllocationItems.remove($index)"><i class="icon-remove"></i></button></td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
<!-- 				<table ng-show="showNewCostAllocationSection" class = "table table-spriped table-condensed details-table"> -->
<!-- 					<tr> -->
<!-- 						<th colspan="4">Add Cost Allocation</th> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td>Cost Code</td> -->
<!-- 						<td class="esm-input"> -->
<!-- 							<div class="input-append" style="margin-bottom: 0;"> -->
<!-- 								<input type="hidden" ng-model="CostAllocationModel.costCode.id" /> -->
<!-- 								<input type="text" class="span2 m-wrap" style="margin-bottom: 0;" name="txtCostCode" ng-disabled="true" ng-model="CostAllocationModel.costCode.name" /> -->
<!-- 								<button ng-disabled="costAllocationCompleted" type="button" ng-click="showCostCodeModal()" class="btn" title="Cost Code Lookup"><i class="icon-search"></i></button> -->
<!-- 							</div> -->
<!-- 						</td> -->
<!-- 						<td>GL Code</td> -->
<!-- 						<td class="esm-input"> -->
<!-- 							<div class="input-append" style="margin-bottom: 0;"> -->
<!-- 								<input type="hidden" ng-model="CostAllocationModel.glCode.id" /> -->
<!-- 								<input type="text" class="span2 m-wrap" style="margin-bottom: 0;" name="txtGLCode" ng-disabled="true" ng-model="CostAllocationModel.glCode.name" /> -->
<!-- 								<button ng-disabled="costAllocationCompleted" type="button" ng-click="showGLCodeModal()" class="btn" title="GL Code Lookup"><i class="icon-search"></i></button> -->
<!-- 							</div> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td>Invoice Line Item</td> -->
<!-- 						<td class="esm-input"> -->
<!-- 							{{ CostAllocationModel.invoiceLineItemNo }} -->
<!-- 							<input type="hidden" ng-model="CostAllocationModel.invoiceLineItemNo" /> -->
<!-- 						</td> -->
<!-- 						<td>Total Allocated</td> -->
<!-- 						<td class="esm-input"> -->
<!-- 							<input ng-disabled="costAllocationCompleted" type="number" style="margin-bottom: 0;" string-to-number ng-model="CostAllocationModel.totalAllocatedAmt" /> -->
<!-- 							<input type="hidden" ng-model = "CostAllocationModel.totalAmount" /> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr ng-show="CostAllocationItems.errors.length > 0"> -->
<!-- 						<td colspan="4" style="text-align: left;font-weight: bold;color: red;"> -->
<!-- 							<ul style="margin-bottom: 0px;"> -->
<!-- 								<li ng-repeat="error in CostAllocationItems.errors" > -->
<!-- 									{{ error }} -->
<!-- 								</li> -->
<!-- 							</ul> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 					<tr> -->
<!-- 						<td colspan="4" style="text-align: center;"> -->
<!-- 							<button ng-disabled="costAllocationCompleted" type="button" class="btn btn-primary btn-small" ng-click="CostAllocationItems.add(CostAllocationModel)">Add</button> -->
<!-- 							<button type="button" class="btn btn-small" ng-click="CostAllocationItems.reset()">Cancel</button> -->
<!-- 						</td> -->
<!-- 					</tr> -->
<!-- 				</table> -->
				<table style="margin-bottom: 0;" class="table table-spriped table-hover table-condensed details-table " >
					<tbody>
						<tr ng-show="isEnableEdit">
							<th colspan="4" >Closure Fields</th>
						</tr>
						<tr ng-show="isEnableEdit">
							<td>Comments</td>
							<td class="esm-input">
								<textarea ng-disabled="!isEnableEdit || showNewCostAllocationSection" rows="3" style="resize:none;" ng-model = "invoice.New_Comments__c"></textarea>
							</td>
							<td>User Action</td>
							<td class="esm-input">
								<select ng-disabled="!isEnableEdit || showNewCostAllocationSection" id="userAction" ng-model="invoice.useraction" ng-change="showDependant(invoice.useraction)">
									<option value="">--None--</option>
									<option value="{{item.value}}" ng-repeat="item in userActionItems">{{item.label}}</option>
								</select>
							</td>
						</tr>
						<tr ng-show="rejectionReasonItems">
							<td >Rejection Reason</td>
							<td class="esm-input">
								<select id="rejectionReason" ng-disabled="showNewCostAllocationSection" ng-model="invoice.rejectionReason">
									<option value="">--None--</option>
									<option value="{{item}}" ng-repeat = "item in rejectionReasonItems">{{item}}</option>
								</select>
							</td>
							<td></td><td></td>
						</tr>
						<tr ng-show="submitInvoiceErrors.length > 0">
							<td colspan="4" style="text-align: left;font-weight: bold;color: red;">
								<ul style="margin-bottom: 0px;">
									<li ng-repeat="error in submitInvoiceErrors" >
										{{ error }}
									</li>
								</ul>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="text-align: center;">
								<button ng-disabled			="errors || showNewCostAllocationSection"
										ng-show				="isEnableEdit" 
										type				="submit" 
										class				="btn btn-primary" 
										ng-click			="submitInvoice()">
										<i class="icon icon-ok"></i> Save</button>
										 <a class="btn btn-primary" ng-show="invoice.relationshipSubObjects.akritivesm__Tracker_Id__r.allFields.akritivtlm__Primary_Document_ID__c != null && invoice.relationshipSubObjects.akritivesm__Tracker_Id__r.allFields.akritivtlm__Primary_Document_ID__c != ''"
								href="rest/notesAndAttachments/viewPrimaryDocument?id={{invoice.relationshipSubObjects.akritivesm__Tracker_Id__r.allFields.akritivtlm__Primary_Document_ID__c}}">
								<i class="icon-large icon-download-alt"></i> View Invoice </a> 
<!-- 								<a href="#/invoices"><button ng-disabled="showNewCostAllocationSection" type="button" class="btn" >Cancel</button></a> -->
								<button ng-click			="cancelInvoice()" 
										ng-disabled			="showNewCostAllocationSection" 
										type				="button" 
										class				="btn" >
										<i class="icon icon-remove"></i> Cancel</button>
							</td>
						</tr>
					</tbody>
				</table>
				<attachments-component model="notesAndAttachemntsComponentModel">
				</attachments-component>
				<history-component model="invoiceHistoryComponentModel">
				</history-component>
			</div>
		</div>
	</div>
</div>