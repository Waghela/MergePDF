<%-- <jsp:include page="../../../taglibs.jsp"></jsp:include> --%>
<%@ include file="../../../taglibs.jsp"%>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-user"></i>
				<h3>User Profile</h3>
			</div>
			<!-- /widget-header -->
			<div class="widget-content" block-ui="userDetailsBlock">
				<div class="alert alert-danger" ng-show="errors">
					<strong>Error!</strong> {{ errors[0].message }}
				</div>
				<table style="margin-bottom: 0;"
					class="table table-spriped table-hover table-condensed details-table ">
					<tbody>
						<tr>
							<th colspan="4">Basic Details</th>
						</tr>
						<tr>
							<td>Full Name:</td>
							<td>{{ $root.showValue(userData.firstName)+ " " +
								$root.showValue(userData.middleInitial) + " "+
								$root.showValue(userData.lastName)}}</td>

							<td>First Name:</td>
							<td><input type="text" ng-model="userData.firstName"
								maxlength="255" /></td>


						</tr>
						<tr>
							<td>Email:</td>
							<td>{{ $root.showValue(userData.email) }}</td>


							<td>Middle Name Initial:</td>
							<td><input type="text" ng-model="userData.middleInitial"
								maxlength="50" /></td>
						</tr>
						<tr>
							<td>User Type:</td>
							<td>{{ $root.showValue(userData.role.roleName) }}</td>



							<td>Last Name:</td>
							<td><input type="text" ng-model="userData.lastName"
								maxlength="255" /></td>
						</tr>
						<tr>

							<td>Status:</td>
							<td>{{ $root.showValue(userData.status) }}</td>
							<td colspan="2" style="text-align: center"></td>
							<sec:authorize access="hasAnyRole('BUYER')">
								<tr>

									<td>Delegate User:</td>
									<td class="esm-input">


										<div class="input-append"
											style="margin-bottom: 0; margin-right: 40px;"
											ng-show="userData.enableDelegation">
											<input ng-model="delegateUserData.fullname"
												placeholder="Select User For Delegation" type="text"
												class="span2 m-wrap" style="margin-bottom: 0;"
												ng-disabled="true" />
											<button type="button" ng-click="showUserForDeligateLookup()"
												class="btn" title="User lookup For Delegation">
												<i class="icon-search"></i>
											</button>
										</div>
									</td>
									<td>Supervisor:</td>
									<td class="esm-input">
										<div class="input-append" style="float: left"
											style="margin-bottom: 0; margin-right: 40px;">
											<input ng-model="superVisiorUserData.fullname"
												placeholder="Select Supervisior" type="text"
												class="span2 m-wrap" style="margin-bottom: 0;"
												ng-disabled="true" />
											<button type="button" ng-click="showSupervisorUserLookup()"
												class="btn" title="User lookup For Delegation">
												<i class="icon-search"></i>


											</button>
										</div>
										<div style="float: right;">
											<button type="button" ng-click="resetSuperVisior();"
												class="btn" title="Reset Supervisor">Reset</button>
										</div>
									</td>



								</tr>
								<tr>
									<td>Enable Delegation:</td>
									<td><input type="checkbox"
										ng-model="userData.enableDelegation"
										value="userData.enableDelegation"
										data-ng-click="changeDelegationStatus(userData.enableDelegation);"
										tooltip="Deleation Enabled"></td>
									<td></td>
									<td></td>
								</tr>
							</sec:authorize>
						<tr>
							<td colspan="4" style="text-align: center;">
								<div class="form-actions">
									<button type="submit" class="btn btn-primary"
										ng-click="submitUserProfile()">
										<i class="icon icon-ok"></i> Save
									</button>


									<button ng-click="cancelUserProfile()" type="button" class="btn">
										<i class="icon icon-remove"></i> Cancel
									</button>
								</div>
							</td>
						</tr>
					</tbody>
				</table>


			</div>
		</div>
	</div>
</div>