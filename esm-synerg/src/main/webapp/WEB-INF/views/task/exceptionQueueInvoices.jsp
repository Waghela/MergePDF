
<div class="accordion-group" >
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
			data-parent="#accordion2" href="#collapseExceptionQueue"> Exception Queue
			(Total Exception Queue: {{  exceptionQueueInvoices.length }}) </a>
	</div>
	<div block-ui="exceptionQueueInvoicesToApproveBlock" id="collapseExceptionQueue" class="accordion-body collapse ">
		<div class="accordion-inner mytask-accordion-inner">
			<button type="button" class="btn btn-small btn-primary" ng-click="loadExceptionQueueToApprove()"><i class="icon-small icon-refresh">&nbsp;&nbsp;Refresh</i></button>
			<table class="table table-spriped table-hover table-condensed" >
				<thead>
			        <tr class="list-table">
			            <th>ESM Ref No#</th>
			            <th>Invoice No</th>
			            <th>Document Type</th>
			            <th>Current State</th>
			            <th>Invoice Date</th>
			            <th>Purchase Order</th>
			            <th>Total Amount</th>
			            <th>Current Approver</th>
			        </tr>
		        </thead>
		        <tbody>
		        	<tr ng-show="exceptionQueueInvoices.length === 0">
		        		<th colspan="8"><center style="color:red;">No Exception Queue invoice to approve.</center></th>
		        	</tr>
			        <tr ng-show="pendingApprovalInvoices.length > 0 " ng-repeat="invoice in exceptionQueueInvoices">
			        	<td><a  href = "#/invoice/{{ invoice.id.idStr }}"><b>{{ invoice.allFields.Name }}</b></a></td>
			            <td>{{ invoice.allFields.akritivesm__Invoice_No__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Document_Type__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Current_State__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Invoice_Date__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Purchase_Order__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Total_Amount__c | number:2 }}</td>
			            <td>{{ invoice.relationshipSubObjects.akritivesm__Current_Approver__r.allFields.Name }}</td>
			        </tr>
		        </tbody>
			</table>
		</div>
	</div>
</div>