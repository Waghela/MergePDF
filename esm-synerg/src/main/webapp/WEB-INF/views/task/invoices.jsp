
<div class="accordion-group" >
	<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse"
			data-parent="#accordion2" href="#collapseInvoices"> Invoices (Total
			Invoices: {{ pendingApprovalInvoices.length }}) </a>
	</div>
	<div block-ui="pendingInvoicesToApproveBlock" id="collapseInvoices" class="accordion-body collapse in" >
		<div class="accordion-inner mytask-accordion-inner" >
			<button type="button" class="btn btn-small btn-primary" ng-click="loadPendingInvoicesToApprove()"><i class="icon-small icon-refresh">&nbsp;&nbsp;Refresh</i></button>
			<table class="table table-spriped table-hover table-condensed" >
				<thead>
			        <tr class="list-table">
			            <th>ESM Ref No#</th>
			            <th>Invoice No</th>
			            <th>Document Type</th>
			            <th>Current State</th>
			            <th>Invoice Date</th>
			            <th>Purchase Order</th>
			            <th>Total Amount</th>
			            <th>Current Approver</th>
			        </tr>
		        </thead>
		        <tbody>
		        	<tr ng-show="pendingApprovalInvoices.length === 0 ">
		        		<th colspan="8"><center style="color:red;">No Invoices to approve.</center></th>
		        	</tr>
			        <tr ng-show="pendingApprovalInvoices.length > 0 " ng-repeat="invoice in pendingApprovalInvoices">
			        	<td><a href = "#/invoice/{{ invoice.id.idStr }}"><b>{{ invoice.allFields.Name }}</b></a></td>
			            <td>{{ invoice.allFields.akritivesm__Invoice_No__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Document_Type__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Current_State__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Invoice_Date__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Purchase_Order__c }}</td>
			            <td>{{ invoice.allFields.akritivesm__Total_Amount__c | number:2 }}</td>
			            <td>{{ invoice.relationshipSubObjects.akritivesm__Current_Approver__r.allFields.Name }}</td>
			        </tr>
		        </tbody>
			</table>
		</div>
	</div>
</div>