<%@ include file="../../../taglibs.jsp" %>

<div class="input-append" style="margin-bottom: 0;">
	<input 	id 				= "{{id}}"
			ng-required		="required"
			type			="text" 
		   	class			="span2 m-wrap" 
		   	style			="margin-bottom: 0;" 
		   	ng-disabled		="true" />
   	<button type			="button" 
			class			="btn">
		<i class="icon-calendar"></i>
	</button>
</div>