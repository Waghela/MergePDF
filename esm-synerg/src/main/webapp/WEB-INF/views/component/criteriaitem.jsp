<%@ include file="../../../taglibs.jsp" %>

<div ng-switch on="item.type">
	<div ng-switch-when="GROUP">
		<div class="rules-group-header">
			<div class="btn-group pull-right">
				<a class="btn btn-mini btn-success " ng-click="addRule()">
					<i class="icon-plus"></i> Add rule</a>
				<a class="btn btn-mini btn-success " ng-click="addGroup()">				
					<i class="icon-plus-sign"></i> Add group</a>
				<a class="btn btn-mini btn-warning " ng-click="clear()" ng-if="!parent">
					<i class="icon-trash"></i> Clear</a>
				<a class="btn btn-mini btn-danger "  ng-click="deleteItem(index)" ng-if="index>0">
					<i class="icon-remove" ></i> Delete</a>
			</div>		
			<div class="btn-group" >
				<a class="btn btn-mini" ng-class="{'btn-primary active': item.condition == '&&'}" ng-click="setCondition('&&')">AND</a>
				<a class="btn btn-mini" ng-class="{'btn-primary active': item.condition == '||'}" ng-click="setCondition('||')">OR</a>
			</div>
		</div>
		<criteria-item 
			parent		="item"
			item		="rule" 
			sobject		="sobject"
			ng-repeat	="rule in item.rules" 
			index		="$index">
		</criteria-item>
	</div>
	<div ng-switch-when="RULE">
		<div class="control-group" style="margin-bottom: 0px;margin-top: 5px;">
			<div>
				<select ng-options		="field as field.label for field in sobject.fields track by field.name" 
						ng-model		="item.field" 
						class			="span2" 
						ng-change		="item.operator=item.value=null">
					<option value		="" 
							ng-disabled	="'true'">--None--</option>
				</select>
				<select ng-disabled		="!item.field" 
						ng-model		="item.operator" 
						style			="width: 100px;">
					<option value		="" 
							ng-disabled	="'true'">--None--</option>
					<option ng-selected	="{{opr.sign == item.operator}}"
							ng-repeat	="opr in operatorlist | filter: item.field.type" 
							value		="{{opr.sign}}">{{opr.label}}</option>
				</select>
				<input  type			="text" 		
						ng-model		="item.value" 
						ng-if			="item.field.type == 'string'" 									
						class			="span2" 
						ng-disabled		="item.operator==null">
				<input 	type			="email" 	
						ng-model		="item.value" 
						ng-if			="item.field.type == 'email'"									
						class			="span2" 
						ng-disabled		="item.operator==null">
				<input 	type			="number" 	
						ng-model		="item.value" 
						ng-if			="item.field.type == 'double' || item.field.type == 'currency'" 	
						class			="span2" 
						ng-disabled		="item.operator==null">
				<input 	type			="checkbox" 	
						ng-model		="item.value" 
						ng-if			="item.field.type == 'boolean'" 
						ng-init			="item.value=true"					  
						ng-disabled		="item.operator==null">
				<select class			="span2" 	
						ng-model		="item.value" 
						ng-if			="item.field.type == 'picklist'" 
						ng-options		="value for value in picklistValues(item.field)"  
						ng-disabled		="item.operator==null">
					<option value		="" 
							ng-disabled	="'true'">--None--</option>
				</select>
				<select class			="span2" 	
						ng-model		="item.value" 
						ng-if			="item.field.type == 'reference'" 
						ng-options		="value for value in namespaceValues"  
						ng-disabled		="item.operator==null">
					<option value		="" 
							ng-disabled	="'true'">--None--</option>
				</select>
				<a 		class			="btn btn-danger" 
						ng-if			="index>0" 
						ng-click		="deleteItem(index)">
					<i 	class			="icon-remove"></i>
				</a>
			</div>
		</div>
	</div>
</div>