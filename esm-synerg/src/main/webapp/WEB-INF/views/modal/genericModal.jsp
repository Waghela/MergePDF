<jsp:include page="../../../taglibs.jsp"></jsp:include>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" block-ui="genericModalBlock">
				<form ng-submit="loadGenericLookUpResult()" style="margin: 0;min-height: 200px;">
					<fieldset>
						<div class="control-group">
							<div class="controls">
								<div class="input-append">
									<input class="span4 m-wrap" placeholder="Search" style="margin-bottom: 0;" ng-model="genericModel.searchStr" type="text">
									<button type="button" class="btn btn-success" ng-click="loadGenericLookUpResult()">Search</button>
								</div>
							</div>
						</div>
						<div style="max-height: 200px;overflow: auto;">
							<table class="table table-spriped table-hover table-condensed" >
								<thead>
							        <tr class="list-table">
							            <th>Name</th>
							            <th>Owner</th>
							        </tr>
						        </thead>
						        <tbody>
						        	<tr ng-show="!searchResult.length || searchResult.length === 0">
						        		<th colspan="2"><center style="color:red;">No result found.</center></th>
						        	</tr>
							        <tr ng-repeat="result in searchResult">
							        	<td><a 	data-dismiss		="modal" 
							        			style				="cursor: pointer;font-weight: bold;" 
							        			ng-click			="selectAndClose(result.id.fullId,$root.showValue(result.allFields.Name,''))">
						        				{{ $root.showValue(result.allFields.Name,'') }}
					        				</a>
				        				</td>
							            <td>{{ result.relationshipSubObjects.Owner.allFields.Name }}</td>
							        </tr>
						        </tbody>
							</table>
						</div>
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
		        <button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</button>
	      	</div>
		</div>
	</div>
</div>