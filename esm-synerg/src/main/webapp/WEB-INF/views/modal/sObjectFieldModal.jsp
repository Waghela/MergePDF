<jsp:include page="../../../taglibs.jsp"></jsp:include>

<div class="modal fade" style="width: 600px;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" style="padding: 0px;" block-ui="sObjectFieldModalBlock">
				<table class="table table-spriped table-hover table-condensed">
					<thead>
						<tr class="list-table">
							<th style="padding-left: 10px;padding-right: 0px;">#</th>
<!-- 							<th style="padding-left: 10px;padding-right: 0px;"><input type="checkbox" ng-model="cbSelectAll" ng-indeterminate="indeterminate" ng-change="toggleSelectAll(cbSelectAll)"> </th> -->
<!-- 							<th style="padding-left: 10px;padding-right: 0px;"><input type="checkbox" id="masterSelectAll" ng-model="selectAll" ng-indeterminate="indeterminate" ng-change="checkAll()"> </th> -->
				            <th>Label</th>
				            <th>Name</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="field in sObjectFields">
							<td style="padding-left: 10px;padding-right: 0px;">
								<input type="checkbox" ng-change="selectField(field,field.selected)" ng-model="field.selected" ng-disabled="field.name==='Id'">
							</td>
<!-- 							<td style="padding-left: 10px;padding-right: 0px;"><input type="checkbox" ng-model="field.selected" ng-change="fieldChange(field.selected)"></td> -->
							<td><b>{{ field.label }}</b></td>
				            <td>{{ field.name }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
		        <button type="button" ng-click="saveAndClose()" class="btn btn-primary" data-dismiss="modal"><i class="icon-ok"></i> Select ( {{ selectedFields.length }} )</button>
		        <button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Cancel</button>
	      	</div>
		</div>
	</div>
</div>