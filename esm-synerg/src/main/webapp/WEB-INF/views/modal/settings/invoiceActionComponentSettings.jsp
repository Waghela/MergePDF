<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" block-ui="invoiceActionComponentSettingsBlock" >
<!-- 				<div class="form-horizontal"> -->
					<form ng-submit="saveAndClose()" style="margin: 0">
						<div class="control-group">
							<label class="control-label" for="{{ section.title }}">Component Title</label>
							<div class="controls">
								<input ng-required="'true'" type="text" class="span5" id="{{ section.title }}" ng-model="section.title">
							</div>
						</div>
						<div class="layout-section form-horizontal">
							<div class="layout-section-title">Approver Fields SFDC Api Name</div>
							<div class="layout-section-content">
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.currentState.apiname }}">Current State</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.currentState.apiname }}" ng-model="section.fields.currentState.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.lastModifiedBy.apiname }}">Last Modified By</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.lastModifiedBy.apiname }}" ng-model="section.fields.lastModifiedBy.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.currentApprover.apiname }}">Current Approver</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.currentApprover.apiname }}" ng-model="section.fields.currentApprover.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.firstApprover.apiname }}">First Approver</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.firstApprover.apiname }}" ng-model="section.fields.firstApprover.apiname">
									</div>
								</div>
							</div> 
						</div>
						<div class="layout-section form-horizontal">
							<div class="layout-section-title">Comments</div>
							<div class="layout-section-content">
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.comments.label }}">Label</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.comments.label }}" ng-model="section.fields.comments.label">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.comments.apiname }}">SFDC Api Name</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.comments.apiname }}" ng-model="section.fields.comments.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.comments.required }}">Required</label>
									<div class="controls">
										<label class="checkbox inline" style="cursor: pointer;">
											<input type="checkbox" ng-model="section.fields.comments.required">
										</label>
									</div>
								</div>
							</div> 
						</div>
						<div class="layout-section form-horizontal">
							<div class="layout-section-title">User Action</div>
							<div class="layout-section-content">
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.useraction.label }}">Label</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.useraction.label }}" ng-model="section.fields.useraction.label">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.useraction.apiname }}">SFDC Api Name</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.useraction.apiname }}" ng-model="section.fields.useraction.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.useraction.herokupicklistapiname }}">Heroku Picklist Api Name</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.useraction.herokupicklistapiname }}" ng-model="section.fields.useraction.herokupicklistapiname">
									</div>
								</div>
							</div> 
						</div>
						<div class="layout-section form-horizontal">
							<div class="layout-section-title">Rejection Reason</div>
							<div class="layout-section-content">
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.rejectionreason.label }}">Label</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.rejectionreason.label }}" ng-model="section.fields.rejectionreason.label">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.rejectionreason.apiname }}">SFDC Api Name</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.rejectionreason.apiname }}" ng-model="section.fields.rejectionreason.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.rejectionreason.useractionvalue }}">User Action Value</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.rejectionreason.useractionvalue }}" ng-model="section.fields.rejectionreason.useractionvalue">
									</div>
								</div>
							</div> 
						</div>
						<div class="layout-section form-horizontal">
							<div class="layout-section-title">Route</div>
							<div class="layout-section-content">
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.route.label }}">Label</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.route.label }}" ng-model="section.fields.route.label">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.route.apiname }}">SFDC Api Name</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.route.apiname }}" ng-model="section.fields.route.apiname">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.route.referenceTo }}">Reference to</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.route.referenceTo }}" ng-model="section.fields.route.referenceTo">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label" for="{{ section.fields.route.useractionvalue }}">User Action Value</label>
									<div class="controls">
										<input ng-required="'true'" type="text" class="span3" id="{{ section.fields.route.useractionvalue }}" ng-model="section.fields.route.useractionvalue">
									</div>
								</div>
							</div> 
						</div>
						<button type="submit" style="display: none;"></button>
					</form>
<!-- 				</div> -->
			</div>
			<div class="modal-footer">
				<div class="draggable-actions" style="float: left;"><div class="required-fields-label">= Required information</div></div>
	        	<button type="button" ng-click="saveAndClose()" class="btn btn-primary" data-dismiss="modal"><i class="icon-ok"></i> Save</button>
	       	 	<button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</button>
	      	</div>
		</div>
	</div>
</div>