<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" block-ui="sectionSettingsBlock">
				<div class="form-horizontal">
					<form ng-submit="saveAndClose()" style="margin: 0">
						<div class="control-group" ng-if="field.type=='picklist' || field.type=='multipicklist'">
							<label class="control-label">Picklist Values</label>
							<div class="controls">
								<textarea rows="4" class="span4" ng-model="field.picklistValues" style="resize:none;margin-bottom: 0px;"></textarea>
							</div>
						</div>
<!-- 						<div class="control-group"> -->
<!-- 							<label class="control-label" for="{{ section.active }}">Active</label> -->
<!-- 							<div class="controls"> -->
<!-- 								<label class="checkbox inline" style="cursor: pointer;"> -->
<!-- 									<input type="checkbox" ng-model="section.active"> Show on <b>Layout</b> page -->
<!-- 								</label> -->
<!-- 							</div> -->
<!-- 						</div> -->
						<button type="submit" style="display: none;"></button>
					</form>
				</div>
			</div>
			<div class="modal-footer">
		        <button type="button" ng-click="saveAndClose()" class="btn btn-primary" data-dismiss="modal"><i class="icon-ok"></i> Save</button>
		        <button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</button>
		      </div>
		</div>
	</div>
</div>