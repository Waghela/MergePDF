<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" block-ui="sectionSettingsBlock">
				<div class="form-horizontal">
					<form ng-submit="saveAndClose()" style="margin: 0">
						<div class="control-group">
							<label class="control-label" for="{{ section.title }}">Title</label>
							<div class="controls">
								<input type="text" class="span3" id="{{ section.title }}" ng-model="section.title">
							</div>
						</div>
						<div class="control-group" ng-if="!section.component">
							<label class="control-label" for="{{ section.columns }}">Columns</label>
							<div class="controls">
								<label><input type="radio" ng-model="section.columns" value="1" style="margin: 0;"> One</label>
								<label><input type="radio" ng-model="section.columns" value="2" style="margin: 0;"> Two</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="{{ section.type }}">Type</label>
							<div class="controls">
								<label><input type="radio" ng-model="section.type" value="DETAIL" style="margin: 0;"> Detail</label>
								<label><input type="radio" ng-model="section.type" value="EDIT" style="margin: 0;"> Edit</label>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="{{ section.active }}">Active</label>
							<div class="controls">
								<label class="checkbox inline" style="cursor: pointer;">
									<input type="checkbox" ng-model="section.active"> Show on <b>Layout</b> page
								</label>
							</div>
						</div>
						<button type="submit" style="display: none;"></button>
					</form>
				</div>
			</div>
			<div class="modal-footer">
		        <button type="button" ng-click="saveAndClose()" class="btn btn-primary" data-dismiss="modal"><i class="icon-ok"></i> Save</button>
		        <button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</button>
		      </div>
		</div>
	</div>
</div>