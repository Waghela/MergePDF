<%@ include file="../../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-tasks"></i>
				<h3>My Task Sections</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="searchMyTaskSectionBlock">
			
				<div class="form-actions" style="margin-top: 0px;text-align: center;">
					<button type="button" class="btn btn-primary" ng-click="createNewMyTaskSection()"><i class="icon-plus"></i>&nbsp; Create New</button>
					<button type="button" class="btn" ng-click="loadMyTaskSections()"><i class="icon-refresh"></i>&nbsp; Reload</button>
				</div>
			
				<div class="controls" >
					
					<div ng-show = "hasCachedData" class="alert">
						Showing cached data, Please <strong>Reload</strong> search to get fresh data.
					</div>
					
					<table datatable="ng" class="table table-spriped table-hover table-condensed">
						<thead>
							<tr class="list-table">
					            <th>#</th>
					            <th>Title</th>
					            <th>Salesforce Object Api Name</th>
					            <th style="width: 50px;text-align: center;">Active</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="section in myTaskSections">
								<td>{{ section.sequence }}</td>
								<td><a href="#/mytasksection/{{ section.id }}"><b>{{ section.title }}</b></a></td>
					            <td>{{ section.sObjectApiName }}</td>
					            <td style="text-align: center;"><i class="icon-large {{ $root.showCheckBoxValue(section.active) }}"></i></td>
							</tr>
						</tbody>
					</table>
					
				</div>
				
				<div class="form-actions" style="margin-top : 45px;margin-bottom:0px;text-align: center;">
					<button type="button" class="btn btn-primary" ng-click="createNewMyTaskSection()"><i class="icon-plus"></i>&nbsp; Create New</button>
					<button type="button" class="btn" ng-click="loadMyTaskSections()"><i class="icon-refresh"></i>&nbsp; Reload</button>
				</div>
			</div>
			<!-- /widget-content -->
		</div>
	</div>
</div>