<%@ include file="../../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-cog"></i>
				<h3>Manage SObjects</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="manageSObjectBlock">
				<div class="form-actions" style="margin-top: 0px;text-align: center;">
					<button type="button" class="btn btn-success" ng-click="save()"><i class="icon-save"></i>&nbsp; Save</button>
					<button type="button" class="btn" ng-click="reloadAll()"><i class="icon-refresh"></i>&nbsp; Reload</button>
					<button type="button" class="btn" ng-click="backToList()"><i class="icon-circle-arrow-left"></i>&nbsp; Back to list</button>
				</div>
				<div class="controls">
					<div class="accordion" id="manageaccordion">
						<!-- Salesforce Objects -->
						<div class="accordion-group" >
							<div class="accordion-heading">
								<a 	class			="accordion-toggle" 
									data-toggle		="collapse"
									data-parent		="#manageaccordion" 
									href			="#collapseSfdcObjects">Salesforce Objects [ Total : {{ (sfdcSObjects) ? sfdcSObjects.length : 0 }} ]</a>
							</div>
							<div block-ui="sfdcSObjectBlock" id="collapseSfdcObjects" class="accordion-body collapse in" >
								<div class="accordion-inner" style="margin-bottom: 30px" >
									<table datatable="ng" class="table table-spriped table-hover table-condensed" >
										<thead>
											<tr>
												<th>Label</th>
												<th>Name</th>
												<th style="width: 50px;text-align: center;">Custom</th>
												<th style="width: 50px;text-align: center;">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="sObj in sfdcSObjects">
												<td><b>{{ sObj.label }}</b></td>
												<td>{{ sObj.name }}</td>
												<td style="text-align: center;"><i class="icon-large {{ $root.showCheckBoxValue(sObj.custom) }}"></i></td>
												<td style="text-align: center;"><button class="btn btn-mini btn-success" ng-click="addToHeroku(sObj)"><i class="icon-plus"></i></button></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- Heroku sObjects -->
						<div class="accordion-group" >
							<div class="accordion-heading">
								<a 	class			="accordion-toggle" 
									data-toggle		="collapse"
									data-parent		="#manageaccordion" 
									href			="#collapseHerokuSObjects">Heroku SObjects [ Total : {{ (herokuSObjects) ? herokuSObjects.length : 0 }} ]</a>
							</div>
							<div block-ui="herokuSObjectBlock" id="collapseHerokuSObjects" class="accordion-body collapse" >
								<div class="accordion-inner" style="margin-bottom: 30px">
									<table datatable="ng" class="table table-spriped table-hover table-condensed" >
										<thead>
											<tr>
												<th>Label</th>
												<th>Name</th>
												<th style="width: 50px;text-align: center;">Custom</th>
												<th style="width: 50px;text-align: center;">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr ng-repeat="sObj in herokuSObjects">
												<td><b>{{ sObj.label }}</b></td>
												<td>{{ sObj.name }}</td>
												<td style="text-align: center;"><i class="icon-large {{ $root.showCheckBoxValue(sObj.custom) }}"></i></td>
												<td style="text-align: center;"><button class="btn btn-mini btn-danger" ng-click="removeFromHeroku(sObj,$index)"><i class="icon-remove"></i></button></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="form-actions" style="margin-bottom:0px; text-align: center;">
					<button type="button" class="btn btn-success" ng-click="save()"><i class="icon-save"></i>&nbsp; Save</button>
					<button type="button" class="btn" ng-click="reloadAll()"><i class="icon-refresh"></i>&nbsp; Reload</button>
					<button type="button" class="btn" ng-click="backToList()"><i class="icon-circle-arrow-left"></i>&nbsp; Back to list</button>
				</div>
			</div>
			<!-- /widget-content -->
		</div>
	</div>
</div>