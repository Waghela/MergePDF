<%@ include file="../../../../taglibs.jsp" %>

<div class="row">
	<div class="span12">
		<div class="widget">
			<!-- widget-header -->
			<div class="widget-header">
				<i class="icon-list-alt"></i>
				<h3>SObject Layouts</h3>
			</div>
			<!-- /widget-header -->
			<!-- widget-content -->
			<div class="widget-content" block-ui="sObjectLayoutsBlock">
			
				<div class="form-actions" style="margin-top: 0px;text-align: center;">
					<button type="button" class="btn" ng-click="loadSObjectLayouts()"><i class="icon-refresh"></i>&nbsp; Reload</button>
				</div>
			
				<div class="controls" >
					
					<div ng-show = "hasCachedData" class="alert">
						Showing cached data, Please <strong>Reload</strong> search to get fresh data.
					</div>
					
					<table datatable="ng" class="table table-spriped table-hover table-condensed">
						<thead>
							<tr class="list-table">
					            <th>#</th>
					            <th>Title</th>
					            <th>Salesforce Object Api Name</th>
					            <th>Type</th>
					            <th style="width: 50px;text-align: center;">Active</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="layout in sObjectLayouts">
								<td>{{ $index + 1 }}</td>
								<td><a ng-click="navigateToEditPage(layout)" style="cursor: pointer;"><b>{{ layout.title }}</b></a></td>
					            <td>{{ layout.sObjectApiName }}</td>
					            <td><b>{{ layout.type }}</b></td>
					            <td style="text-align: center;"><i class="icon-large {{ $root.showCheckBoxValue(layout.active) }}"></i></td>
							</tr>
						</tbody>
					</table>
					
				</div>
				
				<div class="form-actions" style="margin-top : 45px;margin-bottom:0px;text-align: center;">
					<button type="button" class="btn" ng-click="loadSObjectLayouts()"><i class="icon-refresh"></i>&nbsp; Reload</button>
				</div>
			</div>
			<!-- /widget-content -->
		</div>
	</div>
</div>