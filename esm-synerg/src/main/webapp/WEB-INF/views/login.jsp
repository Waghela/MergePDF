<%@ include file="../../taglibs.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Login - Akritiv ESM</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	
	<link href="${local_css }/bootstrap.min.css" rel="stylesheet">
	<link href="${local_css }/bootstrap-responsive.min.css" rel="stylesheet">
	
	<link href="${local_css }/font-awesome.css" rel="stylesheet">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
	
	<link href="${local_css }/style.css" rel="stylesheet">
	<link href="${local_css }/pages/signin.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="account-container">
		<div class="content clearfix">
			<form name="loginForm" action="<c:url value="/login" />" method="POST">
				<h1>Login</h1>
				<div class="login-fields">
					<p>Please provide your details</p>
					<div class="field">
						<label for="username">Username:</label> 
						<input type="text" autocomplete="off"
							id="username" name="username" value="" placeholder="Username"
							class="login username-field" />
					</div>
					<!-- /field -->
					<div class="field">
						<label for="password">Password:</label> 
						<input type="password"
							id="password" name="password" value="" placeholder="Password"
							class="login password-field" />
					</div>
					<!-- /password -->
					<c:if test="${not empty error}">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">�</button>
							<strong>Error!</strong> ${error }
						</div>
					</c:if>
<%-- 					<c:if test="${not empty msg}"> --%>
<!-- 						<div class="alert alert-success"> -->
<!-- 							<button type="button" class="close" data-dismiss="alert">�</button> -->
<%-- 	                       <strong>Success!</strong> ${msg } --%>
<!-- 	                    </div> -->
<%--                     </c:if> --%>
				</div>
				<!-- /login-fields -->
				<div class="login-actions">
					<button class="button btn btn-success btn-large" type="submit">Sign In</button>
				</div>
				<!-- .actions -->
<%-- 				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> --%>
			</form>
<%-- 			<a href="${pageContext.request.contextPath}/sso/login/callback?federationid=703003620">SSO Login?</a> --%>
			<a href="http://sso-module.herokuapp.com/login">SSO Login?</a>
		</div>
		<!-- /content -->
	</div>
	<!-- /account-container -->

	<!-- common	scripts -->
	<script src="${local_js }/jquery-1.7.2.min.js" ></script>
	<script src="${local_js }/bootstrap.js" ></script>
	<script src="${local_js }/signin.js" ></script>
</body>
</html>
