<%@ include file="../../../taglibs.jsp"%>

<div block-ui = "componentBlock" >
	<table class="table table-spriped table-hover table-condensed">
		<thead>
			<tr>
				<th>Amount</th>
				<th ng-repeat="field in configurations.invoiceLineItemFields">
				    {{field.label }}
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="model in dataModelList | filter: {isRemoved: 'false'}" >
				<td>
				    <input class    	= "span2" 
				           ng-required 	= "'true'"
				           type     	= "number" 
					       ng-model 	= "model[configurations.invoiceLineItemAmountApiName]"
					       string-to-number 
					       ng-if		= "section.type=='EDIT'"
					       style		="margin-bottom: 0px;"/>
			        <label ng-if		= "section.type=='DETAIL'"
			        	   style		="margin-bottom: 0px;">
			        	   {{ $root.showNumberValue(model[configurations.invoiceLineItemAmountApiName],3) }}
	        	   </label>
			    </td>
				<td ng-repeat="field in configurations.invoiceLineItemFields">
					 <layout-section-component-field 
					    section = "section"
						field   = "field"
						model   = "model" 
						mode    = "section.type">
					</layout-section-component-field>
				</td>
				<td style="text-align: right;">
					<button class    = "btn btn-mini btn-danger"
					        type     = "button"
						    ng-click = "model['isRemoved'] = 'true'"
						    ng-if		= "section.type=='EDIT'">
						<i class="icon-remove"></i>
					</button>
				</td>
			</tr>
			<tr ng-if		= "section.type=='EDIT'">
				<td colspan="4" style="text-align: center;">
					<button type     = "button" 
					        class    = "btn btn-small btn-mini"
						    ng-click = "addItems();">
						<i class="icon-plus-sign"></i>&nbsp;Add more
					</button>
					<button type    = "button" 
					        class   = "btn btn-small btn-primary btn-mini"
						    ng-click= "save()">
						<i class="icon-ok"></i>&nbsp;Save
					</button>
					<button type     = "button" 
					        class    = "btn btn-mini btn-primary"
						    ng-click = "init()">
						<i class="icon-refresh"></i>&nbsp;Reload
					</button>
				</td>
			</tr>
		</tbody>
	</table>
</div>


