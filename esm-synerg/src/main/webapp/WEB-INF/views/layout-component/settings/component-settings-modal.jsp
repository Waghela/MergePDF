<%@ include file="../../../../taglibs.jsp" %>

<div class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" ng-click="close()" class="close btn btn-small" data-dismiss="modal" aria-hidden="true">
					<i class="icon-remove"></i>
				</button>
				<h3 class="modal-title">{{title}}</h3>
			</div>
			<div class="modal-body" block-ui="sectionCriteriaBlock">
				<div class="form-horizontal">
					<form ng-submit="saveAndClose()" style="margin: 0;">
						<div ng-include="section.componentMetadata.settings.templateUrl"></div>
						<button type="submit" style="display: none;"></button>
					</form>
				</div>
			</div>
			<div class="modal-footer">
		        <button type="button" ng-click="saveAndClose()" class="btn btn-primary" ><i class="icon-ok"></i> Save</button>
		        <button type="button" ng-click="close()" class="btn" data-dismiss="modal"><i class="icon-remove"></i> Close</button>
		      </div>
		</div>
	</div>
</div>