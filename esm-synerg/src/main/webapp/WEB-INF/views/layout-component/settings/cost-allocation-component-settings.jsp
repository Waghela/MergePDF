<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div block-ui="CostAllocationComponentSettingsBlockUi">
    <div class="layout-section-title">SObject</div>
    </br>
	<div class="control-group">
		<label class = "control-label" 
		       for   = "selectSObjects">Invoice Line 
        </label>
		<div class="controls">
			<select id="selectSObjects" 
			        ng-model   = "invoiceLineItemSObject"
			        ng-options = "sObj as sObj.label for sObj in sObjects" 
			        class	   = "span3"
			        ng-change  = "onInvoiceLineItemChange(invoiceLineItemSObject);changeSOQLQuery()">
				       <option value="" ng-disabled="'true'">--Select Invoice Line Item --</option>
			</select>
		</div>
	</div>
	<div class="layout-section-title">Fields</div>
	</br>
	<div>
		<table class="table table-spriped table-hover table-condensed">
			<thead>
				<tr >
					<th>Field</th>
					<th>Label</th>
					<th>Required</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="field in configurations.invoiceLineItemFields">
					<td>{{field.name}}</td>
					<td><input type     = "text"
					           ng-model = "field.label" 
					           class    = "span2"></td>
					<td>
					    <input type    = "checkbox"
					           ng-model= "field.required">
					</td>
					<td>
					    <button type     = "button"
					            class    = "btn btn-mini btn-danger"
					            ng-click = "configurations.invoiceLineItemFields.splice($index,1);changeSOQLQuery()">
					               <i class="icon-remove"></i>
					    </button>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<select ng-disabled = "!invoiceLineItemSObject"
						        ng-model    = "invoiceLineItemField"
						        ng-options  = "field as field.label for field in invoiceLineItemSObject.fields | filter: {type: 'reference',custom: true}"
						        class       = "span5">
							<option value="" >--Select Field--</option>
						</select>
						<button type			="button" 
								class			="btn btn-mini btn-primary" 
								ng-disabled		="!invoiceLineItemField || configurations.invoiceLineItemFields.length >= 5 " 
								ng-click		="(configurations.invoiceLineItemFields.indexOf(invoiceLineItemField) == -1) ? configurations.invoiceLineItemFields.push(invoiceLineItemField) : null ;changeSOQLQuery()">
								<i class="icon-plus"></i> Add
						</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="layout-section-title">Amount Configuration</div>
	</br>
	<div class="control-group">
		<label class = "control-label" 
		       for   = "invoiceLineItemAmount">Amount field
		</label>
		<div class="controls">
			<select ng-disabled = "!invoiceLineItemSObject"
			        id          = "invoiceLineItemAmount"
			        ng-change   = "changeSOQLQuery()" 
			        ng-model    = "configurations.invoiceLineItemAmountApiName" 
			        ng-options  = "field.name as field.label for field in invoiceLineItemSObject.fields | filter: {type: 'double'}" 
			        class       = "span3" >
				         <option value="" ng-disabled="'true'">--Select Amount Field--</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label"
		       for   = "invoiceAmount">Invoice Amount field:
		</label>
		<div class="controls">
			<select ng-disabled = "!invoiceLineItemSObject"
			        id          = "invoiceAmount"
			        ng-model    = "configurations.invoiceAmoutApiName"
			        ng-options  = "field.name as field.label for field in invoiceSObject.fields | filter: {type: 'double'}"
			        class       = "span3" >
				<option ng-disabled="'true'">--Select Invoice Amount Field--</option>
			</select>
		</div>
	</div>
	<div class="layout-section-title">SOQL QUERY </div>
	</br>
	<div class="control-group">
		<label class = "control-label" 
		       for   = "invoiceAmount">Where Clause:
		</label>
		<div class="controls">
		    <textarea ng-model  = "configurations.whereClause" 
		              rows      = "4" 
		              class     = "span3"
		              style     = "resize:none;"
		              ng-change = "changeSOQLQuery()" >
		    </textarea>
		</div>
	</div>
	<div class="control-group">
		<label 
		       class = "control-label"
		       for   = "invoiceAmount">SOQL:
		</label>
		<div class="controls">
		    <textarea ng-model = "soqlQuery" 
		              rows     = "4" 
		              class    = "span3" 
		              style    = "resize:none;"
		              readonly = "readonly">
		    </textarea>
		</div>
	</div>
	<div class="control-group">
		<label class = "control-label" 
		       for   = "invoiceAmount">Record Limit:
		</label>
		<div class="controls">
			<input  type      = "number"
			        ng-model  = "configurations.recordLimit" 
			        class     = "span2"
			        ng-change = "changeSOQLQuery()">
		</div>
	</div>
	<div class="control-group">
	    <button   type     = "button" 
	              style    = "float:right;"
				  class	   = "btn btn-mini btn-primary" 
				  ng-click = "validateQuery()">
			<i class="icon-ok"></i> Validate
		</button>
	</div>
</div>
