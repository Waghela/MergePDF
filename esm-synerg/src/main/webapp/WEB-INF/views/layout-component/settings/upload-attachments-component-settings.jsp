<jsp:include page="../../../../taglibs.jsp"></jsp:include>

<div block-ui="UploadAttachmentsComponentSettingsBlockUi">
	<div class="layout-section-title">Maximum File Size</div>

	</br>
	<div class="control-group">
		<label class="control-label" for="inputAllowedSize">File Size
		</label>
		<div class="controls">
			<input type="text" id="inputAllowedSize"
				ng-model="configurations.allowedSize" class="span2">
		</div>
	</div>
	


	<div class="layout-section-title">Allowed Extensions</div>
	</br>
	<div>
		<table class="table table-spriped table-hover table-condensed">
			<thead>
				<tr>
					<th>Extensions</th>

					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="ext in allowedExtentions">
					<td>{{ ext }}</td>
					<td>
						<button type="button" class="btn btn-mini btn-danger"
							ng-click="allowedExtentions.splice($index,1)">
							<i class="icon-remove"></i>
					</td>
				</tr>
				<tr>
					<td colspan="4"><input type="text"
						  ng-model="allowExt"
						class="span2" />

						<button type="button" class="btn btn-primary btn-mini"
							ng-disabled="!allowExt"
							ng-click="(allowedExtentions.indexOf(allowExt) == -1) ? allowedExtentions.push(allowExt) : null;concatAllowedExtentions();">
							<i class="icon-plus"></i> Add
						</button></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="layout-section-title">Primary Document</div>
	</br>
	<div class="control-group">
			<label class="control-label" for="checkIsPrimaryAllow" style="width: 30%;">Allow Primary Document
		</label>
		<div class="controls">
			<input type="checkbox" ng-model="configurations.allowAttachPrime" id="checkIsPrimaryAllow" >
		</div>
		<table class="table table-spriped table-hover table-condensed" ng-if="configurations.allowAttachPrime">
			<thead>
				<tr>
					<th>Extensions For Primary Documents</th>

					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="ext in allowedExtentionsForPrime">
					<td>{{ ext }}</td>
					<td>
						<button type="button" class="btn btn-mini btn-danger"
							ng-click="allowedExtentionsForPrime.splice($index,1)">
							<i class="icon-remove"></i>
						</button>
					</td>
				</tr>
				<tr>
					<td colspan="4"><input type="text" 
						  ng-model="allowExtForPriamry"
						class="span2" />

						<button type="button" class="btn btn-primary btn-mini"
							ng-disabled="!allowExtForPriamry"
							ng-click="(allowedExtentionsForPrime.indexOf(allowExtForPriamry) == -1) ? allowedExtentionsForPrime.push(allowExtForPriamry) : null;concatAllowedExtentionsForPrime();">
							<i class="icon-plus"></i> Add
						</button></td>
				</tr>
			</tbody>
		</table>
	</div>

</div>


