<%@ include file="../../../../taglibs.jsp" %>
<div block-ui="listComponentSettingsBlock">
	<div class="control-group">
		<label class="control-label" for="selectSObject">SObject</label>
		<div class="controls">
			<select ng-model="selectedSObject" 
					ng-options="sObj as sObj.label for sObj in sObjects" 
					ng-change="onSObjectSelectChange(selectedSObject);changeSOQLQuery();" >
				<option value="" ng-disabled="'true'">--Select--</option>
			</select>
		</div>
	</div>
	<div>
		<table class="table table-spriped table-hover table-condensed">
			<thead>
				<tr class="section-header">
					<th>Fields</th>
					<th>Data type</th>
					<th style="width:50px;text-align: right;">Remove</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="field in configurations.sobjectFields track by field.name">
					<td>{{ field.label }}</td>
					<td>{{ field.type }}</td>
					<td style="width:50px;text-align: right;"><a class="btn btn-danger btn-mini" ng-click="configurations.sobjectFields.splice($index,1);changeSOQLQuery();"><i class="icon-remove"></i></a></td>
				</tr>
				<tr>
					<td colspan="3">
						<select ng-model="field" ng-options="field as field.label for field in selectedSObject.fields" class="span5">
							<option value="" ng-disabled="'true'">--Select--</option>
						</select>
						<button type="button" ng-click="addField(field);changeSOQLQuery();" ng-disabled="!field" class="btn btn-primary"><i class="icon-plus"></i> Add</button>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="layout-section-title">SOQL QUERY </div></br>
	<div class="control-group">
		<label class="control-label" for="invoiceAmount">Where Clause:</label>
		<div class="controls">
		    <textarea ng-model="configurations.whereClause" rows="4" class="span3" style="resize:none;" ng-change="changeSOQLQuery()" >
		    </textarea>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="invoiceAmount">SOQL:</label>
		<div class="controls">
		    <textarea ng-model="configurations.soqlQuery" rows="4" class="span3" style="resize:none;" readonly="readonly">
		    </textarea>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" for="invoiceAmount">Record Limit:</label>
		<div class="controls">
			<input type="number" ng-model="configurations.recordLimit" class="span2" ng-change="changeSOQLQuery()">
		</div>
	</div>
	<div class="control-group">
	    <button   type="button" style="float:right;"
				  class			="btn btn-primary" 
				  ng-click		="validateQuery()">
			<i class="icon-ok"></i> Validate
		</button>
	</div>
</div>