<%@ include file="../../../taglibs.jsp"%>
<div block-ui="downloadPrimaryDocumentBlock">
	<a class="btn btn-primary btn-mini " style="margin-left:38%"
		ng-show="attachmentId != null && attachmentId != ''"
		href="rest/layout-component/download-primary-document-component/viewPrimaryDocument?id={{attachmentId}}">
		<i class="icon-large icon-download-alt"></i> View Primary Document
	</a>
</div>