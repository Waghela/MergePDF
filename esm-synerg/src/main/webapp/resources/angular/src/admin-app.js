'use strict';

var AdminApp = angular.module('ESM-ADMIN',['ui.router','sticky','dndLists']);

AdminApp.config(function($stateProvider){
	/**
	 * UI-ROUTER CONFIGURATION
	 */
	$stateProvider
		/******************************************************************************************** SOBJECT ***/
		.state("admin-sobjects",{ 
			url 			: "/sobjects", 
			templateUrl 	: "admin/sobject/list", 
			controller		: "SObjectListController",
			params			: { reload : false }
		})
		.state("admin-sobjects-manage",{ 
			url 			: "/sobject/manage", 
			templateUrl 	: "admin/sobject/manage", 
			controller		: "SObjectManageController" 
		})
		/************************************************************************************ MY TASK SECTION ***/
		.state("admin-mytasksections",{ 
			url 			: "/mytasksections", 
			templateUrl 	: "admin/mytasksection/list", 
			controller		: "MyTaskSectionListController",
			params			: { reload : false }
		})
		.state("admin-mytasksections-create",{ 
			url 			: "/mytasksections/create", 
			templateUrl 	: "admin/mytasksection/edit", 
			controller		: "MyTaskSectionCRUDController",
			params			: { title : 'Create MyTask Section'}
		})
		.state("admin-mytasksections-edit",{ 
			url 			: "/mytasksection/{id}", 
			templateUrl 	: "admin/mytasksection/edit", 
			controller		: "MyTaskSectionCRUDController",
			params			: { title : 'Edit MyTask Section'}
		})
		/******************************************************************************************** LAYOUTS ***/
		.state("admin-layouts",{ 
			url 			: "/layouts",
			templateUrl 	: "admin/layout/list", 
			controller		: "LayoutListController" 
		})
		.state("admin-layouts-edit",{ 
			url 			: "/layout/{id}",
			templateUrl 	: "admin/layout/edit", 
			controller		: "LayoutEditController",
			params			: { details : null }
		})
		/************************************************************************************************ NEW ***/
		;
});