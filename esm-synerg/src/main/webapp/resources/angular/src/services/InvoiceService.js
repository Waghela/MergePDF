'use strict';

App.service('InvoiceService',
		[	'$http',
 	function($http){
			
		return {
			loadPendingInvoicesToApprove		: loadPendingInvoicesToApprove,
			loadExceptionQueueToApprove			: loadExceptionQueueToApprove,
			searchInvoices						: searchInvoices,
			loadInvoiceDetails					: loadInvoiceDetails,
			submitInvoice						: submitInvoice,
			saveNonPoLineItems					: saveNonPoLineItems,
			loadInvoiceLineItems				: loadInvoiceLineItems,
			loadInvoiceMetadata					: loadInvoiceMetadata,
			saveInvoice							: saveInvoice
		};
		
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadPendingInvoicesToApprove(){
			return $http.get("rest/invoice/pendingapproval/toapprove");
		};
		function loadExceptionQueueToApprove(){
			return $http.get("rest/invoice/parkedinvoices/toapprove");
		};
		function searchInvoices(componentData){
			return $http.post("rest/invoice/search",componentData);
		};
//		function searchInvoices(){
//			return $http.get("rest/invoice/toapprove");
//		};
		function loadInvoiceDetails(invoiceId){
			return $http.get("rest/invoice/details/" + invoiceId);
		};
		function submitInvoice(invoice){
			return $http.post("rest/invoice/submit", invoice);
		};
		function saveNonPoLineItems(requestModel){
			return $http.post("rest/invoice/savenonpolineitems", requestModel);
		};
		function loadInvoiceLineItems(invoice){
			return $http.post("rest/invoice/loadinvoicelineitems", invoice);
		};
		function loadInvoiceMetadata(){
			return $http.get("rest/invoice/metadata");
		};
		function saveInvoice(requestModel){
			return $http.post("rest/invoice/save", requestModel);
		};
}]);

