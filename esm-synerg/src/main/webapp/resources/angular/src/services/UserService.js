'use strict';

App.service('UserService',
		[	'$http',
 	function($http){
			
		return {
			loadUserProfile				    : loadUserProfile,
			saveUserDetails					:saveUserDetails,
			savePassword			        :savePassword
		};
		
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadUserProfile(){
			return $http.get("rest/user/userProfile");
		};
		function saveUserDetails(userData){
			return $http.post("rest/user/saveUserDetails", userData);
		};
		function savePassword(password){
			return $http.post("rest/user/savePassword", password);
		};
		
}]);

