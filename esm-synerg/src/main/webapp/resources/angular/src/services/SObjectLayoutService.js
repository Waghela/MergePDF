'use strict';

AdminApp.service('SObjectLayoutService',
		[	'$http',
 	function($http){
			
		return {
			loadSObjectLayouts				: loadSObjectLayouts,
			loadSObjectLayoutDetails		: loadSObjectLayoutDetails,
			saveSObjectLayout				: saveSObjectLayout
		};
		
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadSObjectLayouts(){
			return $http.get("rest/admin/layout/list");
		};
		function loadSObjectLayoutDetails(id){
			return $http.get("rest/admin/layout/details/" + id);
		};
		function saveSObjectLayout(layoutDetails){
			return $http.post("rest/admin/layout/save" ,layoutDetails);
		};
}]);
