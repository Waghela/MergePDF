'use strict';

App.service('NotesAndAttachmentsService', [
		'$http',
		function($http) {

			return {
				getNote : getNote,
				getNotesDetails : getNotesDetails,
				loadAttachmentsDetails : loadAttachmentsDetails,
			};

			/*
			 * -------------------------------------------------------------------
			 * PUBLIC FUNCTIONS
			 * -------------------------------------------------------------------
			 */
			function getNote(id) {
				return $http.get("rest/notesAndAttachments/note/" + id);
			}
			;
			function getNotesDetails(id) {
				return $http.get("rest/notesAndAttachments/loadNotesDetails/"
						+ id);
			}
			;
			function loadAttachmentsDetails(id) {
				return $http
						.get("rest/notesAndAttachments/loadAttachmentsDetails/"
								+ id);
			}
			;
		} ]);
