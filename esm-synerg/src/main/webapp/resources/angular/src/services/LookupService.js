'use strict';

App.service('LookupService',
		[	'$http',
 	function($http){
			
		return {
			loadCostCodes			: loadCostCodes,
			loadGLCodes				: loadGLCodes,
			loadApproverUsers		: loadApproverUsers,
			loadNote				: loadNote,
			loadGenericLookUpResult	: loadGenericLookUpResult
		};
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function loadCostCodes(costCodeModel){
			return $http.post("rest/costcode/search", costCodeModel);
		};
		function loadGLCodes(glCodeModel){
			return $http.post("rest/glcode/search", glCodeModel);
		};
		function loadApproverUsers(approverUserModel){
			return $http.post("rest/buyeruser/search", approverUserModel);
		};
		function loadNote(id){
			return $http.get("rest/notesAndAttachments/note/"+id);
		};
		function loadGenericLookUpResult(genericModel){
			return $http.post("rest/genericModal/search", genericModel);
		};
}]);

