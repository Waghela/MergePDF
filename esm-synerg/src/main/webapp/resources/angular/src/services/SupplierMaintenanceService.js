'use strict';

App.service('SupplierMaintenanceService',
		[	'$http',
 	function($http){
			
		return {
			searchSupplierMaintenance				: searchSupplierMaintenance,
			myTaskSupplierMaintenanceRecords		: myTaskSupplierMaintenanceRecords,
			loadSupplierMaintenanceDetails			: loadSupplierMaintenanceDetails,
			submitSupplierMaintenanceDetails		: submitSupplierMaintenanceDetails,
			loadSupplierMaintenanceFields			: loadSupplierMaintenanceFields,
			saveSupplierMaintenance					: saveSupplierMaintenance
		};
		
		
		/* -------------------------------------------------------------------
		 * PUBLIC FUNCTIONS 
		 * -------------------------------------------------------------------*/
		function searchSupplierMaintenance(componentData){
			return $http.post("rest/supplier-maintenance/search",componentData);
		};
		function myTaskSupplierMaintenanceRecords(){
			return $http.get("rest/supplier-maintenance/mytask");
		};
		function loadSupplierMaintenanceDetails(supplierMaintenanceId){
			return $http.get("rest/supplier-maintenance/details/" + supplierMaintenanceId);
		};
		function submitSupplierMaintenanceDetails(supplierMaintenanceDetails){
			return $http.post("rest/supplier-maintenance/submit", supplierMaintenanceDetails);
		};
		function loadSupplierMaintenanceFields(){
			return $http.get("rest/supplier-maintenance/create");
		};
		function saveSupplierMaintenance(supplierMaintenance){
			return $http.post("rest/supplier-maintenance/save", supplierMaintenance);
		};
}]);