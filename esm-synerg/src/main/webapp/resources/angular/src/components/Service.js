'use strict';

AdminApp.service('ComponentService',['ComponentRepository', function(ComponentRepository){
	
		var self = this;
	
		this.list = function(sObjectApiName){
			var list = [];
			angular.forEach(ComponentRepository.componentList,function(component,index){
				if(component.type == ComponentRepository.componentType.public || component.sObjectApiName == sObjectApiName){
					var section = self.convertToSection(component);
					list.push(section);
				}
			});
			return list;
		};
		
		this.convertToSection = function(component){
			return {
				id 					: null,
				columns 			: 0,
				custom 				: true,
				title 				: component.title,
				component 			: true,
				componentName 		: component.name,
				componentMetadata 	: component.directive,
				sequence 			: 1,
				active 				: false,
				type 				: "DETAIL",
				deleted 			: false
			};
		};
}]);