'use strict';

AdminApp.controller('ComponentSettingsController',
		[	'$scope','$rootScope','$stateParams','$controller','$http','$timeout','blockUI','$element','title','close','LookupService','Notification','section',
 	function($scope , $rootScope , $stateParams , $controller , $http , $timeout , blockUI , $element , title , close , LookupService , Notification , section ){
		$scope.title = title;
		$scope.section = section;
		$scope.configurations = ($scope.section.componentMetadata.configurations) ? $scope.section.componentMetadata.configurations : null;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
		};
		
		$scope.saveAndClose = function(){
//			console.log("COMPONENT CONFIGS :: "+JSON.stringify($scope.configurations));
			$scope.section.componentMetadata.configurations = $scope.configurations; 
//			console.log("COMPONENT METADATA CONFIGS :: "+JSON.stringify($scope.section.componentMetadata));
			$element.modal('hide');
			close($scope.section, 500); // close, but give 500ms for bootstrap to animate
		};
		
		angular.extend(this,$controller($scope.section.componentMetadata.settings.controller,{$scope: $scope}));
}]);