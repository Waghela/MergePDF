'use strict';

App.controller('DownloadPrimaryDocumentComponentController',
		[	'$scope','$log','$http','blockUI','$stateParams',
	function($scope , $log , $http , blockUI , $stateParams){
		
		/** OVERRIDE */
		$scope.isComponentValid = function(){
			return false;
		}
		 $scope.loadPrimaryDocumentDetails = function(){
				var downloadPrimaryDocumentBlock = blockUI.instances.get("downloadPrimaryDocumentBlock");
				downloadPrimaryDocumentBlock.start('Loading primary document details');
				$scope.jsonNode = {};
				$scope.jsonNode = {
						id :$stateParams.id,
						sectionId :$scope.section.id,
						
				};
	           
				$http.post("rest/layout-component/download-primary-document-component/primaryDocumentDetails",$scope.jsonNode)
					.success(function(response){
						if(response.status == "SUCCESS"){
							$scope.attachmentId = response.data.attachmentId;
							
						}else{
							Notification.error({
								message	:response.error[0].message,
								title	:'Error'
							});
						}
						downloadPrimaryDocumentBlock.stop();
					})
					.error(function(response){
						console.log(response);
						downloadPrimaryDocumentBlock.stop();
						Notification.error({
							message	:'Error Occured in loading record history !!',
							title	:'Error'
						});					
					});
			}
		/** OVERRIDE */
		$scope.init = function(){
			console.log( $scope.section.title +" DownloadPrimaryDocumentComponentController Initializing...");
			 $scope.loadPrimaryDocumentDetails();
		
		};
		$scope.init();
	}
]);