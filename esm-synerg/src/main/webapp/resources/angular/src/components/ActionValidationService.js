'use strict';

App.service('ActionValidationService',function(){
	
	this.registeredItems = [];
	
	this.register = function(itemId,title){
		var item = {
				id			: itemId,
				title		: title,
				status		: "INIT", 			
				valid		: false
			};
		this.registeredItems.push(item);
		return item; 
	};
	this.triggerValidation = function(){
		angular.forEach(this.registeredItems,function(item){
			item.status = "START"; 
		});
	};
	this.registeredItem = function(itemId){
		var registeredItem = {};
		angular.forEach(this.registeredItems,function(item){
			if(item.id == itemId){
				registeredItem = item;
			}
		});
	}
	this.doValidation = false;			// Validation trigger
	this.hasUserAction = true;
	
});