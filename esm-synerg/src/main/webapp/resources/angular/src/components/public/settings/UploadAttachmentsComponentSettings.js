'use strict';

AdminApp.controller('UploadAttachmentsComponentSettingsController',
		[	'$scope','SObjectService','blockUI','$http','Notification',
 	function($scope , SObjectService , blockUI , $http,Notification){
		// Save your configuration in $scope.configurations

	  
	   $scope.isComponentValid = function(){
				return true;
	   }
	  
	   $scope.allowedExtentions=[];
	   $scope.allowedExtentionsForPrime=[];
		$scope.configurations = ($scope.configurations) ? $scope.configurations :{
			allowedSize : '10MB',
			allowedExt : ".pdf,.xls,.xlsx,.txt,.png,.jpeg,.csv" ,
			allowedExtForPrime:".pdf",
			allowAttachPrime : false
		};
		$scope.init = function(){
			
			angular.forEach($scope.configurations.allowedExt.split(','),function(ext){
				$scope.allowedExtentions.push(ext);
			});
			
			angular.forEach($scope.configurations.allowedExtForPrime.split(','),function(ext){
				$scope.allowedExtentionsForPrime.push(ext);
			});
		};
		$scope.concatAllowedExtentionsForPrime= function (){
			$scope.allowedExtForPrime='';
			angular.forEach( $scope.allowedExtentionsForPrime,function(ext){
				$scope.allowedExtForPrime += ext + ',';
			});
		};
		$scope.concatAllowedExtentions= function (){
			$scope.configurations.allowedExt='';
			angular.forEach( $scope.allowedExtentions,function(ext){
				$scope.configurations.allowedExt += ext + ',';
			});
		};
		$scope.init();
		console.log("UploadAttachmentsComponentSettingsController initialized...");
}]);