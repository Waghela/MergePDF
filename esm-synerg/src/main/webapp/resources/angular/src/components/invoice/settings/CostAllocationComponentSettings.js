'use strict';

AdminApp.controller('CostAllocationComponentSettingsController',
		[	'$scope','SObjectService','blockUI','$http','Notification',
 	function($scope , SObjectService , blockUI , $http,Notification){
		// Save your configuration in $scope.configurations

	   $scope.isComponentValid = function(){
				
	   }
	   $scope.changeSOQLQuery = function () {
			   var fields = '';
			   $scope.configurations.isSOQLValidate = false;
			   angular.forEach($scope.configurations.invoiceLineItemFields,function(sObject){
				   fields += sObject.name + ',';
			   });
			   fields += $scope.configurations.invoiceLineItemAmountApiName;
			   $scope.soqlQuery = "SELECT " + fields + " FROM " + $scope.configurations.invoiceLineItemApiName ;
			   if($scope.configurations.whereClause != null && $scope.configurations.whereClause != ''){
				   $scope.soqlQuery += ' WHERE ' + $scope.configurations.whereClause;
			   }
			   if($scope.configurations.recordLimit != null && $scope.configurations.recordLimit != ''){
				   $scope.soqlQuery += ' LIMIT ' + $scope.configurations.recordLimit;
			   }
		   
	   }
	   $scope.validateQuery = function () {
		   var CostAllocationComponentSettingsBlockUi = blockUI.instances.get('CostAllocationComponentSettingsBlockUi');
			CostAllocationComponentSettingsBlockUi.start("Validating SOQL...");
		   $http.post("rest/layout-component/cost-allocation-component/validateQuery",$scope.soqlQuery)
			.success(function(response){
				if(response.status=="SUCCESS"){
					$scope.configurations.isSOQLValidate = true;
					$scope.configurations.soqlQuery =$scope.soqlQuery ;
					Notification.success({
						message	:"SOQL query is correct.",
						title	:'Sucess'
					});
				}else{
					$scope.configurations.isSOQLValidate = false;
					Notification.error({
						message	:response.errors[0].message,
						title	:'Error In Soql'
					});
				}
				CostAllocationComponentSettingsBlockUi.stop();
			})
			.error(function(response){
				CostAllocationComponentSettingsBlockUi.stop();
				$scope.configurations.isSOQLValidate = true;
				Notification.error({
					message	:"Unexpected error occured while validating soql sObjects!",
					title	:'Error'
				});				
			});
	   
      }
	   
      $scope.loadSObjects = function(){
		var CostAllocationComponentSettingsBlockUi = blockUI.instances.get('CostAllocationComponentSettingsBlockUi');
		CostAllocationComponentSettingsBlockUi.start("Loading sObjects...");
		
		$http.get("rest/layout-component/cost-allocation-component/sobjects")
			.success(function(response){
				if(response.status=="SUCCESS"){
					$scope.sObjects = response.data.sObjects;
					angular.forEach($scope.sObjects,function(sObject){
					if($scope.configurations.invoiceLineItemApiName == sObject.name){
							$scope.invoiceLineItemSObject = sObject;
					}
					if(sObject.name == 'akritivesm__Invoice__c'){
							$scope.invoiceSObject = sObject;
					}
						  $scope.changeSOQLQuery();
				  });
				}else{
					Notification.error({
						message	:"Error occured while loading sObjects!!",
						title	:'Error'
					});	
				}
					CostAllocationComponentSettingsBlockUi.stop();
				})
				.error(function(response){
					CostAllocationComponentSettingsBlockUi.stop();
					Notification.error({
						message	:"Unexpected error occured while loading sObjects!",
						title	:'Error'
					});	
				});
		};
		$scope.onInvoiceLineItemChange = function(sObject){
			$scope.configurations.invoiceLineItemApiName = sObject.name;
			$scope.configurations.invoiceLineItemFields = [];
		}
		
		$scope.configurations = ($scope.configurations) ? $scope.configurations :{
			invoiceLineItemApiName 			: null,
			invoiceLineItemFields			: [],
			invoiceLineItemAmountApiName	: null,
			invoiceAmoutApiName				: null,
			whereClause						: null,
			recordLimit						: 0,
			soqlQuery						: '',
			isSOQLValidate					:false,
		};
		$scope.init = function(){
			$scope.soqlQuery = $scope.configurations.soqlQuery;
			$scope.loadSObjects();
		};
		$scope.init();
		console.log("CostAllocationComponentSettingsController initialized...");
}]);