'use strict';

AdminApp.controller('DownloadPrimaryDocumentComponentSettingsController',
		[	'$scope','SObjectService','blockUI','$http','Notification',
 	function($scope , SObjectService , blockUI , $http , Notification){
		// Save your configuration in $scope.configurations

	  
	   $scope.isComponentValid = function(){
				
	   }
	  
	   $scope.onSObjectChange = function(sObject){
			$scope.configurations.sObjectApiName = sObject.name;
		}
	   $scope.onAttachmentIdFieldChange = function(field){
			$scope.configurations.attachmentIdField = field;
		}
	   $scope.getTrackerSObjectFields= function (trackerSObjectField){
		   $scope.configurations.trackerSObjectField = trackerSObjectField;
		   angular.forEach($scope.sObjects,function(sobject){
				if(trackerSObjectField.referenceTo.replace(",","") == sobject.name){
					
						$scope.trackerSObject = sobject;
				}	
				
				
			  });
	   }
      $scope.loadSobjects = function(){
		var DownloadPrimaryDocumentComponentSettingsBlockUi = blockUI.instances.get('DownloadPrimaryDocumentComponentSettingsBlockUi');
		DownloadPrimaryDocumentComponentSettingsBlockUi.start("Loading sObjects...");
		
		$http.get("rest/layout-component/download-primary-document-component/sobjects")
			.success(function(response){					// SUCCESS
				if(response.status=="SUCCESS"){
					$scope.sObjects = response.data.sObjects;
					angular.forEach($scope.sObjects,function(sObject){
						if($scope.configurations.sObjectApiName == sObject.name){
								$scope.sObject = sObject;
								angular.forEach($scope.sObjects,function(sObject2){
									if($scope.configurations.trackerSObjectField != null && $scope.configurations.trackerSObjectField.referenceTo.replace(",","") == sObject2.name){
										$scope.trackerSObject = sObject2;	
										angular.forEach(sObject.fields,function(field){
											if($scope.configurations.trackerSObjectField != null && $scope.configurations.trackerSObjectField.name == field.name){
												$scope.trackerIdLookupFiled = field;
												angular.forEach($scope.trackerSObject.fields,function(field2){
													if($scope.configurations.attachmentIdField != null && $scope.configurations.attachmentIdField.name == field2.name){
														$scope.attachmentIdField = field2;									
													}	
												});
											}	
										});
									}	
								});
								
								
								
						}	
						
						
					  });
				}else{
					Notification.error({
						message	:"Error occured while sobjects loading!!",
						title	:'Error'
					});	
				}
				DownloadPrimaryDocumentComponentSettingsBlockUi.stop();
			})						
			.error(function(response){					// ERROR
				DownloadPrimaryDocumentComponentSettingsBlockUi.stop();
				Notification.error({
					message	:"Unexpected error occured while sobjects loading!",
					title	:'Error'
				});	
			});
		};
		
		
		$scope.configurations = ($scope.configurations) ? $scope.configurations :{
			sObjectApiName 								: null,
			trackerSObjectField	: null,
			attachmentIdField   : null,
			

		};
		$scope.init = function(){
			$scope.loadSobjects();
		};
		$scope.init();
		console.log("DownloadPrimaryDocumentComponentSettingsController initialized...");
}]);