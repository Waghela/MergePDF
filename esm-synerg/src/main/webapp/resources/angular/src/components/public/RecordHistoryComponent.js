'use strict';

App.controller('RecordHistoryComponentController',
		[	'$scope','$log','$http','blockUI','$stateParams',
	function($scope , $log , $http , blockUI , $stateParams){
			
		
	
		/** OVERRIDE */
		$scope.isComponentValid = function(){
			return false;
		}
		 $scope.loadRecordHistory = function(){
				var recordHistoryBlock = blockUI.instances.get("recordHistoryBlock");
				recordHistoryBlock.start('Loading invoice line items...');
				$scope.jsonNode = {};
				$scope.jsonNode = {
						id :$stateParams.id,
						sectionId :$scope.section.id,
						
				};
	           
				$http.post("rest/layout-component/record-history-component/recordHistory",$scope.jsonNode)
					.success(function(response){
						if(response.status == "SUCCESS"){
							$scope.historyData = response.data.historyData;
							
						}else{
							Notification.error({
								message	:response.error[0].message,
								title	:'Error'
							});
						}
						recordHistoryBlock.stop();
					})
					.error(function(response){
						console.log(response);
						recordHistoryBlock.stop();
						Notification.error({
							message	:'Error Occured in loading record history !!',
							title	:'Error'
						});					
					});
			}
		/** OVERRIDE */
		$scope.init = function(){
			console.log( $scope.section.title +" RecordHistoryComponentController Initializing...");
			 $scope.loadRecordHistory();
		
		};
		$scope.init();
	}
]);