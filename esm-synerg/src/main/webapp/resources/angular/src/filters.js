'use strict';

App.filter('currencyFilter', 
			[ '$filter',
	  function($filter){
			return function(input){
				var numberFilter = $filter('number');
				var inputVal = (input) 
								? numberFilter(input,2).toString().trim().replace(',','').trim() 
								: null;
				return parseFloat(inputVal);
			};
}]);
