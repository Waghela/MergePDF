'use strict';

App.directive('ngRequired', function($compile) {
	return {
		link : function(scope, element, attrs) {
			if(attrs.ngRequired==="true"){
				element.attr('required',true);
				element.removeAttr('ng-required');
				$compile(el[0])(scope);
			}
		}
	};
});

App.directive('stringToNumber', function() {
	return {
		require : 'ngModel',
		link : function(scope, element, attrs, ngModel) {
			ngModel.$parsers.push(function(value) {
				return '' + value;
			});
			ngModel.$formatters.push(function(value) {
				return parseFloat(value, 10);
			});
		}
	};
});

App.directive('ngIndeterminate', function($compile) {
    return {
        restrict: 'A',
        link: function(scope, element, attributes) {
            scope.$watch(attributes['ngIndeterminate'], function (value) {
                element.prop('indeterminate', !!value);
            });
        }
    };
});
//HISTORY COMPONENT DIRECTIVE
App.directive('historyComponent', function($compile) {
	return {
		restrict	:'E',
		scope		:{ 
						model			:'=',
					 },
		templateUrl	:"history",
		controller	:function($scope){
			$scope.model.loadData();
		}

	};
});
//NOTES & ATTACHMENTS COMPONENT DIRECTIVE
App.directive('attachmentsComponent',
	[		'$compile','$http','blockUI','$log','esmCache','$timeout','ModalService',
	function($compile , $http , blockUI , $log , esmCache , $timeout,ModalService) {
		return {
			restrict	:'E',
			scope		:{ 
							model			:'=',
						 },
			templateUrl	:"notesAndAttachments",
			controller	:function($scope){
				$scope.model.loadData();
				
				// FUNCTION :: showNoteModal()
				$scope.showNoteModal = function(id){
				ModalService.showModal({
						templateUrl		: "modal/note",
						controller		: "NoteLookupModalController",
						inputs			: {
							title	: "Notes",
							noteId		: id,
						}
					}).then(function(modal){
						modal.element.modal();
						modal.close.then(function(result){
							//note.costCode = result;
						});
					});
				};
			}
		};
}]);
