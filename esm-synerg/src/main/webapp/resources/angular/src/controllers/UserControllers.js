'use strict';

/** *********************** USER PROFILE CONTROLLER ************************ */
App.controller('UserProfileController', [
		'$scope',
		'$rootScope',
		'$state',
		'$stateParams',
		'$http',
		'$filter',
		'blockUI',
		'ModalService',
		'Notification',
		'UserService',
		'$timeout',
		function($scope, $rootScope, $state, $stateParams, $http, $filter,
				blockUI, ModalService, Notification, UserService, $timeout) {

			// FUNCTION :: loadUserProfile()
			$scope.loadUserProfile = function() {
				var userDetailsBlock = blockUI.instances
						.get('userDetailsBlock');
				userDetailsBlock.start("Loading User details...");

				UserService.loadUserProfile($stateParams.id).success(
						function(response) {
							if (response.status === "SUCCESS") {
								$scope.userData = response.data.userData;
								$scope.superVisiorUserData = response.data.superVisiorUserData;
								$scope.delegateUserData = response.data.delegateUserData;
							} else {
								$scope.errors = response.errors;
							}

							userDetailsBlock.stop();
						}).error(function() {
							userDetailsBlock.stop();
				});

			};

			
			 /** Changed By Balbhdra | JIRA - APMA-221 | 09/12/2015 | Starts **/
			/**seperate  modal for delegation and supervisor **/
			// FUNCTION :: showUserForDeligateLookup()
			$scope.showUserForDeligateLookup = function(){
				ModalService.showModal({
					templateUrl		: "modal/delegateUserModal",
					controller		: "UsersForDelegationModalController",
					inputs			: {
						title						: "Users For Deligates Lookup",
					
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						$scope.delegateUserData = result;
					});
				});
			};
			
			// FUNCTION :: showSupervisorUserLookup()
			$scope.showSupervisorUserLookup = function(){
				ModalService.showModal({
					templateUrl		: "modal/superVisorUserModal",
					controller		: "UsersForSuperVisorModalController",
					inputs			: {
						title						: "Supervisor User Lookup",
					
					}
				}).then(function(modal){
					modal.element.modal();
					modal.close.then(function(result){
						$scope.superVisiorUserData = result;
					});
				});
			};
			
			 /** Changed By Balbhdra | JIRA - APMA-221 | 09/12/2015 | Ends **/
			
			// FUNCTION :: changeDelegationStatus()
			$scope.changeDelegationStatus = function(delegateUserEnabled){
				$scope.userData.enableDelegation = delegateUserEnabled;
				if(!delegateUserEnabled){
					$scope.delegateUserData = null;					
				}
			};
			
			// FUNCTION :: resetSuperVisior()
			$scope.resetSuperVisior = function(){
				$scope.superVisiorUserData = null;
			};
			
			// FUNCTION :: submitDeligates()
			$scope.submitUserProfile = function(){
				
				
				if($scope.userData.enableDelegation  == true && $scope.delegateUserData == null ){
					Notification.warning({
						title	: "Warning!",
						message	: "No delegate  user  selected!!"
					});
					return;
				}
				
				
				
				var userDetailsBlock = blockUI.instances
				.get('userDetailsBlock');
				userDetailsBlock.start("Saving User details...");
		         $scope.userData.delegateUserId =$scope.delegateUserData != null ? $scope.delegateUserData.saleforceId : null;
		         $scope.userData.supervisorId =$scope.superVisiorUserData != null ? $scope.superVisiorUserData.saleforceId : null;
				
				// Save  User Details
			     UserService.saveUserDetails($scope.userData)
					.success(function(response){
						// SUCCESS STUFF
						userDetailsBlock.stop();
						if(response.status === "SUCCESS"){
							$scope.loadUserProfile();
							Notification.success({
								message	:'User Details Saved Sucessfully.',
								title	:'Success'
							});
						}else{
							Notification.error({
								message	:'Error occured while saving user details!!',
								title	:'Error!'
							});
							$scope.errors = response.errors;
						}
					})
					.error(function(response){
						// ERROR STUFF
						userDetailsBlock.stop();
					});
			};
			// FUNCTION :: cancelUserProfile()
			$scope.cancelUserProfile = function() {
				$state.go("index"); // REDIRECT TO HOME PAGE...
			};

			$scope.init = function() {
				$scope.loadUserProfile();
			};
			$scope.init();

		} ]);

/************************* CHANGE PASSWORD  CONTROLLER *************************/
App.controller('ChangePasswordController',[
		'$scope',
		'$rootScope',
		'$state',
		'$stateParams',
		'$http',
		'$filter',
		'blockUI',
		'ModalService',
		'Notification',
		'UserService',
		'$timeout',
		function($scope, $rootScope, $state, $stateParams, $http, $filter,
				blockUI, ModalService, Notification, UserService, $timeout){
			
			$scope.password = {
					oldPassword : null,
					newPassword : null,
					confirmNewPassword : null,
			};
			
		  		    
		 // FUNCTION :: canceChangePassword()
			$scope.canceChangePassword = function() {
				$state.go("index"); // REDIRECT TO HOME PAGE...
			};
			
			$scope.savePassword = function(){
				
				$scope.submitChangePasswordErrors = [];

				if(!$scope.password.oldPassword){
					$scope.submitChangePasswordErrors.push("Old Password is required!");
				}
				if(!$scope.password.newPassword){
					$scope.submitChangePasswordErrors.push("New Password is required!");
				}
				if(!$scope.password.confirmNewPassword){
					$scope.submitChangePasswordErrors.push("Confirm New Password is required!");
				}
				if($scope.password.newPassword != $scope.password.confirmNewPassword ){
					$scope.submitChangePasswordErrors.push("New Password And Confirm New Password must be same!");
				}
				if(!$scope.password.newPassword.match(/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+.]{5,19}$/)){
					$scope.submitChangePasswordErrors.push("New Password must have following !");
					$scope.submitChangePasswordErrors.push("1) One albhabet!");
					$scope.submitChangePasswordErrors.push("2) One numeric!");
					$scope.submitChangePasswordErrors.push("3) One special characeter from !@#$%^&*()_+. ");
					$scope.submitChangePasswordErrors.push("4) Password size must be more than 5 Characters");

				}
				if($scope.password.newPassword == $scope.password.oldPassword ){
					$scope.submitChangePasswordErrors.push("New Password must be different from Old Password!");
				}
				if($scope.submitChangePasswordErrors.length > 0){
					return;
				}
				else{
					
				}
				
				var changePasswordModalBlock = blockUI.instances.get('changePasswordModalBlock');
				changePasswordModalBlock.start("Saving Password");
				
				UserService.savePassword($scope.password)
				.success(function(response){
									
					if(response.status === "SUCCESS"){
					
						Notification.success({
							message	:'Password Changed Sucessfully.',
							title	:'Success'
						});
						
						changePasswordModalBlock.stop();	
						$state.go("index");		
					}else{
						Notification.error({
							message	:response.errors.length > 0 && response.errors[0].message != null ? response.errors[0].message : 'Error Occurs while changing password',
							title	:'Error!'
						});
						changePasswordModalBlock.stop();
						
					}
					
					
					
				})
				.error(function(){
					Notification.error({
						message	:'Unexpected error occured while searching users !!',
						title	:'Error!'
					});					
					changePasswordModalBlock.stop();
				
				});
			};
			

		} ]);
