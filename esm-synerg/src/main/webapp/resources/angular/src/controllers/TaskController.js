'use strict';

App.controller('TaskController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','InvoiceService','SupplierMaintenanceService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , InvoiceService , SupplierMaintenanceService , esmCache ){
		
		$scope.loadPendingInvoicesToApprove = function(){
			var pendingInvoicesToApproveBlock = blockUI.instances.get('pendingInvoicesToApproveBlock');
			pendingInvoicesToApproveBlock.start("Loading Invoices...");
			
			InvoiceService.loadPendingInvoicesToApprove()
				.success(function(data){
					$scope.pendingApprovalInvoices = data;
					pendingInvoicesToApproveBlock.stop();
				})
				.error(function(){
					pendingInvoicesToApproveBlock.stop();
				});
		}
		$scope.loadExceptionQueueToApprove = function(){
			var exceptionQueueInvoicesToApproveBlock = blockUI.instances.get('exceptionQueueInvoicesToApproveBlock');
			exceptionQueueInvoicesToApproveBlock.start("Loading Exception Queue Invoices...");
			
			InvoiceService.loadExceptionQueueToApprove()
				.success(function(data){
					$scope.exceptionQueueInvoices = data;
					exceptionQueueInvoicesToApproveBlock.stop();
				})
				.error(function(){
					exceptionQueueInvoicesToApproveBlock.stop();
				});
		};
		$scope.loadSupplierMaintenanceRecords = function(){
			var myTaskSupplierMaintenanceBlock = blockUI.instances.get('myTaskSupplierMaintenanceBlock');
			myTaskSupplierMaintenanceBlock.start("Loading Supplier Maintenance records...");
			
			SupplierMaintenanceService.myTaskSupplierMaintenanceRecords()
				.success(function(response){
					$scope.supplierMaintenanceRecords = response.data.supplierMaintenanceRecords;
					myTaskSupplierMaintenanceBlock.stop();
				})
				.error(function(){
					myTaskSupplierMaintenanceBlock.stop();
				});
		};
		
		$scope.loadData = function(records,section,blockId,sectionTitle){
			
			var block = blockUI.instances.get(blockId);
			block.start("Loading " + sectionTitle + " . . .");
			
			$http.post("rest/mytask/loadsectiondata",{sectionId : section})
				 .success(function(response){
					 if(response.status === "SUCCESS"){
						 $scope[records] = response.data.records;
//					 }else{
//						 alert("Some error occured");
					 }
					 block.stop();
				 })
				 .error(function(response){
					 block.stop();
					 alert("Some error occured");
				 });
		}
		

		$scope.init = function(){
//			$scope.loadPendingInvoicesToApprove();
//			$scope.loadExceptionQueueToApprove();
//			$scope.loadSupplierMaintenanceRecords();
			
//			console.log(JSON.stringify($state.get()));
		};
		$scope.init();
}]);