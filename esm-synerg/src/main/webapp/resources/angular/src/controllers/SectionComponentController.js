'use strict';

App.controller('SectionComponentController',
		['$scope','ActionValidationService',
 function($scope , ActionValidationService){
		
		/**
		 * INSTANCE VARIABLES
		 * ----------------
		 * @section				: section details
		 * @sectionIndex		: position of Section/Component in layout
		 * @model				: data model for SObject Types
		 * 
		 */
			

		$scope.showStandardAlert = function(msg){
			alert("Standard :: " + msg);
		}
		
		// INSTANCE METHODS
		$scope.doValidate = function(){
			var isValid = ($scope.section.component) ? $scope.isComponentValid() : $scope.isAllSectionFieldsValid();
			return $scope.getValidated(isValid);
		}
		$scope.getValidated = function(result){
			$scope.item.status = "VALIDATED";
			$scope.item.valid = result;
			return result;
		}
		$scope.isAllSectionFieldsValid = function(){
			var invalidFields = 0;
			angular.forEach($scope.section.sectionFieldsJsonArray,function(field){
				// REQUIRED FIELD VALIDATION
				if(field.required && ($scope.model[field.name] == '' || $scope.model[field.name] == null)){
					invalidFields++;
				}
			});
			return invalidFields == 0;
		};
		$scope.watchHasAction = $scope.$watch(
				function(scope){
					return ActionValidationService.hasUserAction;
				},
				function(value){
					if(!value){
						$scope.section.type = "DETAIL";
						$scope.watchValidation();
						$scope.watchHasAction();
					}
				});
		$scope.watchValidation = $scope.$watch(
			function(scope){
				return (angular.isDefined($scope.item)) ? $scope.item.status == "START" : false;
			},
			function(value){
				if(value){
					$scope.doValidate();
				}
			});
		$scope.init = function(){
			if($scope.section.type == "EDIT"){
				$scope.validationService = ActionValidationService;
				$scope.item = ActionValidationService.register($scope.section.id,$scope.section.title);
			}else{
				$scope.watchValidation(); // 
			}
		};
		$scope.init();
}]);