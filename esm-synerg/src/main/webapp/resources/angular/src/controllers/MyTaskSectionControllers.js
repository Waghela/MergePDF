'use strict';

/************************* MY TASK SECTION LIST CONTROLLER *************************/
AdminApp.controller('MyTaskSectionListController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','MyTaskSectionService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , MyTaskSectionService , esmCache){
		
		$scope.loadMyTaskSections = function(){
			var searchMyTaskSectionBlock = blockUI.instances.get('searchMyTaskSectionBlock');
			searchMyTaskSectionBlock.start("Loading mytask sections...");
			
			MyTaskSectionService.loadMyTaskSections()
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.myTaskSections = response.data.myTaskSections;
						esmCache.put($scope.myTaskSectionsCacheId,$scope.myTaskSections);
						$scope.hasCachedData = false;
					}
					searchMyTaskSectionBlock.stop();
				})
				.error(function(){
					searchMyTaskSectionBlock.stop();
				});
		};
		$scope.createNewMyTaskSection = function(){
			$state.go("admin-mytasksections-create");
		};
			
		$scope.init = function(){
			$scope.myTaskSectionsCacheId = 'MyTaskSectionListController_myTaskSections';
			$scope.reload = ($stateParams.reload) ? true : false;

			$scope.myTaskSections = esmCache.get($scope.myTaskSectionsCacheId);
			$scope.hasCachedData = ($scope.myTaskSections && $scope.myTaskSections.length > 0) ? true : false;
			if(!$scope.hasCachedData || $scope.reload){
				$scope.loadMyTaskSections();
			}
		};
		$scope.init();
}]);

/************************* MY TASK SECTION CREATE/READ/UPDATE/DELETE CONTROLLER *************************/
AdminApp.controller('MyTaskSectionCRUDController',
		[	'$scope','$rootScope','$state','$stateParams','$http','$filter','blockUI','ModalService','Notification','MyTaskSectionService','SObjectService','esmCache',
 	function($scope , $rootScope , $state , $stateParams , $http , $filter , blockUI , ModalService , Notification , MyTaskSectionService , SObjectService , esmCache){
			
		$scope.loadHerokuSObjects = function(){
			var myTaskSectionBlock = blockUI.instances.get('myTaskSectionBlock');
			myTaskSectionBlock.start("Loading Heroku sObjects...");
			
			SObjectService.loadSObjects()
				.success(function(response){
					if(response.status === "SUCCESS"){
						$scope.herokuSObjects = response.data.sObjects;
						$scope.loadSectionDetails();
					}
					myTaskSectionBlock.stop();
				})
				.error(function(){
					myTaskSectionBlock.stop();
				});
		};
		$scope.loadSectionDetails = function(){
			var sectionId = $stateParams.id;
			if(sectionId){
				var myTaskSectionBlock = blockUI.instances.get('myTaskSectionBlock');
				myTaskSectionBlock.start("Loading details...");
				
				MyTaskSectionService.loadMyTaskSectionDetails(sectionId)
					.success(function(response){
						myTaskSectionBlock.stop();
						if(response.status === "SUCCESS"){
							$scope.sectionModel = response.data.myTaskSectionDetails;
							$scope.selectedSObject = $scope.getHerokuSObject($scope.sectionModel.sObjectApiName);
							$scope.manageConfigureFieldsButton($scope.selectedSObject);
							$scope.setSectionModelTableFieldJsonArray($scope.sectionModel.tableFieldJsonArray);
							
							if($scope.sectionModel && $scope.sectionModel.id){
								$scope.enableDelete = true;
							}
						}
					})
					.error(function(response){
						myTaskSectionBlock.stop();
					});
			}
		};
		$scope.getHerokuSObject = function(sObjectApiName){
			var selectedSObjectIndex = null;
			if(sObjectApiName){
				angular.forEach($scope.herokuSObjects,function(sObject,index){
					if(sObject.name == sObjectApiName){
						selectedSObjectIndex = index;
					}
				});
			}
			return (selectedSObjectIndex) ? $scope.herokuSObjects[selectedSObjectIndex] : {};;
		};
		
		$scope.manageConfigureFieldsButton = function(sObj){
			$scope.disableConfigureFieldsButton = (sObj) ? false : true;
		};
		
		$scope.showSObjectFieldLookup = function(sObj){

			ModalService.showModal({
				templateUrl		: "modal/sobjectfield",
				controller		: "SObjectFieldLookupModalController",
				inputs			: {
					title					: "SObject Fields Lookup",
					tableFieldJsonArray		: $scope.sectionModel.tableFieldJsonArray,
					selectedSObject			: sObj
				}
			}).then(function(modal){
				modal.element.modal();
				modal.close.then(function(selectedFieldsArray){
					$scope.setSectionModelTableFieldJsonArray(selectedFieldsArray);
				});
			});
		};
		
		$scope.setSectionModelTableFieldJsonArray = function(selectedFieldsArray){
			$scope.sectionModel.tableFieldJsonArray = selectedFieldsArray;
			$scope.loadSObjectFieldsForReferenceType($scope.sectionModel.tableFieldJsonArray);
		};
		
		$scope.setSoqlName = function(field,relativeField){
			if(field.type==="reference"){
				field.soqlname = field.relationshipName+"."+relativeField.key;
			}else{
				field.soqlname = field.name;
			}
		}
		
		$scope.loadSObjectFieldsForReferenceType = function(tableFieldJsonArray){
			
			var myTaskSectionBlock = blockUI.instances.get('myTaskSectionBlock');
			myTaskSectionBlock.start("Loading Fields...");
			
			var refToSObjectNames = [];
			angular.forEach(tableFieldJsonArray,function(sObjectField){
				if(sObjectField.type==="reference"){
					refToSObjectNames.push(sObjectField.referenceTo.substring(0,sObjectField.referenceTo.indexOf(",")));
				}
			});
			
			SObjectService.loadSfdcSObjectFields(refToSObjectNames)
				.success(function(response){
					var sfdcSObjectFields = response.data.sfdcSObjectFields;
					angular.forEach(tableFieldJsonArray,function(sObjectField){
						if(sObjectField.type==="reference"){
							var refFields = sfdcSObjectFields[sObjectField.referenceTo.substring(0,sObjectField.referenceTo.indexOf(","))];
							sObjectField.refFields = refFields;
							
							// SET SELECTED VALUE FOR FIELD REFERENCE FIELDS
							angular.forEach(sObjectField.refFields,function(refField){
								if(refField.key === sObjectField.relativeField.key){
									sObjectField.relativeField = refField;
								}
							});
						}
					});
					myTaskSectionBlock.stop();
				})
				.error(function(response){
					myTaskSectionBlock.stop();
				});
			
		};
		
		$scope.move = function(oldIndex,newIndex){
			$scope.sectionModel.tableFieldJsonArray.splice(newIndex,0,$scope.sectionModel.tableFieldJsonArray.splice(oldIndex,1)[0]);
		};
		
		$scope.backToList = function(){
			$state.go("admin-mytasksections");
		};
		
		$scope.parsedTableFieldJsonArray = function(){
			var array = [];
			angular.forEach($scope.sectionModel.tableFieldJsonArray,function(field){
				var newField = angular.copy(field);
				newField.href 				= (field.href) ? field.href : null;
				newField.soqlname			= (field.soqlname) ? field.soqlname : field.name;
				array.push(newField);
//				array.push({
//					name				: field.name,
//					label				: field.label,
//					href				: (field.href) ? field.href : null,
//					hidden				: field.hidden,
//					relationshipName	: (field.relationshipName) ? field.relationshipName : null
//				});
			});
			return array;
		};
		$scope.parsedSectionModel = function(selectedSObject){
			var model = angular.copy($scope.sectionModel);
			model.tableFieldJsonArray = $scope.parsedTableFieldJsonArray(); //JSON.stringify($scope.parsedTableFieldJsonArray());
			model.sObjectApiName = (selectedSObject && selectedSObject.name) ? selectedSObject.name : null;
			return model;
		};
		
		$scope.save = function(selectedSObject){
			var section = $scope.parsedSectionModel(selectedSObject);
			$scope.errors = [];
			if(!section.title || section.title == ''){
				$scope.errors.push("Title is required!");
			}if(!section.sObjectApiName || section.sObjectApiName == ''){
				$scope.errors.push("SObject is required!");
			}if(!section.whereClause || section.whereClause == ''){
				$scope.errors.push("Where clause is required!");
			}if(!section.sequence || section.sequence < 1 || section.sequence == ''){
				$scope.errors.push("Sequence must be greater than 0!");
			}if(!section.tableFieldJsonArray || section.tableFieldJsonArray.length == 0){
				$scope.errors.push("Section table details is required!");
			}else{
				section.tableFieldJsonArray = JSON.stringify(section.tableFieldJsonArray);
			}
			
			if($scope.errors.length > 0){
				return;
			};
			
			var myTaskSectionBlock = blockUI.instances.get('myTaskSectionBlock');
			myTaskSectionBlock.start("Creating new My Task Section...");
			
			MyTaskSectionService.saveMyTaskSection(section)
				.success(function(response){
					myTaskSectionBlock.stop();
					if(response.status === "SUCCESS"){
						$state.go("admin-mytasksections",{reload : true});
					}
				})
				.error(function(){
					myTaskSectionBlock.stop();
				});
			
		};
		
		$scope.deleteSection = function(model){
			var confirmDelete = confirm("Section will be removed from MyTask page. \n\nAre you sure to delete ?");
			if(confirmDelete && $scope.sectionModel && $scope.sectionModel.id){
				var myTaskSectionBlock = blockUI.instances.get('myTaskSectionBlock');
				myTaskSectionBlock.start("Deleting My Task Section...");
				
				MyTaskSectionService.deleteMyTaskSection($scope.sectionModel.id)
					.success(function(response){
						myTaskSectionBlock.stop();
						if(response.status === "SUCCESS"){
							Notification.success({
								message 	:"My Task Section deleted successfully.",
								title		:"Success"
							});
							
							$state.go("admin-mytasksections",{reload : true});
						}
					})
					.error(function(response){
						myTaskSectionBlock.stop();
					});
			}
		}
		
		$scope.init = function(){
			$scope.disableConfigureFieldsButton = true;
			$scope.title = $stateParams.title;
			
			$scope.selectedSObject = {};
			$scope.sectionModel = {
				title 					: null,
				sObjectApiName			: null,
				tableFieldJsonArray		: [],
				whereClause				: null,
				sequence				: 1,
				active					: false
			};
			
			$scope.loadHerokuSObjects();
			
//			$scope.loadSectionDetails();
		};
		$scope.init();
}]);


/************************* SOBJECTFIELD LOOKUP MODAL CONTROLLER *************************/
AdminApp.controller('SObjectFieldLookupModalController',
		[	'$scope','$rootScope','$stateParams','$http','$timeout','blockUI','$element','title','close','tableFieldJsonArray','selectedSObject','$document',
 	function($scope , $rootScope , $stateParams , $http , $timeout , blockUI , $element , title , close , tableFieldJsonArray , selectedSObject , $document){
//		$scope.title = title;
		
		//  This close function doesn't need to use jQuery or bootstrap, because
		//  the button has the 'data-dismiss' attribute.
		$scope.close = function(){
			$element.modal('hide');
//			close({},500); // close, but give 500ms for bootstrap to animate
		};
		$scope.selectAndClose = function(id,name){
			close({
				id		: id,
				name	: name
			}, 500); // close, but give 500ms for bootstrap to animate
		};
		
		//  This cancel function must use the bootstrap, 'modal' function because
		//  the doesn't have the 'data-dismiss' attribute.
		$scope.cancel = function(){
			//  Manually hide the modal.
			$element.modal('hide');
			
			//  Now call close, returning control to the caller.
//			close($scope.result,500); // close, but give 500ms for bootstrap to animate
		};
		
		// CUSTOM CODING
		
//		$scope.toggleSelectAll = function(toggle){
//			$scope.cbSelectAll = toggle;
//			angular.forEach($scope.sObjectFields,function(field){
//				field.selected = $scope.cbSelectAll;
//			});
//			$scope.indeterminate = false;
//		};
//		$scope.fieldChange = function(toggle){
//			var checkedCount = $scope.checkedCount();
//			alert(checkedCount + " | " + $scope.sObjectFields.length);
//			if(checkedCount === $scope.sObjectFields.length){
//				$scope.cbSelectAll = true;
//				$scope.indeterminate = false;
////				$scope.toggleSelectAll(true);
////				$scope.indeterminate = false;
//			}else if(checkedCount == 0){
//				$scope.cbSelectAll = false;
////				$scope.toggleSelectAll(false);
//				$scope.indeterminate = false;
//			}else{
//				$scope.cbSelectAll = false;
//				$scope.indeterminate = true;
//			}
//		}
		
//		$scope.checkedCount = function(){
//			var checkedCount = 0;
//			angular.forEach($scope.sObjectFields,function(field){
//				if(field.selected && angular.isDefined(field.selected))
//					checkedCount++
//			});
//			return checkedCount;
//		};
		
		$scope.selectField = function(field,toggle){
			if(toggle){
				var newField = true;
				angular.forEach($scope.selectedFields,function(selectedField,index){
					if(selectedField.name == field.name){
						newField = false;
					}
				});
				if(newField){
					$scope.selectedFields.push(field);
				}
			}else{
				var indexToDelete = null;
				angular.forEach($scope.selectedFields,function(selectedField,index){
					if(selectedField.name == field.name){
						indexToDelete = index;
					}
				});
				$scope.selectedFields.splice(indexToDelete,1);
			}
		};
		$scope.saveAndClose = function(){
			close($scope.selectedFields, 500); // close, but give 500ms for bootstrap to animate
		};
		
		$scope.initSelectedFields = function(){
			angular.forEach($scope.sObjectFields,function(sObjectField,sObjectFieldIndex){
				angular.forEach($scope.tableFieldJsonArray,function(tableField,tableFieldIndex){
					if(tableField.name === sObjectField.name){
						sObjectField.selected = true;
						$scope.selectedFields.push(tableField);
//						$scope.selectField(tableField,true);
					}
				});
			});
		};
		
		$scope.init = function(){
			$scope.title 					= selectedSObject.label + ' fields lookup';
			$scope.sObjectFields 			= angular.copy(selectedSObject.fields);
			$scope.tableFieldJsonArray		= angular.copy(tableFieldJsonArray);
			$scope.selectedFields			= [];
			$scope.initSelectedFields();
//			$scope.cbSelectAll = false;
//			$scope.fieldChange(false);
		};
		$scope.init();
}]);