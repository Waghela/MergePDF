'use strict';

App.factory('esmCache',
		[	'$cacheFactory',
	function($cacheFactory){
		return $cacheFactory('ESM_DATA_CACHE');
}]);