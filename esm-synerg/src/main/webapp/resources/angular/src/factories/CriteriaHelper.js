'use strict';

App.factory('CriteriaHelper', function(){
	var createGroupExpression = function(criteria,exp,condition,model){
		exp += "(";
		angular.forEach(criteria.rules,function(rule){
			if(rule.type == 'GROUP'){
				exp += createGroupExpression(rule,"",rule.condition,model);
			}else{
				exp += createRuleExpression(rule,model);
			}
			exp += " " + condition + " ";
		})
		exp = exp.slice(0,-4);
		exp += ")";
		return exp;
	}
	var createRuleExpression = function(rule,model){
		var exp = "";
		if(rule.field){
			var modelValue = model[rule.field.name] ;
			var ruleValue = rule.value;
			switch(rule.field.type){ // double,currency,string,email,picklist,boolean,reference
			case "double":
			case "currency":
				modelValue 	= (modelValue) 	? modelValue 	: 0;
				ruleValue 	= (ruleValue) 	? ruleValue 	: 0;
				break;
				
			case "string":
			case "email":
			case "picklist":
				modelValue 	= (modelValue) 	? "'" + modelValue 	+ "'" : "''";
				ruleValue 	= (ruleValue) 	? "'" + ruleValue 	+ "'" : "''";
				break;

			case "reference":
				modelValue 	= (modelValue) 	? "'" + modelValue 	+ "'" : "''";
				ruleValue 	= (ruleValue) 	? "'" + model[ruleValue]	+ "'" : "''";
				break;
			
			case "boolean":
				modelValue 	= (modelValue) 	? modelValue 	: false;
				ruleValue 	= (ruleValue) 	? ruleValue 	: false;
				break;
			}
			exp += "(" + modelValue + " " + rule.operator + " " + ruleValue + ")";
		}
		return exp;
	};
	
	return {
		validate: function(criteria,model,throwException){
			var strCriteria = JSON.stringify(angular.copy(criteria));
			var strGroupNode = JSON.stringify(this.groupNode());
			if(strCriteria == strGroupNode){
				criteria = null;
				return null;
			}
			
			var exp = createGroupExpression(criteria,"",criteria.condition,model);
			var result = false;
			try{
				result = eval(exp);
			}catch(e){
				if(throwException){
					throw "Invalid Criteria!!";
				}
				result = false;
				console.error("[HELPER] - Invalid Expression!!");
			}
			return result;
		},
		operatorList: function(){
			return  [{ sign: '==', 		label: 'equal',						types : 'double,currency,string,email,picklist,boolean,reference' },
	                 { sign: '!=', 		label: 'not equal',					types : 'double,currency,string,email,picklist,boolean,reference' },
	                 { sign: '<', 		label: 'less than',					types : 'double,currency' },
	                 { sign: '>', 		label: 'greater than',				types : 'double,currency' },
					 { sign: '<=', 		label: 'less than or equal',		types : 'double,currency' },
					 { sign: '>=', 		label: 'greater than or equal',		types : 'double,currency' }];
		},
		namespaceValues: function(){
			return ['USER_ID','LOGGED_IN_USER_DELEGATION_ID'];
		},
		groupNode: function(){
			return {
				type		:'GROUP',
				condition	:'&&',
				rules		:[this.ruleNode()]
			};
		},
		ruleNode: function(){
			return {
				type		:'RULE',
				field		:null
			};
		}
	};
});