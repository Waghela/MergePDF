'use strict';

/*********************************************************************************** SEARCH COMPONENT DIRECTIVE **/
App.directive('searchComponent',
	[		'$compile','$http','blockUI','$log','esmCache','$timeout',
	function($compile , $http , blockUI , $log , esmCache , $timeout) {
		return {
			restrict	:'E',
			scope		:{ 
							componentId 	:'@',
							searchAction	:'&'
						 },
			templateUrl	:"component/search",
			controller	:function($scope){
				$log.debug("Pointer to search-component directive...");
				var scopeData 	= {componentId	: $scope.componentId };
				$scope.newModel	= {};
				
				$scope.loadSearchComponentFields = function(){
					
					var SearchComponentBlock = blockUI.instances.get($scope.componentId);
					SearchComponentBlock.start("Loading search fields...");
					
					$http.post("rest/component/search", scopeData)
						.success(function(response){
							if(response.status === "SUCCESS"){
								$scope.model = response.data.SearchComponent;
								angular.forEach($scope.model.fields, function(field, key){
									field.id = key;
								});
								
								$scope.newModel = angular.copy($scope.model);
								
								// Put component models into cache when loaded.
								var component = {
									model 		: $scope.model,
									newModel	: $scope.newModel
								};
								esmCache.put($scope.cacheId,component);
							}
							
							SearchComponentBlock.stop();
						})
						.error(function(response){
							SearchComponentBlock.stop();
						});
				};
				
				$scope.executeSearchAction = function(){
					var modelData = angular.copy($scope.model);
					var newModelData = angular.copy($scope.newModel);
					var component = {
						model 		: modelData,
						newModel	: newModelData
					};
					$scope.searchAction()(modelData);
					esmCache.put($scope.cacheId,component);
				};
				
				$scope.showDateTimePicker = function(field, timepicker){
					var $input = $scope.element.find('#'+field.id);
					$input.datetimepicker({
						scrollInput : false,
						timepicker: (timepicker) ? true : false,
						format: (timepicker) ? 'M d, Y H:i:s a' : 'M d, Y',
					});
					$input.datetimepicker('show');
				};
				
				$scope.reset = function(){
					angular.copy($scope.newModel,$scope.model);
				};
				
				if($scope.componentId){
					$scope.cacheId = 'SearchComponent_' + $scope.componentId;
					
					var cachedComponent = esmCache.get($scope.cacheId);
					if(cachedComponent){
						$scope.model = angular.copy(cachedComponent.model);
						$scope.newModel = angular.copy(cachedComponent.newModel);
						return;
					}else{
						$scope.loadSearchComponentFields();
					}
				}
			},
			link: function(scope, element) {
		        scope.element = element;
		    }
		};
}]);
/*********************************************************************************** EXPORT TABLE TO CSV DIRECTIVE **/
App.directive('exportToCsv',function(){
  	return {
    	restrict: 'A',
    	link: function (scope, element, attrs) {
    		var el = element[0];
	        element.bind('click', function(e){
	        	attrs.$observe('exportToCsv', function(value){
	                var table = $('#'+value);
	                var csvString = '';
	                for(var i=0; i<table[0].rows.length;i++){
	                	var rowData = table[0].rows[i].cells;
	                	for(var j=0; j<rowData.length;j++){
	                		var dataStr = rowData[j].innerHTML 
//	                		if(j == 0){
//	                			dataStr = dataStr.replace(dataStr.substr(datastr.indexOf('#/invoice'), 'ding">'),'').replace('</b></a>', '');
//	                		}
	                		csvString = csvString + dataStr.replace(',','') + ",";
	                	}
	                	csvString = csvString.substring(0,csvString.length - 1);
	                	csvString = csvString + "\n";
	                }
	                csvString = csvString.substring(0, csvString.length - 1);
	                var a = $('<a/>', {
	                	style:'display:none',
	                	href:'data:application/octet-stream;base64,'+btoa(csvString),
	                	download:'search-result.csv'
	                }).appendTo('body')
	                a[0].click()
	                a.remove();
	            });
	        });
    	}
  	}
});
App.directive('exportJsonToCsv',
	[function(){
		return {
			restrict	:'E',
			require		:['^data', '^reportTitle', '^columnHeader', '^columnApiNameArr'],
			transclude	: true,
			scope		:{ 
							data			:'@',
							reportTitle		:'@',
							columnHeader	:'=',
							columnApiNameArr:'=',
							disabled		:'='
						 },
			template	:"<button type='button' class='btn btn-primary' ng-disabled='disabled' ng-click='JSONToCSVConvertor();'><i class='icon-download-alt'>&nbsp;&nbsp;CSV Download</i></button>",
			controller	: function($scope, $attrs){
				
				$scope.JSONToCSVConvertor = function() {
				    var arrData = typeof $scope.data != 'object' ? JSON.parse($scope.data) : $scope.data;
				    
				    var CSV = '';    
				    CSV += $scope.reportTitle + '\r\n\n';

			        var row = "";
			        angular.forEach($scope.columnHeader, function(headerValue){
			        	row += headerValue + ',';
			        });
			        row = row.slice(0, -1);

			        CSV += row + '\r\n';
				    
			        angular.forEach(arrData, function(rowVal){
			        	var row = "";
			        	angular.forEach($scope.columnApiNameArr, function(apiName){
			        		row += (rowVal.allFields[apiName] == null || rowVal.allFields[apiName] == undefined) ? ('"",') : ('"' + rowVal.allFields[apiName].toString() + '",');
			        	});
			        	
			        	row.slice(0, row.length - 1);
				        CSV += row + '\r\n';
			        });
			        
				    if (CSV == '') {        
				        alert("Invalid data");
				        return;
				    }   
				    
				    var fileName = "MyReport_";
				    fileName += $scope.reportTitle.replace(/ /g,"_");   
				    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
				    var link = document.createElement("a");    
				    link.href = uri;
				    link.style = "visibility:hidden";
				    link.download = fileName + ".csv";
				    document.body.appendChild(link);
				    link.click();
				    document.body.removeChild(link);
				}
			}
		}
	}]
);
/*********************************************************************************** ATTACH FILE DIRECTIVE **/
App.directive('attachFile',
['blockUI', 'Notification', 'AttachmentService',
function(blockUI, Notification, AttachmentService){
	return {
		restrict	:'E',
		require		:['^allowedsize', '^allowedExt', '^allowedExtForPrime', '^allowAttachPrime', '^files'],
		transclude: true,
		scope		:{ 
						allowedSize			:'@',
						allowedExt			:'@',
						allowedExtForPrime	:'@',
						allowAttachPrime	:'@',
						files				:'=',
						baseScope			:'='
					 },
//		scope		:true,
		templateUrl	:"component/attachment",
		controller	: function($scope, $attrs){
			var AttachmentBlock = blockUI.instances.get('AttachmentBlock');
			$scope.errFiles = [];
			$scope.allowedExtentions = [];
			$scope.disablePrimaryDocuent = false;
			$scope.primaryDocument;
			$scope.primaryFileName = "";
			
			$scope.allowedSize = $attrs.allowedSize;
			$scope.allowedExt = $attrs.allowedExt;
			$scope.allowedExtForPrime = $attrs.allowedExtForPrime;
			$scope.allowAttachPrime = $attrs.allowAttachPrime;
			
			$scope.selectFile = function(files, errFiles) {
				var blackListedFiles="";
				var sizeExceededFiles="";
				if($scope.primaryDocument)
				{
					$scope.allowedExtentions=[];
					$scope.allowedExtentions.push($attrs.allowedExtForPrime);
				}
				else
				{
					$scope.allowedExtentions=[];
					angular.forEach($attrs.allowedExt.split(','),function(ext){
						$scope.allowedExtentions.push(ext);
					});
				}
				angular.forEach(files,function(file){
	        		if(($scope.allowedExtentions.indexOf(file.name.substr(file.name.indexOf("."),file.name.length - 1)) > -1) && (file.size/(1024*1024) <= 10))
	        		{
	        			file.isPersisted = false;
		        		var pushOnScope = true;
		        		angular.forEach($scope.files,function(fileInScope){
		        			if(fileInScope.name == file.name)
	        				{
		        				pushOnScope = false;
		        				if($scope.primaryDocument  && $scope.primaryFileName == "" && fileInScope.name == file.name)
		        					fileInScope.primaryDocument = true;
			        			else
			        				fileInScope.primaryDocument = false;
		        				return;
	        				}
		        			return;
			        	});
		        		if($scope.primaryDocument && $scope.primaryFileName == "")
		        		{
		        			$scope.primaryFileName = file.name;
		        			$scope.disablePrimaryDocuent = true;
		        			$scope.primaryDocument = false;
		        		}
		        		if(pushOnScope)
	        			{
		        			if($scope.primaryDocument && $scope.primaryFileName == file.name)
		        				file.primaryDocument = true;
		        			else
		        				file.primaryDocument = false;
		        			$scope.files.push(file);
	        			}
	        		}
	        		else
        			{
	        			blackListedFiles += file.name + ",";
        			}
	        	});

				if(blackListedFiles.length > 0)
				{
					Notification.error({
						title	:'Error',
						message	:'Extention is black listed for ' + blackListedFiles
					});
				}
		        	
		        	
	        	angular.forEach(errFiles,function(errFile){
	        		sizeExceededFiles += errFile.name + ","
	        	});
	        	
	        	if(sizeExceededFiles.length > 0)
	        	{
	        		Notification.error({
		        		title	:'Error',
		        		message	:'Error occured, may be size limit exceeded for files' + sizeExceededFiles 
	        		});
	        	}
			};
		    
			$scope.primaryDocumentClicked = function(primaryDoc){
				$scope.primaryDocument = primaryDoc;
			};
			
		    $scope.attachFiles = function(files)
		    {
		    	AttachmentBlock.start("Attaching File...");
		        angular.forEach($scope.files, function(file) {
		        	if(files == file)
	        		{
		        		var response;
			        	if(file.name != $scope.primaryFileName)
		        		{
				        	AttachmentService.uploadAttachments(file)
				        		.then(function(response){
				        			if(response.data.status == "SUCCESS")
									{
						        		file.fileName = response.data.data.fileName;
						        		file.isPersisted = true;
							        	Notification.success({
												title 	:'Success',
												message	: file.name + ' File Uploaded successfully.'
										});
									}
				        			else
									{
										Notification.error({
											title 	:'Error',
											message	: file.name + ' upload failed.'
										});
									}
				        			AttachmentBlock.stop();
				        		});
		        		}
			        	else
		        		{
			        		AttachmentService.uploadPrimary(file)
				        		.then(function(response){
				        			if(response.data.status == "SUCCESS")
									{
						        		file.fileName = response.data.data.fileName;
						        		file.isPersisted = true;
							        	Notification.success({
												title 	:'Success',
												message	: file.name + ' File Uploaded successfully.'
										});
									}
				        			else
									{
										Notification.error({
											title 	:'Error',
											message	: file.name + ' upload failed.'
										});
									}
				        			AttachmentBlock.stop();
				        		});
		        		}
	        		}
		        });
		    };
		    
		    $scope.deleteFile = function(file)
		    {
		    	AttachmentBlock.start("Deleting File...");
		    	var index = $scope.files.indexOf(file);
		    	var dataObj = {
		    				fileName : file.fileName,
		    		};
		    	
		    	AttachmentService.deleteFile(dataObj)
		    		.success(function(response){
							if(response.status === "SUCCESS"){
								Notification.success({
									title 	:'Success',
									message	: file.name + ' deleted successfully.'
								});
							} else {
								$scope.errors = response.errors;
							}
							AttachmentBlock.stop();
						});
		    	if(file.primaryDocument)
	    		{
		    		$scope.disablePrimaryDocuent = false;
		    		$scope.primaryFileName = "";
		    		$scope.primaryDocument = false;
	    		}
		    	if(index > -1)
	    		{
		    		$scope.files.splice(index,1);
	    		}
		    }
		}
	};
}]);
/************************************************************************************* LAYOUT SECTION DIRECTIVE **/
App.directive('layoutSectionComponent',
	[		'$compile','$http','blockUI','$log','esmCache','$timeout','$controller','CriteriaHelper',
	function($compile , $http , blockUI , $log , esmCache , $timeout , $controller , CriteriaHelper) {
		return {
			restrict	:'E',
			scope		:{ 
							section		 			:'=',
							sectionIndex			:'=',
							model					:'=',
							layoutBlockUiInstance	:'='	
						 },
			templateUrl	:"component/layoutsection",
			controller	:function($scope){
				var ctrl = this;
				
				$scope.log = function(msg){
					console.log(msg);
				}
				
				$scope.doRender = function(){
					$scope.rendered = true;

					angular.extend(ctrl,$controller("SectionComponentController",{$scope: $scope}));
					if($scope.section.componentMetadata.controller)
						angular.extend(ctrl,$controller($scope.section.componentMetadata.controller,{$scope: $scope}));
				};
				$scope.init = function(){
					$scope.rendered = false;
					
					if($scope.section.componentMetadata.criteria){
						var loading = true;
						var criteriaWatch = $scope.$watch(
							function(scope){
								return $scope.layoutBlockUiInstance.state().blocking;
							},
							function(value){
								if(!value && !loading){
									criteriaWatch();
									var criteriaMatched = CriteriaHelper.validate($scope.section.componentMetadata.criteria,$scope.model);
									if(criteriaMatched){
										$scope.doRender();
									}
								}
								loading = false;
							}
						);
					}else{
						$scope.doRender();
					}
				};
				$scope.init();
			},
			link : function(scope, element, attrs) {
				$compile(element.contents())(scope);
			}
		};
}]);

App.constant('uiDateFormatConfig', '');
App.directive('uiDateFormat', ['uiDateFormatConfig','$filter', function(uiDateFormatConfig,$filter){
	return {
		require : 'ngModel',
		link : function($scope,$element,$attr,ngModelCtrl){
			var dateFormat = $attr.uiDateFormat || uiDateFormatConfig;
			if(dateFormat){
				ngModelCtrl.$formatters.push(function(value) {
		          if (angular.isString(value) ) {
		        	 var date = $filter('date')(value, dateFormat, null);
		            return date;//jQuery.datepicker.parseDate(dateFormat, value);
		          }
		          return null;
		        });
				ngModelCtrl.$parsers.push(function(value){
		          if (value) {
		            return value; //jQuery.datepicker.formatDate(dateFormat, value);
		          }
		          return null;
		        });
			}
		}
	};
}]);

App.directive('layoutSectionComponentField',
	[		'$compile','$http','blockUI','$log','esmCache','$timeout','ModalService',
	function($compile , $http , blockUI , $log , esmCache , $timeout, ModalService) {
		return {
			restrict	:'E',
			scope		:{
				section			:'=',
				field		 	:'=',
				mode			:'=',
				model			:'=',
				ctrlScope		:'=',
				removeAction	:'&'
			},
			templateUrl	:"component/layoutsectionfield",
			controller	:function($scope){
				if($scope.field.type=='picklist' || $scope.field.type == 'multipicklist'){
					var array = $scope.field.picklistValues.split(',');
					$scope.picklistvalues = [];
					angular.forEach(array,function(item){
						if(item){
							$scope.picklistvalues.push(item);
						}
					});
				}
				
				$scope.showLookupModal = function(){
					ModalService.showModal({
						templateUrl		: "modal/genericModal",
						controller		: "GenericLookUpModalController",
						inputs			: {
							title		: $scope.field.label +" Lookup",
							referenceTo	: $scope.field.referenceTo
						}
					}).then(function(modal){
						modal.element.modal();
						modal.close.then(function(result){
							$scope.model[$scope.field.name] = result.id;
							$scope.model[$scope.field.name+'__Name'] = result.name;
						});
					});
				};
				
				$scope.removeField = function(){
					$scope.removeAction();
				}
				
				$scope.applyValue = function(datetime){
					$scope.$apply(function(){
						$scope.model[$scope.field.name] = datetime;
					});
				}
			},
			link : function($scope, element, attrs, ngModel) {
				
				$scope.showDateTimePicker = function(timepicker){
					var $input = element.find('#'+$scope.field.id);
					$input.datetimepicker({
						scrollInput : false,
						timepicker: (timepicker) ? true : false,
						format: (timepicker) ? 'M d, Y H:i:s a' : 'M d, Y',
					});
					$input.datetimepicker('show');
				}
			}
		};
}]);
/*************************************************************************** LAYOUT CRITERIA ITEM DIRECTIVE **/
App.directive('criteriaItem',
		[		'$compile','$http','blockUI','$log','esmCache','$timeout','ModalService','RecursionHelper','CriteriaHelper',
		function($compile , $http , blockUI , $log , esmCache , $timeout , ModalService , RecursionHelper , CriteriaHelper) {
			return {
				restrict	:'E',
				scope		:{
					parent		: '=',
					item		: '=',
					sobject		: '=',
					index		: '='
				},
				templateUrl	:"component/criteriaitem",
				controller	:function($scope){
					$scope.addRule = function(){
						$scope.item.rules.push(CriteriaHelper.ruleNode());
					};
					$scope.addGroup = function(){
						$scope.item.rules.push(CriteriaHelper.groupNode());
					};
					$scope.clear = function(){
						$scope.item = CriteriaHelper.groupNode();
					};
					$scope.deleteItem = function(index){
						$scope.parent.rules.splice(index,1);
					}
					$scope.setCondition = function(condition){
						$scope.item.condition = condition;
					};
					$scope.picklistValues = function(field){
						var array = field.picklistValues.split(',');
						var picklistvalues = [];
						angular.forEach(array,function(item){
							if(item){
								picklistvalues.push(item);
							}
						});
						return picklistvalues;
					};
					$scope.namespaceValues = CriteriaHelper.namespaceValues();
					$scope.operatorlist = CriteriaHelper.operatorList();
				},
				compile		:function(element){
					return RecursionHelper.compile(element,
							function(scope, iElement, iAttrs, controller, transcludeFn){
						
					});
				}
			};
		}]);
/************************************************************************************************************** **/